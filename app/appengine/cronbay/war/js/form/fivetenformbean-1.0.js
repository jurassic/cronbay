//////////////////////////////////////////////////////////
// <script src="/js/form/fivetenformbean-1.0.js"></script>
// Last modified time: 1337996953567.
// Place holder...
//////////////////////////////////////////////////////////


var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.form = cronbay.wa.form || {};
cronbay.wa.form.FiveTenFormBean = ( function() {


  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var isEmpty = function(obj) {
    for(var prop in obj) {
      if(obj.hasOwnProperty(prop)) {
        return false;
      }
    }
    return true;
  };


  /////////////////////////////
  // Constructor
  // Note: We use "parasitic inheritance"
  //       http://www.crockford.com/javascript/inheritance.html
  /////////////////////////////

  var cls = function(jsBean) {

    // Private vars.
    var errorMap = {};

    // Inheritance
    var that;
    if(jsBean) {
      that = jsBean;
    } else {
      that = new cronbay.wa.bean.FiveTenJsBean();
    }


    /////////////////////////////
    // Constants
    /////////////////////////////

    // To indicate the "global" errors...
    that.FIELD_BEANWIDE = "_beanwide_";


    /////////////////////////////
    // Subclass methods
    /////////////////////////////

    that.hasErrors = function(f) {
      if(f) {
        var errorList = errorMap[f];
        if(errorList && errorList.length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        if(isEmpty(errorMap)) {
          return false;  
        } else {
          return true;
        }
      }
    };

    that.getLastError = function(f) {
      if(f) {
        var errorList = errorMap[f];
        if(errorList && errorList.length > 0) {
          return errorList[errorList.length - 1];
        } else {
          return null;
        }
      } else {
        // ???
    	return null;
      }
    };
    that.getErrors = function(f) {
      if(f) {
        var errorList = errorMap[f];
        if(errorList) {
          return errorList;
          //return errorList.slice(0);
        } else {
          return [];
        }
      } else {
        // ???
      	return null;
      }
    };

    that.addError = function(f, error) {
      if(f && error) {
        var errorList = errorMap[f];
        if(!errorList) {
          errorList = [];
        }
        errorList.push(error);
        errorMap[f] = errorList;
      } else {
        // ???
      }
    };
    that.setError = function(f, error) {
      if(f && error) {
        var errorList = [];
        errorList.push(error);
        errorMap[f] = errorList;
      } else {
        // ???
      }
    };

    that.addErrors = function(f, errors) {
      if(f && errors && errors.length > 0) {
        var errorList = errorMap[f];
        if(!errorList) {
          errorList = [];
        }
        errorList = errorList.concat(errors);
        //errorList = errorList.concat(errors.slice(0));
        errorMap[f] = errorList;
      } else {
        // ???
      }
    };
    that.setErrors = function(f, errors) {
      if(f) {
        if(errors) {
          errorMap[f] = errors;
          //errorMap[f] = errors.slice(0);
        } else {
          delete errorMap[f];
        }
      } else {
        // ???
      }
    };

    that.resetErrors = function(f) { 
      if(f) {
        delete errorMap[f]; 
      } else {
        errorMap = {}; 
      }
    };


    that.validate = function() { 
      var allOK = true;

//      // TBD
//      if(!this.getGuid()) {
//    	  this.addError("guid", "guid is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getCounter()) {
//    	  this.addError("counter", "counter is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getRequesterIpAddress()) {
//    	  this.addError("requesterIpAddress", "requesterIpAddress is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getCreatedTime()) {
//    	  this.addError("createdTime", "createdTime is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getModifiedTime()) {
//    	  this.addError("modifiedTime", "modifiedTime is null");
//        allOK = false;
//      }

      return allOK; 
    };

    
    /////////////////////////////
    // Convenience methods
    // Note that we are "overriding" superclass methods
    // but reusing their implementations 
    /////////////////////////////
    
    // Clone this bean.
    that.clone = function() {
      var jsBean = this._clone();
      var o = new cronbay.wa.form.FiveTenFormBean(jsBean);

      if(errorMap) {
        for(var f in errorMap) {
          var errorList = errorMap[f];
          o.setErrors(f, errorList.slice(0));
        }
      }
    
      return o;
    };

    // This will be called by JSON.stringify().
    that.toJSON = function() {
      var jsonObj = this._toJSON();

      // ???
      if(!isEmpty(errorMap)) {
          jsonObj.errorMap = errorMap;
      }
      // ...

      return jsonObj;
    };


    /////////////////////////////
    // For debugging.
    /////////////////////////////

    that.toString = function() {
      var str = this._toString();

      str += "errors: {";
      for(var f in errorMap) {
        if(errorMap.hasOwnProperty(f)) {
          var errorList = errorMap[f];
          if(errorList) {
            str += f + ": [";
            for(var i=0; i<errorList.length; i++) {
              str += errorList[i] + ", ";
            }
            str += "];";
          }
        }
      }
      str += "};";

      return str;
    };

    return that;
  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.form.FiveTenFormBean.create = function(obj) {
  var jsBean = cronbay.wa.bean.FiveTenJsBean.create(obj);
  var o = new cronbay.wa.form.FiveTenFormBean(jsBean);

  if(obj.errorMap) {
    for(var f in obj.errorMap) {
      var errorList = obj.errorMap[f];
      o.setErrors(f, errorList.slice(0));
    }
  }

  return o;
};

cronbay.wa.form.FiveTenFormBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.form.FiveTenFormBean.create(jsonObj);
  return obj;
};

