//////////////////////////////////////////////////////////
// <script src="/js/form/restrequeststructformbean-1.0.js"></script>
// Last modified time: 1337996953545.
// Place holder...
//////////////////////////////////////////////////////////


var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.form = cronbay.wa.form || {};
cronbay.wa.form.RestRequestStructFormBean = ( function() {


  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var isEmpty = function(obj) {
    for(var prop in obj) {
      if(obj.hasOwnProperty(prop)) {
        return false;
      }
    }
    return true;
  };


  /////////////////////////////
  // Constructor
  // Note: We use "parasitic inheritance"
  //       http://www.crockford.com/javascript/inheritance.html
  /////////////////////////////

  var cls = function(jsBean) {

    // Private vars.
    var errorMap = {};

    // Inheritance
    var that;
    if(jsBean) {
      that = jsBean;
    } else {
      that = new cronbay.wa.bean.RestRequestStructJsBean();
    }


    /////////////////////////////
    // Constants
    /////////////////////////////

    // To indicate the "global" errors...
    that.FIELD_BEANWIDE = "_beanwide_";


    /////////////////////////////
    // Subclass methods
    /////////////////////////////

    that.hasErrors = function(f) {
      if(f) {
        var errorList = errorMap[f];
        if(errorList && errorList.length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        if(isEmpty(errorMap)) {
          return false;  
        } else {
          return true;
        }
      }
    };

    that.getLastError = function(f) {
      if(f) {
        var errorList = errorMap[f];
        if(errorList && errorList.length > 0) {
          return errorList[errorList.length - 1];
        } else {
          return null;
        }
      } else {
        // ???
    	return null;
      }
    };
    that.getErrors = function(f) {
      if(f) {
        var errorList = errorMap[f];
        if(errorList) {
          return errorList;
          //return errorList.slice(0);
        } else {
          return [];
        }
      } else {
        // ???
      	return null;
      }
    };

    that.addError = function(f, error) {
      if(f && error) {
        var errorList = errorMap[f];
        if(!errorList) {
          errorList = [];
        }
        errorList.push(error);
        errorMap[f] = errorList;
      } else {
        // ???
      }
    };
    that.setError = function(f, error) {
      if(f && error) {
        var errorList = [];
        errorList.push(error);
        errorMap[f] = errorList;
      } else {
        // ???
      }
    };

    that.addErrors = function(f, errors) {
      if(f && errors && errors.length > 0) {
        var errorList = errorMap[f];
        if(!errorList) {
          errorList = [];
        }
        errorList = errorList.concat(errors);
        //errorList = errorList.concat(errors.slice(0));
        errorMap[f] = errorList;
      } else {
        // ???
      }
    };
    that.setErrors = function(f, errors) {
      if(f) {
        if(errors) {
          errorMap[f] = errors;
          //errorMap[f] = errors.slice(0);
        } else {
          delete errorMap[f];
        }
      } else {
        // ???
      }
    };

    that.resetErrors = function(f) { 
      if(f) {
        delete errorMap[f]; 
      } else {
        errorMap = {}; 
      }
    };


    that.validate = function() { 
      var allOK = true;

//      // TBD
//      if(!this.getServiceName()) {
//    	  this.addError("serviceName", "serviceName is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getServiceUrl()) {
//    	  this.addError("serviceUrl", "serviceUrl is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getRequestMethod()) {
//    	  this.addError("requestMethod", "requestMethod is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getRequestUrl()) {
//    	  this.addError("requestUrl", "requestUrl is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getTargetEntity()) {
//    	  this.addError("targetEntity", "targetEntity is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getQueryString()) {
//    	  this.addError("queryString", "queryString is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getQueryParams()) {
//    	  this.addError("queryParams", "queryParams is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getInputFormat()) {
//    	  this.addError("inputFormat", "inputFormat is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getInputContent()) {
//    	  this.addError("inputContent", "inputContent is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getOutputFormat()) {
//    	  this.addError("outputFormat", "outputFormat is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getMaxRetries()) {
//    	  this.addError("maxRetries", "maxRetries is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getRetryInterval()) {
//    	  this.addError("retryInterval", "retryInterval is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getNote()) {
//    	  this.addError("note", "note is null");
//        allOK = false;
//      }

      return allOK; 
    };

    
    /////////////////////////////
    // Convenience methods
    // Note that we are "overriding" superclass methods
    // but reusing their implementations 
    /////////////////////////////
    
    // Clone this bean.
    that.clone = function() {
      var jsBean = this._clone();
      var o = new cronbay.wa.form.RestRequestStructFormBean(jsBean);

      if(errorMap) {
        for(var f in errorMap) {
          var errorList = errorMap[f];
          o.setErrors(f, errorList.slice(0));
        }
      }
    
      return o;
    };

    // This will be called by JSON.stringify().
    that.toJSON = function() {
      var jsonObj = this._toJSON();

      // ???
      if(!isEmpty(errorMap)) {
          jsonObj.errorMap = errorMap;
      }
      // ...

      return jsonObj;
    };


    /////////////////////////////
    // For debugging.
    /////////////////////////////

    that.toString = function() {
      var str = this._toString();

      str += "errors: {";
      for(var f in errorMap) {
        if(errorMap.hasOwnProperty(f)) {
          var errorList = errorMap[f];
          if(errorList) {
            str += f + ": [";
            for(var i=0; i<errorList.length; i++) {
              str += errorList[i] + ", ";
            }
            str += "];";
          }
        }
      }
      str += "};";

      return str;
    };

    return that;
  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.form.RestRequestStructFormBean.create = function(obj) {
  var jsBean = cronbay.wa.bean.RestRequestStructJsBean.create(obj);
  var o = new cronbay.wa.form.RestRequestStructFormBean(jsBean);

  if(obj.errorMap) {
    for(var f in obj.errorMap) {
      var errorList = obj.errorMap[f];
      o.setErrors(f, errorList.slice(0));
    }
  }

  return o;
};

cronbay.wa.form.RestRequestStructFormBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.form.RestRequestStructFormBean.create(jsonObj);
  return obj;
};

