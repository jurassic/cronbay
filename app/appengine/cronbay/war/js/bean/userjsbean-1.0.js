//////////////////////////////////////////////////////////
// <script src="/js/bean/userjsbean-1.0.js"></script>
// Last modified time: 1337996953365.
//////////////////////////////////////////////////////////

var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.bean = cronbay.wa.bean || {};
cronbay.wa.bean.UserJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var managerApp;
    var appAcl;
    var gaeApp;
    var aeryId;
    var sessionId;
    var username;
    var nickname;
    var avatar;
    var email;
    var openId;
    var gaeUser;
    var timeZone;
    var location;
    var ipAddress;
    var referer;
    var obsolete;
    var status;
    var verifiedTime;
    var authenticatedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getManagerApp = function() { return managerApp; };
    this.setManagerApp = function(value) { managerApp = value; };
    this.getAppAcl = function() { return appAcl; };
    this.setAppAcl = function(value) { appAcl = value; };
    this.getGaeApp = function() { return gaeApp; };
    this.setGaeApp = function(value) { gaeApp = value; };
    this.getAeryId = function() { return aeryId; };
    this.setAeryId = function(value) { aeryId = value; };
    this.getSessionId = function() { return sessionId; };
    this.setSessionId = function(value) { sessionId = value; };
    this.getUsername = function() { return username; };
    this.setUsername = function(value) { username = value; };
    this.getNickname = function() { return nickname; };
    this.setNickname = function(value) { nickname = value; };
    this.getAvatar = function() { return avatar; };
    this.setAvatar = function(value) { avatar = value; };
    this.getEmail = function() { return email; };
    this.setEmail = function(value) { email = value; };
    this.getOpenId = function() { return openId; };
    this.setOpenId = function(value) { openId = value; };
    this.getGaeUser = function() { return gaeUser; };
    this.setGaeUser = function(value) { gaeUser = value; };
    this.getTimeZone = function() { return timeZone; };
    this.setTimeZone = function(value) { timeZone = value; };
    this.getLocation = function() { return location; };
    this.setLocation = function(value) { location = value; };
    this.getIpAddress = function() { return ipAddress; };
    this.setIpAddress = function(value) { ipAddress = value; };
    this.getReferer = function() { return referer; };
    this.setReferer = function(value) { referer = value; };
    this.getObsolete = function() { return obsolete; };
    this.setObsolete = function(value) { obsolete = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getVerifiedTime = function() { return verifiedTime; };
    this.setVerifiedTime = function(value) { verifiedTime = value; };
    this.getAuthenticatedTime = function() { return authenticatedTime; };
    this.setAuthenticatedTime = function(value) { authenticatedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cronbay.wa.bean.UserJsBean();

      o.setGuid(generateUuid());
      if(managerApp) {
        o.setManagerApp(managerApp);
      }
      if(appAcl) {
        o.setAppAcl(appAcl);
      }
      //o.setGaeApp(gaeApp.clone());
      if(gaeApp) {
        o.setGaeApp(gaeApp);
      }
      if(aeryId) {
        o.setAeryId(aeryId);
      }
      if(sessionId) {
        o.setSessionId(sessionId);
      }
      if(username) {
        o.setUsername(username);
      }
      if(nickname) {
        o.setNickname(nickname);
      }
      if(avatar) {
        o.setAvatar(avatar);
      }
      if(email) {
        o.setEmail(email);
      }
      if(openId) {
        o.setOpenId(openId);
      }
      //o.setGaeUser(gaeUser.clone());
      if(gaeUser) {
        o.setGaeUser(gaeUser);
      }
      if(timeZone) {
        o.setTimeZone(timeZone);
      }
      if(location) {
        o.setLocation(location);
      }
      if(ipAddress) {
        o.setIpAddress(ipAddress);
      }
      if(referer) {
        o.setReferer(referer);
      }
      if(obsolete) {
        o.setObsolete(obsolete);
      }
      if(status) {
        o.setStatus(status);
      }
      if(verifiedTime) {
        o.setVerifiedTime(verifiedTime);
      }
      if(authenticatedTime) {
        o.setAuthenticatedTime(authenticatedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(managerApp) {
        jsonObj.managerApp = managerApp;
      } // Otherwise ignore...
      if(appAcl) {
        jsonObj.appAcl = appAcl;
      } // Otherwise ignore...
      if(gaeApp) {
        jsonObj.gaeApp = gaeApp;
      } // Otherwise ignore...
      if(aeryId) {
        jsonObj.aeryId = aeryId;
      } // Otherwise ignore...
      if(sessionId) {
        jsonObj.sessionId = sessionId;
      } // Otherwise ignore...
      if(username) {
        jsonObj.username = username;
      } // Otherwise ignore...
      if(nickname) {
        jsonObj.nickname = nickname;
      } // Otherwise ignore...
      if(avatar) {
        jsonObj.avatar = avatar;
      } // Otherwise ignore...
      if(email) {
        jsonObj.email = email;
      } // Otherwise ignore...
      if(openId) {
        jsonObj.openId = openId;
      } // Otherwise ignore...
      if(gaeUser) {
        jsonObj.gaeUser = gaeUser;
      } // Otherwise ignore...
      if(timeZone) {
        jsonObj.timeZone = timeZone;
      } // Otherwise ignore...
      if(location) {
        jsonObj.location = location;
      } // Otherwise ignore...
      if(ipAddress) {
        jsonObj.ipAddress = ipAddress;
      } // Otherwise ignore...
      if(referer) {
        jsonObj.referer = referer;
      } // Otherwise ignore...
      if(obsolete) {
        jsonObj.obsolete = obsolete;
      } // Otherwise ignore...
      if(status) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(verifiedTime) {
        jsonObj.verifiedTime = verifiedTime;
      } // Otherwise ignore...
      if(authenticatedTime) {
        jsonObj.authenticatedTime = authenticatedTime;
      } // Otherwise ignore...
      if(createdTime) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(managerApp) {
        str += "\"managerApp\":\"" + managerApp + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"managerApp\":null, ";
      }
      if(appAcl) {
        str += "\"appAcl\":" + appAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appAcl\":null, ";
      }
      str += "\"gaeApp\":" + gaeApp.toJsonString() + ", ";
      if(aeryId) {
        str += "\"aeryId\":\"" + aeryId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"aeryId\":null, ";
      }
      if(sessionId) {
        str += "\"sessionId\":\"" + sessionId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sessionId\":null, ";
      }
      if(username) {
        str += "\"username\":\"" + username + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"username\":null, ";
      }
      if(nickname) {
        str += "\"nickname\":\"" + nickname + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nickname\":null, ";
      }
      if(avatar) {
        str += "\"avatar\":\"" + avatar + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"avatar\":null, ";
      }
      if(email) {
        str += "\"email\":\"" + email + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"email\":null, ";
      }
      if(openId) {
        str += "\"openId\":\"" + openId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"openId\":null, ";
      }
      str += "\"gaeUser\":" + gaeUser.toJsonString() + ", ";
      if(timeZone) {
        str += "\"timeZone\":\"" + timeZone + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"timeZone\":null, ";
      }
      if(location) {
        str += "\"location\":\"" + location + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"location\":null, ";
      }
      if(ipAddress) {
        str += "\"ipAddress\":\"" + ipAddress + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ipAddress\":null, ";
      }
      if(referer) {
        str += "\"referer\":\"" + referer + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"referer\":null, ";
      }
      if(obsolete) {
        str += "\"obsolete\":" + obsolete + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"obsolete\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(verifiedTime) {
        str += "\"verifiedTime\":" + verifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"verifiedTime\":null, ";
      }
      if(authenticatedTime) {
        str += "\"authenticatedTime\":" + authenticatedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authenticatedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "managerApp:" + managerApp + ", ";
      str += "appAcl:" + appAcl + ", ";
      str += "gaeApp:" + gaeApp + ", ";
      str += "aeryId:" + aeryId + ", ";
      str += "sessionId:" + sessionId + ", ";
      str += "username:" + username + ", ";
      str += "nickname:" + nickname + ", ";
      str += "avatar:" + avatar + ", ";
      str += "email:" + email + ", ";
      str += "openId:" + openId + ", ";
      str += "gaeUser:" + gaeUser + ", ";
      str += "timeZone:" + timeZone + ", ";
      str += "location:" + location + ", ";
      str += "ipAddress:" + ipAddress + ", ";
      str += "referer:" + referer + ", ";
      str += "obsolete:" + obsolete + ", ";
      str += "status:" + status + ", ";
      str += "verifiedTime:" + verifiedTime + ", ";
      str += "authenticatedTime:" + authenticatedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.bean.UserJsBean.create = function(obj) {
  var o = new cronbay.wa.bean.UserJsBean();

  if(obj.guid) {
    o.setGuid(obj.guid);
  }
  if(obj.managerApp) {
    o.setManagerApp(obj.managerApp);
  }
  if(obj.appAcl) {
    o.setAppAcl(obj.appAcl);
  }
  if(obj.gaeApp) {
    o.setGaeApp(obj.gaeApp);
  }
  if(obj.aeryId) {
    o.setAeryId(obj.aeryId);
  }
  if(obj.sessionId) {
    o.setSessionId(obj.sessionId);
  }
  if(obj.username) {
    o.setUsername(obj.username);
  }
  if(obj.nickname) {
    o.setNickname(obj.nickname);
  }
  if(obj.avatar) {
    o.setAvatar(obj.avatar);
  }
  if(obj.email) {
    o.setEmail(obj.email);
  }
  if(obj.openId) {
    o.setOpenId(obj.openId);
  }
  if(obj.gaeUser) {
    o.setGaeUser(obj.gaeUser);
  }
  if(obj.timeZone) {
    o.setTimeZone(obj.timeZone);
  }
  if(obj.location) {
    o.setLocation(obj.location);
  }
  if(obj.ipAddress) {
    o.setIpAddress(obj.ipAddress);
  }
  if(obj.referer) {
    o.setReferer(obj.referer);
  }
  if(obj.obsolete) {
    o.setObsolete(obj.obsolete);
  }
  if(obj.status) {
    o.setStatus(obj.status);
  }
  if(obj.verifiedTime) {
    o.setVerifiedTime(obj.verifiedTime);
  }
  if(obj.authenticatedTime) {
    o.setAuthenticatedTime(obj.authenticatedTime);
  }
  if(obj.createdTime) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cronbay.wa.bean.UserJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.bean.UserJsBean.create(jsonObj);
  return obj;
};
