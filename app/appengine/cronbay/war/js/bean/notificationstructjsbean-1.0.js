//////////////////////////////////////////////////////////
// <script src="/js/bean/notificationstructjsbean-1.0.js"></script>
// Last modified time: 1337996953330.
//////////////////////////////////////////////////////////

var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.bean = cronbay.wa.bean || {};
cronbay.wa.bean.NotificationStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var preferredMode;
    var mobileNumber;
    var emailAddress;
    var callbackUrl;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getPreferredMode = function() { return preferredMode; };
    this.setPreferredMode = function(value) { preferredMode = value; };
    this.getMobileNumber = function() { return mobileNumber; };
    this.setMobileNumber = function(value) { mobileNumber = value; };
    this.getEmailAddress = function() { return emailAddress; };
    this.setEmailAddress = function(value) { emailAddress = value; };
    this.getCallbackUrl = function() { return callbackUrl; };
    this.setCallbackUrl = function(value) { callbackUrl = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cronbay.wa.bean.NotificationStructJsBean();

      if(preferredMode) {
        o.setPreferredMode(preferredMode);
      }
      if(mobileNumber) {
        o.setMobileNumber(mobileNumber);
      }
      if(emailAddress) {
        o.setEmailAddress(emailAddress);
      }
      if(callbackUrl) {
        o.setCallbackUrl(callbackUrl);
      }
      if(note) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(preferredMode) {
        jsonObj.preferredMode = preferredMode;
      } // Otherwise ignore...
      if(mobileNumber) {
        jsonObj.mobileNumber = mobileNumber;
      } // Otherwise ignore...
      if(emailAddress) {
        jsonObj.emailAddress = emailAddress;
      } // Otherwise ignore...
      if(callbackUrl) {
        jsonObj.callbackUrl = callbackUrl;
      } // Otherwise ignore...
      if(note) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(preferredMode) {
        str += "\"preferredMode\":\"" + preferredMode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"preferredMode\":null, ";
      }
      if(mobileNumber) {
        str += "\"mobileNumber\":\"" + mobileNumber + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"mobileNumber\":null, ";
      }
      if(emailAddress) {
        str += "\"emailAddress\":\"" + emailAddress + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"emailAddress\":null, ";
      }
      if(callbackUrl) {
        str += "\"callbackUrl\":\"" + callbackUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"callbackUrl\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "preferredMode:" + preferredMode + ", ";
      str += "mobileNumber:" + mobileNumber + ", ";
      str += "emailAddress:" + emailAddress + ", ";
      str += "callbackUrl:" + callbackUrl + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.bean.NotificationStructJsBean.create = function(obj) {
  var o = new cronbay.wa.bean.NotificationStructJsBean();

  if(obj.preferredMode) {
    o.setPreferredMode(obj.preferredMode);
  }
  if(obj.mobileNumber) {
    o.setMobileNumber(obj.mobileNumber);
  }
  if(obj.emailAddress) {
    o.setEmailAddress(obj.emailAddress);
  }
  if(obj.callbackUrl) {
    o.setCallbackUrl(obj.callbackUrl);
  }
  if(obj.note) {
    o.setNote(obj.note);
  }
    
  return o;
};

cronbay.wa.bean.NotificationStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.bean.NotificationStructJsBean.create(jsonObj);
  return obj;
};
