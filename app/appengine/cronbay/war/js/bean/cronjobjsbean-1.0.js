//////////////////////////////////////////////////////////
// <script src="/js/bean/cronjobjsbean-1.0.js"></script>
// Last modified time: 1337996953429.
//////////////////////////////////////////////////////////

var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.bean = cronbay.wa.bean || {};
cronbay.wa.bean.CronJobJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var managerApp;
    var appAcl;
    var gaeApp;
    var ownerUser;
    var userAcl;
    var user;
    var jobId;
    var title;
    var description;
    var originJob;
    var restRequest;
    var permalink;
    var shortlink;
    var status;
    var jobStatus;
    var extra;
    var note;
    var alert;
    var notificationPref;
    var referrerInfo;
    var cronSchedule;
    var nextRunTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getManagerApp = function() { return managerApp; };
    this.setManagerApp = function(value) { managerApp = value; };
    this.getAppAcl = function() { return appAcl; };
    this.setAppAcl = function(value) { appAcl = value; };
    this.getGaeApp = function() { return gaeApp; };
    this.setGaeApp = function(value) { gaeApp = value; };
    this.getOwnerUser = function() { return ownerUser; };
    this.setOwnerUser = function(value) { ownerUser = value; };
    this.getUserAcl = function() { return userAcl; };
    this.setUserAcl = function(value) { userAcl = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getJobId = function() { return jobId; };
    this.setJobId = function(value) { jobId = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getOriginJob = function() { return originJob; };
    this.setOriginJob = function(value) { originJob = value; };
    this.getRestRequest = function() { return restRequest; };
    this.setRestRequest = function(value) { restRequest = value; };
    this.getPermalink = function() { return permalink; };
    this.setPermalink = function(value) { permalink = value; };
    this.getShortlink = function() { return shortlink; };
    this.setShortlink = function(value) { shortlink = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getJobStatus = function() { return jobStatus; };
    this.setJobStatus = function(value) { jobStatus = value; };
    this.getExtra = function() { return extra; };
    this.setExtra = function(value) { extra = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getAlert = function() { return alert; };
    this.setAlert = function(value) { alert = value; };
    this.getNotificationPref = function() { return notificationPref; };
    this.setNotificationPref = function(value) { notificationPref = value; };
    this.getReferrerInfo = function() { return referrerInfo; };
    this.setReferrerInfo = function(value) { referrerInfo = value; };
    this.getCronSchedule = function() { return cronSchedule; };
    this.setCronSchedule = function(value) { cronSchedule = value; };
    this.getNextRunTime = function() { return nextRunTime; };
    this.setNextRunTime = function(value) { nextRunTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cronbay.wa.bean.CronJobJsBean();

      o.setGuid(generateUuid());
      if(managerApp) {
        o.setManagerApp(managerApp);
      }
      if(appAcl) {
        o.setAppAcl(appAcl);
      }
      //o.setGaeApp(gaeApp.clone());
      if(gaeApp) {
        o.setGaeApp(gaeApp);
      }
      if(ownerUser) {
        o.setOwnerUser(ownerUser);
      }
      if(userAcl) {
        o.setUserAcl(userAcl);
      }
      if(user) {
        o.setUser(user);
      }
      if(jobId) {
        o.setJobId(jobId);
      }
      if(title) {
        o.setTitle(title);
      }
      if(description) {
        o.setDescription(description);
      }
      if(originJob) {
        o.setOriginJob(originJob);
      }
      //o.setRestRequest(restRequest.clone());
      if(restRequest) {
        o.setRestRequest(restRequest);
      }
      if(permalink) {
        o.setPermalink(permalink);
      }
      if(shortlink) {
        o.setShortlink(shortlink);
      }
      if(status) {
        o.setStatus(status);
      }
      if(jobStatus) {
        o.setJobStatus(jobStatus);
      }
      if(extra) {
        o.setExtra(extra);
      }
      if(note) {
        o.setNote(note);
      }
      if(alert) {
        o.setAlert(alert);
      }
      //o.setNotificationPref(notificationPref.clone());
      if(notificationPref) {
        o.setNotificationPref(notificationPref);
      }
      //o.setReferrerInfo(referrerInfo.clone());
      if(referrerInfo) {
        o.setReferrerInfo(referrerInfo);
      }
      //o.setCronSchedule(cronSchedule.clone());
      if(cronSchedule) {
        o.setCronSchedule(cronSchedule);
      }
      if(nextRunTime) {
        o.setNextRunTime(nextRunTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(managerApp) {
        jsonObj.managerApp = managerApp;
      } // Otherwise ignore...
      if(appAcl) {
        jsonObj.appAcl = appAcl;
      } // Otherwise ignore...
      if(gaeApp) {
        jsonObj.gaeApp = gaeApp;
      } // Otherwise ignore...
      if(ownerUser) {
        jsonObj.ownerUser = ownerUser;
      } // Otherwise ignore...
      if(userAcl) {
        jsonObj.userAcl = userAcl;
      } // Otherwise ignore...
      if(user) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(jobId) {
        jsonObj.jobId = jobId;
      } // Otherwise ignore...
      if(title) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(description) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(originJob) {
        jsonObj.originJob = originJob;
      } // Otherwise ignore...
      if(restRequest) {
        jsonObj.restRequest = restRequest;
      } // Otherwise ignore...
      if(permalink) {
        jsonObj.permalink = permalink;
      } // Otherwise ignore...
      if(shortlink) {
        jsonObj.shortlink = shortlink;
      } // Otherwise ignore...
      if(status) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(jobStatus) {
        jsonObj.jobStatus = jobStatus;
      } // Otherwise ignore...
      if(extra) {
        jsonObj.extra = extra;
      } // Otherwise ignore...
      if(note) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(alert) {
        jsonObj.alert = alert;
      } // Otherwise ignore...
      if(notificationPref) {
        jsonObj.notificationPref = notificationPref;
      } // Otherwise ignore...
      if(referrerInfo) {
        jsonObj.referrerInfo = referrerInfo;
      } // Otherwise ignore...
      if(cronSchedule) {
        jsonObj.cronSchedule = cronSchedule;
      } // Otherwise ignore...
      if(nextRunTime) {
        jsonObj.nextRunTime = nextRunTime;
      } // Otherwise ignore...
      if(createdTime) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(managerApp) {
        str += "\"managerApp\":\"" + managerApp + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"managerApp\":null, ";
      }
      if(appAcl) {
        str += "\"appAcl\":" + appAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appAcl\":null, ";
      }
      str += "\"gaeApp\":" + gaeApp.toJsonString() + ", ";
      if(ownerUser) {
        str += "\"ownerUser\":\"" + ownerUser + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ownerUser\":null, ";
      }
      if(userAcl) {
        str += "\"userAcl\":" + userAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userAcl\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(jobId) {
        str += "\"jobId\":" + jobId + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"jobId\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(originJob) {
        str += "\"originJob\":\"" + originJob + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"originJob\":null, ";
      }
      str += "\"restRequest\":" + restRequest.toJsonString() + ", ";
      if(permalink) {
        str += "\"permalink\":\"" + permalink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"permalink\":null, ";
      }
      if(shortlink) {
        str += "\"shortlink\":\"" + shortlink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortlink\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(jobStatus) {
        str += "\"jobStatus\":" + jobStatus + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"jobStatus\":null, ";
      }
      if(extra) {
        str += "\"extra\":\"" + extra + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"extra\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(alert) {
        str += "\"alert\":" + alert + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"alert\":null, ";
      }
      str += "\"notificationPref\":" + notificationPref.toJsonString() + ", ";
      str += "\"referrerInfo\":" + referrerInfo.toJsonString() + ", ";
      str += "\"cronSchedule\":" + cronSchedule.toJsonString() + ", ";
      if(nextRunTime) {
        str += "\"nextRunTime\":" + nextRunTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nextRunTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "managerApp:" + managerApp + ", ";
      str += "appAcl:" + appAcl + ", ";
      str += "gaeApp:" + gaeApp + ", ";
      str += "ownerUser:" + ownerUser + ", ";
      str += "userAcl:" + userAcl + ", ";
      str += "user:" + user + ", ";
      str += "jobId:" + jobId + ", ";
      str += "title:" + title + ", ";
      str += "description:" + description + ", ";
      str += "originJob:" + originJob + ", ";
      str += "restRequest:" + restRequest + ", ";
      str += "permalink:" + permalink + ", ";
      str += "shortlink:" + shortlink + ", ";
      str += "status:" + status + ", ";
      str += "jobStatus:" + jobStatus + ", ";
      str += "extra:" + extra + ", ";
      str += "note:" + note + ", ";
      str += "alert:" + alert + ", ";
      str += "notificationPref:" + notificationPref + ", ";
      str += "referrerInfo:" + referrerInfo + ", ";
      str += "cronSchedule:" + cronSchedule + ", ";
      str += "nextRunTime:" + nextRunTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.bean.CronJobJsBean.create = function(obj) {
  var o = new cronbay.wa.bean.CronJobJsBean();

  if(obj.guid) {
    o.setGuid(obj.guid);
  }
  if(obj.managerApp) {
    o.setManagerApp(obj.managerApp);
  }
  if(obj.appAcl) {
    o.setAppAcl(obj.appAcl);
  }
  if(obj.gaeApp) {
    o.setGaeApp(obj.gaeApp);
  }
  if(obj.ownerUser) {
    o.setOwnerUser(obj.ownerUser);
  }
  if(obj.userAcl) {
    o.setUserAcl(obj.userAcl);
  }
  if(obj.user) {
    o.setUser(obj.user);
  }
  if(obj.jobId) {
    o.setJobId(obj.jobId);
  }
  if(obj.title) {
    o.setTitle(obj.title);
  }
  if(obj.description) {
    o.setDescription(obj.description);
  }
  if(obj.originJob) {
    o.setOriginJob(obj.originJob);
  }
  if(obj.restRequest) {
    o.setRestRequest(obj.restRequest);
  }
  if(obj.permalink) {
    o.setPermalink(obj.permalink);
  }
  if(obj.shortlink) {
    o.setShortlink(obj.shortlink);
  }
  if(obj.status) {
    o.setStatus(obj.status);
  }
  if(obj.jobStatus) {
    o.setJobStatus(obj.jobStatus);
  }
  if(obj.extra) {
    o.setExtra(obj.extra);
  }
  if(obj.note) {
    o.setNote(obj.note);
  }
  if(obj.alert) {
    o.setAlert(obj.alert);
  }
  if(obj.notificationPref) {
    o.setNotificationPref(obj.notificationPref);
  }
  if(obj.referrerInfo) {
    o.setReferrerInfo(obj.referrerInfo);
  }
  if(obj.cronSchedule) {
    o.setCronSchedule(obj.cronSchedule);
  }
  if(obj.nextRunTime) {
    o.setNextRunTime(obj.nextRunTime);
  }
  if(obj.createdTime) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cronbay.wa.bean.CronJobJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.bean.CronJobJsBean.create(jsonObj);
  return obj;
};
