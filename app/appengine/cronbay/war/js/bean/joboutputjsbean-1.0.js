//////////////////////////////////////////////////////////
// <script src="/js/bean/joboutputjsbean-1.0.js"></script>
// Last modified time: 1337996953442.
//////////////////////////////////////////////////////////

var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.bean = cronbay.wa.bean || {};
cronbay.wa.bean.JobOutputJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var managerApp;
    var appAcl;
    var gaeApp;
    var ownerUser;
    var userAcl;
    var user;
    var cronJob;
    var previousRun;
    var previousTry;
    var responseCode;
    var outputFormat;
    var outputText;
    var outputData;
    var outputHash;
    var permalink;
    var shortlink;
    var result;
    var status;
    var extra;
    var note;
    var retryCounter;
    var scheduledTime;
    var processedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getManagerApp = function() { return managerApp; };
    this.setManagerApp = function(value) { managerApp = value; };
    this.getAppAcl = function() { return appAcl; };
    this.setAppAcl = function(value) { appAcl = value; };
    this.getGaeApp = function() { return gaeApp; };
    this.setGaeApp = function(value) { gaeApp = value; };
    this.getOwnerUser = function() { return ownerUser; };
    this.setOwnerUser = function(value) { ownerUser = value; };
    this.getUserAcl = function() { return userAcl; };
    this.setUserAcl = function(value) { userAcl = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getCronJob = function() { return cronJob; };
    this.setCronJob = function(value) { cronJob = value; };
    this.getPreviousRun = function() { return previousRun; };
    this.setPreviousRun = function(value) { previousRun = value; };
    this.getPreviousTry = function() { return previousTry; };
    this.setPreviousTry = function(value) { previousTry = value; };
    this.getResponseCode = function() { return responseCode; };
    this.setResponseCode = function(value) { responseCode = value; };
    this.getOutputFormat = function() { return outputFormat; };
    this.setOutputFormat = function(value) { outputFormat = value; };
    this.getOutputText = function() { return outputText; };
    this.setOutputText = function(value) { outputText = value; };
    this.getOutputData = function() { return outputData; };
    this.setOutputData = function(value) { outputData = value; };
    this.getOutputHash = function() { return outputHash; };
    this.setOutputHash = function(value) { outputHash = value; };
    this.getPermalink = function() { return permalink; };
    this.setPermalink = function(value) { permalink = value; };
    this.getShortlink = function() { return shortlink; };
    this.setShortlink = function(value) { shortlink = value; };
    this.getResult = function() { return result; };
    this.setResult = function(value) { result = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getExtra = function() { return extra; };
    this.setExtra = function(value) { extra = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getRetryCounter = function() { return retryCounter; };
    this.setRetryCounter = function(value) { retryCounter = value; };
    this.getScheduledTime = function() { return scheduledTime; };
    this.setScheduledTime = function(value) { scheduledTime = value; };
    this.getProcessedTime = function() { return processedTime; };
    this.setProcessedTime = function(value) { processedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cronbay.wa.bean.JobOutputJsBean();

      o.setGuid(generateUuid());
      if(managerApp) {
        o.setManagerApp(managerApp);
      }
      if(appAcl) {
        o.setAppAcl(appAcl);
      }
      //o.setGaeApp(gaeApp.clone());
      if(gaeApp) {
        o.setGaeApp(gaeApp);
      }
      if(ownerUser) {
        o.setOwnerUser(ownerUser);
      }
      if(userAcl) {
        o.setUserAcl(userAcl);
      }
      if(user) {
        o.setUser(user);
      }
      if(cronJob) {
        o.setCronJob(cronJob);
      }
      if(previousRun) {
        o.setPreviousRun(previousRun);
      }
      if(previousTry) {
        o.setPreviousTry(previousTry);
      }
      if(responseCode) {
        o.setResponseCode(responseCode);
      }
      if(outputFormat) {
        o.setOutputFormat(outputFormat);
      }
      if(outputText) {
        o.setOutputText(outputText);
      }
      if(outputData) {
        o.setOutputData(outputData);
      }
      if(outputHash) {
        o.setOutputHash(outputHash);
      }
      if(permalink) {
        o.setPermalink(permalink);
      }
      if(shortlink) {
        o.setShortlink(shortlink);
      }
      if(result) {
        o.setResult(result);
      }
      if(status) {
        o.setStatus(status);
      }
      if(extra) {
        o.setExtra(extra);
      }
      if(note) {
        o.setNote(note);
      }
      if(retryCounter) {
        o.setRetryCounter(retryCounter);
      }
      if(scheduledTime) {
        o.setScheduledTime(scheduledTime);
      }
      if(processedTime) {
        o.setProcessedTime(processedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(managerApp) {
        jsonObj.managerApp = managerApp;
      } // Otherwise ignore...
      if(appAcl) {
        jsonObj.appAcl = appAcl;
      } // Otherwise ignore...
      if(gaeApp) {
        jsonObj.gaeApp = gaeApp;
      } // Otherwise ignore...
      if(ownerUser) {
        jsonObj.ownerUser = ownerUser;
      } // Otherwise ignore...
      if(userAcl) {
        jsonObj.userAcl = userAcl;
      } // Otherwise ignore...
      if(user) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(cronJob) {
        jsonObj.cronJob = cronJob;
      } // Otherwise ignore...
      if(previousRun) {
        jsonObj.previousRun = previousRun;
      } // Otherwise ignore...
      if(previousTry) {
        jsonObj.previousTry = previousTry;
      } // Otherwise ignore...
      if(responseCode) {
        jsonObj.responseCode = responseCode;
      } // Otherwise ignore...
      if(outputFormat) {
        jsonObj.outputFormat = outputFormat;
      } // Otherwise ignore...
      if(outputText) {
        jsonObj.outputText = outputText;
      } // Otherwise ignore...
      if(outputData) {
        jsonObj.outputData = outputData;
      } // Otherwise ignore...
      if(outputHash) {
        jsonObj.outputHash = outputHash;
      } // Otherwise ignore...
      if(permalink) {
        jsonObj.permalink = permalink;
      } // Otherwise ignore...
      if(shortlink) {
        jsonObj.shortlink = shortlink;
      } // Otherwise ignore...
      if(result) {
        jsonObj.result = result;
      } // Otherwise ignore...
      if(status) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(extra) {
        jsonObj.extra = extra;
      } // Otherwise ignore...
      if(note) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(retryCounter) {
        jsonObj.retryCounter = retryCounter;
      } // Otherwise ignore...
      if(scheduledTime) {
        jsonObj.scheduledTime = scheduledTime;
      } // Otherwise ignore...
      if(processedTime) {
        jsonObj.processedTime = processedTime;
      } // Otherwise ignore...
      if(createdTime) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(managerApp) {
        str += "\"managerApp\":\"" + managerApp + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"managerApp\":null, ";
      }
      if(appAcl) {
        str += "\"appAcl\":" + appAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appAcl\":null, ";
      }
      str += "\"gaeApp\":" + gaeApp.toJsonString() + ", ";
      if(ownerUser) {
        str += "\"ownerUser\":\"" + ownerUser + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ownerUser\":null, ";
      }
      if(userAcl) {
        str += "\"userAcl\":" + userAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userAcl\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(cronJob) {
        str += "\"cronJob\":\"" + cronJob + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"cronJob\":null, ";
      }
      if(previousRun) {
        str += "\"previousRun\":\"" + previousRun + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"previousRun\":null, ";
      }
      if(previousTry) {
        str += "\"previousTry\":\"" + previousTry + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"previousTry\":null, ";
      }
      if(responseCode) {
        str += "\"responseCode\":\"" + responseCode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"responseCode\":null, ";
      }
      if(outputFormat) {
        str += "\"outputFormat\":\"" + outputFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputFormat\":null, ";
      }
      if(outputText) {
        str += "\"outputText\":\"" + outputText + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputText\":null, ";
      }
      if(outputData) {
        str += "\"outputData\":\"" + outputData + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputData\":null, ";
      }
      if(outputHash) {
        str += "\"outputHash\":\"" + outputHash + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputHash\":null, ";
      }
      if(permalink) {
        str += "\"permalink\":\"" + permalink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"permalink\":null, ";
      }
      if(shortlink) {
        str += "\"shortlink\":\"" + shortlink + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortlink\":null, ";
      }
      if(result) {
        str += "\"result\":\"" + result + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"result\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(extra) {
        str += "\"extra\":\"" + extra + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"extra\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(retryCounter) {
        str += "\"retryCounter\":" + retryCounter + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"retryCounter\":null, ";
      }
      if(scheduledTime) {
        str += "\"scheduledTime\":" + scheduledTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"scheduledTime\":null, ";
      }
      if(processedTime) {
        str += "\"processedTime\":" + processedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"processedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "managerApp:" + managerApp + ", ";
      str += "appAcl:" + appAcl + ", ";
      str += "gaeApp:" + gaeApp + ", ";
      str += "ownerUser:" + ownerUser + ", ";
      str += "userAcl:" + userAcl + ", ";
      str += "user:" + user + ", ";
      str += "cronJob:" + cronJob + ", ";
      str += "previousRun:" + previousRun + ", ";
      str += "previousTry:" + previousTry + ", ";
      str += "responseCode:" + responseCode + ", ";
      str += "outputFormat:" + outputFormat + ", ";
      str += "outputText:" + outputText + ", ";
      str += "outputData:" + outputData + ", ";
      str += "outputHash:" + outputHash + ", ";
      str += "permalink:" + permalink + ", ";
      str += "shortlink:" + shortlink + ", ";
      str += "result:" + result + ", ";
      str += "status:" + status + ", ";
      str += "extra:" + extra + ", ";
      str += "note:" + note + ", ";
      str += "retryCounter:" + retryCounter + ", ";
      str += "scheduledTime:" + scheduledTime + ", ";
      str += "processedTime:" + processedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.bean.JobOutputJsBean.create = function(obj) {
  var o = new cronbay.wa.bean.JobOutputJsBean();

  if(obj.guid) {
    o.setGuid(obj.guid);
  }
  if(obj.managerApp) {
    o.setManagerApp(obj.managerApp);
  }
  if(obj.appAcl) {
    o.setAppAcl(obj.appAcl);
  }
  if(obj.gaeApp) {
    o.setGaeApp(obj.gaeApp);
  }
  if(obj.ownerUser) {
    o.setOwnerUser(obj.ownerUser);
  }
  if(obj.userAcl) {
    o.setUserAcl(obj.userAcl);
  }
  if(obj.user) {
    o.setUser(obj.user);
  }
  if(obj.cronJob) {
    o.setCronJob(obj.cronJob);
  }
  if(obj.previousRun) {
    o.setPreviousRun(obj.previousRun);
  }
  if(obj.previousTry) {
    o.setPreviousTry(obj.previousTry);
  }
  if(obj.responseCode) {
    o.setResponseCode(obj.responseCode);
  }
  if(obj.outputFormat) {
    o.setOutputFormat(obj.outputFormat);
  }
  if(obj.outputText) {
    o.setOutputText(obj.outputText);
  }
  if(obj.outputData) {
    o.setOutputData(obj.outputData);
  }
  if(obj.outputHash) {
    o.setOutputHash(obj.outputHash);
  }
  if(obj.permalink) {
    o.setPermalink(obj.permalink);
  }
  if(obj.shortlink) {
    o.setShortlink(obj.shortlink);
  }
  if(obj.result) {
    o.setResult(obj.result);
  }
  if(obj.status) {
    o.setStatus(obj.status);
  }
  if(obj.extra) {
    o.setExtra(obj.extra);
  }
  if(obj.note) {
    o.setNote(obj.note);
  }
  if(obj.retryCounter) {
    o.setRetryCounter(obj.retryCounter);
  }
  if(obj.scheduledTime) {
    o.setScheduledTime(obj.scheduledTime);
  }
  if(obj.processedTime) {
    o.setProcessedTime(obj.processedTime);
  }
  if(obj.createdTime) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cronbay.wa.bean.JobOutputJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.bean.JobOutputJsBean.create(jsonObj);
  return obj;
};
