//////////////////////////////////////////////////////////
// <script src="/js/bean/restrequeststructjsbean-1.0.js"></script>
// Last modified time: 1337996953418.
//////////////////////////////////////////////////////////

var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.bean = cronbay.wa.bean || {};
cronbay.wa.bean.RestRequestStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var serviceName;
    var serviceUrl;
    var requestMethod;
    var requestUrl;
    var targetEntity;
    var queryString;
    var queryParams;
    var inputFormat;
    var inputContent;
    var outputFormat;
    var maxRetries;
    var retryInterval;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getServiceName = function() { return serviceName; };
    this.setServiceName = function(value) { serviceName = value; };
    this.getServiceUrl = function() { return serviceUrl; };
    this.setServiceUrl = function(value) { serviceUrl = value; };
    this.getRequestMethod = function() { return requestMethod; };
    this.setRequestMethod = function(value) { requestMethod = value; };
    this.getRequestUrl = function() { return requestUrl; };
    this.setRequestUrl = function(value) { requestUrl = value; };
    this.getTargetEntity = function() { return targetEntity; };
    this.setTargetEntity = function(value) { targetEntity = value; };
    this.getQueryString = function() { return queryString; };
    this.setQueryString = function(value) { queryString = value; };
    this.getQueryParams = function() { return queryParams; };
    this.setQueryParams = function(value) { queryParams = value; };
    this.getInputFormat = function() { return inputFormat; };
    this.setInputFormat = function(value) { inputFormat = value; };
    this.getInputContent = function() { return inputContent; };
    this.setInputContent = function(value) { inputContent = value; };
    this.getOutputFormat = function() { return outputFormat; };
    this.setOutputFormat = function(value) { outputFormat = value; };
    this.getMaxRetries = function() { return maxRetries; };
    this.setMaxRetries = function(value) { maxRetries = value; };
    this.getRetryInterval = function() { return retryInterval; };
    this.setRetryInterval = function(value) { retryInterval = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cronbay.wa.bean.RestRequestStructJsBean();

      if(serviceName) {
        o.setServiceName(serviceName);
      }
      if(serviceUrl) {
        o.setServiceUrl(serviceUrl);
      }
      if(requestMethod) {
        o.setRequestMethod(requestMethod);
      }
      if(requestUrl) {
        o.setRequestUrl(requestUrl);
      }
      if(targetEntity) {
        o.setTargetEntity(targetEntity);
      }
      if(queryString) {
        o.setQueryString(queryString);
      }
      if(queryParams) {
        o.setQueryParams(queryParams);
      }
      if(inputFormat) {
        o.setInputFormat(inputFormat);
      }
      if(inputContent) {
        o.setInputContent(inputContent);
      }
      if(outputFormat) {
        o.setOutputFormat(outputFormat);
      }
      if(maxRetries) {
        o.setMaxRetries(maxRetries);
      }
      if(retryInterval) {
        o.setRetryInterval(retryInterval);
      }
      if(note) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(serviceName) {
        jsonObj.serviceName = serviceName;
      } // Otherwise ignore...
      if(serviceUrl) {
        jsonObj.serviceUrl = serviceUrl;
      } // Otherwise ignore...
      if(requestMethod) {
        jsonObj.requestMethod = requestMethod;
      } // Otherwise ignore...
      if(requestUrl) {
        jsonObj.requestUrl = requestUrl;
      } // Otherwise ignore...
      if(targetEntity) {
        jsonObj.targetEntity = targetEntity;
      } // Otherwise ignore...
      if(queryString) {
        jsonObj.queryString = queryString;
      } // Otherwise ignore...
      if(queryParams) {
        jsonObj.queryParams = queryParams;
      } // Otherwise ignore...
      if(inputFormat) {
        jsonObj.inputFormat = inputFormat;
      } // Otherwise ignore...
      if(inputContent) {
        jsonObj.inputContent = inputContent;
      } // Otherwise ignore...
      if(outputFormat) {
        jsonObj.outputFormat = outputFormat;
      } // Otherwise ignore...
      if(maxRetries) {
        jsonObj.maxRetries = maxRetries;
      } // Otherwise ignore...
      if(retryInterval) {
        jsonObj.retryInterval = retryInterval;
      } // Otherwise ignore...
      if(note) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(serviceName) {
        str += "\"serviceName\":\"" + serviceName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"serviceName\":null, ";
      }
      if(serviceUrl) {
        str += "\"serviceUrl\":\"" + serviceUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"serviceUrl\":null, ";
      }
      if(requestMethod) {
        str += "\"requestMethod\":\"" + requestMethod + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"requestMethod\":null, ";
      }
      if(requestUrl) {
        str += "\"requestUrl\":\"" + requestUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"requestUrl\":null, ";
      }
      if(targetEntity) {
        str += "\"targetEntity\":\"" + targetEntity + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"targetEntity\":null, ";
      }
      if(queryString) {
        str += "\"queryString\":\"" + queryString + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"queryString\":null, ";
      }
      if(queryParams) {
        str += "\"queryParams\":\"" + queryParams + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"queryParams\":null, ";
      }
      if(inputFormat) {
        str += "\"inputFormat\":\"" + inputFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"inputFormat\":null, ";
      }
      if(inputContent) {
        str += "\"inputContent\":\"" + inputContent + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"inputContent\":null, ";
      }
      if(outputFormat) {
        str += "\"outputFormat\":\"" + outputFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputFormat\":null, ";
      }
      if(maxRetries) {
        str += "\"maxRetries\":" + maxRetries + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"maxRetries\":null, ";
      }
      if(retryInterval) {
        str += "\"retryInterval\":" + retryInterval + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"retryInterval\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "serviceName:" + serviceName + ", ";
      str += "serviceUrl:" + serviceUrl + ", ";
      str += "requestMethod:" + requestMethod + ", ";
      str += "requestUrl:" + requestUrl + ", ";
      str += "targetEntity:" + targetEntity + ", ";
      str += "queryString:" + queryString + ", ";
      str += "queryParams:" + queryParams + ", ";
      str += "inputFormat:" + inputFormat + ", ";
      str += "inputContent:" + inputContent + ", ";
      str += "outputFormat:" + outputFormat + ", ";
      str += "maxRetries:" + maxRetries + ", ";
      str += "retryInterval:" + retryInterval + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.bean.RestRequestStructJsBean.create = function(obj) {
  var o = new cronbay.wa.bean.RestRequestStructJsBean();

  if(obj.serviceName) {
    o.setServiceName(obj.serviceName);
  }
  if(obj.serviceUrl) {
    o.setServiceUrl(obj.serviceUrl);
  }
  if(obj.requestMethod) {
    o.setRequestMethod(obj.requestMethod);
  }
  if(obj.requestUrl) {
    o.setRequestUrl(obj.requestUrl);
  }
  if(obj.targetEntity) {
    o.setTargetEntity(obj.targetEntity);
  }
  if(obj.queryString) {
    o.setQueryString(obj.queryString);
  }
  if(obj.queryParams) {
    o.setQueryParams(obj.queryParams);
  }
  if(obj.inputFormat) {
    o.setInputFormat(obj.inputFormat);
  }
  if(obj.inputContent) {
    o.setInputContent(obj.inputContent);
  }
  if(obj.outputFormat) {
    o.setOutputFormat(obj.outputFormat);
  }
  if(obj.maxRetries) {
    o.setMaxRetries(obj.maxRetries);
  }
  if(obj.retryInterval) {
    o.setRetryInterval(obj.retryInterval);
  }
  if(obj.note) {
    o.setNote(obj.note);
  }
    
  return o;
};

cronbay.wa.bean.RestRequestStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.bean.RestRequestStructJsBean.create(jsonObj);
  return obj;
};
