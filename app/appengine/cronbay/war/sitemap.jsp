<%@ page import="com.cronbay.ws.core.*, com.cronbay.af.auth.*, com.cronbay.fe.*, com.cronbay.fe.core.*, com.cronbay.fe.bean.*, com.cronbay.fe.util.*, com.cronbay.wa.service.*, com.cronbay.util.*, com.cronbay.helper.*" 
%><%@ page contentType="application/xml; charset=UTF-8" 
%><?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

<%
//String requestUrl = request.getRequestURL().toString();
//String topLevelUrl = URLHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String weekAgo = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*7*3600000L);
if(weekAgo == null || weekAgo.isEmpty()) {  // This should not happen.
   weekAgo = "2011-11-11";   // Just use an arbitrary date...
}
%>

   <url>
      <loc>http://www.cronbay.com/</loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.8</priority>
   </url>
   <url>
      <loc>http://www.cronbay.com/urlcoder</loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.5</priority>
   </url>

</urlset>
