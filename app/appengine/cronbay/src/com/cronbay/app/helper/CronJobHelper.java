package com.cronbay.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.app.service.CronJobAppService;
import com.cronbay.app.service.UserAppService;
import com.cronbay.common.CronJobStatus;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.CronJob;


public class CronJobHelper
{
    private static final Logger log = Logger.getLogger(CronJobHelper.class.getName());

    private CronJobHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private CronJobAppService cronJobAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private CronJobAppService getCronJobService()
    {
        if(cronJobAppService == null) {
            cronJobAppService = new CronJobAppService();
        }
        return cronJobAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class CronJobHelperHolder
    {
        private static final CronJobHelper INSTANCE = new CronJobHelper();
    }

    // Singleton method
    public static CronJobHelper getInstance()
    {
        return CronJobHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public CronJob getCronJob(String guid) 
    {
        CronJob message = null;
        try {
            message = getCronJobService().getCronJob(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
//    // ????
//    public CronJob getCronJobByKey(String key) 
//    {
//        CronJob message = null;
//        try {
//            // ??????
//            String guid = key;
//            message = getCronJobService().getCronJob(key);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve the message for key = " + key, e);
//        }
//        return message;
//    }


    
    ///////////////////////////////////////////////////////////////////////////
    // Note: Google app engine puts so many restrictions as to what kind of query can be performed....
    //       http://code.google.com/appengine/docs/java/datastore/queries.html
    ///////////////////////////////////////////////////////////////////////////
    

    // TBD
    
    public List<String> getCronJobKeysForProcessing(Integer maxCount)
    {
        List<String> keys = null;
        String filter = "cronJobStatus == " + CronJobStatus.STATUS_SCHEDULED;
        String ordering = null;
        try {
            keys = getCronJobService().findCronJobKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve job request key list.", e);
        }

        return keys;
    }
    
    public List<CronJob> getCronJobsForProcessing(Integer maxCount)
    {
        List<CronJob> cronJobs = null;
        String filter = "cronJobStatus == " + CronJobStatus.STATUS_SCHEDULED;
        String ordering = null;
        try {
            cronJobs = getCronJobService().findCronJobs(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve job request list.", e);
        }

        return cronJobs;
    }
 

}
