package com.cronbay.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.app.service.JobOutputAppService;
import com.cronbay.app.service.UserAppService;
import com.cronbay.common.CronJobStatus;
import com.cronbay.common.RefreshStatus;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.JobOutput;


public class JobOutputHelper
{
    private static final Logger log = Logger.getLogger(JobOutputHelper.class.getName());

    private JobOutputHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private JobOutputAppService cronJobAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private JobOutputAppService getJobOutputService()
    {
        if(cronJobAppService == null) {
            cronJobAppService = new JobOutputAppService();
        }
        return cronJobAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class JobOutputHelperHolder
    {
        private static final JobOutputHelper INSTANCE = new JobOutputHelper();
    }

    // Singleton method
    public static JobOutputHelper getInstance()
    {
        return JobOutputHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public JobOutput getJobOutput(String guid) 
    {
        JobOutput message = null;
        try {
            message = getJobOutputService().getJobOutput(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getJobOutputKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getJobOutputService().findJobOutputKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve job request key list.", e);
        }

        return keys;
    }
    
    public List<JobOutput> getJobOutputsForRefreshProcessing(Integer maxCount)
    {
        List<JobOutput> jobOutputs = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            jobOutputs = getJobOutputService().findJobOutputs(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve job request list.", e);
        }

        return jobOutputs;
    }


}
