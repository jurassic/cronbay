package com.cronbay.app.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class QueryStringUtil
{
    private static final Logger log = Logger.getLogger(QueryStringUtil.class.getName());

    // Static methods only.
    private QueryStringUtil() {}


    // Parse the given query string and returns a list of url-decoded query param pairs (separated by "=").
    // Note: We assume query param key does not contain "=" (even if url encoded).
    public static List<String> parseQueryString(String query)
    {
        if(query == null) {
            return null;
        }
        List<String> params = new ArrayList<String>();
        if(query.isEmpty()) {
            return params;
        }
        
        String[] paramsEncoded = query.split("&");
        for(String pe : paramsEncoded) {
            String[] pair = pe.split("=", 2);
            try {
                String row = URLDecoder.decode(pair[0], "UTF-8");
                if(pair.length > 1) {
                    row += "=" + URLDecoder.decode(pair[1], "UTF-8");
                }
                params.add(row);
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to url decode a query param, " + pe, e);
                // ignore, and continue.
            }
        }

        return params;
    }
    
    
    // ...

}
