package com.cronbay.app.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlizeUtil
{
    private static final Logger log = Logger.getLogger(HtmlizeUtil.class.getName());

    private static final Pattern URL_PATTERN = Pattern.compile("https?://([-\\w\\.]+)+(:\\d+)?(/([\\w/_\\.]*(\\?\\S+)?)?)?", Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);

    // Static methods only.
    private HtmlizeUtil() {}

    public static String htmlize(String content)
    {
        // TBD
        String htmlized = content;

        // Parse content, and find urls
        // Include urls in <A> links...
        // ...
        
        Matcher matcher = URL_PATTERN.matcher(content);
        
        while(matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            String group = matcher.group();
            // ....
            log.log(Level.INFO, "start = " + start + "; end = " + end + "; group = " + group);
            // ....
        }
        
        
        
        
        return htmlized;
    }


    // TBD
    /*
    Pattern pattern = Pattern.compile("https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?", Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);
    Matcher myMatcher = pattern.matcher(myStringWithUrls);
    while (myMatcher.find()) {
        ...
    }
    */
    
}
