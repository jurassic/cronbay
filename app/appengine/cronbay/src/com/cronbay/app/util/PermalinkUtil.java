package com.cronbay.app.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.logging.Logger;

import com.cronbay.fe.bean.CronJobJsBean;

public class PermalinkUtil
{
    private static final Logger log = Logger.getLogger(PermalinkUtil.class.getName());

    // Note: These are not actual lower/upper limits/counts of the permalink.
    private static final int MIN_TOTAL_LENGTH = 60;
    private static final int MAX_TOTAL_LENGTH = 75;
    private static final int MIN_TAIL_LENGTH = 25;
    private static final int MAX_TAIL_LENGTH = 40;

    private static Random sRandom = null;
    
    // Static methods only.
    private PermalinkUtil() {}


    // TBD: Content is really needed in case the cronJob.title is too short...
    public static String generatePermalink(String topUrl, CronJobJsBean cronJobBean)
    {
        if(topUrl == null || cronJobBean == null) {
            log.warning("Invalid arguments: topUrl or cronJobBean is null.");
            return null;
        }

//        String title = cronJobBean.getTitle();
        String title = "";  // ???????????????????????????????
        StringBuffer sb = new StringBuffer();
        
        // [1] hostname, top level path, etc.
        sb.append(topUrl);
        if(!topUrl.endsWith("/")) {
            sb.append("/");
        }
        
        // [2] Today's date
        // "yyyy/mm/dd"
        Calendar cal = Calendar.getInstance();
        sb.append(cal.get(Calendar.YEAR));
        sb.append("/");   // ???
        String month = String.format("%02d", cal.get(Calendar.MONTH) + 1);
        sb.append(month);
        sb.append("/");   // ???
        String day = String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
        sb.append(day);
        sb.append("/");

        // [3] Random (numeric) string
        sb.append(generateRandomString(4));
        sb.append("/");

        // [4] Title
        sb.append(sanitizePhrase(title, MAX_TAIL_LENGTH));
        
        // [5] Content
        if(sb.length() < MIN_TOTAL_LENGTH) {
            // TBD
            // ...
        }

        // [6] Random string padding. (This shound't normally be necessary.)
        if(sb.length() < MAX_TOTAL_LENGTH) {
            int numPads = 3;
            if(sb.length() < MIN_TOTAL_LENGTH) {
                numPads = 6;   // A million is a sufficiently big number...
            }
            //String randomPhrase = generateRandomString(MIN_TOTAL_LENGTH - sb.length());
            String randomPhrase = generateRandomString(numPads);
            if(randomPhrase != null && randomPhrase.length() > 0) {
                sb.append("-");
                sb.append(randomPhrase);
            }
        }

        String permalink = sb.toString();
        log.info("Permalink with length, " + sb.length() + ", created: " + permalink);
        return permalink;
    }

    private static String sanitizePhrase(String phrase, int cutoffLen)
    {
        if(phrase == null) {
            return "";
        }
        phrase = phrase.toLowerCase();  // Use lower case only. ???
        phrase = phrase.replaceAll("[^a-z0-9]", " ");
        phrase = phrase.trim();
        if(phrase.length() == 0) {
            return "";
        }
        
        String[] words = phrase.split("\\s+");
        int totLen = 0;
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<words.length && totLen<=cutoffLen; i++) {
            String word = words[i];
            sb.append(word);
            totLen += word.length();
            if(i<words.length-1 && totLen<=cutoffLen-1) {
                sb.append("-");
            }
        }

        return sb.toString();
    }

    private static String generateRandomString(int len)
    {
        if(sRandom == null) {
            sRandom = new Random(new Date().getTime());
        }
        StringBuffer sb = new StringBuffer();
        for(int i=0; i< len; i++) {
            sb.append(sRandom.nextInt(10));
        }
        return sb.toString();
    }

}
