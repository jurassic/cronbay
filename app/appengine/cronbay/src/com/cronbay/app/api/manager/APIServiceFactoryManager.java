package com.cronbay.app.api.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.app.api.APIServiceFactory;
import com.cronbay.app.api.gae.GaeAPIServiceFactory;
import com.cronbay.app.api.generic.GenericAPIServiceFactory;


// We use Abstract Factory pattern.
// This "manager" class provides a way to choose a concrete factory.
public final class APIServiceFactoryManager
{
    private static final Logger log = Logger.getLogger(APIServiceFactoryManager.class.getName());

    // Prevents instantiation.
    private APIServiceFactoryManager() {}

    // Returns a api service factory.
    public static APIServiceFactory getAPIServiceFactory() 
    {
        // For now, hard-coded.
        // TBD: Read it from a config.
        return GenericAPIServiceFactory.getInstance();
    }

}
