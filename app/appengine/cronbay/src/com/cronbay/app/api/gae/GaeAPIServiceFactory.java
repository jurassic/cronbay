package com.cronbay.app.api.gae;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.app.api.APIServiceFactory;
import com.cronbay.app.api.UrlShortenerAPIService;
import com.cronbay.app.api.TwitterAPIService;


public class GaeAPIServiceFactory extends APIServiceFactory
{
    private static final Logger log = Logger.getLogger(GaeAPIServiceFactory.class.getName());

    private GaeAPIServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class GaeAPIServiceFactoryHolder
    {
        private static final GaeAPIServiceFactory INSTANCE = new GaeAPIServiceFactory();
    }

    // Singleton method
    public static GaeAPIServiceFactory getInstance()
    {
        return GaeAPIServiceFactoryHolder.INSTANCE;
    }


    // API Services

    public UrlShortenerAPIService getUrlShortenerAPIService()
    {
        return GaeUrlShortenerAPIService.getInstance();
    }

    public TwitterAPIService getTwitterAPIService()
    {
        return new GaeTwitterAPIService();
    }


}
