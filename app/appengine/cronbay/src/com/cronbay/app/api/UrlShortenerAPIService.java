package com.cronbay.app.api;

import com.cronbay.ws.BaseException;

// API service.
public abstract class UrlShortenerAPIService
{
    // temporary
    // http://code.google.com/apis/urlshortener/v1/getting_started.html
    protected final String GOOGLE_SHORTENER_ENDPOINT = "https://www.googleapis.com/urlshortener/v1/url";
    protected final String GOOGLE_API_KEY = "AIzaSyD1az3ngKSl3jfdbUxiUYMGyhhX4DQ_W1M";
    // temporary

    // ...
    public abstract void setUrlShortenerService(String serviceName);
    public abstract String createShortUrl(String longUrl) throws BaseException;    
    public abstract String getLongUrl(String shortUrl) throws BaseException;    
    // ...
    
}
