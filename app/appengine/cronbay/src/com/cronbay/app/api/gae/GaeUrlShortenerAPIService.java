package com.cronbay.app.api.gae;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.app.api.UrlShortenerAPIService;
import com.cronbay.app.common.UrlShortenerServiceName;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.platform.gae.GaeAppIdentityPlatformService;
import com.cronbay.ws.platform.gae.GaePlatformServiceFactory;
import com.cronbay.ws.platform.gae.GoogleApiAccessToken;

// GAE API service.
public class GaeUrlShortenerAPIService extends UrlShortenerAPIService
{
    private static final Logger log = Logger.getLogger(GaeUrlShortenerAPIService.class.getName());

    private String serviceName = null;
    
    
    private GaeUrlShortenerAPIService()
    {
    }

    // Initialization-on-demand holder.
    private static class GaeUrlShortenerAPIServiceHolder
    {
        private static final GaeUrlShortenerAPIService INSTANCE = new GaeUrlShortenerAPIService();
    }

    // Singleton method
    public static GaeUrlShortenerAPIService getInstance()
    {
        return GaeUrlShortenerAPIServiceHolder.INSTANCE;
    }

    
    @Override
    public void setUrlShortenerService(String serviceName)
    {
        // TBD...
        if(UrlShortenerServiceName.URL_SHORTENER_GOOGLE.equals(serviceName)) {
            this.serviceName = serviceName;
        } else {
            log.warning("URL shortener service, " + serviceName + ", not supported.");

            // temporary
            this.serviceName = UrlShortenerServiceName.URL_SHORTENER_GOOGLE;
        }
    }
    
    
    // ...
    // TBD: Has not been tested.... 
    // ...
    
    @Override
    public String createShortUrl(String longUrl) throws BaseException
    {
        String shortUrl = null;
        
        // TBD:
        // if(serviceName == "google") ...
        try {
            ArrayList scopes = new ArrayList();
            scopes.add("https://www.googleapis.com/auth/urlshortener");
     
            GaeAppIdentityPlatformService appIdentityPlatformService = (GaeAppIdentityPlatformService) GaePlatformServiceFactory.getInstance().getAppIdentityPlatformService();
            GoogleApiAccessToken accessToken = appIdentityPlatformService.getAccessToken(scopes);

            URL url = new URL("https://www.googleapis.com/urlshortener/v1/url?pp=1");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.addRequestProperty("Content-Type", "application/json");
            connection.addRequestProperty("Authorization", "OAuth " + accessToken.getAccessToken());
            
            String payload = "{\"longUrl\": \"" + longUrl + "\"}";
           OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(payload);
            writer.close();

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream is = connection.getInputStream();

                JsonFactory factory = new JsonFactory(); 
                JsonParser parser = factory.createJsonParser(is);
                parser.setCodec(new ObjectMapper());  // ????
                JsonNode topNode =  parser.readValueAsTree();
                JsonNode idNode = topNode.findValue("id");
                if(idNode != null) {
                    shortUrl = idNode.getTextValue();
                    log.info("createShortUrl(): shortUrl = " + shortUrl + "; longUrl = " + longUrl);
                } else {
                    log.log(Level.WARNING, "createShortUrl(): Invalid response. longUrl = " + longUrl);
                    throw new BaseException("createShortUrl(): Invalid response. longUrl = " + longUrl);
                }
            } else {
                log.log(Level.WARNING, "Web service call failed. responseCode = " + responseCode);
                throw new BaseException("Web service call failed. responseCode = " + responseCode);
            }
        } catch (MalformedURLException e) {
            log.log(Level.WARNING, "", e);
            throw new BaseException(e);
        } catch (ProtocolException e) {
            log.log(Level.WARNING, "", e);
            throw new BaseException(e);
        } catch (IOException e) {
            log.log(Level.WARNING, "", e);
            throw new BaseException(e);
        }
        
        return shortUrl;
    }

    @Override
    public String getLongUrl(String shortUrl) throws BaseException
    {
        // TBD...
        return null;
    }

    // ...

    
}
