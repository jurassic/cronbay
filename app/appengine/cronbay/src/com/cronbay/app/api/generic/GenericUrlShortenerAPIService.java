package com.cronbay.app.api.generic;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.app.api.UrlShortenerAPIService;
import com.cronbay.app.common.UrlShortenerServiceName;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.core.StatusCode;


// Generic API service.
public class GenericUrlShortenerAPIService extends UrlShortenerAPIService
{
    private static final Logger log = Logger.getLogger(GenericUrlShortenerAPIService.class.getName());

    private String serviceName = null;
    
    
    private GenericUrlShortenerAPIService()
    {
    }

    // Initialization-on-demand holder.
    private static class GenericUrlShortenerAPIServiceHolder
    {
        private static final GenericUrlShortenerAPIService INSTANCE = new GenericUrlShortenerAPIService();
    }

    // Singleton method
    public static GenericUrlShortenerAPIService getInstance()
    {
        return GenericUrlShortenerAPIServiceHolder.INSTANCE;
    }

    
    @Override
    public void setUrlShortenerService(String serviceName)
    {
        // TBD...
        if(UrlShortenerServiceName.URL_SHORTENER_GOOGLE.equals(serviceName)) {
            this.serviceName = serviceName;
        } else {
            log.warning("URL shortener service, " + serviceName + ", not supported.");

            // temporary
            this.serviceName = UrlShortenerServiceName.URL_SHORTENER_GOOGLE;
        }
    }

    @Override
    public String createShortUrl(String longUrl) throws BaseException
    {
        String shortUrl = null;
        
        try {
            URL url = new URL(GOOGLE_SHORTENER_ENDPOINT + "?key=" + GOOGLE_API_KEY);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            String payload = "{\"longUrl\": \"" + longUrl + "\"}";
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(payload);
            writer.close();
            
            int statusCode = connection.getResponseCode();
            log.info("statusCode = " + statusCode);

            // TBD:
            if(statusCode == StatusCode.OK) {
                InputStream is = connection.getInputStream();

                JsonFactory factory = new JsonFactory(); 
                JsonParser parser = factory.createJsonParser(is);
                parser.setCodec(new ObjectMapper());  // ????
                JsonNode topNode =  parser.readValueAsTree();
                JsonNode idNode = topNode.findValue("id");
                if(idNode != null) {
                    shortUrl = idNode.getTextValue();
                    log.info("createShortUrl(): shortUrl = " + shortUrl + "; longUrl = " + longUrl);
                } else {
                    log.log(Level.WARNING, "createShortUrl(): Invalid response. longUrl = " + longUrl);
                    throw new BaseException("createShortUrl(): Invalid response. longUrl = " + longUrl);
                }
            } else {
                log.warning("createShortUrl(): Http Post returned statusCode = " + statusCode + "; longUrl = " + longUrl);
                throw new BaseException("createShortUrl(): Http Post returned statusCode = " + statusCode + "; longUrl = " + longUrl);
            }
        } catch (IllegalStateException e) {
            log.log(Level.WARNING, "createShortUrl(): Http Post failed.", e);
            throw new BaseException("createShortUrl(): Http Post failed. longUrl = " + longUrl, e);
        } catch (MalformedURLException e) {
            log.log(Level.WARNING, "createShortUrl(): Http Post failed.", e);
            throw new BaseException("createShortUrl(): Http Post failed. longUrl = " + longUrl, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "createShortUrl(): Http Post failed.", e);
            throw new BaseException("createShortUrl(): Http Post failed. longUrl = " + longUrl, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "createShortUrl(): Http Post failed.", e);
            throw new BaseException("createShortUrl(): Http Post failed. longUrl = " + longUrl, e);
        }

        return shortUrl;
    }

    @Override
    public String getLongUrl(String shortUrl) throws BaseException
    {
        // TBD...
        return null;
    }

    // ...

    
}
