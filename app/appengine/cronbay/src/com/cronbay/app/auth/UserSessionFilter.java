package com.cronbay.app.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cronbay.af.auth.SessionBean;
import com.cronbay.af.auth.UserSessionManager;
import com.cronbay.af.bean.UserBean;
import com.cronbay.app.service.UserAppService;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.core.GUID;


public class UserSessionFilter implements Filter
{
    private static final Logger log = Logger.getLogger(UserSessionFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    
    // ...
    private UserAppService userAppService = null;
    private UserAppService getUserAppService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }

    
    public UserSessionFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
        // TBD: 
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // Add a sessionBean to the user session.
        SessionBean sessionBean = null;
        if(req != null && req instanceof HttpServletRequest) {
            sessionBean = UserSessionManager.getInstance().setSessionBean((HttpServletRequest) req, (HttpServletResponse) res);
            if(sessionBean != null) {
                String sessionId = sessionBean.getGuid();
                String sessionToken = sessionBean.getToken();
                log.info("Session: sessionId = " + sessionId + "; sessionToken = " + sessionToken);
            } else {
                log.warning("Failed to create/retrieve a sessionBean from the current session.");
            }
        } else {
            // This cannot happen.
            log.warning("Failed to create a session due to unknown reasons.");
        }
        
        // TBD: 
        if(sessionBean != null) {
            String userId = sessionBean.getUserId();
            if(userId != null) {
                // Ignore for now...
            } else {
                // Create a user object,
                UserBean userBean = new UserBean();
                userId = GUID.generate();
                userBean.setGuid(userId);
                userBean.setSessionId(sessionBean.getGuid());
                try {
                    userId = getUserAppService().createUser(userBean);  // TBD: Validate the result?
                    sessionBean.setUserId(userId);
                    UserSessionManager.getInstance().refreshSessionBean((HttpServletRequest) req, (HttpServletResponse) res, sessionBean);
                    log.info("New user object created. userId = " + userId);
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Failed to save a user object.", e);
                }
            }
        }

        // Continue through filter chain.
        chain.doFilter(req, res);
    }
    
}
