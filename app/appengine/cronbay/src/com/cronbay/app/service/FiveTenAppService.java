package com.cronbay.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.FiveTen;
import com.cronbay.af.bean.FiveTenBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.FiveTenService;
import com.cronbay.af.service.impl.FiveTenServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class FiveTenAppService extends FiveTenServiceImpl implements FiveTenService
{
    private static final Logger log = Logger.getLogger(FiveTenAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public FiveTenAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // FiveTen related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        return super.getFiveTen(guid);
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        return super.getFiveTen(guid, field);
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        return super.getFiveTens(guids);
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return super.getAllFiveTens();
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllFiveTenKeys(ordering, offset, count);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findFiveTens(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createFiveTen(FiveTen fiveTen) throws BaseException
    {
        return super.createFiveTen(fiveTen);
    }

    @Override
    public FiveTen constructFiveTen(FiveTen fiveTen) throws BaseException
    {
        return super.constructFiveTen(fiveTen);
    }


    @Override
    public Boolean updateFiveTen(FiveTen fiveTen) throws BaseException
    {
        return super.updateFiveTen(fiveTen);
    }
        
    @Override
    public FiveTen refreshFiveTen(FiveTen fiveTen) throws BaseException
    {
        return super.refreshFiveTen(fiveTen);
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        return super.deleteFiveTen(guid);
    }

    @Override
    public Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException
    {
        return super.deleteFiveTen(fiveTen);
    }

    @Override
    public Integer createFiveTens(List<FiveTen> fiveTens) throws BaseException
    {
        return super.createFiveTens(fiveTens);
    }

    // TBD
    //@Override
    //public Boolean updateFiveTens(List<FiveTen> fiveTens) throws BaseException
    //{
    //}

}
