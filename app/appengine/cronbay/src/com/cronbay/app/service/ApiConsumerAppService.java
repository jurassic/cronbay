package com.cronbay.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.ApiConsumer;
import com.cronbay.af.bean.ApiConsumerBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.ApiConsumerService;
import com.cronbay.af.service.impl.ApiConsumerServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ApiConsumerAppService extends ApiConsumerServiceImpl implements ApiConsumerService
{
    private static final Logger log = Logger.getLogger(ApiConsumerAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ApiConsumerAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ApiConsumer related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ApiConsumer getApiConsumer(String guid) throws BaseException
    {
        return super.getApiConsumer(guid);
    }

    @Override
    public Object getApiConsumer(String guid, String field) throws BaseException
    {
        return super.getApiConsumer(guid, field);
    }

    @Override
    public List<ApiConsumer> getApiConsumers(List<String> guids) throws BaseException
    {
        return super.getApiConsumers(guids);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers() throws BaseException
    {
        return super.getAllApiConsumers();
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllApiConsumerKeys(ordering, offset, count);
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return super.createApiConsumer(apiConsumer);
    }

    @Override
    public ApiConsumer constructApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return super.constructApiConsumer(apiConsumer);
    }


    @Override
    public Boolean updateApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return super.updateApiConsumer(apiConsumer);
    }
        
    @Override
    public ApiConsumer refreshApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return super.refreshApiConsumer(apiConsumer);
    }

    @Override
    public Boolean deleteApiConsumer(String guid) throws BaseException
    {
        return super.deleteApiConsumer(guid);
    }

    @Override
    public Boolean deleteApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        return super.deleteApiConsumer(apiConsumer);
    }

    @Override
    public Integer createApiConsumers(List<ApiConsumer> apiConsumers) throws BaseException
    {
        return super.createApiConsumers(apiConsumers);
    }

    // TBD
    //@Override
    //public Boolean updateApiConsumers(List<ApiConsumer> apiConsumers) throws BaseException
    //{
    //}

}
