package com.cronbay.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.af.bean.NotificationStructBean;
import com.cronbay.af.bean.RestRequestStructBean;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.af.bean.ReferrerInfoStructBean;
import com.cronbay.af.bean.CronJobBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.CronJobService;
import com.cronbay.af.service.impl.CronJobServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class CronJobAppService extends CronJobServiceImpl implements CronJobService
{
    private static final Logger log = Logger.getLogger(CronJobAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public CronJobAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // CronJob related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public CronJob getCronJob(String guid) throws BaseException
    {
        return super.getCronJob(guid);
    }

    @Override
    public Object getCronJob(String guid, String field) throws BaseException
    {
        return super.getCronJob(guid, field);
    }

    @Override
    public List<CronJob> getCronJobs(List<String> guids) throws BaseException
    {
        return super.getCronJobs(guids);
    }

    @Override
    public List<CronJob> getAllCronJobs() throws BaseException
    {
        return super.getAllCronJobs();
    }

    @Override
    public List<String> getAllCronJobKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllCronJobKeys(ordering, offset, count);
    }

    @Override
    public List<CronJob> findCronJobs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findCronJobs(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findCronJobKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findCronJobKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createCronJob(CronJob cronJob) throws BaseException
    {
        return super.createCronJob(cronJob);
    }

    @Override
    public CronJob constructCronJob(CronJob cronJob) throws BaseException
    {
        return super.constructCronJob(cronJob);
    }


    @Override
    public Boolean updateCronJob(CronJob cronJob) throws BaseException
    {
        return super.updateCronJob(cronJob);
    }
        
    @Override
    public CronJob refreshCronJob(CronJob cronJob) throws BaseException
    {
        return super.refreshCronJob(cronJob);
    }

    @Override
    public Boolean deleteCronJob(String guid) throws BaseException
    {
        return super.deleteCronJob(guid);
    }

    @Override
    public Boolean deleteCronJob(CronJob cronJob) throws BaseException
    {
        return super.deleteCronJob(cronJob);
    }

    @Override
    public Integer createCronJobs(List<CronJob> cronJobs) throws BaseException
    {
        return super.createCronJobs(cronJobs);
    }

    // TBD
    //@Override
    //public Boolean updateCronJobs(List<CronJob> cronJobs) throws BaseException
    //{
    //}

}
