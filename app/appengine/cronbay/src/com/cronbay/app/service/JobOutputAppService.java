package com.cronbay.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.af.bean.JobOutputBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.JobOutputService;
import com.cronbay.af.service.impl.JobOutputServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class JobOutputAppService extends JobOutputServiceImpl implements JobOutputService
{
    private static final Logger log = Logger.getLogger(JobOutputAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public JobOutputAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // JobOutput related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public JobOutput getJobOutput(String guid) throws BaseException
    {
        return super.getJobOutput(guid);
    }

    @Override
    public Object getJobOutput(String guid, String field) throws BaseException
    {
        return super.getJobOutput(guid, field);
    }

    @Override
    public List<JobOutput> getJobOutputs(List<String> guids) throws BaseException
    {
        return super.getJobOutputs(guids);
    }

    @Override
    public List<JobOutput> getAllJobOutputs() throws BaseException
    {
        return super.getAllJobOutputs();
    }

    @Override
    public List<String> getAllJobOutputKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllJobOutputKeys(ordering, offset, count);
    }

    @Override
    public List<JobOutput> findJobOutputs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findJobOutputs(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findJobOutputKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findJobOutputKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createJobOutput(JobOutput jobOutput) throws BaseException
    {
        return super.createJobOutput(jobOutput);
    }

    @Override
    public JobOutput constructJobOutput(JobOutput jobOutput) throws BaseException
    {
        return super.constructJobOutput(jobOutput);
    }


    @Override
    public Boolean updateJobOutput(JobOutput jobOutput) throws BaseException
    {
        return super.updateJobOutput(jobOutput);
    }
        
    @Override
    public JobOutput refreshJobOutput(JobOutput jobOutput) throws BaseException
    {
        return super.refreshJobOutput(jobOutput);
    }

    @Override
    public Boolean deleteJobOutput(String guid) throws BaseException
    {
        return super.deleteJobOutput(guid);
    }

    @Override
    public Boolean deleteJobOutput(JobOutput jobOutput) throws BaseException
    {
        return super.deleteJobOutput(jobOutput);
    }

    @Override
    public Integer createJobOutputs(List<JobOutput> jobOutputs) throws BaseException
    {
        return super.createJobOutputs(jobOutputs);
    }

    // TBD
    //@Override
    //public Boolean updateJobOutputs(List<JobOutput> jobOutputs) throws BaseException
    //{
    //}

}
