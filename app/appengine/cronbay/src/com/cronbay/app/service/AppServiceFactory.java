package com.cronbay.app.service;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.service.AbstractServiceFactory;
import com.cronbay.af.service.ApiConsumerService;
import com.cronbay.af.service.UserService;
import com.cronbay.af.service.CronJobService;
import com.cronbay.af.service.JobOutputService;
import com.cronbay.af.service.ServiceInfoService;
import com.cronbay.af.service.FiveTenService;

public class AppServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(AppServiceFactory.class.getName());

    private AppServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AppServiceFactoryHolder
    {
        private static final AppServiceFactory INSTANCE = new AppServiceFactory();
    }

    // Singleton method
    public static AppServiceFactory getInstance()
    {
        return AppServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerAppService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserAppService();
    }

    @Override
    public CronJobService getCronJobService()
    {
        return new CronJobAppService();
    }

    @Override
    public JobOutputService getJobOutputService()
    {
        return new JobOutputAppService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoAppService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenAppService();
    }


}
