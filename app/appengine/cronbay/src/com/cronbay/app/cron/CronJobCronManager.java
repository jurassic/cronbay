package com.cronbay.app.cron;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.app.job.JobProcessor;
import com.cronbay.app.helper.CronJobHelper;
import com.cronbay.app.helper.JobOutputHelper;
import com.cronbay.app.service.CronJobAppService;
import com.cronbay.app.service.JobOutputAppService;
import com.cronbay.app.service.UserAppService;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.JobOutput;


public class CronJobCronManager
{
    private static final Logger log = Logger.getLogger(CronJobCronManager.class.getName());   

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private CronJobAppService cronJobAppService = null;
    private JobOutputAppService jobOutputAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private CronJobAppService getCronJobService()
    {
        if(cronJobAppService == null) {
            cronJobAppService = new CronJobAppService();
        }
        return cronJobAppService;
    }
    private JobOutputAppService getJobOutputService()
    {
        if(jobOutputAppService == null) {
            jobOutputAppService = new JobOutputAppService();
        }
        return jobOutputAppService;
    }
    
        // etc. ...

    
    private CronJobCronManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class CronJobCronManagerHolder
    {
        private static final CronJobCronManager INSTANCE = new CronJobCronManager();
    }

    // Singleton method
    public static CronJobCronManager getInstance()
    {
        return CronJobCronManagerHolder.INSTANCE;
    }

    private void init()
    {
        // TBD: ...
    }
    
    
    
    public int processPageRefresh()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = JobOutputHelper.getInstance().getJobOutputKeysForRefreshProcessing(maxCount);
        return processJobOutputs(keys);
    }

    private int processJobOutputs(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    JobOutput jobOutput = JobOutputHelper.getInstance().getJobOutput(key);
                    log.fine("JobOutput retrieved for processing: jobOutput = " + jobOutput);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    jobOutput = JobProcessor.getInstance().processPageFetch(jobOutput);
                    log.fine("JobOutput processed: jobOutput = " + jobOutput);
                   
                    // ????
                    // ...
                    Boolean suc = getJobOutputService().updateJobOutput(jobOutput);
                    log.info("Fetch request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated job record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
    
    
    
    // TBD:
    
    public int processPageFetch()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = CronJobHelper.getInstance().getCronJobKeysForProcessing(maxCount);
        return processCronJobs(keys);
    }
    
    private int processCronJobs(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    CronJob cronJob = CronJobHelper.getInstance().getCronJob(key);
                    log.fine("CronJob retrieved for processing: cronJob = " + cronJob);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    cronJob = JobProcessor.getInstance().processPageFetch(cronJob);
                    log.fine("CronJob processed: cronJob = " + cronJob);
                    
                    // ????
                    // ...
                    // TBD:
                    // Create or update jobOutput as well...
                    // ...
                    
                    Boolean suc = getCronJobService().updateCronJob(cronJob);
                    log.info("Fetch request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated job record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
    

}
