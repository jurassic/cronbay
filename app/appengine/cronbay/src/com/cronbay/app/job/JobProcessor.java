package com.cronbay.app.job;

import java.util.logging.Logger;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.exception.BadRequestException;


// Note: HTMLUnit API.
// http://htmlunit.sourceforge.net/apidocs/index.html
public class JobProcessor
{
    private static final Logger log = Logger.getLogger(JobProcessor.class.getName());   
    
    // temporary
    private static final long REFRESH_INTERVAL = 3600000 * 24 * 7;   // a week

    // Max entity size on GAE data store/memcache is 1M.  (1 char == 2 bytes)
    private static final int MAX_OUTPUT_LENGTH = 450000;  // ???

    
    private JobProcessor()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class JobProcessorHolder
    {
        private static final JobProcessor INSTANCE = new JobProcessor();
    }

    // Singleton method
    public static JobProcessor getInstance()
    {
        return JobProcessorHolder.INSTANCE;
    }

    private void init()
    {
        // TBD:
        // ...
    }


    
    // TBD
    // refresh meta fails for htmlunit on gae...
    // e.g., <meta http-equiv="refresh" content="1800;url=http://www.cnn.com/?refresh=1"/>
    // ...
    // Error message: Refresh to http://www.cnn.com/?refresh=1 (1800s) aborted by HtmlUnit: Attempted to refresh a page using an ImmediateRefreshHandler which could have caused an OutOfMemoryError Please use WaitingRefreshHandler or ThreadedRefreshHandler instead.
    // ...
    

    public JobOutput processPageFetch(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN: processPageFetch() called with jobOutput = " + jobOutput);
        if(jobOutput == null) {
            // ????
            log.warning("jobOutput is null.");
            throw new BadRequestException("jobOutput is null.");
        }

//        // TargetUrl/input cannot be null
//        String targetUrl =  jobOutput.getTargetUrl();
//        // ...
//                
//        //final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_3_6);  // ???
//        final WebClient webClient = new WebClient();
//        webClient.setCssEnabled(false);               // So many CSS/JS related errors on GAE.
//        webClient.setJavaScriptEnabled(false);        // So many CSS/JS related errors on GAE.
//        webClient.setTimeout(30000);                  // Does this work on GAE????
//        webClient.setRedirectEnabled(true);           // temporary.... Use redirect request field... ???
//        
//        // ????
//        //webClient.setRefreshHandler(new WaitingRefreshHandler(1));  // ???
//        // Same page refresh (as opposed to the refresh used for redirect) normally does not terminate.
//        // There is really no good solution other than disabling it....
//        webClient.setRefreshHandler(new RefreshHandler() {
//            @Override
//            public void handleRefresh(Page page, URL url, int seconds) throws IOException
//            {
//                // Do nothing.
//            }
//        });
//        
//        try {
//            // Starting processing...
//            long now = System.currentTimeMillis();
//            ((JobOutputBean) jobOutput).setLastCheckedTime(now);
//            ((JobOutputBean) jobOutput).setLastCronJobResult(CronJobResult.RES_UNKNOWN);  // ???
//            //((JobOutputBean) jobOutput).setRefreshStatus(RefreshStatus.STATUS_PROCESSING);  // ???
//            log.finer("Before calling getPage()");
//            // TBD: text pages??? xml pages???
//            final HtmlPage targetPage = webClient.getPage(targetUrl);
//            //final HtmlPage targetPage = webClient.getPage(new URL(targetUrl));  // ???
//            log.finer("After calling getPage()");
//            
//            if(targetPage != null) {
//                final String pageTitle = targetPage.getTitleText();
//                log.info("pageTitle = " + pageTitle);
//                ((JobOutputBean) jobOutput).setPageTitle(pageTitle);
//    
//                @SuppressWarnings("unchecked")
//                List<HtmlMeta> metaDescs = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='description']");
//                if(metaDescs != null && !metaDescs.isEmpty()) {
//                    String pageDescription = metaDescs.get(0).getAttribute("content");
//                    if(pageDescription != null && !pageDescription.isEmpty()) {
//                        ((JobOutputBean) jobOutput).setPageDescription(pageDescription.trim());  // trim ???
//                    }
//                }
//                
//                @SuppressWarnings("unchecked")
//                List<HtmlMeta> metaAuthors = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='author']");
//                if(metaAuthors != null && !metaAuthors.isEmpty()) {
//                    String pageAuthor = metaAuthors.get(0).getAttribute("content");
//                    if(pageAuthor != null && !pageAuthor.isEmpty()) {
//                        ((JobOutputBean) jobOutput).setPageAuthor(pageAuthor.trim());  // trim ???
//                    }
//                }
//   
//                ((JobOutputBean) jobOutput).setLastCronJobResult(CronJobResult.RES_SUCCESS);
//                ((JobOutputBean) jobOutput).setLastUpdatedTime(now);  // ???
//                // etc...
//                
//            } else {
//                // TBD ....
//                ((JobOutputBean) jobOutput).setLastCronJobResult(CronJobResult.RES_FAILURE);
//                // etc...
//                // What to do???
//                // ????
//            }
//
//            // TBD:
//            // Update these only if RefreshStatus != STATUS_NOREFRESH???
//            // ...
//            ((JobOutputBean) jobOutput).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
//            ((JobOutputBean) jobOutput).setNextRefreshTime(now + REFRESH_INTERVAL);
//            // ...
//
//        } catch (MalformedURLException e) {
//            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
//        } catch (FailingHttpStatusCodeException e) {
//            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
//        } catch (IOException e) {
//            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
//        } catch (Exception e) {
//            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
//        } finally{
//            // ???
//            try {
//                webClient.closeAllWindows();
//            } catch (Exception e) {
//                // ????
//                //throw new BaseException("Error while closing HTMLUnit WebClient.", e);
//                log.log(Level.WARNING, "Error while closing HTMLUnit WebClient.", e);  // ignore...
//            }
//        }
        
        log.finer("END: processPageFetch()");
        return jobOutput;
    }
    
    
    
    
    // TBD
    
    public CronJob processPageFetch(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN: processPageFetch() called with cronJob = " + cronJob);
        if(cronJob == null) {
            // ????
            log.warning("cronJob is null.");
            throw new BadRequestException("cronJob is null.");
        }

        
        // TBD
        // ...
        
        
//        // TargetUrl/input cannot be null
//        String targetUrl =  cronJob.getTargetUrl();
//        // ...
//        
//        
//        //final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_3_6);  // ???
//        final WebClient webClient = new WebClient();
//        webClient.setCssEnabled(false);               // So many CSS/JS related errors on GAE.
//        webClient.setJavaScriptEnabled(false);        // So many CSS/JS related errors on GAE.
//        webClient.setTimeout(30000);                  // Does this work on GAE????
//        try {
//            // Starting processing...
//            ((CronJobBean) cronJob).setCronJobStatus(CronJobStatus.STATUS_PROCESSING);
//            // etc....
//
//            log.finer("Before calling getPage()");
//            final HtmlPage targetPage = webClient.getPage(targetUrl);
//            log.finer("After calling getPage()");
//            
//            final String pageTitle = targetPage.getTitleText();
////            log.info("pageTitle = " + pageTitle);
////            ((CronJobBean) cronJob).setPageTitle(pageTitle);
//
//            @SuppressWarnings("unchecked")
//            List<HtmlMeta> metaDescs = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='description']");
//            if(metaDescs != null && !metaDescs.isEmpty()) {
//                String pageDescription = metaDescs.get(0).getAttribute("content");
//                if(pageDescription != null && !pageDescription.isEmpty()) {
////                    ((CronJobBean) cronJob).setPageDescription(pageDescription.trim());  // trim ???
//                }
//            }
//            
//            // TBD
//            // Update
//            // CronJob
//            // JobOutput
//            // ...
//            
//            // TBD ....
//            // ...
//            
//        } catch (MalformedURLException e) {
//            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
//        } catch (FailingHttpStatusCodeException e) {
//            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
//        } catch (IOException e) {
//            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
//        } catch (Exception e) {
//            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
//        } finally{
//            // ???
//            try {
//                webClient.closeAllWindows();
//            } catch (Exception e) {
//                // ????
//                //throw new BaseException("Error while closing HTMLUnit WebClient.", e);
//                log.log(Level.WARNING, "Error while closing HTMLUnit WebClient.", e);  // ignore...
//            }
//        }
        
        log.finer("END: processPageFetch()");
        return cronJob;
    }
    
    

}
