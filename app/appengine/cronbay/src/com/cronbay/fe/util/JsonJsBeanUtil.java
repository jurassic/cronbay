package com.cronbay.fe.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.fe.bean.ReferrerInfoStructJsBean;
import com.cronbay.fe.bean.GaeAppStructJsBean;
import com.cronbay.fe.bean.GaeUserStructJsBean;
import com.cronbay.fe.bean.NotificationStructJsBean;
import com.cronbay.fe.bean.PagerStateStructJsBean;
import com.cronbay.fe.bean.ApiConsumerJsBean;
import com.cronbay.fe.bean.UserJsBean;
import com.cronbay.fe.bean.CronScheduleStructJsBean;
import com.cronbay.fe.bean.RestRequestStructJsBean;
import com.cronbay.fe.bean.CronJobJsBean;
import com.cronbay.fe.bean.JobOutputJsBean;
import com.cronbay.fe.bean.ServiceInfoJsBean;
import com.cronbay.fe.bean.FiveTenJsBean;


// Utility functions for combining/decomposing into json string segments.
public class JsonJsBeanUtil
{
    private static final Logger log = Logger.getLogger(JsonJsBeanUtil.class.getName());

    private JsonJsBeanUtil() {}

    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> objJsonString
    public static Map<String, String> parseJsonObjectMapString(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, String> jsonObjectMap = new HashMap<String, String>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            ObjectMapper om = new ObjectMapper();  // ????
            factory.setCodec(om);
            JsonParser parser = factory.createJsonParser(jsonStr);

            JsonNode topNode = parser.readValueAsTree();
            Iterator<String> fieldNames = topNode.getFieldNames();
            
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                String value = topNode.get(name).toString();
                jsonObjectMap.put(name, value);
                log.info("jsonObjectMap: name = " + name + "; value = " + value);
            }
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> objJsonString
    public static String generateJsonObjectMapString(Map<String, String> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for(String key : jsonObjectMap.keySet()) {
            String json = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(json == null) {
                sb.append("null");  // ????
            } else {
                sb.append(json);
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        log.info("generateJsonObjectMapString(): jsonStr = " + jsonStr);

        return jsonStr;
    }


    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> jsBean
    public static Map<String, Object> parseJsonObjectMap(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, Object> jsonObjectMap = new HashMap<String, Object>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            ObjectMapper om = new ObjectMapper();  // ????
            factory.setCodec(om);
            JsonParser parser = factory.createJsonParser(jsonStr);
            JsonNode topNode = parser.readValueAsTree();

/*
            // TBD: Remove the loop.
            Iterator<String> fieldNames = topNode.getFieldNames();
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                String value = topNode.get(name).toString();
                log.info("jsonObjectMap: name = " + name + "; value = " + value);

                if(name.equals("referrerInfoStruct")) {
                    ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeAppStruct")) {
                    GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeUserStruct")) {
                    GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("notificationStruct")) {
                    NotificationStructJsBean bean = NotificationStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("pagerStateStruct")) {
                    PagerStateStructJsBean bean = PagerStateStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("apiConsumer")) {
                    ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("user")) {
                    UserJsBean bean = UserJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("cronScheduleStruct")) {
                    CronScheduleStructJsBean bean = CronScheduleStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("restRequestStruct")) {
                    RestRequestStructJsBean bean = RestRequestStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("cronJob")) {
                    CronJobJsBean bean = CronJobJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("jobOutput")) {
                    JobOutputJsBean bean = JobOutputJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("serviceInfo")) {
                    ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fiveTen")) {
                    FiveTenJsBean bean = FiveTenJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
            }
*/

            JsonNode objNode = null;
            String objValueStr = null;
            objNode = topNode.findValue("referrerInfoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("referrerInfoStruct", bean);               
            }
            objNode = topNode.findValue("gaeAppStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeAppStruct", bean);               
            }
            objNode = topNode.findValue("gaeUserStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeUserStruct", bean);               
            }
            objNode = topNode.findValue("notificationStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                NotificationStructJsBean bean = NotificationStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("notificationStruct", bean);               
            }
            objNode = topNode.findValue("pagerStateStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                PagerStateStructJsBean bean = PagerStateStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("pagerStateStruct", bean);               
            }
            objNode = topNode.findValue("apiConsumer");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("apiConsumer", bean);               
            }
            objNode = topNode.findValue("user");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserJsBean bean = UserJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("user", bean);               
            }
            objNode = topNode.findValue("cronScheduleStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                CronScheduleStructJsBean bean = CronScheduleStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("cronScheduleStruct", bean);               
            }
            objNode = topNode.findValue("restRequestStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                RestRequestStructJsBean bean = RestRequestStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("restRequestStruct", bean);               
            }
            objNode = topNode.findValue("cronJob");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                CronJobJsBean bean = CronJobJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("cronJob", bean);               
            }
            objNode = topNode.findValue("jobOutput");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                JobOutputJsBean bean = JobOutputJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("jobOutput", bean);               
            }
            objNode = topNode.findValue("serviceInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("serviceInfo", bean);               
            }
            objNode = topNode.findValue("fiveTen");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FiveTenJsBean bean = FiveTenJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fiveTen", bean);               
            }
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> jsBean
    public static String generateJsonObjectMap(Map<String, Object> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        // TBD: Remove the loop.
        for(String key : jsonObjectMap.keySet()) {
            Object jsonObj = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(jsonObj == null) {
                sb.append("null");  // ????
            } else {
                if(jsonObj instanceof ReferrerInfoStructJsBean) {
                    sb.append(((ReferrerInfoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeAppStructJsBean) {
                    sb.append(((GaeAppStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeUserStructJsBean) {
                    sb.append(((GaeUserStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof NotificationStructJsBean) {
                    sb.append(((NotificationStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof PagerStateStructJsBean) {
                    sb.append(((PagerStateStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ApiConsumerJsBean) {
                    sb.append(((ApiConsumerJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserJsBean) {
                    sb.append(((UserJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof CronScheduleStructJsBean) {
                    sb.append(((CronScheduleStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof RestRequestStructJsBean) {
                    sb.append(((RestRequestStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof CronJobJsBean) {
                    sb.append(((CronJobJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof JobOutputJsBean) {
                    sb.append(((JobOutputJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ServiceInfoJsBean) {
                    sb.append(((ServiceInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FiveTenJsBean) {
                    sb.append(((FiveTenJsBean) jsonObj).toJsonString());
                }
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        log.info("generateJsonObjectMap(): jsonStr = " + jsonStr);

        return jsonStr;
    }
    
    
}
