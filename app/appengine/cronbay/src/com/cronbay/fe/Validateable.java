package com.cronbay.fe;

import java.util.List;


// To be implemented by "FormBeans"...
public interface Validateable
{
    // To indicate the global/bean-wide (as opposed to specific to a particular field) errors...
    static final String FIELD_BEANWIDE = "_beanwide_";

    // Returns the true if the bean does not have validation errors.
    // Note that this method has side effect: The errors are "saved" within the bean.
    boolean validate();

    // Error-related functions 
    boolean hasErrors(); 
    boolean hasErrors(String f); 
    String getLastError(String f); 
    List<String> getErrors(String f); 
    List<String> addError(String f, String error); 
    void setError(String f, String error); 
    List<String> addErrors(String f, List<String> errors); 
    void setErrors(String f, List<String> errors); 
    void resetErrors();
    void resetErrors(String f);
}
