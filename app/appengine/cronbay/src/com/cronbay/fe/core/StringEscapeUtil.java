package com.cronbay.fe.core;

import java.io.StringWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


// Utility functions for escaping strings for Java and JavaScript.
// Note: Copied from org.apache.commons.lang.StringEscapeUtils
public class StringEscapeUtil
{
    private static final Logger log = Logger.getLogger(StringEscapeUtil.class.getName());

    private StringEscapeUtil() {}

    public static String escapeForJavascript(String javaStr)
    {
        return escape(javaStr, true, true);
    }

    public static String escapeForJava(String jsStr)
    {
        return escape(jsStr, true, true);
    }

    private static String escape(String str, boolean escapeSingleQuote, boolean escapeForwardSlash)
    {
        if (str == null) {
            return null;
        }

        String outStr = null;
        try {
            StringWriter out = new StringWriter(str.length() * 2);

            int sz;
            sz = str.length();
            for (int i = 0; i < sz; i++) {
                char ch = str.charAt(i);
   
                // handle unicode
                if (ch > 0xfff) {
                    out.write("\\u" + hex(ch));
                } else if (ch > 0xff) {
                    out.write("\\u0" + hex(ch));
                } else if (ch > 0x7f) {
                    out.write("\\u00" + hex(ch));
                } else if (ch < 32) {
                    switch (ch) {
                    case '\b' :
                        out.write('\\');
                        out.write('b');
                        break;
                    case '\n' :
                        out.write('\\');
                        out.write('n');
                        break;
                    case '\t' :
                        out.write('\\');
                        out.write('t');
                        break;
                    case '\f' :
                        out.write('\\');
                        out.write('f');
                        break;
                    case '\r' :
                        out.write('\\');
                        out.write('r');
                        break;
                    default :
                        if (ch > 0xf) {
                            out.write("\\u00" + hex(ch));
                        } else {
                            out.write("\\u000" + hex(ch));
                        }
                        break;
                    }
                } else {
                    switch (ch) {
                    case '\'' :
                        if (escapeSingleQuote) {
                            out.write('\\');
                        }
                        out.write('\'');
                        break;
                    case '"' :
                        out.write('\\');
                        out.write('"');
                        break;
                    case '\\' :
                        out.write('\\');
                        out.write('\\');
                        break;
                    case '/' :
                        if (escapeForwardSlash) {
                            out.write('\\');
                        }
                        out.write('/');
                        break;
                    default :
                        out.write(ch);
                        break;
                    }
                }
            }

            outStr = out.toString();
        } catch (Exception e) {
            // This should not happen. Ignore
            log.log(Level.WARNING, "String ecape failed.", e);
        }
        
        return outStr;
    }

    private static String hex(char ch)
    {
         return Integer.toHexString(ch).toUpperCase(Locale.ENGLISH);
    }
  
}
