package com.cronbay.fe;

import java.io.Serializable;


public class WebException extends Exception implements Serializable
{
    private static final long serialVersionUID = 1L;

    public WebException() 
    {
        super();
    }
    public WebException(String message) 
    {
        super(message);
    }
    public WebException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public WebException(Throwable cause) 
    {
        super(cause);
    }

}