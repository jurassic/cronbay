package com.cronbay.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.fe.core.StringEscapeUtil;


public class CronScheduleStructJsBean implements Serializable, Cloneable  //, CronScheduleStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CronScheduleStructJsBean.class.getName());

    private String type;
    private String schedule;
    private String timezone;
    private Integer maxIterations;
    private Integer repeatInterval;
    private Long firtRunTime;
    private Long startTime;
    private Long endTime;
    private String note;

    // Ctors.
    public CronScheduleStructJsBean()
    {
        //this((String) null);
    }
    public CronScheduleStructJsBean(String type, String schedule, String timezone, Integer maxIterations, Integer repeatInterval, Long firtRunTime, Long startTime, Long endTime, String note)
    {
        this.type = type;
        this.schedule = schedule;
        this.timezone = timezone;
        this.maxIterations = maxIterations;
        this.repeatInterval = repeatInterval;
        this.firtRunTime = firtRunTime;
        this.startTime = startTime;
        this.endTime = endTime;
        this.note = note;
    }
    public CronScheduleStructJsBean(CronScheduleStructJsBean bean)
    {
        if(bean != null) {
            setType(bean.getType());   
            setSchedule(bean.getSchedule());   
            setTimezone(bean.getTimezone());   
            setMaxIterations(bean.getMaxIterations());   
            setRepeatInterval(bean.getRepeatInterval());   
            setFirtRunTime(bean.getFirtRunTime());   
            setStartTime(bean.getStartTime());   
            setEndTime(bean.getEndTime());   
            setNote(bean.getNote());   
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static CronScheduleStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        CronScheduleStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(CronScheduleStructJsBean.class);

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            bean = mapper.readValue(jsonStr, CronScheduleStructJsBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getSchedule()
    {
        return this.schedule;
    }
    public void setSchedule(String schedule)
    {
        this.schedule = schedule;
    }

    public String getTimezone()
    {
        return this.timezone;
    }
    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }

    public Integer getMaxIterations()
    {
        return this.maxIterations;
    }
    public void setMaxIterations(Integer maxIterations)
    {
        this.maxIterations = maxIterations;
    }

    public Integer getRepeatInterval()
    {
        return this.repeatInterval;
    }
    public void setRepeatInterval(Integer repeatInterval)
    {
        this.repeatInterval = repeatInterval;
    }

    public Long getFirtRunTime()
    {
        return this.firtRunTime;
    }
    public void setFirtRunTime(Long firtRunTime)
    {
        this.firtRunTime = firtRunTime;
    }

    public Long getStartTime()
    {
        return this.startTime;
    }
    public void setStartTime(Long startTime)
    {
        this.startTime = startTime;
    }

    public Long getEndTime()
    {
        return this.endTime;
    }
    public void setEndTime(Long endTime)
    {
        this.endTime = endTime;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("type:null, ");
        sb.append("schedule:null, ");
        sb.append("timezone:null, ");
        sb.append("maxIterations:0, ");
        sb.append("repeatInterval:0, ");
        sb.append("firtRunTime:0, ");
        sb.append("startTime:0, ");
        sb.append("endTime:0, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("type:");
        if(this.getType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getType()).append("\", ");
        }
        sb.append("schedule:");
        if(this.getSchedule() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSchedule()).append("\", ");
        }
        sb.append("timezone:");
        if(this.getTimezone() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTimezone()).append("\", ");
        }
        sb.append("maxIterations:" + this.getMaxIterations()).append(", ");
        sb.append("repeatInterval:" + this.getRepeatInterval()).append(", ");
        sb.append("firtRunTime:" + this.getFirtRunTime()).append(", ");
        sb.append("startTime:" + this.getStartTime()).append(", ");
        sb.append("endTime:" + this.getEndTime()).append(", ");
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getType() != null) {
            sb.append("\"type\":").append("\"").append(this.getType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"type\":").append("null, ");
        }
        if(this.getSchedule() != null) {
            sb.append("\"schedule\":").append("\"").append(this.getSchedule()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"schedule\":").append("null, ");
        }
        if(this.getTimezone() != null) {
            sb.append("\"timezone\":").append("\"").append(this.getTimezone()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"timezone\":").append("null, ");
        }
        if(this.getMaxIterations() != null) {
            sb.append("\"maxIterations\":").append("").append(this.getMaxIterations()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"maxIterations\":").append("null, ");
        }
        if(this.getRepeatInterval() != null) {
            sb.append("\"repeatInterval\":").append("").append(this.getRepeatInterval()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"repeatInterval\":").append("null, ");
        }
        if(this.getFirtRunTime() != null) {
            sb.append("\"firtRunTime\":").append("").append(this.getFirtRunTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"firtRunTime\":").append("null, ");
        }
        if(this.getStartTime() != null) {
            sb.append("\"startTime\":").append("").append(this.getStartTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"startTime\":").append("null, ");
        }
        if(this.getEndTime() != null) {
            sb.append("\"endTime\":").append("").append(this.getEndTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"endTime\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("type = " + this.type).append(";");
        sb.append("schedule = " + this.schedule).append(";");
        sb.append("timezone = " + this.timezone).append(";");
        sb.append("maxIterations = " + this.maxIterations).append(";");
        sb.append("repeatInterval = " + this.repeatInterval).append(";");
        sb.append("firtRunTime = " + this.firtRunTime).append(";");
        sb.append("startTime = " + this.startTime).append(";");
        sb.append("endTime = " + this.endTime).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        CronScheduleStructJsBean cloned = new CronScheduleStructJsBean();
        cloned.setType(this.getType());   
        cloned.setSchedule(this.getSchedule());   
        cloned.setTimezone(this.getTimezone());   
        cloned.setMaxIterations(this.getMaxIterations());   
        cloned.setRepeatInterval(this.getRepeatInterval());   
        cloned.setFirtRunTime(this.getFirtRunTime());   
        cloned.setStartTime(this.getStartTime());   
        cloned.setEndTime(this.getEndTime());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
