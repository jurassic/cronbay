package com.cronbay.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.fe.core.StringEscapeUtil;


public class FiveTenJsBean implements Serializable, Cloneable  //, FiveTen
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FiveTenJsBean.class.getName());

    private String guid;
    private Integer counter;
    private String requesterIpAddress;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FiveTenJsBean()
    {
        //this((String) null);
    }
    public FiveTenJsBean(String guid)
    {
        this(guid, null, null, null, null);
    }
    public FiveTenJsBean(String guid, Integer counter, String requesterIpAddress)
    {
        this(guid, counter, requesterIpAddress, null, null);
    }
    public FiveTenJsBean(String guid, Integer counter, String requesterIpAddress, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.counter = counter;
        this.requesterIpAddress = requesterIpAddress;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FiveTenJsBean(FiveTenJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());   
            setCounter(bean.getCounter());   
            setRequesterIpAddress(bean.getRequesterIpAddress());   
            setCreatedTime(bean.getCreatedTime());   
            setModifiedTime(bean.getModifiedTime());   
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static FiveTenJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        FiveTenJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(FiveTenJsBean.class);

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            bean = mapper.readValue(jsonStr, FiveTenJsBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public Integer getCounter()
    {
        return this.counter;
    }
    public void setCounter(Integer counter)
    {
        this.counter = counter;
    }

    public String getRequesterIpAddress()
    {
        return this.requesterIpAddress;
    }
    public void setRequesterIpAddress(String requesterIpAddress)
    {
        this.requesterIpAddress = requesterIpAddress;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("counter:0, ");
        sb.append("requesterIpAddress:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("counter:" + this.getCounter()).append(", ");
        sb.append("requesterIpAddress:");
        if(this.getRequesterIpAddress() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRequesterIpAddress()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getCounter() != null) {
            sb.append("\"counter\":").append("").append(this.getCounter()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"counter\":").append("null, ");
        }
        if(this.getRequesterIpAddress() != null) {
            sb.append("\"requesterIpAddress\":").append("\"").append(this.getRequesterIpAddress()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"requesterIpAddress\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("counter = " + this.counter).append(";");
        sb.append("requesterIpAddress = " + this.requesterIpAddress).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        FiveTenJsBean cloned = new FiveTenJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setCounter(this.getCounter());   
        cloned.setRequesterIpAddress(this.getRequesterIpAddress());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
