package com.cronbay.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.fe.core.StringEscapeUtil;


public class PagerStateStructJsBean implements Serializable, Cloneable  //, PagerStateStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PagerStateStructJsBean.class.getName());

    private String pagerMode;
    private String primaryOrdering;
    private String secondaryOrdering;
    private Long currentOffset;
    private Long currentPage;
    private Integer pageSize;
    private Long totalCount;
    private Long lowerBoundTotalCount;
    private Long previousPageOffset;
    private Long nextPageOffset;
    private Long lastPageOffset;
    private Long lastPageIndex;
    private Boolean firstActionEnabled;
    private Boolean previousActionEnabled;
    private Boolean nextActionEnabled;
    private Boolean lastActionEnabled;
    private String note;

    // Ctors.
    public PagerStateStructJsBean()
    {
        //this((String) null);
    }
    public PagerStateStructJsBean(String pagerMode, String primaryOrdering, String secondaryOrdering, Long currentOffset, Long currentPage, Integer pageSize, Long totalCount, Long lowerBoundTotalCount, Long previousPageOffset, Long nextPageOffset, Long lastPageOffset, Long lastPageIndex, Boolean firstActionEnabled, Boolean previousActionEnabled, Boolean nextActionEnabled, Boolean lastActionEnabled, String note)
    {
        this.pagerMode = pagerMode;
        this.primaryOrdering = primaryOrdering;
        this.secondaryOrdering = secondaryOrdering;
        this.currentOffset = currentOffset;
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.lowerBoundTotalCount = lowerBoundTotalCount;
        this.previousPageOffset = previousPageOffset;
        this.nextPageOffset = nextPageOffset;
        this.lastPageOffset = lastPageOffset;
        this.lastPageIndex = lastPageIndex;
        this.firstActionEnabled = firstActionEnabled;
        this.previousActionEnabled = previousActionEnabled;
        this.nextActionEnabled = nextActionEnabled;
        this.lastActionEnabled = lastActionEnabled;
        this.note = note;
    }
    public PagerStateStructJsBean(PagerStateStructJsBean bean)
    {
        if(bean != null) {
            setPagerMode(bean.getPagerMode());   
            setPrimaryOrdering(bean.getPrimaryOrdering());   
            setSecondaryOrdering(bean.getSecondaryOrdering());   
            setCurrentOffset(bean.getCurrentOffset());   
            setCurrentPage(bean.getCurrentPage());   
            setPageSize(bean.getPageSize());   
            setTotalCount(bean.getTotalCount());   
            setLowerBoundTotalCount(bean.getLowerBoundTotalCount());   
            setPreviousPageOffset(bean.getPreviousPageOffset());   
            setNextPageOffset(bean.getNextPageOffset());   
            setLastPageOffset(bean.getLastPageOffset());   
            setLastPageIndex(bean.getLastPageIndex());   
            setFirstActionEnabled(bean.isFirstActionEnabled());   
            setPreviousActionEnabled(bean.isPreviousActionEnabled());   
            setNextActionEnabled(bean.isNextActionEnabled());   
            setLastActionEnabled(bean.isLastActionEnabled());   
            setNote(bean.getNote());   
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static PagerStateStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        PagerStateStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(PagerStateStructJsBean.class);

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            bean = mapper.readValue(jsonStr, PagerStateStructJsBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getPagerMode()
    {
        return this.pagerMode;
    }
    public void setPagerMode(String pagerMode)
    {
        this.pagerMode = pagerMode;
    }

    public String getPrimaryOrdering()
    {
        return this.primaryOrdering;
    }
    public void setPrimaryOrdering(String primaryOrdering)
    {
        this.primaryOrdering = primaryOrdering;
    }

    public String getSecondaryOrdering()
    {
        return this.secondaryOrdering;
    }
    public void setSecondaryOrdering(String secondaryOrdering)
    {
        this.secondaryOrdering = secondaryOrdering;
    }

    public Long getCurrentOffset()
    {
        return this.currentOffset;
    }
    public void setCurrentOffset(Long currentOffset)
    {
        this.currentOffset = currentOffset;
    }

    public Long getCurrentPage()
    {
        return this.currentPage;
    }
    public void setCurrentPage(Long currentPage)
    {
        this.currentPage = currentPage;
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }
    public void setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;
    }

    public Long getTotalCount()
    {
        return this.totalCount;
    }
    public void setTotalCount(Long totalCount)
    {
        this.totalCount = totalCount;
    }

    public Long getLowerBoundTotalCount()
    {
        return this.lowerBoundTotalCount;
    }
    public void setLowerBoundTotalCount(Long lowerBoundTotalCount)
    {
        this.lowerBoundTotalCount = lowerBoundTotalCount;
    }

    public Long getPreviousPageOffset()
    {
        return this.previousPageOffset;
    }
    public void setPreviousPageOffset(Long previousPageOffset)
    {
        this.previousPageOffset = previousPageOffset;
    }

    public Long getNextPageOffset()
    {
        return this.nextPageOffset;
    }
    public void setNextPageOffset(Long nextPageOffset)
    {
        this.nextPageOffset = nextPageOffset;
    }

    public Long getLastPageOffset()
    {
        return this.lastPageOffset;
    }
    public void setLastPageOffset(Long lastPageOffset)
    {
        this.lastPageOffset = lastPageOffset;
    }

    public Long getLastPageIndex()
    {
        return this.lastPageIndex;
    }
    public void setLastPageIndex(Long lastPageIndex)
    {
        this.lastPageIndex = lastPageIndex;
    }

    public Boolean isFirstActionEnabled()
    {
        return this.firstActionEnabled;
    }
    public void setFirstActionEnabled(Boolean firstActionEnabled)
    {
        this.firstActionEnabled = firstActionEnabled;
    }

    public Boolean isPreviousActionEnabled()
    {
        return this.previousActionEnabled;
    }
    public void setPreviousActionEnabled(Boolean previousActionEnabled)
    {
        this.previousActionEnabled = previousActionEnabled;
    }

    public Boolean isNextActionEnabled()
    {
        return this.nextActionEnabled;
    }
    public void setNextActionEnabled(Boolean nextActionEnabled)
    {
        this.nextActionEnabled = nextActionEnabled;
    }

    public Boolean isLastActionEnabled()
    {
        return this.lastActionEnabled;
    }
    public void setLastActionEnabled(Boolean lastActionEnabled)
    {
        this.lastActionEnabled = lastActionEnabled;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("pagerMode:null, ");
        sb.append("primaryOrdering:null, ");
        sb.append("secondaryOrdering:null, ");
        sb.append("currentOffset:0, ");
        sb.append("currentPage:0, ");
        sb.append("pageSize:0, ");
        sb.append("totalCount:0, ");
        sb.append("lowerBoundTotalCount:0, ");
        sb.append("previousPageOffset:0, ");
        sb.append("nextPageOffset:0, ");
        sb.append("lastPageOffset:0, ");
        sb.append("lastPageIndex:0, ");
        sb.append("firstActionEnabled:false, ");
        sb.append("previousActionEnabled:false, ");
        sb.append("nextActionEnabled:false, ");
        sb.append("lastActionEnabled:false, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("pagerMode:");
        if(this.getPagerMode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPagerMode()).append("\", ");
        }
        sb.append("primaryOrdering:");
        if(this.getPrimaryOrdering() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPrimaryOrdering()).append("\", ");
        }
        sb.append("secondaryOrdering:");
        if(this.getSecondaryOrdering() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSecondaryOrdering()).append("\", ");
        }
        sb.append("currentOffset:" + this.getCurrentOffset()).append(", ");
        sb.append("currentPage:" + this.getCurrentPage()).append(", ");
        sb.append("pageSize:" + this.getPageSize()).append(", ");
        sb.append("totalCount:" + this.getTotalCount()).append(", ");
        sb.append("lowerBoundTotalCount:" + this.getLowerBoundTotalCount()).append(", ");
        sb.append("previousPageOffset:" + this.getPreviousPageOffset()).append(", ");
        sb.append("nextPageOffset:" + this.getNextPageOffset()).append(", ");
        sb.append("lastPageOffset:" + this.getLastPageOffset()).append(", ");
        sb.append("lastPageIndex:" + this.getLastPageIndex()).append(", ");
        sb.append("firstActionEnabled:" + this.isFirstActionEnabled()).append(", ");
        sb.append("previousActionEnabled:" + this.isPreviousActionEnabled()).append(", ");
        sb.append("nextActionEnabled:" + this.isNextActionEnabled()).append(", ");
        sb.append("lastActionEnabled:" + this.isLastActionEnabled()).append(", ");
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getPagerMode() != null) {
            sb.append("\"pagerMode\":").append("\"").append(this.getPagerMode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pagerMode\":").append("null, ");
        }
        if(this.getPrimaryOrdering() != null) {
            sb.append("\"primaryOrdering\":").append("\"").append(this.getPrimaryOrdering()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"primaryOrdering\":").append("null, ");
        }
        if(this.getSecondaryOrdering() != null) {
            sb.append("\"secondaryOrdering\":").append("\"").append(this.getSecondaryOrdering()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"secondaryOrdering\":").append("null, ");
        }
        if(this.getCurrentOffset() != null) {
            sb.append("\"currentOffset\":").append("").append(this.getCurrentOffset()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"currentOffset\":").append("null, ");
        }
        if(this.getCurrentPage() != null) {
            sb.append("\"currentPage\":").append("").append(this.getCurrentPage()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"currentPage\":").append("null, ");
        }
        if(this.getPageSize() != null) {
            sb.append("\"pageSize\":").append("").append(this.getPageSize()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pageSize\":").append("null, ");
        }
        if(this.getTotalCount() != null) {
            sb.append("\"totalCount\":").append("").append(this.getTotalCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"totalCount\":").append("null, ");
        }
        if(this.getLowerBoundTotalCount() != null) {
            sb.append("\"lowerBoundTotalCount\":").append("").append(this.getLowerBoundTotalCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lowerBoundTotalCount\":").append("null, ");
        }
        if(this.getPreviousPageOffset() != null) {
            sb.append("\"previousPageOffset\":").append("").append(this.getPreviousPageOffset()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"previousPageOffset\":").append("null, ");
        }
        if(this.getNextPageOffset() != null) {
            sb.append("\"nextPageOffset\":").append("").append(this.getNextPageOffset()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"nextPageOffset\":").append("null, ");
        }
        if(this.getLastPageOffset() != null) {
            sb.append("\"lastPageOffset\":").append("").append(this.getLastPageOffset()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastPageOffset\":").append("null, ");
        }
        if(this.getLastPageIndex() != null) {
            sb.append("\"lastPageIndex\":").append("").append(this.getLastPageIndex()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastPageIndex\":").append("null, ");
        }
        if(this.isFirstActionEnabled() != null) {
            sb.append("\"firstActionEnabled\":").append("").append(this.isFirstActionEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"firstActionEnabled\":").append("null, ");
        }
        if(this.isPreviousActionEnabled() != null) {
            sb.append("\"previousActionEnabled\":").append("").append(this.isPreviousActionEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"previousActionEnabled\":").append("null, ");
        }
        if(this.isNextActionEnabled() != null) {
            sb.append("\"nextActionEnabled\":").append("").append(this.isNextActionEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"nextActionEnabled\":").append("null, ");
        }
        if(this.isLastActionEnabled() != null) {
            sb.append("\"lastActionEnabled\":").append("").append(this.isLastActionEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastActionEnabled\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("pagerMode = " + this.pagerMode).append(";");
        sb.append("primaryOrdering = " + this.primaryOrdering).append(";");
        sb.append("secondaryOrdering = " + this.secondaryOrdering).append(";");
        sb.append("currentOffset = " + this.currentOffset).append(";");
        sb.append("currentPage = " + this.currentPage).append(";");
        sb.append("pageSize = " + this.pageSize).append(";");
        sb.append("totalCount = " + this.totalCount).append(";");
        sb.append("lowerBoundTotalCount = " + this.lowerBoundTotalCount).append(";");
        sb.append("previousPageOffset = " + this.previousPageOffset).append(";");
        sb.append("nextPageOffset = " + this.nextPageOffset).append(";");
        sb.append("lastPageOffset = " + this.lastPageOffset).append(";");
        sb.append("lastPageIndex = " + this.lastPageIndex).append(";");
        sb.append("firstActionEnabled = " + this.firstActionEnabled).append(";");
        sb.append("previousActionEnabled = " + this.previousActionEnabled).append(";");
        sb.append("nextActionEnabled = " + this.nextActionEnabled).append(";");
        sb.append("lastActionEnabled = " + this.lastActionEnabled).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        PagerStateStructJsBean cloned = new PagerStateStructJsBean();
        cloned.setPagerMode(this.getPagerMode());   
        cloned.setPrimaryOrdering(this.getPrimaryOrdering());   
        cloned.setSecondaryOrdering(this.getSecondaryOrdering());   
        cloned.setCurrentOffset(this.getCurrentOffset());   
        cloned.setCurrentPage(this.getCurrentPage());   
        cloned.setPageSize(this.getPageSize());   
        cloned.setTotalCount(this.getTotalCount());   
        cloned.setLowerBoundTotalCount(this.getLowerBoundTotalCount());   
        cloned.setPreviousPageOffset(this.getPreviousPageOffset());   
        cloned.setNextPageOffset(this.getNextPageOffset());   
        cloned.setLastPageOffset(this.getLastPageOffset());   
        cloned.setLastPageIndex(this.getLastPageIndex());   
        cloned.setFirstActionEnabled(this.isFirstActionEnabled());   
        cloned.setPreviousActionEnabled(this.isPreviousActionEnabled());   
        cloned.setNextActionEnabled(this.isNextActionEnabled());   
        cloned.setLastActionEnabled(this.isLastActionEnabled());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
