package com.cronbay.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.fe.core.StringEscapeUtil;


public class CronJobJsBean implements Serializable, Cloneable  //, CronJob
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CronJobJsBean.class.getName());

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructJsBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private Integer jobId;
    private String title;
    private String description;
    private String originJob;
    private RestRequestStructJsBean restRequest;
    private String permalink;
    private String shortlink;
    private String status;
    private Integer jobStatus;
    private String extra;
    private String note;
    private Boolean alert;
    private NotificationStructJsBean notificationPref;
    private ReferrerInfoStructJsBean referrerInfo;
    private CronScheduleStructJsBean cronSchedule;
    private Long nextRunTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public CronJobJsBean()
    {
        //this((String) null);
    }
    public CronJobJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public CronJobJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStructJsBean restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStructJsBean notificationPref, ReferrerInfoStructJsBean referrerInfo, CronScheduleStructJsBean cronSchedule, Long nextRunTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, jobId, title, description, originJob, restRequest, permalink, shortlink, status, jobStatus, extra, note, alert, notificationPref, referrerInfo, cronSchedule, nextRunTime, null, null);
    }
    public CronJobJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStructJsBean restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStructJsBean notificationPref, ReferrerInfoStructJsBean referrerInfo, CronScheduleStructJsBean cronSchedule, Long nextRunTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.jobId = jobId;
        this.title = title;
        this.description = description;
        this.originJob = originJob;
        this.restRequest = restRequest;
        this.permalink = permalink;
        this.shortlink = shortlink;
        this.status = status;
        this.jobStatus = jobStatus;
        this.extra = extra;
        this.note = note;
        this.alert = alert;
        this.notificationPref = notificationPref;
        this.referrerInfo = referrerInfo;
        this.cronSchedule = cronSchedule;
        this.nextRunTime = nextRunTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public CronJobJsBean(CronJobJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());   
            setManagerApp(bean.getManagerApp());   
            setAppAcl(bean.getAppAcl());   
            setGaeApp(bean.getGaeApp());   
            setOwnerUser(bean.getOwnerUser());   
            setUserAcl(bean.getUserAcl());   
            setUser(bean.getUser());   
            setJobId(bean.getJobId());   
            setTitle(bean.getTitle());   
            setDescription(bean.getDescription());   
            setOriginJob(bean.getOriginJob());   
            setRestRequest(bean.getRestRequest());   
            setPermalink(bean.getPermalink());   
            setShortlink(bean.getShortlink());   
            setStatus(bean.getStatus());   
            setJobStatus(bean.getJobStatus());   
            setExtra(bean.getExtra());   
            setNote(bean.getNote());   
            setAlert(bean.isAlert());   
            setNotificationPref(bean.getNotificationPref());   
            setReferrerInfo(bean.getReferrerInfo());   
            setCronSchedule(bean.getCronSchedule());   
            setNextRunTime(bean.getNextRunTime());   
            setCreatedTime(bean.getCreatedTime());   
            setModifiedTime(bean.getModifiedTime());   
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static CronJobJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        CronJobJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(CronJobJsBean.class);

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            bean = mapper.readValue(jsonStr, CronJobJsBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStructJsBean getGaeApp()
    {  
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStructJsBean gaeApp)
    {
        this.gaeApp = gaeApp;
    }

    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public Integer getJobId()
    {
        return this.jobId;
    }
    public void setJobId(Integer jobId)
    {
        this.jobId = jobId;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getOriginJob()
    {
        return this.originJob;
    }
    public void setOriginJob(String originJob)
    {
        this.originJob = originJob;
    }

    public RestRequestStructJsBean getRestRequest()
    {  
        return this.restRequest;
    }
    public void setRestRequest(RestRequestStructJsBean restRequest)
    {
        this.restRequest = restRequest;
    }

    public String getPermalink()
    {
        return this.permalink;
    }
    public void setPermalink(String permalink)
    {
        this.permalink = permalink;
    }

    public String getShortlink()
    {
        return this.shortlink;
    }
    public void setShortlink(String shortlink)
    {
        this.shortlink = shortlink;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Integer getJobStatus()
    {
        return this.jobStatus;
    }
    public void setJobStatus(Integer jobStatus)
    {
        this.jobStatus = jobStatus;
    }

    public String getExtra()
    {
        return this.extra;
    }
    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Boolean isAlert()
    {
        return this.alert;
    }
    public void setAlert(Boolean alert)
    {
        this.alert = alert;
    }

    public NotificationStructJsBean getNotificationPref()
    {  
        return this.notificationPref;
    }
    public void setNotificationPref(NotificationStructJsBean notificationPref)
    {
        this.notificationPref = notificationPref;
    }

    public ReferrerInfoStructJsBean getReferrerInfo()
    {  
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStructJsBean referrerInfo)
    {
        this.referrerInfo = referrerInfo;
    }

    public CronScheduleStructJsBean getCronSchedule()
    {  
        return this.cronSchedule;
    }
    public void setCronSchedule(CronScheduleStructJsBean cronSchedule)
    {
        this.cronSchedule = cronSchedule;
    }

    public Long getNextRunTime()
    {
        return this.nextRunTime;
    }
    public void setNextRunTime(Long nextRunTime)
    {
        this.nextRunTime = nextRunTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("managerApp:null, ");
        sb.append("appAcl:0, ");
        sb.append("gaeApp:{}, ");
        sb.append("ownerUser:null, ");
        sb.append("userAcl:0, ");
        sb.append("user:null, ");
        sb.append("jobId:0, ");
        sb.append("title:null, ");
        sb.append("description:null, ");
        sb.append("originJob:null, ");
        sb.append("restRequest:{}, ");
        sb.append("permalink:null, ");
        sb.append("shortlink:null, ");
        sb.append("status:null, ");
        sb.append("jobStatus:0, ");
        sb.append("extra:null, ");
        sb.append("note:null, ");
        sb.append("alert:false, ");
        sb.append("notificationPref:{}, ");
        sb.append("referrerInfo:{}, ");
        sb.append("cronSchedule:{}, ");
        sb.append("nextRunTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("managerApp:");
        if(this.getManagerApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getManagerApp()).append("\", ");
        }
        sb.append("appAcl:" + this.getAppAcl()).append(", ");
        sb.append("gaeApp:");
        if(this.getGaeApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGaeApp()).append("\", ");
        }
        sb.append("ownerUser:");
        if(this.getOwnerUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOwnerUser()).append("\", ");
        }
        sb.append("userAcl:" + this.getUserAcl()).append(", ");
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("jobId:" + this.getJobId()).append(", ");
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("originJob:");
        if(this.getOriginJob() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOriginJob()).append("\", ");
        }
        sb.append("restRequest:");
        if(this.getRestRequest() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRestRequest()).append("\", ");
        }
        sb.append("permalink:");
        if(this.getPermalink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPermalink()).append("\", ");
        }
        sb.append("shortlink:");
        if(this.getShortlink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortlink()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("jobStatus:" + this.getJobStatus()).append(", ");
        sb.append("extra:");
        if(this.getExtra() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getExtra()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("alert:" + this.isAlert()).append(", ");
        sb.append("notificationPref:");
        if(this.getNotificationPref() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNotificationPref()).append("\", ");
        }
        sb.append("referrerInfo:");
        if(this.getReferrerInfo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferrerInfo()).append("\", ");
        }
        sb.append("cronSchedule:");
        if(this.getCronSchedule() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCronSchedule()).append("\", ");
        }
        sb.append("nextRunTime:" + this.getNextRunTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getManagerApp() != null) {
            sb.append("\"managerApp\":").append("\"").append(this.getManagerApp()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"managerApp\":").append("null, ");
        }
        if(this.getAppAcl() != null) {
            sb.append("\"appAcl\":").append("").append(this.getAppAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appAcl\":").append("null, ");
        }
        sb.append("\"gaeApp\":").append(this.gaeApp.toJsonString()).append(", ");
        if(this.getOwnerUser() != null) {
            sb.append("\"ownerUser\":").append("\"").append(this.getOwnerUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ownerUser\":").append("null, ");
        }
        if(this.getUserAcl() != null) {
            sb.append("\"userAcl\":").append("").append(this.getUserAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userAcl\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getJobId() != null) {
            sb.append("\"jobId\":").append("").append(this.getJobId()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"jobId\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getOriginJob() != null) {
            sb.append("\"originJob\":").append("\"").append(this.getOriginJob()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"originJob\":").append("null, ");
        }
        sb.append("\"restRequest\":").append(this.restRequest.toJsonString()).append(", ");
        if(this.getPermalink() != null) {
            sb.append("\"permalink\":").append("\"").append(this.getPermalink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"permalink\":").append("null, ");
        }
        if(this.getShortlink() != null) {
            sb.append("\"shortlink\":").append("\"").append(this.getShortlink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortlink\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getJobStatus() != null) {
            sb.append("\"jobStatus\":").append("").append(this.getJobStatus()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"jobStatus\":").append("null, ");
        }
        if(this.getExtra() != null) {
            sb.append("\"extra\":").append("\"").append(this.getExtra()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"extra\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.isAlert() != null) {
            sb.append("\"alert\":").append("").append(this.isAlert()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"alert\":").append("null, ");
        }
        sb.append("\"notificationPref\":").append(this.notificationPref.toJsonString()).append(", ");
        sb.append("\"referrerInfo\":").append(this.referrerInfo.toJsonString()).append(", ");
        sb.append("\"cronSchedule\":").append(this.cronSchedule.toJsonString()).append(", ");
        if(this.getNextRunTime() != null) {
            sb.append("\"nextRunTime\":").append("").append(this.getNextRunTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"nextRunTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("managerApp = " + this.managerApp).append(";");
        sb.append("appAcl = " + this.appAcl).append(";");
        sb.append("gaeApp = " + this.gaeApp).append(";");
        sb.append("ownerUser = " + this.ownerUser).append(";");
        sb.append("userAcl = " + this.userAcl).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("jobId = " + this.jobId).append(";");
        sb.append("title = " + this.title).append(";");
        sb.append("description = " + this.description).append(";");
        sb.append("originJob = " + this.originJob).append(";");
        sb.append("restRequest = " + this.restRequest).append(";");
        sb.append("permalink = " + this.permalink).append(";");
        sb.append("shortlink = " + this.shortlink).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("jobStatus = " + this.jobStatus).append(";");
        sb.append("extra = " + this.extra).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("alert = " + this.alert).append(";");
        sb.append("notificationPref = " + this.notificationPref).append(";");
        sb.append("referrerInfo = " + this.referrerInfo).append(";");
        sb.append("cronSchedule = " + this.cronSchedule).append(";");
        sb.append("nextRunTime = " + this.nextRunTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        CronJobJsBean cloned = new CronJobJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setManagerApp(this.getManagerApp());   
        cloned.setAppAcl(this.getAppAcl());   
        cloned.setGaeApp( (GaeAppStructJsBean) this.getGaeApp().clone() );
        cloned.setOwnerUser(this.getOwnerUser());   
        cloned.setUserAcl(this.getUserAcl());   
        cloned.setUser(this.getUser());   
        cloned.setJobId(this.getJobId());   
        cloned.setTitle(this.getTitle());   
        cloned.setDescription(this.getDescription());   
        cloned.setOriginJob(this.getOriginJob());   
        cloned.setRestRequest( (RestRequestStructJsBean) this.getRestRequest().clone() );
        cloned.setPermalink(this.getPermalink());   
        cloned.setShortlink(this.getShortlink());   
        cloned.setStatus(this.getStatus());   
        cloned.setJobStatus(this.getJobStatus());   
        cloned.setExtra(this.getExtra());   
        cloned.setNote(this.getNote());   
        cloned.setAlert(this.isAlert());   
        cloned.setNotificationPref( (NotificationStructJsBean) this.getNotificationPref().clone() );
        cloned.setReferrerInfo( (ReferrerInfoStructJsBean) this.getReferrerInfo().clone() );
        cloned.setCronSchedule( (CronScheduleStructJsBean) this.getCronSchedule().clone() );
        cloned.setNextRunTime(this.getNextRunTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
