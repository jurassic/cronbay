package com.cronbay.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.fe.core.StringEscapeUtil;


public class NotificationStructJsBean implements Serializable, Cloneable  //, NotificationStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(NotificationStructJsBean.class.getName());

    private String preferredMode;
    private String mobileNumber;
    private String emailAddress;
    private String callbackUrl;
    private String note;

    // Ctors.
    public NotificationStructJsBean()
    {
        //this((String) null);
    }
    public NotificationStructJsBean(String preferredMode, String mobileNumber, String emailAddress, String callbackUrl, String note)
    {
        this.preferredMode = preferredMode;
        this.mobileNumber = mobileNumber;
        this.emailAddress = emailAddress;
        this.callbackUrl = callbackUrl;
        this.note = note;
    }
    public NotificationStructJsBean(NotificationStructJsBean bean)
    {
        if(bean != null) {
            setPreferredMode(bean.getPreferredMode());   
            setMobileNumber(bean.getMobileNumber());   
            setEmailAddress(bean.getEmailAddress());   
            setCallbackUrl(bean.getCallbackUrl());   
            setNote(bean.getNote());   
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static NotificationStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        NotificationStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(NotificationStructJsBean.class);

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            bean = mapper.readValue(jsonStr, NotificationStructJsBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getPreferredMode()
    {
        return this.preferredMode;
    }
    public void setPreferredMode(String preferredMode)
    {
        this.preferredMode = preferredMode;
    }

    public String getMobileNumber()
    {
        return this.mobileNumber;
    }
    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailAddress()
    {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getCallbackUrl()
    {
        return this.callbackUrl;
    }
    public void setCallbackUrl(String callbackUrl)
    {
        this.callbackUrl = callbackUrl;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("preferredMode:null, ");
        sb.append("mobileNumber:null, ");
        sb.append("emailAddress:null, ");
        sb.append("callbackUrl:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("preferredMode:");
        if(this.getPreferredMode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPreferredMode()).append("\", ");
        }
        sb.append("mobileNumber:");
        if(this.getMobileNumber() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getMobileNumber()).append("\", ");
        }
        sb.append("emailAddress:");
        if(this.getEmailAddress() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getEmailAddress()).append("\", ");
        }
        sb.append("callbackUrl:");
        if(this.getCallbackUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCallbackUrl()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getPreferredMode() != null) {
            sb.append("\"preferredMode\":").append("\"").append(this.getPreferredMode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"preferredMode\":").append("null, ");
        }
        if(this.getMobileNumber() != null) {
            sb.append("\"mobileNumber\":").append("\"").append(this.getMobileNumber()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"mobileNumber\":").append("null, ");
        }
        if(this.getEmailAddress() != null) {
            sb.append("\"emailAddress\":").append("\"").append(this.getEmailAddress()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"emailAddress\":").append("null, ");
        }
        if(this.getCallbackUrl() != null) {
            sb.append("\"callbackUrl\":").append("\"").append(this.getCallbackUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"callbackUrl\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("preferredMode = " + this.preferredMode).append(";");
        sb.append("mobileNumber = " + this.mobileNumber).append(";");
        sb.append("emailAddress = " + this.emailAddress).append(";");
        sb.append("callbackUrl = " + this.callbackUrl).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        NotificationStructJsBean cloned = new NotificationStructJsBean();
        cloned.setPreferredMode(this.getPreferredMode());   
        cloned.setMobileNumber(this.getMobileNumber());   
        cloned.setEmailAddress(this.getEmailAddress());   
        cloned.setCallbackUrl(this.getCallbackUrl());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
