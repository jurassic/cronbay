package com.cronbay.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.fe.core.StringEscapeUtil;


public class ApiConsumerJsBean implements Serializable, Cloneable  //, ApiConsumer
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ApiConsumerJsBean.class.getName());

    private String guid;
    private String aeryId;
    private String name;
    private String description;
    private String appKey;
    private String appSecret;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ApiConsumerJsBean()
    {
        //this((String) null);
    }
    public ApiConsumerJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public ApiConsumerJsBean(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status)
    {
        this(guid, aeryId, name, description, appKey, appSecret, status, null, null);
    }
    public ApiConsumerJsBean(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.aeryId = aeryId;
        this.name = name;
        this.description = description;
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ApiConsumerJsBean(ApiConsumerJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());   
            setAeryId(bean.getAeryId());   
            setName(bean.getName());   
            setDescription(bean.getDescription());   
            setAppKey(bean.getAppKey());   
            setAppSecret(bean.getAppSecret());   
            setStatus(bean.getStatus());   
            setCreatedTime(bean.getCreatedTime());   
            setModifiedTime(bean.getModifiedTime());   
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ApiConsumerJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ApiConsumerJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ApiConsumerJsBean.class);

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            bean = mapper.readValue(jsonStr, ApiConsumerJsBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getAeryId()
    {
        return this.aeryId;
    }
    public void setAeryId(String aeryId)
    {
        this.aeryId = aeryId;
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAppKey()
    {
        return this.appKey;
    }
    public void setAppKey(String appKey)
    {
        this.appKey = appKey;
    }

    public String getAppSecret()
    {
        return this.appSecret;
    }
    public void setAppSecret(String appSecret)
    {
        this.appSecret = appSecret;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("aeryId:null, ");
        sb.append("name:null, ");
        sb.append("description:null, ");
        sb.append("appKey:null, ");
        sb.append("appSecret:null, ");
        sb.append("status:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("aeryId:");
        if(this.getAeryId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAeryId()).append("\", ");
        }
        sb.append("name:");
        if(this.getName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getName()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("appKey:");
        if(this.getAppKey() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppKey()).append("\", ");
        }
        sb.append("appSecret:");
        if(this.getAppSecret() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppSecret()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getAeryId() != null) {
            sb.append("\"aeryId\":").append("\"").append(this.getAeryId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"aeryId\":").append("null, ");
        }
        if(this.getName() != null) {
            sb.append("\"name\":").append("\"").append(this.getName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"name\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getAppKey() != null) {
            sb.append("\"appKey\":").append("\"").append(this.getAppKey()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appKey\":").append("null, ");
        }
        if(this.getAppSecret() != null) {
            sb.append("\"appSecret\":").append("\"").append(this.getAppSecret()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appSecret\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("aeryId = " + this.aeryId).append(";");
        sb.append("name = " + this.name).append(";");
        sb.append("description = " + this.description).append(";");
        sb.append("appKey = " + this.appKey).append(";");
        sb.append("appSecret = " + this.appSecret).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ApiConsumerJsBean cloned = new ApiConsumerJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setAeryId(this.getAeryId());   
        cloned.setName(this.getName());   
        cloned.setDescription(this.getDescription());   
        cloned.setAppKey(this.getAppKey());   
        cloned.setAppSecret(this.getAppSecret());   
        cloned.setStatus(this.getStatus());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
