package com.cronbay.fe.bean.mock;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.fe.Validateable;
import com.cronbay.fe.core.StringEscapeUtil;
import com.cronbay.fe.bean.GaeAppStructJsBean;
import com.cronbay.fe.bean.GaeUserStructJsBean;
import com.cronbay.fe.bean.UserJsBean;


// Place holder...
public class UserMockBean extends UserJsBean implements Serializable, Cloneable, Validateable  //, User
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserMockBean.class.getName());

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public UserMockBean()
    {
        super();
    }
    public UserMockBean(String guid)
    {
       super(guid);
    }
    public UserMockBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStructJsBean gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime);
    }
    public UserMockBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStructJsBean gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime, Long createdTime, Long modifiedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime, createdTime, modifiedTime);
    }
    public UserMockBean(UserJsBean bean)
    {
        super(bean);
    }

    public static UserMockBean fromJsonString(String jsonStr)
    {
        UserMockBean bean = null;
        try {
            // TBD:
            ObjectMapper mapper = new ObjectMapper();   // can reuse, share globally
            bean = mapper.readValue(jsonStr, UserMockBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getManagerApp() == null) {
//            addError("managerApp", "managerApp is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAppAcl() == null) {
//            addError("appAcl", "appAcl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getGaeApp() == null) {
//            addError("gaeApp", "gaeApp is null");
//            allOK = false;
//        } else {
//            GaeAppStructJsBean gaeApp = getGaeApp();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getAeryId() == null) {
//            addError("aeryId", "aeryId is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSessionId() == null) {
//            addError("sessionId", "sessionId is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUsername() == null) {
//            addError("username", "username is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNickname() == null) {
//            addError("nickname", "nickname is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAvatar() == null) {
//            addError("avatar", "avatar is null");
//            allOK = false;
//        }
//        // TBD
//        if(getEmail() == null) {
//            addError("email", "email is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOpenId() == null) {
//            addError("openId", "openId is null");
//            allOK = false;
//        }
//        // TBD
//        if(getGaeUser() == null) {
//            addError("gaeUser", "gaeUser is null");
//            allOK = false;
//        } else {
//            GaeUserStructJsBean gaeUser = getGaeUser();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getTimeZone() == null) {
//            addError("timeZone", "timeZone is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLocation() == null) {
//            addError("location", "location is null");
//            allOK = false;
//        }
//        // TBD
//        if(getIpAddress() == null) {
//            addError("ipAddress", "ipAddress is null");
//            allOK = false;
//        }
//        // TBD
//        if(getReferer() == null) {
//            addError("referer", "referer is null");
//            allOK = false;
//        }
//        // TBD
//        if(isObsolete() == null) {
//            addError("obsolete", "obsolete is null");
//            allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getVerifiedTime() == null) {
//            addError("verifiedTime", "verifiedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAuthenticatedTime() == null) {
//            addError("authenticatedTime", "authenticatedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UserMockBean cloned = new UserMockBean((UserJsBean) super.clone());
        return cloned;
    }

}
