package com.cronbay.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import java.util.List;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.fe.core.StringEscapeUtil;


public class JobOutputJsBean implements Serializable, Cloneable  //, JobOutput
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(JobOutputJsBean.class.getName());

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructJsBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private String cronJob;
    private String previousRun;
    private String previousTry;
    private String responseCode;
    private String outputFormat;
    private String outputText;
    private List<String> outputData;
    private String outputHash;
    private String permalink;
    private String shortlink;
    private String result;
    private String status;
    private String extra;
    private String note;
    private Integer retryCounter;
    private Long scheduledTime;
    private Long processedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public JobOutputJsBean()
    {
        //this((String) null);
    }
    public JobOutputJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public JobOutputJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime, null, null);
    }
    public JobOutputJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.cronJob = cronJob;
        this.previousRun = previousRun;
        this.previousTry = previousTry;
        this.responseCode = responseCode;
        this.outputFormat = outputFormat;
        this.outputText = outputText;
        this.outputData = outputData;
        this.outputHash = outputHash;
        this.permalink = permalink;
        this.shortlink = shortlink;
        this.result = result;
        this.status = status;
        this.extra = extra;
        this.note = note;
        this.retryCounter = retryCounter;
        this.scheduledTime = scheduledTime;
        this.processedTime = processedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public JobOutputJsBean(JobOutputJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());   
            setManagerApp(bean.getManagerApp());   
            setAppAcl(bean.getAppAcl());   
            setGaeApp(bean.getGaeApp());   
            setOwnerUser(bean.getOwnerUser());   
            setUserAcl(bean.getUserAcl());   
            setUser(bean.getUser());   
            setCronJob(bean.getCronJob());   
            setPreviousRun(bean.getPreviousRun());   
            setPreviousTry(bean.getPreviousTry());   
            setResponseCode(bean.getResponseCode());   
            setOutputFormat(bean.getOutputFormat());   
            setOutputText(bean.getOutputText());   
            setOutputData(bean.getOutputData());   
            setOutputHash(bean.getOutputHash());   
            setPermalink(bean.getPermalink());   
            setShortlink(bean.getShortlink());   
            setResult(bean.getResult());   
            setStatus(bean.getStatus());   
            setExtra(bean.getExtra());   
            setNote(bean.getNote());   
            setRetryCounter(bean.getRetryCounter());   
            setScheduledTime(bean.getScheduledTime());   
            setProcessedTime(bean.getProcessedTime());   
            setCreatedTime(bean.getCreatedTime());   
            setModifiedTime(bean.getModifiedTime());   
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static JobOutputJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        JobOutputJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(JobOutputJsBean.class);

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            bean = mapper.readValue(jsonStr, JobOutputJsBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStructJsBean getGaeApp()
    {  
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStructJsBean gaeApp)
    {
        this.gaeApp = gaeApp;
    }

    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getCronJob()
    {
        return this.cronJob;
    }
    public void setCronJob(String cronJob)
    {
        this.cronJob = cronJob;
    }

    public String getPreviousRun()
    {
        return this.previousRun;
    }
    public void setPreviousRun(String previousRun)
    {
        this.previousRun = previousRun;
    }

    public String getPreviousTry()
    {
        return this.previousTry;
    }
    public void setPreviousTry(String previousTry)
    {
        this.previousTry = previousTry;
    }

    public String getResponseCode()
    {
        return this.responseCode;
    }
    public void setResponseCode(String responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    public String getOutputText()
    {
        return this.outputText;
    }
    public void setOutputText(String outputText)
    {
        this.outputText = outputText;
    }

    public List<String> getOutputData()
    {
        return this.outputData;
    }
    public void setOutputData(List<String> outputData)
    {
        this.outputData = outputData;
    }

    public String getOutputHash()
    {
        return this.outputHash;
    }
    public void setOutputHash(String outputHash)
    {
        this.outputHash = outputHash;
    }

    public String getPermalink()
    {
        return this.permalink;
    }
    public void setPermalink(String permalink)
    {
        this.permalink = permalink;
    }

    public String getShortlink()
    {
        return this.shortlink;
    }
    public void setShortlink(String shortlink)
    {
        this.shortlink = shortlink;
    }

    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getExtra()
    {
        return this.extra;
    }
    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Integer getRetryCounter()
    {
        return this.retryCounter;
    }
    public void setRetryCounter(Integer retryCounter)
    {
        this.retryCounter = retryCounter;
    }

    public Long getScheduledTime()
    {
        return this.scheduledTime;
    }
    public void setScheduledTime(Long scheduledTime)
    {
        this.scheduledTime = scheduledTime;
    }

    public Long getProcessedTime()
    {
        return this.processedTime;
    }
    public void setProcessedTime(Long processedTime)
    {
        this.processedTime = processedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("managerApp:null, ");
        sb.append("appAcl:0, ");
        sb.append("gaeApp:{}, ");
        sb.append("ownerUser:null, ");
        sb.append("userAcl:0, ");
        sb.append("user:null, ");
        sb.append("cronJob:null, ");
        sb.append("previousRun:null, ");
        sb.append("previousTry:null, ");
        sb.append("responseCode:null, ");
        sb.append("outputFormat:null, ");
        sb.append("outputText:null, ");
        sb.append("outputData:null, ");
        sb.append("outputHash:null, ");
        sb.append("permalink:null, ");
        sb.append("shortlink:null, ");
        sb.append("result:null, ");
        sb.append("status:null, ");
        sb.append("extra:null, ");
        sb.append("note:null, ");
        sb.append("retryCounter:0, ");
        sb.append("scheduledTime:0, ");
        sb.append("processedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("managerApp:");
        if(this.getManagerApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getManagerApp()).append("\", ");
        }
        sb.append("appAcl:" + this.getAppAcl()).append(", ");
        sb.append("gaeApp:");
        if(this.getGaeApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGaeApp()).append("\", ");
        }
        sb.append("ownerUser:");
        if(this.getOwnerUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOwnerUser()).append("\", ");
        }
        sb.append("userAcl:" + this.getUserAcl()).append(", ");
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("cronJob:");
        if(this.getCronJob() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCronJob()).append("\", ");
        }
        sb.append("previousRun:");
        if(this.getPreviousRun() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPreviousRun()).append("\", ");
        }
        sb.append("previousTry:");
        if(this.getPreviousTry() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPreviousTry()).append("\", ");
        }
        sb.append("responseCode:");
        if(this.getResponseCode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getResponseCode()).append("\", ");
        }
        sb.append("outputFormat:");
        if(this.getOutputFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputFormat()).append("\", ");
        }
        sb.append("outputText:");
        if(this.getOutputText() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputText()).append("\", ");
        }
        sb.append("outputData:");
        if(this.getOutputData() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputData()).append("\", ");
        }
        sb.append("outputHash:");
        if(this.getOutputHash() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputHash()).append("\", ");
        }
        sb.append("permalink:");
        if(this.getPermalink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPermalink()).append("\", ");
        }
        sb.append("shortlink:");
        if(this.getShortlink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortlink()).append("\", ");
        }
        sb.append("result:");
        if(this.getResult() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getResult()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("extra:");
        if(this.getExtra() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getExtra()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("retryCounter:" + this.getRetryCounter()).append(", ");
        sb.append("scheduledTime:" + this.getScheduledTime()).append(", ");
        sb.append("processedTime:" + this.getProcessedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getManagerApp() != null) {
            sb.append("\"managerApp\":").append("\"").append(this.getManagerApp()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"managerApp\":").append("null, ");
        }
        if(this.getAppAcl() != null) {
            sb.append("\"appAcl\":").append("").append(this.getAppAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appAcl\":").append("null, ");
        }
        sb.append("\"gaeApp\":").append(this.gaeApp.toJsonString()).append(", ");
        if(this.getOwnerUser() != null) {
            sb.append("\"ownerUser\":").append("\"").append(this.getOwnerUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ownerUser\":").append("null, ");
        }
        if(this.getUserAcl() != null) {
            sb.append("\"userAcl\":").append("").append(this.getUserAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userAcl\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getCronJob() != null) {
            sb.append("\"cronJob\":").append("\"").append(this.getCronJob()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"cronJob\":").append("null, ");
        }
        if(this.getPreviousRun() != null) {
            sb.append("\"previousRun\":").append("\"").append(this.getPreviousRun()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"previousRun\":").append("null, ");
        }
        if(this.getPreviousTry() != null) {
            sb.append("\"previousTry\":").append("\"").append(this.getPreviousTry()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"previousTry\":").append("null, ");
        }
        if(this.getResponseCode() != null) {
            sb.append("\"responseCode\":").append("\"").append(this.getResponseCode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"responseCode\":").append("null, ");
        }
        if(this.getOutputFormat() != null) {
            sb.append("\"outputFormat\":").append("\"").append(this.getOutputFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputFormat\":").append("null, ");
        }
        if(this.getOutputText() != null) {
            sb.append("\"outputText\":").append("\"").append(this.getOutputText()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputText\":").append("null, ");
        }
        if(this.getOutputData() != null) {
            sb.append("\"outputData\":").append("\"").append(this.getOutputData()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputData\":").append("null, ");
        }
        if(this.getOutputHash() != null) {
            sb.append("\"outputHash\":").append("\"").append(this.getOutputHash()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputHash\":").append("null, ");
        }
        if(this.getPermalink() != null) {
            sb.append("\"permalink\":").append("\"").append(this.getPermalink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"permalink\":").append("null, ");
        }
        if(this.getShortlink() != null) {
            sb.append("\"shortlink\":").append("\"").append(this.getShortlink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortlink\":").append("null, ");
        }
        if(this.getResult() != null) {
            sb.append("\"result\":").append("\"").append(this.getResult()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"result\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getExtra() != null) {
            sb.append("\"extra\":").append("\"").append(this.getExtra()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"extra\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getRetryCounter() != null) {
            sb.append("\"retryCounter\":").append("").append(this.getRetryCounter()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"retryCounter\":").append("null, ");
        }
        if(this.getScheduledTime() != null) {
            sb.append("\"scheduledTime\":").append("").append(this.getScheduledTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"scheduledTime\":").append("null, ");
        }
        if(this.getProcessedTime() != null) {
            sb.append("\"processedTime\":").append("").append(this.getProcessedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"processedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("managerApp = " + this.managerApp).append(";");
        sb.append("appAcl = " + this.appAcl).append(";");
        sb.append("gaeApp = " + this.gaeApp).append(";");
        sb.append("ownerUser = " + this.ownerUser).append(";");
        sb.append("userAcl = " + this.userAcl).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("cronJob = " + this.cronJob).append(";");
        sb.append("previousRun = " + this.previousRun).append(";");
        sb.append("previousTry = " + this.previousTry).append(";");
        sb.append("responseCode = " + this.responseCode).append(";");
        sb.append("outputFormat = " + this.outputFormat).append(";");
        sb.append("outputText = " + this.outputText).append(";");
        sb.append("outputData = " + this.outputData).append(";");
        sb.append("outputHash = " + this.outputHash).append(";");
        sb.append("permalink = " + this.permalink).append(";");
        sb.append("shortlink = " + this.shortlink).append(";");
        sb.append("result = " + this.result).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("extra = " + this.extra).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("retryCounter = " + this.retryCounter).append(";");
        sb.append("scheduledTime = " + this.scheduledTime).append(";");
        sb.append("processedTime = " + this.processedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        JobOutputJsBean cloned = new JobOutputJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setManagerApp(this.getManagerApp());   
        cloned.setAppAcl(this.getAppAcl());   
        cloned.setGaeApp( (GaeAppStructJsBean) this.getGaeApp().clone() );
        cloned.setOwnerUser(this.getOwnerUser());   
        cloned.setUserAcl(this.getUserAcl());   
        cloned.setUser(this.getUser());   
        cloned.setCronJob(this.getCronJob());   
        cloned.setPreviousRun(this.getPreviousRun());   
        cloned.setPreviousTry(this.getPreviousTry());   
        cloned.setResponseCode(this.getResponseCode());   
        cloned.setOutputFormat(this.getOutputFormat());   
        cloned.setOutputText(this.getOutputText());   
        cloned.setOutputData(this.getOutputData());   
        cloned.setOutputHash(this.getOutputHash());   
        cloned.setPermalink(this.getPermalink());   
        cloned.setShortlink(this.getShortlink());   
        cloned.setResult(this.getResult());   
        cloned.setStatus(this.getStatus());   
        cloned.setExtra(this.getExtra());   
        cloned.setNote(this.getNote());   
        cloned.setRetryCounter(this.getRetryCounter());   
        cloned.setScheduledTime(this.getScheduledTime());   
        cloned.setProcessedTime(this.getProcessedTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
