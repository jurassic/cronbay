package com.cronbay.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import java.util.List;

import com.cronbay.fe.core.StringEscapeUtil;


public class RestRequestStructJsBean implements Serializable, Cloneable  //, RestRequestStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RestRequestStructJsBean.class.getName());

    private String serviceName;
    private String serviceUrl;
    private String requestMethod;
    private String requestUrl;
    private String targetEntity;
    private String queryString;
    private List<String> queryParams;
    private String inputFormat;
    private String inputContent;
    private String outputFormat;
    private Integer maxRetries;
    private Integer retryInterval;
    private String note;

    // Ctors.
    public RestRequestStructJsBean()
    {
        //this((String) null);
    }
    public RestRequestStructJsBean(String serviceName, String serviceUrl, String requestMethod, String requestUrl, String targetEntity, String queryString, List<String> queryParams, String inputFormat, String inputContent, String outputFormat, Integer maxRetries, Integer retryInterval, String note)
    {
        this.serviceName = serviceName;
        this.serviceUrl = serviceUrl;
        this.requestMethod = requestMethod;
        this.requestUrl = requestUrl;
        this.targetEntity = targetEntity;
        this.queryString = queryString;
        this.queryParams = queryParams;
        this.inputFormat = inputFormat;
        this.inputContent = inputContent;
        this.outputFormat = outputFormat;
        this.maxRetries = maxRetries;
        this.retryInterval = retryInterval;
        this.note = note;
    }
    public RestRequestStructJsBean(RestRequestStructJsBean bean)
    {
        if(bean != null) {
            setServiceName(bean.getServiceName());   
            setServiceUrl(bean.getServiceUrl());   
            setRequestMethod(bean.getRequestMethod());   
            setRequestUrl(bean.getRequestUrl());   
            setTargetEntity(bean.getTargetEntity());   
            setQueryString(bean.getQueryString());   
            setQueryParams(bean.getQueryParams());   
            setInputFormat(bean.getInputFormat());   
            setInputContent(bean.getInputContent());   
            setOutputFormat(bean.getOutputFormat());   
            setMaxRetries(bean.getMaxRetries());   
            setRetryInterval(bean.getRetryInterval());   
            setNote(bean.getNote());   
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static RestRequestStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        RestRequestStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(RestRequestStructJsBean.class);

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            bean = mapper.readValue(jsonStr, RestRequestStructJsBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getServiceName()
    {
        return this.serviceName;
    }
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    public String getRequestMethod()
    {
        return this.requestMethod;
    }
    public void setRequestMethod(String requestMethod)
    {
        this.requestMethod = requestMethod;
    }

    public String getRequestUrl()
    {
        return this.requestUrl;
    }
    public void setRequestUrl(String requestUrl)
    {
        this.requestUrl = requestUrl;
    }

    public String getTargetEntity()
    {
        return this.targetEntity;
    }
    public void setTargetEntity(String targetEntity)
    {
        this.targetEntity = targetEntity;
    }

    public String getQueryString()
    {
        return this.queryString;
    }
    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    public List<String> getQueryParams()
    {
        return this.queryParams;
    }
    public void setQueryParams(List<String> queryParams)
    {
        this.queryParams = queryParams;
    }

    public String getInputFormat()
    {
        return this.inputFormat;
    }
    public void setInputFormat(String inputFormat)
    {
        this.inputFormat = inputFormat;
    }

    public String getInputContent()
    {
        return this.inputContent;
    }
    public void setInputContent(String inputContent)
    {
        this.inputContent = inputContent;
    }

    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    public Integer getMaxRetries()
    {
        return this.maxRetries;
    }
    public void setMaxRetries(Integer maxRetries)
    {
        this.maxRetries = maxRetries;
    }

    public Integer getRetryInterval()
    {
        return this.retryInterval;
    }
    public void setRetryInterval(Integer retryInterval)
    {
        this.retryInterval = retryInterval;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("serviceName:null, ");
        sb.append("serviceUrl:null, ");
        sb.append("requestMethod:null, ");
        sb.append("requestUrl:null, ");
        sb.append("targetEntity:null, ");
        sb.append("queryString:null, ");
        sb.append("queryParams:null, ");
        sb.append("inputFormat:null, ");
        sb.append("inputContent:null, ");
        sb.append("outputFormat:null, ");
        sb.append("maxRetries:0, ");
        sb.append("retryInterval:0, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("serviceName:");
        if(this.getServiceName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getServiceName()).append("\", ");
        }
        sb.append("serviceUrl:");
        if(this.getServiceUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getServiceUrl()).append("\", ");
        }
        sb.append("requestMethod:");
        if(this.getRequestMethod() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRequestMethod()).append("\", ");
        }
        sb.append("requestUrl:");
        if(this.getRequestUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRequestUrl()).append("\", ");
        }
        sb.append("targetEntity:");
        if(this.getTargetEntity() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTargetEntity()).append("\", ");
        }
        sb.append("queryString:");
        if(this.getQueryString() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQueryString()).append("\", ");
        }
        sb.append("queryParams:");
        if(this.getQueryParams() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQueryParams()).append("\", ");
        }
        sb.append("inputFormat:");
        if(this.getInputFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getInputFormat()).append("\", ");
        }
        sb.append("inputContent:");
        if(this.getInputContent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getInputContent()).append("\", ");
        }
        sb.append("outputFormat:");
        if(this.getOutputFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputFormat()).append("\", ");
        }
        sb.append("maxRetries:" + this.getMaxRetries()).append(", ");
        sb.append("retryInterval:" + this.getRetryInterval()).append(", ");
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getServiceName() != null) {
            sb.append("\"serviceName\":").append("\"").append(this.getServiceName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"serviceName\":").append("null, ");
        }
        if(this.getServiceUrl() != null) {
            sb.append("\"serviceUrl\":").append("\"").append(this.getServiceUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"serviceUrl\":").append("null, ");
        }
        if(this.getRequestMethod() != null) {
            sb.append("\"requestMethod\":").append("\"").append(this.getRequestMethod()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"requestMethod\":").append("null, ");
        }
        if(this.getRequestUrl() != null) {
            sb.append("\"requestUrl\":").append("\"").append(this.getRequestUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"requestUrl\":").append("null, ");
        }
        if(this.getTargetEntity() != null) {
            sb.append("\"targetEntity\":").append("\"").append(this.getTargetEntity()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"targetEntity\":").append("null, ");
        }
        if(this.getQueryString() != null) {
            sb.append("\"queryString\":").append("\"").append(this.getQueryString()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryString\":").append("null, ");
        }
        if(this.getQueryParams() != null) {
            sb.append("\"queryParams\":").append("\"").append(this.getQueryParams()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryParams\":").append("null, ");
        }
        if(this.getInputFormat() != null) {
            sb.append("\"inputFormat\":").append("\"").append(this.getInputFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"inputFormat\":").append("null, ");
        }
        if(this.getInputContent() != null) {
            sb.append("\"inputContent\":").append("\"").append(this.getInputContent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"inputContent\":").append("null, ");
        }
        if(this.getOutputFormat() != null) {
            sb.append("\"outputFormat\":").append("\"").append(this.getOutputFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputFormat\":").append("null, ");
        }
        if(this.getMaxRetries() != null) {
            sb.append("\"maxRetries\":").append("").append(this.getMaxRetries()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"maxRetries\":").append("null, ");
        }
        if(this.getRetryInterval() != null) {
            sb.append("\"retryInterval\":").append("").append(this.getRetryInterval()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"retryInterval\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("serviceName = " + this.serviceName).append(";");
        sb.append("serviceUrl = " + this.serviceUrl).append(";");
        sb.append("requestMethod = " + this.requestMethod).append(";");
        sb.append("requestUrl = " + this.requestUrl).append(";");
        sb.append("targetEntity = " + this.targetEntity).append(";");
        sb.append("queryString = " + this.queryString).append(";");
        sb.append("queryParams = " + this.queryParams).append(";");
        sb.append("inputFormat = " + this.inputFormat).append(";");
        sb.append("inputContent = " + this.inputContent).append(";");
        sb.append("outputFormat = " + this.outputFormat).append(";");
        sb.append("maxRetries = " + this.maxRetries).append(";");
        sb.append("retryInterval = " + this.retryInterval).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        RestRequestStructJsBean cloned = new RestRequestStructJsBean();
        cloned.setServiceName(this.getServiceName());   
        cloned.setServiceUrl(this.getServiceUrl());   
        cloned.setRequestMethod(this.getRequestMethod());   
        cloned.setRequestUrl(this.getRequestUrl());   
        cloned.setTargetEntity(this.getTargetEntity());   
        cloned.setQueryString(this.getQueryString());   
        cloned.setQueryParams(this.getQueryParams());   
        cloned.setInputFormat(this.getInputFormat());   
        cloned.setInputContent(this.getInputContent());   
        cloned.setOutputFormat(this.getOutputFormat());   
        cloned.setMaxRetries(this.getMaxRetries());   
        cloned.setRetryInterval(this.getRetryInterval());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
