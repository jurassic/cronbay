package com.cronbay.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.fe.core.StringEscapeUtil;


public class UserJsBean implements Serializable, Cloneable  //, User
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserJsBean.class.getName());

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructJsBean gaeApp;
    private String aeryId;
    private String sessionId;
    private String username;
    private String nickname;
    private String avatar;
    private String email;
    private String openId;
    private GaeUserStructJsBean gaeUser;
    private String timeZone;
    private String location;
    private String ipAddress;
    private String referer;
    private Boolean obsolete;
    private String status;
    private Long verifiedTime;
    private Long authenticatedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UserJsBean()
    {
        //this((String) null);
    }
    public UserJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public UserJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStructJsBean gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime)
    {
        this(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime, null, null);
    }
    public UserJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStructJsBean gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.aeryId = aeryId;
        this.sessionId = sessionId;
        this.username = username;
        this.nickname = nickname;
        this.avatar = avatar;
        this.email = email;
        this.openId = openId;
        this.gaeUser = gaeUser;
        this.timeZone = timeZone;
        this.location = location;
        this.ipAddress = ipAddress;
        this.referer = referer;
        this.obsolete = obsolete;
        this.status = status;
        this.verifiedTime = verifiedTime;
        this.authenticatedTime = authenticatedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UserJsBean(UserJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());   
            setManagerApp(bean.getManagerApp());   
            setAppAcl(bean.getAppAcl());   
            setGaeApp(bean.getGaeApp());   
            setAeryId(bean.getAeryId());   
            setSessionId(bean.getSessionId());   
            setUsername(bean.getUsername());   
            setNickname(bean.getNickname());   
            setAvatar(bean.getAvatar());   
            setEmail(bean.getEmail());   
            setOpenId(bean.getOpenId());   
            setGaeUser(bean.getGaeUser());   
            setTimeZone(bean.getTimeZone());   
            setLocation(bean.getLocation());   
            setIpAddress(bean.getIpAddress());   
            setReferer(bean.getReferer());   
            setObsolete(bean.isObsolete());   
            setStatus(bean.getStatus());   
            setVerifiedTime(bean.getVerifiedTime());   
            setAuthenticatedTime(bean.getAuthenticatedTime());   
            setCreatedTime(bean.getCreatedTime());   
            setModifiedTime(bean.getModifiedTime());   
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static UserJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        UserJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(UserJsBean.class);

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            bean = mapper.readValue(jsonStr, UserJsBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStructJsBean getGaeApp()
    {  
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStructJsBean gaeApp)
    {
        this.gaeApp = gaeApp;
    }

    public String getAeryId()
    {
        return this.aeryId;
    }
    public void setAeryId(String aeryId)
    {
        this.aeryId = aeryId;
    }

    public String getSessionId()
    {
        return this.sessionId;
    }
    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getNickname()
    {
        return this.nickname;
    }
    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getAvatar()
    {
        return this.avatar;
    }
    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getOpenId()
    {
        return this.openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    public GaeUserStructJsBean getGaeUser()
    {  
        return this.gaeUser;
    }
    public void setGaeUser(GaeUserStructJsBean gaeUser)
    {
        this.gaeUser = gaeUser;
    }

    public String getTimeZone()
    {
        return this.timeZone;
    }
    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }

    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public String getReferer()
    {
        return this.referer;
    }
    public void setReferer(String referer)
    {
        this.referer = referer;
    }

    public Boolean isObsolete()
    {
        return this.obsolete;
    }
    public void setObsolete(Boolean obsolete)
    {
        this.obsolete = obsolete;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getVerifiedTime()
    {
        return this.verifiedTime;
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        this.verifiedTime = verifiedTime;
    }

    public Long getAuthenticatedTime()
    {
        return this.authenticatedTime;
    }
    public void setAuthenticatedTime(Long authenticatedTime)
    {
        this.authenticatedTime = authenticatedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("managerApp:null, ");
        sb.append("appAcl:0, ");
        sb.append("gaeApp:{}, ");
        sb.append("aeryId:null, ");
        sb.append("sessionId:null, ");
        sb.append("username:null, ");
        sb.append("nickname:null, ");
        sb.append("avatar:null, ");
        sb.append("email:null, ");
        sb.append("openId:null, ");
        sb.append("gaeUser:{}, ");
        sb.append("timeZone:null, ");
        sb.append("location:null, ");
        sb.append("ipAddress:null, ");
        sb.append("referer:null, ");
        sb.append("obsolete:false, ");
        sb.append("status:null, ");
        sb.append("verifiedTime:0, ");
        sb.append("authenticatedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("managerApp:");
        if(this.getManagerApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getManagerApp()).append("\", ");
        }
        sb.append("appAcl:" + this.getAppAcl()).append(", ");
        sb.append("gaeApp:");
        if(this.getGaeApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGaeApp()).append("\", ");
        }
        sb.append("aeryId:");
        if(this.getAeryId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAeryId()).append("\", ");
        }
        sb.append("sessionId:");
        if(this.getSessionId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSessionId()).append("\", ");
        }
        sb.append("username:");
        if(this.getUsername() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUsername()).append("\", ");
        }
        sb.append("nickname:");
        if(this.getNickname() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNickname()).append("\", ");
        }
        sb.append("avatar:");
        if(this.getAvatar() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAvatar()).append("\", ");
        }
        sb.append("email:");
        if(this.getEmail() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getEmail()).append("\", ");
        }
        sb.append("openId:");
        if(this.getOpenId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOpenId()).append("\", ");
        }
        sb.append("gaeUser:");
        if(this.getGaeUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGaeUser()).append("\", ");
        }
        sb.append("timeZone:");
        if(this.getTimeZone() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTimeZone()).append("\", ");
        }
        sb.append("location:");
        if(this.getLocation() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLocation()).append("\", ");
        }
        sb.append("ipAddress:");
        if(this.getIpAddress() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getIpAddress()).append("\", ");
        }
        sb.append("referer:");
        if(this.getReferer() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferer()).append("\", ");
        }
        sb.append("obsolete:" + this.isObsolete()).append(", ");
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("verifiedTime:" + this.getVerifiedTime()).append(", ");
        sb.append("authenticatedTime:" + this.getAuthenticatedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getManagerApp() != null) {
            sb.append("\"managerApp\":").append("\"").append(this.getManagerApp()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"managerApp\":").append("null, ");
        }
        if(this.getAppAcl() != null) {
            sb.append("\"appAcl\":").append("").append(this.getAppAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appAcl\":").append("null, ");
        }
        sb.append("\"gaeApp\":").append(this.gaeApp.toJsonString()).append(", ");
        if(this.getAeryId() != null) {
            sb.append("\"aeryId\":").append("\"").append(this.getAeryId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"aeryId\":").append("null, ");
        }
        if(this.getSessionId() != null) {
            sb.append("\"sessionId\":").append("\"").append(this.getSessionId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sessionId\":").append("null, ");
        }
        if(this.getUsername() != null) {
            sb.append("\"username\":").append("\"").append(this.getUsername()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"username\":").append("null, ");
        }
        if(this.getNickname() != null) {
            sb.append("\"nickname\":").append("\"").append(this.getNickname()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"nickname\":").append("null, ");
        }
        if(this.getAvatar() != null) {
            sb.append("\"avatar\":").append("\"").append(this.getAvatar()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"avatar\":").append("null, ");
        }
        if(this.getEmail() != null) {
            sb.append("\"email\":").append("\"").append(this.getEmail()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"email\":").append("null, ");
        }
        if(this.getOpenId() != null) {
            sb.append("\"openId\":").append("\"").append(this.getOpenId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"openId\":").append("null, ");
        }
        sb.append("\"gaeUser\":").append(this.gaeUser.toJsonString()).append(", ");
        if(this.getTimeZone() != null) {
            sb.append("\"timeZone\":").append("\"").append(this.getTimeZone()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"timeZone\":").append("null, ");
        }
        if(this.getLocation() != null) {
            sb.append("\"location\":").append("\"").append(this.getLocation()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"location\":").append("null, ");
        }
        if(this.getIpAddress() != null) {
            sb.append("\"ipAddress\":").append("\"").append(this.getIpAddress()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ipAddress\":").append("null, ");
        }
        if(this.getReferer() != null) {
            sb.append("\"referer\":").append("\"").append(this.getReferer()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"referer\":").append("null, ");
        }
        if(this.isObsolete() != null) {
            sb.append("\"obsolete\":").append("").append(this.isObsolete()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"obsolete\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getVerifiedTime() != null) {
            sb.append("\"verifiedTime\":").append("").append(this.getVerifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"verifiedTime\":").append("null, ");
        }
        if(this.getAuthenticatedTime() != null) {
            sb.append("\"authenticatedTime\":").append("").append(this.getAuthenticatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"authenticatedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("managerApp = " + this.managerApp).append(";");
        sb.append("appAcl = " + this.appAcl).append(";");
        sb.append("gaeApp = " + this.gaeApp).append(";");
        sb.append("aeryId = " + this.aeryId).append(";");
        sb.append("sessionId = " + this.sessionId).append(";");
        sb.append("username = " + this.username).append(";");
        sb.append("nickname = " + this.nickname).append(";");
        sb.append("avatar = " + this.avatar).append(";");
        sb.append("email = " + this.email).append(";");
        sb.append("openId = " + this.openId).append(";");
        sb.append("gaeUser = " + this.gaeUser).append(";");
        sb.append("timeZone = " + this.timeZone).append(";");
        sb.append("location = " + this.location).append(";");
        sb.append("ipAddress = " + this.ipAddress).append(";");
        sb.append("referer = " + this.referer).append(";");
        sb.append("obsolete = " + this.obsolete).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("verifiedTime = " + this.verifiedTime).append(";");
        sb.append("authenticatedTime = " + this.authenticatedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UserJsBean cloned = new UserJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setManagerApp(this.getManagerApp());   
        cloned.setAppAcl(this.getAppAcl());   
        cloned.setGaeApp( (GaeAppStructJsBean) this.getGaeApp().clone() );
        cloned.setAeryId(this.getAeryId());   
        cloned.setSessionId(this.getSessionId());   
        cloned.setUsername(this.getUsername());   
        cloned.setNickname(this.getNickname());   
        cloned.setAvatar(this.getAvatar());   
        cloned.setEmail(this.getEmail());   
        cloned.setOpenId(this.getOpenId());   
        cloned.setGaeUser( (GaeUserStructJsBean) this.getGaeUser().clone() );
        cloned.setTimeZone(this.getTimeZone());   
        cloned.setLocation(this.getLocation());   
        cloned.setIpAddress(this.getIpAddress());   
        cloned.setReferer(this.getReferer());   
        cloned.setObsolete(this.isObsolete());   
        cloned.setStatus(this.getStatus());   
        cloned.setVerifiedTime(this.getVerifiedTime());   
        cloned.setAuthenticatedTime(this.getAuthenticatedTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
