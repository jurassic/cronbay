package com.cronbay.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

// TBD: ....
public class RefreshStatus
{
    private static final Logger log = Logger.getLogger(RefreshStatus.class.getName());

    // Static methods only.
    private RefreshStatus() {}

    // TBD
    //public static final int STATUS_UNKNOWN = -1;  1000 ???
    public static final int STATUS_SCHEDULED = 1;         // ???
    public static final int STATUS_PROCESSING = 2;        // ??? 
    public static final int STATUS_NOREFRESH = 4;         // ???  No refresh. 
    // etc.
    //...
    
}
