package com.cronbay.helper;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.analytics.AnalyticsManager;


public class AnalyticsHelper
{
    private static final Logger log = Logger.getLogger(AnalyticsHelper.class.getName());

    private AnalyticsHelper() {}

    // Initialization-on-demand holder.
    private static final class AnalyticsHelperHolder
    {
        private static final AnalyticsHelper INSTANCE = new AnalyticsHelper();
    }

    // Singleton method
    public static AnalyticsHelper getInstance()
    {
        return AnalyticsHelperHolder.INSTANCE;
    }

    
    public String getGoogleAnalyticsCode()
    {
        String code = AnalyticsManager.getInstance().getGoogleAnalyticsCode();
        log.fine("Google analytics code = " + code);
        return code;
    }
 

}
