package com.cronbay.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.common.RefreshStatus;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.JobOutputJsBean;
import com.cronbay.wa.service.JobOutputWebService;
import com.cronbay.wa.service.UserWebService;


public class JobOutputHelper
{
    private static final Logger log = Logger.getLogger(JobOutputHelper.class.getName());

    private JobOutputHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private JobOutputWebService cronJobWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private JobOutputWebService getJobOutputService()
    {
        if(cronJobWebService == null) {
            cronJobWebService = new JobOutputWebService();
        }
        return cronJobWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class JobOutputHelperHolder
    {
        private static final JobOutputHelper INSTANCE = new JobOutputHelper();
    }

    // Singleton method
    public static JobOutputHelper getInstance()
    {
        return JobOutputHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public JobOutputJsBean getJobOutput(String guid) 
    {
        JobOutputJsBean bean = null;
        try {
            bean = getJobOutputService().getJobOutput(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    
    public JobOutputJsBean getJobOutputByTargetUrl(String targetUrl) 
    {
        JobOutputJsBean bean = null;
        try {
            String filter = "targetUrl=='" + targetUrl + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<JobOutputJsBean> beans = getJobOutputService().findJobOutputs(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for targetUrl = " + targetUrl, e);
        }
        return bean;
    }

    

    public List<String> getJobOutputKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getJobOutputService().findJobOutputKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve job request key list.", e);
        }

        return keys;
    }
    
    public List<JobOutputJsBean> getJobOutputsForRefreshProcessing(Integer maxCount)
    {
        List<JobOutputJsBean> jobOutputs = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            jobOutputs = getJobOutputService().findJobOutputs(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve job request list.", e);
        }

        return jobOutputs;
    }


}
