package com.cronbay.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.CronJobJsBean;
import com.cronbay.wa.service.CronJobWebService;
import com.cronbay.wa.service.UserWebService;


public class SitemapHelper
{
    private static final Logger log = Logger.getLogger(SitemapHelper.class.getName());
    
    // temporary
    private static final int MAX_MEMO_COUNT = 5000;   // Sitemap.xml max count...
    private static final int DEFAULT_MAX_MEMO_COUNT = 2500;
    // temporary

    private UserWebService userWebService = null;
    private CronJobWebService cronJobWebService = null;
    // ...

    private SitemapHelper() {}

    // Initialization-on-demand holder.
    private static final class SitemapHelperHolder
    {
        private static final SitemapHelper INSTANCE = new SitemapHelper();
    }

    // Singleton method
    public static SitemapHelper getInstance()
    {
        return SitemapHelperHolder.INSTANCE;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private CronJobWebService getCronJobWebService()
    {
        if(cronJobWebService == null) {
            cronJobWebService = new CronJobWebService();
        }
        return cronJobWebService;
    }

    public List<CronJobJsBean> findRecentCronJobs()
    {
        return findRecentCronJobs(DEFAULT_MAX_MEMO_COUNT);
    }

    public List<CronJobJsBean> findRecentCronJobs(int maxCount)
    {
        List<CronJobJsBean> cronJobs = null;
        try {
            String filter = null;
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = maxCount;   // TBD: Validation??? ( maxCount < MAX_MEMO_COUNT ???? )
            cronJobs = getCronJobWebService().findCronJobs(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find cronJobs.", e);
            return null;
        }

        return cronJobs;
    }

    
    // Format the timestamp to W3C date format: "yyyy-mm-dd".
    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date(time));
        return date;
    }
    
}
