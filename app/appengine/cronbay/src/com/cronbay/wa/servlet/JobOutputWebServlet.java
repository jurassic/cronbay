package com.cronbay.wa.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

//import com.cronbay.ws.JobOutput;
//import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.core.StatusCode;
import com.cronbay.fe.Validateable;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.JobOutputJsBean;
import com.cronbay.fe.bean.GaeAppStructJsBean;
import com.cronbay.wa.service.JobOutputWebService;


public class JobOutputWebServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(JobOutputWebServlet.class.getName());
    private static final String FORMBEAN_CLASS_NAME = "com.cronbay.form.bean.JobOutputFormBean";


    // TBD: Is this safe for concurrent calls???
    private JobOutputWebService mService = null;
    private JobOutputWebService getService()
    {
        if(mService == null) {
            mService = new JobOutputWebService();
        }
        return mService;
    }

    private Class<?> formBeanClass = null;
    private void loadFormBeanClass()
    {
        try {
            formBeanClass = Class.forName(FORMBEAN_CLASS_NAME);
        } catch(Exception e) {
            // ignore
            log.log(Level.INFO, FORMBEAN_CLASS_NAME + " does not exist.", e);
        }
    }


    @Override
    public void init() throws ServletException
    {
        super.init();
        loadFormBeanClass();
    }

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        loadFormBeanClass();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doGet() called.");

        // TBD: Check Accept header. Etc...
        String guid = null;
        String pathInfo = req.getPathInfo();
        if(pathInfo == null || pathInfo.isEmpty()) {
            // Get list???
            // For now, use the query param. ???
            guid = req.getParameter("guid");
        } else {
            if(pathInfo.startsWith("/")) {
                guid = pathInfo.substring(1);
            } else {
                guid = pathInfo;
            }
        }
        if(guid != null && !guid.isEmpty()) {
            JobOutputJsBean bean = null;
            try {
                bean = getService().getJobOutput(guid);
                resp.setContentType("application/json;charset=UTF-8");
                // resp.setHeader("Cache-Control", "no-cache");     // ?????
                resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
                PrintWriter out = resp.getWriter();
                out.write(bean.toJsonString());
            } catch (WebException e) {
                log.log(Level.WARNING, "Failed.", e);
                resp.setStatus(StatusCode.NOT_FOUND);  // ???
            }
        } else {
            // If guid is not set, fetch the list.

            String filter = req.getParameter("filter");
            String ordering = req.getParameter("ordering");
            Long offset = null;
            String strOffset = req.getParameter("offset");
            if(strOffset != null) {
                try {
                    offset = Long.parseLong(strOffset);
                } catch(Exception e) {
                    // ignore
                    log.log(Level.INFO, "Invalid param: offset = " + strOffset, e);
                }
            }
            Integer count = null;
            String strCount = req.getParameter("count");
            if(strCount != null) {
                try {
                    count = Integer.parseInt(strCount);
                } catch(Exception e) {
                    // ignore
                    log.log(Level.INFO, "Invalid param: count = " + strCount, e);
                }
            }
            
            List<JobOutputJsBean> beans = null;
            try {
                beans = getService().findJobOutputs(filter, ordering, null, null, null, null, offset, count);
            } catch (WebException e) {
                log.log(Level.WARNING, "Failed to get JobOutputJsBean list.", e);
            }
            
            if(beans != null) {
                resp.setContentType("application/json;charset=UTF-8");
                // resp.setHeader("Cache-Control", "no-cache");     // ?????
                resp.setStatus(StatusCode.OK);

                StringBuilder sb = new StringBuilder();
                sb.append("[");
                for(int i=0; i<beans.size(); i++) {
                    JobOutputJsBean b = beans.get(i);
                    sb.append(b.toJsonString());
                    if(i < beans.size()-1) {
                        sb.append(",");
                    }
                }
                sb.append("]");
                String jsonStr = sb.toString(); 
                PrintWriter out = resp.getWriter();
                out.write(jsonStr);                
            } else {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doPost() called.");

        // TBD: Check Accept header. Etc...
        boolean validated = true;
        boolean succeeded = false;
        JobOutputJsBean outBean = null;
        try {
            BufferedReader reader = req.getReader();
            String jsonStr = reader.readLine();
            JobOutputJsBean inBean = null;
            if(formBeanClass != null) {
                try {
                    inBean = (JobOutputJsBean) formBeanClass.getMethod("fromJsonString", String.class).invoke(null, jsonStr);
                    //inBean = (JobOutputJsBean) formBeanClass.getConstructor(String.class).newInstance(jsonStr);
                } catch(Exception e1) {
                    log.log(Level.WARNING, FORMBEAN_CLASS_NAME + ".fromJsonString() failed.", e1);
                }
            }
            if(inBean == null) {
                inBean = JobOutputJsBean.fromJsonString(jsonStr);                
            }
            if(inBean instanceof Validateable) {
                validated = ((Validateable) inBean).validate();
            }
            if(validated == true) {
                try {
                    outBean = getService().constructJobOutput(inBean);
                    // String guid = getService().createJobOutput(inBean);
                    succeeded = true;
                } catch (WebException e1) {
                    log.log(Level.WARNING, "Server error.", e1);
                }
            }
            if(outBean == null) {
                // ???
                outBean = inBean;
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error.", e);
        }
        
        if(succeeded == true) {
            resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            resp.setContentType("application/json;charset=UTF-8");
            // resp.setHeader("Cache-Control", "no-cache");     // ?????

            // Location header???
            //String guid = outBean.getGuid();

            PrintWriter out = resp.getWriter();
            out.write(outBean.toJsonString());
        } else {
            if(outBean == null) {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
            } else {
                if(validated == false) {
                    resp.setStatus(StatusCode.BAD_REQUEST);
                } else {
                    resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ???
                    if(outBean instanceof Validateable) {
                        ((Validateable) outBean).addError(Validateable.FIELD_BEANWIDE, "Server error.");
                    }
                }
                PrintWriter out = resp.getWriter();
                out.write(outBean.toJsonString());
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doPut() called.");

        // TBD: Check Accept header. Etc...
        String guid = null;
        String pathInfo = req.getPathInfo();
        if(pathInfo == null || pathInfo.isEmpty()) {
            // Get list???
            // For now, use the query param. ???
            guid = req.getParameter("guid");
        } else {
            if(pathInfo.startsWith("/")) {
                guid = pathInfo.substring(1);
            } else {
                guid = pathInfo;
            }
        }
        // TBD: guid should not be null!!!

        boolean validated = true;
        boolean succeeded = false;
        JobOutputJsBean outBean = null;
        if(guid != null && !guid.isEmpty()) {
            try {
                BufferedReader reader = req.getReader();
                String jsonStr = reader.readLine();
                JobOutputJsBean inBean = null;
                if(formBeanClass != null) {
                    try {
                        inBean = (JobOutputJsBean) formBeanClass.getMethod("fromJsonString", String.class).invoke(null, jsonStr);
                        //inBean = (JobOutputJsBean) formBeanClass.getConstructor(String.class).newInstance(jsonStr);
                    } catch(Exception e1) {
                        log.log(Level.WARNING, FORMBEAN_CLASS_NAME + ".fromJsonString() failed.", e1);
                    }
                }
                if(inBean == null) {
                    inBean = JobOutputJsBean.fromJsonString(jsonStr);                
                }
                String beanGuid = inBean.getGuid();
                if(guid.equals(beanGuid)) {
                    if(inBean instanceof Validateable) {
                        validated = ((Validateable) inBean).validate();
                    }
                } else {
                    log.log(Level.WARNING, "Inconsistent input. pathGuid = " + guid + "; beanGuid = " + beanGuid);
                    validated = false;
                    if(inBean instanceof Validateable) {
                        ((Validateable) inBean).addError("guid", "Inconsistent input.");
                    }
                }
                if(validated == true) {
                    try {
                        outBean = getService().refreshJobOutput(inBean);
                        // Boolean suc = getService().updateJobOutput(inBean);
                        succeeded = true;
                    } catch (WebException e1) {
                        log.log(Level.WARNING, "Server error.", e1);
                    }
                }
                if(outBean == null) {
                    // ???
                    outBean = inBean;
                }
            } catch (Exception e) {
                log.log(Level.WARNING, "Unknown error.", e);
            }
        } else {
            // ???
            log.warning("Required arg, guid, is missing.");
        }

        if(succeeded == true) {
            resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            resp.setContentType("application/json;charset=UTF-8");
            // resp.setHeader("Cache-Control", "no-cache");     // ?????

            // Location header???
            //String guid = outBean.getGuid();

            PrintWriter out = resp.getWriter();
            out.write(outBean.toJsonString());
        } else {
            if(outBean == null) {
                if(guid == null || guid.isEmpty()) {
                    resp.setStatus(StatusCode.BAD_REQUEST);  // ???
                } else {
                    resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
                }
            } else {
                if(validated == false) {
                    resp.setStatus(StatusCode.BAD_REQUEST);
                } else {
                    resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ???
                    if(outBean instanceof Validateable) {
                        ((Validateable) outBean).addError(Validateable.FIELD_BEANWIDE, "Server error.");
                    }
                }
                PrintWriter out = resp.getWriter();
                out.write(outBean.toJsonString());
            }
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doDelete() called.");

        // TBD: Check Accept header. Etc...
        String guid = null;
        String pathInfo = req.getPathInfo();
        if(pathInfo == null || pathInfo.isEmpty()) {
            // Get list???
            // For now, use the query param. ???
            guid = req.getParameter("guid");
        } else {
            if(pathInfo.startsWith("/")) {
                guid = pathInfo.substring(1);
            } else {
                guid = pathInfo;
            }
        }
        // TBD: guid should not be null!!!

        if(guid != null && !guid.isEmpty()) {
            // TBD:
            Boolean suc = null;
            try {
                suc = getService().deleteJobOutput(guid);
            } catch (WebException e) {
                log.log(Level.WARNING, "Failed.", e);
            }
            if(suc != null && suc.equals(Boolean.TRUE)) {
                resp.setStatus(StatusCode.OK);
            } else {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
            }
        } else {
            log.warning("Required arg, guid, is missing.");
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
        }
    }
    
    
}
