package com.cronbay.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.af.bean.NotificationStructBean;
import com.cronbay.fe.bean.NotificationStructJsBean;


public class NotificationStructWebUtil
{
    private static final Logger log = Logger.getLogger(NotificationStructWebUtil.class.getName());

    // Static methods only.
    private NotificationStructWebUtil() {}
    

    public static NotificationStructJsBean convertNotificationStructToJsBean(NotificationStruct notificationStruct)
    {
        NotificationStructJsBean jsBean = new NotificationStructJsBean();
        if(notificationStruct != null) {
            jsBean.setPreferredMode(notificationStruct.getPreferredMode());
            jsBean.setMobileNumber(notificationStruct.getMobileNumber());
            jsBean.setEmailAddress(notificationStruct.getEmailAddress());
            jsBean.setCallbackUrl(notificationStruct.getCallbackUrl());
            jsBean.setNote(notificationStruct.getNote());
        }
        return jsBean;
    }

    public static NotificationStruct convertNotificationStructJsBeanToBean(NotificationStructJsBean jsBean)
    {
        NotificationStructBean notificationStruct = new NotificationStructBean();
        if(jsBean != null) {
            notificationStruct.setPreferredMode(jsBean.getPreferredMode());
            notificationStruct.setMobileNumber(jsBean.getMobileNumber());
            notificationStruct.setEmailAddress(jsBean.getEmailAddress());
            notificationStruct.setCallbackUrl(jsBean.getCallbackUrl());
            notificationStruct.setNote(jsBean.getNote());
        }
        return notificationStruct;
    }

}
