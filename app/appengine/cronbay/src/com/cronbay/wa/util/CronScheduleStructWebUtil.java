package com.cronbay.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.af.bean.CronScheduleStructBean;
import com.cronbay.fe.bean.CronScheduleStructJsBean;


public class CronScheduleStructWebUtil
{
    private static final Logger log = Logger.getLogger(CronScheduleStructWebUtil.class.getName());

    // Static methods only.
    private CronScheduleStructWebUtil() {}
    

    public static CronScheduleStructJsBean convertCronScheduleStructToJsBean(CronScheduleStruct cronScheduleStruct)
    {
        CronScheduleStructJsBean jsBean = new CronScheduleStructJsBean();
        if(cronScheduleStruct != null) {
            jsBean.setType(cronScheduleStruct.getType());
            jsBean.setSchedule(cronScheduleStruct.getSchedule());
            jsBean.setTimezone(cronScheduleStruct.getTimezone());
            jsBean.setMaxIterations(cronScheduleStruct.getMaxIterations());
            jsBean.setRepeatInterval(cronScheduleStruct.getRepeatInterval());
            jsBean.setFirtRunTime(cronScheduleStruct.getFirtRunTime());
            jsBean.setStartTime(cronScheduleStruct.getStartTime());
            jsBean.setEndTime(cronScheduleStruct.getEndTime());
            jsBean.setNote(cronScheduleStruct.getNote());
        }
        return jsBean;
    }

    public static CronScheduleStruct convertCronScheduleStructJsBeanToBean(CronScheduleStructJsBean jsBean)
    {
        CronScheduleStructBean cronScheduleStruct = new CronScheduleStructBean();
        if(jsBean != null) {
            cronScheduleStruct.setType(jsBean.getType());
            cronScheduleStruct.setSchedule(jsBean.getSchedule());
            cronScheduleStruct.setTimezone(jsBean.getTimezone());
            cronScheduleStruct.setMaxIterations(jsBean.getMaxIterations());
            cronScheduleStruct.setRepeatInterval(jsBean.getRepeatInterval());
            cronScheduleStruct.setFirtRunTime(jsBean.getFirtRunTime());
            cronScheduleStruct.setStartTime(jsBean.getStartTime());
            cronScheduleStruct.setEndTime(jsBean.getEndTime());
            cronScheduleStruct.setNote(jsBean.getNote());
        }
        return cronScheduleStruct;
    }

}
