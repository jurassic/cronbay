package com.cronbay.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;

import com.cronbay.ws.RestRequestStruct;
import com.cronbay.af.bean.RestRequestStructBean;
import com.cronbay.fe.bean.RestRequestStructJsBean;


public class RestRequestStructWebUtil
{
    private static final Logger log = Logger.getLogger(RestRequestStructWebUtil.class.getName());

    // Static methods only.
    private RestRequestStructWebUtil() {}
    

    public static RestRequestStructJsBean convertRestRequestStructToJsBean(RestRequestStruct restRequestStruct)
    {
        RestRequestStructJsBean jsBean = new RestRequestStructJsBean();
        if(restRequestStruct != null) {
            jsBean.setServiceName(restRequestStruct.getServiceName());
            jsBean.setServiceUrl(restRequestStruct.getServiceUrl());
            jsBean.setRequestMethod(restRequestStruct.getRequestMethod());
            jsBean.setRequestUrl(restRequestStruct.getRequestUrl());
            jsBean.setTargetEntity(restRequestStruct.getTargetEntity());
            jsBean.setQueryString(restRequestStruct.getQueryString());
            jsBean.setQueryParams(restRequestStruct.getQueryParams());
            jsBean.setInputFormat(restRequestStruct.getInputFormat());
            jsBean.setInputContent(restRequestStruct.getInputContent());
            jsBean.setOutputFormat(restRequestStruct.getOutputFormat());
            jsBean.setMaxRetries(restRequestStruct.getMaxRetries());
            jsBean.setRetryInterval(restRequestStruct.getRetryInterval());
            jsBean.setNote(restRequestStruct.getNote());
        }
        return jsBean;
    }

    public static RestRequestStruct convertRestRequestStructJsBeanToBean(RestRequestStructJsBean jsBean)
    {
        RestRequestStructBean restRequestStruct = new RestRequestStructBean();
        if(jsBean != null) {
            restRequestStruct.setServiceName(jsBean.getServiceName());
            restRequestStruct.setServiceUrl(jsBean.getServiceUrl());
            restRequestStruct.setRequestMethod(jsBean.getRequestMethod());
            restRequestStruct.setRequestUrl(jsBean.getRequestUrl());
            restRequestStruct.setTargetEntity(jsBean.getTargetEntity());
            restRequestStruct.setQueryString(jsBean.getQueryString());
            restRequestStruct.setQueryParams(jsBean.getQueryParams());
            restRequestStruct.setInputFormat(jsBean.getInputFormat());
            restRequestStruct.setInputContent(jsBean.getInputContent());
            restRequestStruct.setOutputFormat(jsBean.getOutputFormat());
            restRequestStruct.setMaxRetries(jsBean.getMaxRetries());
            restRequestStruct.setRetryInterval(jsBean.getRetryInterval());
            restRequestStruct.setNote(jsBean.getNote());
        }
        return restRequestStruct;
    }

}
