package com.cronbay.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.af.bean.CronJobBean;
import com.cronbay.af.service.CronJobService;
import com.cronbay.af.service.manager.ServiceManager;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.NotificationStructJsBean;
import com.cronbay.fe.bean.RestRequestStructJsBean;
import com.cronbay.fe.bean.GaeAppStructJsBean;
import com.cronbay.fe.bean.ReferrerInfoStructJsBean;
import com.cronbay.fe.bean.CronScheduleStructJsBean;
import com.cronbay.fe.bean.CronJobJsBean;
import com.cronbay.wa.util.NotificationStructWebUtil;
import com.cronbay.wa.util.RestRequestStructWebUtil;
import com.cronbay.wa.util.GaeAppStructWebUtil;
import com.cronbay.wa.util.ReferrerInfoStructWebUtil;
import com.cronbay.wa.util.CronScheduleStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class CronJobWebService // implements CronJobService
{
    private static final Logger log = Logger.getLogger(CronJobWebService.class.getName());
     
    // Af service interface.
    private CronJobService mService = null;

    public CronJobWebService()
    {
        this(ServiceManager.getCronJobService());
    }
    public CronJobWebService(CronJobService service)
    {
        mService = service;
    }
    
    private CronJobService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getCronJobService();
        }
        return mService;
    }
    
    
    public CronJobJsBean getCronJob(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            CronJob cronJob = getService().getCronJob(guid);
            CronJobJsBean bean = convertCronJobToJsBean(cronJob);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getCronJob(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getCronJob(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<CronJobJsBean> getCronJobs(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<CronJobJsBean> jsBeans = new ArrayList<CronJobJsBean>();
            List<CronJob> cronJobs = getService().getCronJobs(guids);
            if(cronJobs != null) {
                for(CronJob cronJob : cronJobs) {
                    jsBeans.add(convertCronJobToJsBean(cronJob));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<CronJobJsBean> getAllCronJobs() throws WebException
    {
        return getAllCronJobs(null, null, null);
    }

    public List<CronJobJsBean> getAllCronJobs(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<CronJobJsBean> jsBeans = new ArrayList<CronJobJsBean>();
            List<CronJob> cronJobs = getService().getAllCronJobs(ordering, offset, count);
            if(cronJobs != null) {
                for(CronJob cronJob : cronJobs) {
                    jsBeans.add(convertCronJobToJsBean(cronJob));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllCronJobKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllCronJobKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<CronJobJsBean> findCronJobs(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findCronJobs(filter, ordering, params, values, null, null, null, null);
    }

    public List<CronJobJsBean> findCronJobs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<CronJobJsBean> jsBeans = new ArrayList<CronJobJsBean>();
            List<CronJob> cronJobs = getService().findCronJobs(filter, ordering, params, values, grouping, unique, offset, count);
            if(cronJobs != null) {
                for(CronJob cronJob : cronJobs) {
                    jsBeans.add(convertCronJobToJsBean(cronJob));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findCronJobKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findCronJobKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createCronJob(String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStructJsBean restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStructJsBean notificationPref, ReferrerInfoStructJsBean referrerInfo, CronScheduleStructJsBean cronSchedule, Long nextRunTime) throws WebException
    {
        try {
            return getService().createCronJob(managerApp, appAcl, GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, user, jobId, title, description, originJob, RestRequestStructWebUtil.convertRestRequestStructJsBeanToBean(restRequest), permalink, shortlink, status, jobStatus, extra, note, alert, NotificationStructWebUtil.convertNotificationStructJsBeanToBean(notificationPref), ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(referrerInfo), CronScheduleStructWebUtil.convertCronScheduleStructJsBeanToBean(cronSchedule), nextRunTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createCronJob(String jsonStr) throws WebException
    {
        return createCronJob(CronJobJsBean.fromJsonString(jsonStr));
    }

    public String createCronJob(CronJobJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            CronJob cronJob = convertCronJobJsBeanToBean(jsBean);
            return getService().createCronJob(cronJob);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public CronJobJsBean constructCronJob(String jsonStr) throws WebException
    {
        return constructCronJob(CronJobJsBean.fromJsonString(jsonStr));
    }

    public CronJobJsBean constructCronJob(CronJobJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            CronJob cronJob = convertCronJobJsBeanToBean(jsBean);
            cronJob = getService().constructCronJob(cronJob);
            jsBean = convertCronJobToJsBean(cronJob);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateCronJob(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStructJsBean restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStructJsBean notificationPref, ReferrerInfoStructJsBean referrerInfo, CronScheduleStructJsBean cronSchedule, Long nextRunTime) throws WebException
    {
        try {
            return getService().updateCronJob(guid, managerApp, appAcl, GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, user, jobId, title, description, originJob, RestRequestStructWebUtil.convertRestRequestStructJsBeanToBean(restRequest), permalink, shortlink, status, jobStatus, extra, note, alert, NotificationStructWebUtil.convertNotificationStructJsBeanToBean(notificationPref), ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(referrerInfo), CronScheduleStructWebUtil.convertCronScheduleStructJsBeanToBean(cronSchedule), nextRunTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateCronJob(String jsonStr) throws WebException
    {
        return updateCronJob(CronJobJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateCronJob(CronJobJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            CronJob cronJob = convertCronJobJsBeanToBean(jsBean);
            return getService().updateCronJob(cronJob);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public CronJobJsBean refreshCronJob(String jsonStr) throws WebException
    {
        return refreshCronJob(CronJobJsBean.fromJsonString(jsonStr));
    }

    public CronJobJsBean refreshCronJob(CronJobJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            CronJob cronJob = convertCronJobJsBeanToBean(jsBean);
            cronJob = getService().refreshCronJob(cronJob);
            jsBean = convertCronJobToJsBean(cronJob);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteCronJob(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteCronJob(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteCronJob(CronJobJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            CronJob cronJob = convertCronJobJsBeanToBean(jsBean);
            return getService().deleteCronJob(cronJob);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteCronJobs(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteCronJobs(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static CronJobJsBean convertCronJobToJsBean(CronJob cronJob)
    {
        CronJobJsBean jsBean = new CronJobJsBean();
        if(cronJob != null) {
            jsBean.setGuid(cronJob.getGuid());
            jsBean.setManagerApp(cronJob.getManagerApp());
            jsBean.setAppAcl(cronJob.getAppAcl());
            jsBean.setGaeApp(GaeAppStructWebUtil.convertGaeAppStructToJsBean(cronJob.getGaeApp()));
            jsBean.setOwnerUser(cronJob.getOwnerUser());
            jsBean.setUserAcl(cronJob.getUserAcl());
            jsBean.setUser(cronJob.getUser());
            jsBean.setJobId(cronJob.getJobId());
            jsBean.setTitle(cronJob.getTitle());
            jsBean.setDescription(cronJob.getDescription());
            jsBean.setOriginJob(cronJob.getOriginJob());
            jsBean.setRestRequest(RestRequestStructWebUtil.convertRestRequestStructToJsBean(cronJob.getRestRequest()));
            jsBean.setPermalink(cronJob.getPermalink());
            jsBean.setShortlink(cronJob.getShortlink());
            jsBean.setStatus(cronJob.getStatus());
            jsBean.setJobStatus(cronJob.getJobStatus());
            jsBean.setExtra(cronJob.getExtra());
            jsBean.setNote(cronJob.getNote());
            jsBean.setAlert(cronJob.isAlert());
            jsBean.setNotificationPref(NotificationStructWebUtil.convertNotificationStructToJsBean(cronJob.getNotificationPref()));
            jsBean.setReferrerInfo(ReferrerInfoStructWebUtil.convertReferrerInfoStructToJsBean(cronJob.getReferrerInfo()));
            jsBean.setCronSchedule(CronScheduleStructWebUtil.convertCronScheduleStructToJsBean(cronJob.getCronSchedule()));
            jsBean.setNextRunTime(cronJob.getNextRunTime());
            jsBean.setCreatedTime(cronJob.getCreatedTime());
            jsBean.setModifiedTime(cronJob.getModifiedTime());
        }
        return jsBean;
    }

    public static CronJob convertCronJobJsBeanToBean(CronJobJsBean jsBean)
    {
        CronJobBean cronJob = new CronJobBean();
        if(jsBean != null) {
            cronJob.setGuid(jsBean.getGuid());
            cronJob.setManagerApp(jsBean.getManagerApp());
            cronJob.setAppAcl(jsBean.getAppAcl());
            cronJob.setGaeApp(GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(jsBean.getGaeApp()));
            cronJob.setOwnerUser(jsBean.getOwnerUser());
            cronJob.setUserAcl(jsBean.getUserAcl());
            cronJob.setUser(jsBean.getUser());
            cronJob.setJobId(jsBean.getJobId());
            cronJob.setTitle(jsBean.getTitle());
            cronJob.setDescription(jsBean.getDescription());
            cronJob.setOriginJob(jsBean.getOriginJob());
            cronJob.setRestRequest(RestRequestStructWebUtil.convertRestRequestStructJsBeanToBean(jsBean.getRestRequest()));
            cronJob.setPermalink(jsBean.getPermalink());
            cronJob.setShortlink(jsBean.getShortlink());
            cronJob.setStatus(jsBean.getStatus());
            cronJob.setJobStatus(jsBean.getJobStatus());
            cronJob.setExtra(jsBean.getExtra());
            cronJob.setNote(jsBean.getNote());
            cronJob.setAlert(jsBean.isAlert());
            cronJob.setNotificationPref(NotificationStructWebUtil.convertNotificationStructJsBeanToBean(jsBean.getNotificationPref()));
            cronJob.setReferrerInfo(ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            cronJob.setCronSchedule(CronScheduleStructWebUtil.convertCronScheduleStructJsBeanToBean(jsBean.getCronSchedule()));
            cronJob.setNextRunTime(jsBean.getNextRunTime());
            cronJob.setCreatedTime(jsBean.getCreatedTime());
            cronJob.setModifiedTime(jsBean.getModifiedTime());
        }
        return cronJob;
    }

}
