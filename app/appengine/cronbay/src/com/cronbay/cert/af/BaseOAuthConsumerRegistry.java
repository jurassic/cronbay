package com.cronbay.cert.af;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseOAuthConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseOAuthConsumerRegistry.class.getName());

    // Consumer key-secret map.
    private Map<String, String> consumerSecretMap;

    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD:
        consumerSecretMap.put("70d4368d-63ab-45c8-b6a7-fbaaa6e0254c", "b4635d82-e39a-4e84-bb00-03184f2dc4fa");  // CronBayApp
        consumerSecretMap.put("0e2d42f9-8dd2-4f84-8709-f0b99bae0959", "aa486a15-2e33-4146-b3d6-6682f5f63d64");  // CronBayApp + CronBayWeb
        consumerSecretMap.put("be08ff70-62c3-4597-8f55-b59ea6a7eb3d", "085134ca-e9d1-4a36-a2da-9cff6cf0c6f8");  // CronBayApp + QueryClient
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
