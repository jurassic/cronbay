package com.cronbay.util;

import java.util.logging.Logger;


// Temporary
public final class CronJobUtil
{
    private static final Logger log = Logger.getLogger(CronJobUtil.class.getName());

    private CronJobUtil() {}
    
    
//    // TBD: This should be read from config, etc....
//    public static String getDefaultCronJobType()
//    {
//        // temporary
//        return CronJobContentType.ITEM_TYPE_TEXT;
//    }

    // TBD:
    public static String createDefaultCronJobTitle()
    {
        // temporary
        String title = "CronJob " + System.currentTimeMillis();
        return title;
    }

    // TBD:
    public static String createHtmlPageTitle(String memoTitle)
    {
        // temporary
        if(memoTitle == null) {
            memoTitle = "";
        }
        String title = "CronJob - " + memoTitle;
        return title;
    }
    
    
}
