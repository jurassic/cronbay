package com.cronbay.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import java.util.List;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.fe.Validateable;
import com.cronbay.fe.core.StringEscapeUtil;
import com.cronbay.fe.bean.GaeAppStructJsBean;
import com.cronbay.fe.bean.JobOutputJsBean;


// Place holder...
public class JobOutputFormBean extends JobOutputJsBean implements Serializable, Cloneable, Validateable  //, JobOutput
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(JobOutputFormBean.class.getName());

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public JobOutputFormBean()
    {
        super();
    }
    public JobOutputFormBean(String guid)
    {
       super(guid);
    }
    public JobOutputFormBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
    }
    public JobOutputFormBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime, Long createdTime, Long modifiedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime, createdTime, modifiedTime);
    }
    public JobOutputFormBean(JobOutputJsBean bean)
    {
        super(bean);
    }

    public static JobOutputFormBean fromJsonString(String jsonStr)
    {
        JobOutputFormBean bean = null;
        try {
            // TBD:
            ObjectMapper mapper = new ObjectMapper();   // can reuse, share globally
            bean = mapper.readValue(jsonStr, JobOutputFormBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getManagerApp() == null) {
//            addError("managerApp", "managerApp is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAppAcl() == null) {
//            addError("appAcl", "appAcl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getGaeApp() == null) {
//            addError("gaeApp", "gaeApp is null");
//            allOK = false;
//        } else {
//            GaeAppStructJsBean gaeApp = getGaeApp();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getOwnerUser() == null) {
//            addError("ownerUser", "ownerUser is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUserAcl() == null) {
//            addError("userAcl", "userAcl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUser() == null) {
//            addError("user", "user is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCronJob() == null) {
//            addError("cronJob", "cronJob is null");
//            allOK = false;
//        }
//        // TBD
//        if(getPreviousRun() == null) {
//            addError("previousRun", "previousRun is null");
//            allOK = false;
//        }
//        // TBD
//        if(getPreviousTry() == null) {
//            addError("previousTry", "previousTry is null");
//            allOK = false;
//        }
//        // TBD
//        if(getResponseCode() == null) {
//            addError("responseCode", "responseCode is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOutputFormat() == null) {
//            addError("outputFormat", "outputFormat is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOutputText() == null) {
//            addError("outputText", "outputText is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOutputData() == null) {
//            addError("outputData", "outputData is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOutputHash() == null) {
//            addError("outputHash", "outputHash is null");
//            allOK = false;
//        }
//        // TBD
//        if(getPermalink() == null) {
//            addError("permalink", "permalink is null");
//            allOK = false;
//        }
//        // TBD
//        if(getShortlink() == null) {
//            addError("shortlink", "shortlink is null");
//            allOK = false;
//        }
//        // TBD
//        if(getResult() == null) {
//            addError("result", "result is null");
//            allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExtra() == null) {
//            addError("extra", "extra is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNote() == null) {
//            addError("note", "note is null");
//            allOK = false;
//        }
//        // TBD
//        if(getRetryCounter() == null) {
//            addError("retryCounter", "retryCounter is null");
//            allOK = false;
//        }
//        // TBD
//        if(getScheduledTime() == null) {
//            addError("scheduledTime", "scheduledTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getProcessedTime() == null) {
//            addError("processedTime", "processedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        JobOutputFormBean cloned = new JobOutputFormBean((JobOutputJsBean) super.clone());
        return cloned;
    }

}
