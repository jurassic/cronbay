package com.cronbay.af.proxy;

public abstract class AbstractProxyFactory
{
    public abstract ApiConsumerServiceProxy getApiConsumerServiceProxy();
    public abstract UserServiceProxy getUserServiceProxy();
    public abstract CronJobServiceProxy getCronJobServiceProxy();
    public abstract JobOutputServiceProxy getJobOutputServiceProxy();
    public abstract ServiceInfoServiceProxy getServiceInfoServiceProxy();
    public abstract FiveTenServiceProxy getFiveTenServiceProxy();
}
