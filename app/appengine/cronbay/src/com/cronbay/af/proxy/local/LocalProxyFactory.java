package com.cronbay.af.proxy.local;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.ApiConsumerServiceProxy;
import com.cronbay.af.proxy.UserServiceProxy;
import com.cronbay.af.proxy.CronJobServiceProxy;
import com.cronbay.af.proxy.JobOutputServiceProxy;
import com.cronbay.af.proxy.ServiceInfoServiceProxy;
import com.cronbay.af.proxy.FiveTenServiceProxy;

public class LocalProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(LocalProxyFactory.class.getName());

    private LocalProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class LocalProxyFactoryHolder
    {
        private static final LocalProxyFactory INSTANCE = new LocalProxyFactory();
    }

    // Singleton method
    public static LocalProxyFactory getInstance()
    {
        return LocalProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new LocalApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new LocalUserServiceProxy();
    }

    @Override
    public CronJobServiceProxy getCronJobServiceProxy()
    {
        return new LocalCronJobServiceProxy();
    }

    @Override
    public JobOutputServiceProxy getJobOutputServiceProxy()
    {
        return new LocalJobOutputServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new LocalServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new LocalFiveTenServiceProxy();
    }

}
