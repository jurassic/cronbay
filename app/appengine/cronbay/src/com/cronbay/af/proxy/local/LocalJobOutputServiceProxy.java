package com.cronbay.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.service.JobOutputService;
import com.cronbay.af.proxy.JobOutputServiceProxy;

public class LocalJobOutputServiceProxy extends BaseLocalServiceProxy implements JobOutputServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalJobOutputServiceProxy.class.getName());

    public LocalJobOutputServiceProxy()
    {
    }

    @Override
    public JobOutput getJobOutput(String guid) throws BaseException
    {
        return getJobOutputService().getJobOutput(guid);
    }

    @Override
    public Object getJobOutput(String guid, String field) throws BaseException
    {
        return getJobOutputService().getJobOutput(guid, field);       
    }

    @Override
    public List<JobOutput> getJobOutputs(List<String> guids) throws BaseException
    {
        return getJobOutputService().getJobOutputs(guids);
    }

    @Override
    public List<JobOutput> getAllJobOutputs() throws BaseException
    {
        return getAllJobOutputs(null, null, null);
    }

    @Override
    public List<JobOutput> getAllJobOutputs(String ordering, Long offset, Integer count) throws BaseException
    {
        return getJobOutputService().getAllJobOutputs(ordering, offset, count);
    }

    @Override
    public List<String> getAllJobOutputKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getJobOutputService().getAllJobOutputKeys(ordering, offset, count);
    }

    @Override
    public List<JobOutput> findJobOutputs(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findJobOutputs(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<JobOutput> findJobOutputs(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getJobOutputService().findJobOutputs(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findJobOutputKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getJobOutputService().findJobOutputKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getJobOutputService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createJobOutput(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws BaseException
    {
        return getJobOutputService().createJobOutput(managerApp, appAcl, gaeApp, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
    }

    @Override
    public String createJobOutput(JobOutput jobOutput) throws BaseException
    {
        return getJobOutputService().createJobOutput(jobOutput);
    }

    @Override
    public Boolean updateJobOutput(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws BaseException
    {
        return getJobOutputService().updateJobOutput(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
    }

    @Override
    public Boolean updateJobOutput(JobOutput jobOutput) throws BaseException
    {
        return getJobOutputService().updateJobOutput(jobOutput);
    }

    @Override
    public Boolean deleteJobOutput(String guid) throws BaseException
    {
        return getJobOutputService().deleteJobOutput(guid);
    }

    @Override
    public Boolean deleteJobOutput(JobOutput jobOutput) throws BaseException
    {
        return getJobOutputService().deleteJobOutput(jobOutput);
    }

    @Override
    public Long deleteJobOutputs(String filter, String params, List<String> values) throws BaseException
    {
        return getJobOutputService().deleteJobOutputs(filter, params, values);
    }

}
