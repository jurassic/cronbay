package com.cronbay.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.ServiceInfo;
import com.cronbay.ws.service.ServiceInfoService;
import com.cronbay.af.proxy.ServiceInfoServiceProxy;

public class LocalServiceInfoServiceProxy extends BaseLocalServiceProxy implements ServiceInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalServiceInfoServiceProxy.class.getName());

    public LocalServiceInfoServiceProxy()
    {
    }

    @Override
    public ServiceInfo getServiceInfo(String guid) throws BaseException
    {
        return getServiceInfoService().getServiceInfo(guid);
    }

    @Override
    public Object getServiceInfo(String guid, String field) throws BaseException
    {
        return getServiceInfoService().getServiceInfo(guid, field);       
    }

    @Override
    public List<ServiceInfo> getServiceInfos(List<String> guids) throws BaseException
    {
        return getServiceInfoService().getServiceInfos(guids);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos() throws BaseException
    {
        return getAllServiceInfos(null, null, null);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getServiceInfoService().getAllServiceInfos(ordering, offset, count);
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getServiceInfoService().getAllServiceInfoKeys(ordering, offset, count);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findServiceInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getServiceInfoService().findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getServiceInfoService().findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getServiceInfoService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createServiceInfo(String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        return getServiceInfoService().createServiceInfo(title, content, type, status, scheduledTime);
    }

    @Override
    public String createServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        return getServiceInfoService().createServiceInfo(serviceInfo);
    }

    @Override
    public Boolean updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        return getServiceInfoService().updateServiceInfo(guid, title, content, type, status, scheduledTime);
    }

    @Override
    public Boolean updateServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        return getServiceInfoService().updateServiceInfo(serviceInfo);
    }

    @Override
    public Boolean deleteServiceInfo(String guid) throws BaseException
    {
        return getServiceInfoService().deleteServiceInfo(guid);
    }

    @Override
    public Boolean deleteServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        return getServiceInfoService().deleteServiceInfo(serviceInfo);
    }

    @Override
    public Long deleteServiceInfos(String filter, String params, List<String> values) throws BaseException
    {
        return getServiceInfoService().deleteServiceInfos(filter, params, values);
    }

}
