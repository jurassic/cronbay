package com.cronbay.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.ws.User;
import com.cronbay.ws.service.UserService;
import com.cronbay.af.proxy.UserServiceProxy;

public class LocalUserServiceProxy extends BaseLocalServiceProxy implements UserServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserServiceProxy.class.getName());

    public LocalUserServiceProxy()
    {
    }

    @Override
    public User getUser(String guid) throws BaseException
    {
        return getUserService().getUser(guid);
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        return getUserService().getUser(guid, field);       
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        return getUserService().getUsers(guids);
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserService().getAllUsers(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getUserService().getAllUserKeys(ordering, offset, count);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserService().findUsers(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getUserService().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime) throws BaseException
    {
        return getUserService().createUser(managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime);
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        return getUserService().createUser(user);
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime) throws BaseException
    {
        return getUserService().updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUser, timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime);
    }

    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        return getUserService().updateUser(user);
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        return getUserService().deleteUser(guid);
    }

    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        return getUserService().deleteUser(user);
    }

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        return getUserService().deleteUsers(filter, params, values);
    }

}
