package com.cronbay.af.proxy.local;

import com.cronbay.ws.service.ApiConsumerService;
import com.cronbay.ws.service.UserService;
import com.cronbay.ws.service.CronJobService;
import com.cronbay.ws.service.JobOutputService;
import com.cronbay.ws.service.ServiceInfoService;
import com.cronbay.ws.service.FiveTenService;

// TBD: How to best inject the service instances?
public abstract class BaseLocalServiceProxy
{
    private ApiConsumerService apiConsumerService;
    private UserService userService;
    private CronJobService cronJobService;
    private JobOutputService jobOutputService;
    private ServiceInfoService serviceInfoService;
    private FiveTenService fiveTenService;

    public BaseLocalServiceProxy()
    {
        this(null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService)
    {
        this(apiConsumerService, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserService userService)
    {
        this(null, userService, null, null, null, null);
    }
    public BaseLocalServiceProxy(CronJobService cronJobService)
    {
        this(null, null, cronJobService, null, null, null);
    }
    public BaseLocalServiceProxy(JobOutputService jobOutputService)
    {
        this(null, null, null, jobOutputService, null, null);
    }
    public BaseLocalServiceProxy(ServiceInfoService serviceInfoService)
    {
        this(null, null, null, null, serviceInfoService, null);
    }
    public BaseLocalServiceProxy(FiveTenService fiveTenService)
    {
        this(null, null, null, null, null, fiveTenService);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService, UserService userService, CronJobService cronJobService, JobOutputService jobOutputService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.userService = userService;
        this.cronJobService = cronJobService;
        this.jobOutputService = jobOutputService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }
    
    // Inject dependencies.
    public void setApiConsumerService(ApiConsumerService apiConsumerService)
    {
        this.apiConsumerService = apiConsumerService;
    }
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }
    public void setCronJobService(CronJobService cronJobService)
    {
        this.cronJobService = cronJobService;
    }
    public void setJobOutputService(JobOutputService jobOutputService)
    {
        this.jobOutputService = jobOutputService;
    }
    public void setServiceInfoService(ServiceInfoService serviceInfoService)
    {
        this.serviceInfoService = serviceInfoService;
    }
    public void setFiveTenService(FiveTenService fiveTenService)
    {
        this.fiveTenService = fiveTenService;
    }
   
    // Returns a ApiConsumerService instance.
    public ApiConsumerService getApiConsumerService() 
    {
        return apiConsumerService;
    }

    // Returns a UserService instance.
    public UserService getUserService() 
    {
        return userService;
    }

    // Returns a CronJobService instance.
    public CronJobService getCronJobService() 
    {
        return cronJobService;
    }

    // Returns a JobOutputService instance.
    public JobOutputService getJobOutputService() 
    {
        return jobOutputService;
    }

    // Returns a ServiceInfoService instance.
    public ServiceInfoService getServiceInfoService() 
    {
        return serviceInfoService;
    }

    // Returns a FiveTenService instance.
    public FiveTenService getFiveTenService() 
    {
        return fiveTenService;
    }

}
