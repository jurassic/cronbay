package com.cronbay.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.stub.ErrorStub;
import com.cronbay.ws.stub.GaeAppStructStub;
import com.cronbay.ws.stub.GaeAppStructListStub;
import com.cronbay.ws.stub.JobOutputStub;
import com.cronbay.ws.stub.JobOutputListStub;
import com.cronbay.af.util.MarshalHelper;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.af.bean.JobOutputBean;
import com.cronbay.ws.service.JobOutputService;
import com.cronbay.af.proxy.JobOutputServiceProxy;
import com.cronbay.af.proxy.remote.RemoteJobOutputServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncJobOutputServiceProxy extends BaseAsyncServiceProxy implements JobOutputServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncJobOutputServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteJobOutputServiceProxy remoteProxy;

    public AsyncJobOutputServiceProxy()
    {
        remoteProxy = new RemoteJobOutputServiceProxy();
    }

    @Override
    public JobOutput getJobOutput(String guid) throws BaseException
    {
        return remoteProxy.getJobOutput(guid);
    }

    @Override
    public Object getJobOutput(String guid, String field) throws BaseException
    {
        return remoteProxy.getJobOutput(guid, field);       
    }

    @Override
    public List<JobOutput> getJobOutputs(List<String> guids) throws BaseException
    {
        return remoteProxy.getJobOutputs(guids);
    }

    @Override
    public List<JobOutput> getAllJobOutputs() throws BaseException
    {
        return getAllJobOutputs(null, null, null);
    }

    @Override
    public List<JobOutput> getAllJobOutputs(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllJobOutputs(ordering, offset, count);
    }

    @Override
    public List<String> getAllJobOutputKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllJobOutputKeys(ordering, offset, count);
    }

    @Override
    public List<JobOutput> findJobOutputs(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findJobOutputs(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<JobOutput> findJobOutputs(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findJobOutputs(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findJobOutputKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findJobOutputKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createJobOutput(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws BaseException
    {
        JobOutputBean bean = new JobOutputBean(null, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
        return createJobOutput(bean);        
    }

    @Override
    public String createJobOutput(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        String guid = jobOutput.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((JobOutputBean) jobOutput).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateJobOutput-" + guid;
        String taskName = "RsCreateJobOutput-" + guid + "-" + (new Date()).getTime();
        JobOutputStub stub = MarshalHelper.convertJobOutputToStub(jobOutput);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(JobOutputStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = jobOutput.toString().length() * 2;  // 2 bytes per char
            log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    JobOutputStub dummyStub = new JobOutputStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    log.log(Level.FINE, "createJobOutput(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "jobOutputs/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                log.log(Level.INFO, "createJobOutput(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "jobOutputs/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateJobOutput(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "JobOutput guid is invalid.");
        	throw new BaseException("JobOutput guid is invalid.");
        }
        JobOutputBean bean = new JobOutputBean(guid, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
        return updateJobOutput(bean);        
    }

    @Override
    public Boolean updateJobOutput(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        String guid = jobOutput.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "JobOutput object is invalid.");
        	throw new BaseException("JobOutput object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateJobOutput-" + guid;
        String taskName = "RsUpdateJobOutput-" + guid + "-" + (new Date()).getTime();
        JobOutputStub stub = MarshalHelper.convertJobOutputToStub(jobOutput);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(JobOutputStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = jobOutput.toString().length() * 2;  // 2 bytes per char
            log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    JobOutputStub dummyStub = new JobOutputStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    log.log(Level.FINE, "updateJobOutput(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "jobOutputs/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                log.log(Level.INFO, "updateJobOutput(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "jobOutputs/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteJobOutput(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteJobOutput-" + guid;
        String taskName = "RsDeleteJobOutput-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "jobOutputs/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteJobOutput(JobOutput jobOutput) throws BaseException
    {
        String guid = jobOutput.getGuid();
        return deleteJobOutput(guid);
    }

    @Override
    public Long deleteJobOutputs(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteJobOutputs(filter, params, values);
    }

}
