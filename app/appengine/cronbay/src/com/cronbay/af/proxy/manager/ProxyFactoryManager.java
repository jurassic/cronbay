package com.cronbay.af.proxy.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.config.Config;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.local.LocalProxyFactory;
import com.cronbay.af.proxy.remote.RemoteProxyFactory;
import com.cronbay.af.proxy.async.AsyncProxyFactory;


// We use Abstract Factory pattern.
// This "manager" class provides a way to choose a concrete factory.
public final class ProxyFactoryManager
{
    private static final Logger log = Logger.getLogger(ProxyFactoryManager.class.getName());

    // temporary
    private static final String CONFIG_KEY_PROXY_TYPE = "cronbayapp.dataservice.proxytype";
    private static final String DEFAULT_PROXY_TYPE = "remote";  // Use "local"???

    // Prevents instantiation.
    private ProxyFactoryManager() {}

    // Returns a proxy factory.
    public static AbstractProxyFactory getProxyFactory() 
    {
        // TBD: This should really be hard-coded during deployment??
        String proxyType = Config.getInstance().getString(CONFIG_KEY_PROXY_TYPE, DEFAULT_PROXY_TYPE);
        if("local".equals(proxyType)) {
            return LocalProxyFactory.getInstance();
        } else if("async".equals(proxyType)) {
            return AsyncProxyFactory.getInstance();
        } else {
            return RemoteProxyFactory.getInstance();
        }
    }

}
