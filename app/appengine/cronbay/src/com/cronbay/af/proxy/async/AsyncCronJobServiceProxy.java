package com.cronbay.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.stub.ErrorStub;
import com.cronbay.ws.stub.NotificationStructStub;
import com.cronbay.ws.stub.NotificationStructListStub;
import com.cronbay.ws.stub.RestRequestStructStub;
import com.cronbay.ws.stub.RestRequestStructListStub;
import com.cronbay.ws.stub.GaeAppStructStub;
import com.cronbay.ws.stub.GaeAppStructListStub;
import com.cronbay.ws.stub.ReferrerInfoStructStub;
import com.cronbay.ws.stub.ReferrerInfoStructListStub;
import com.cronbay.ws.stub.CronScheduleStructStub;
import com.cronbay.ws.stub.CronScheduleStructListStub;
import com.cronbay.ws.stub.CronJobStub;
import com.cronbay.ws.stub.CronJobListStub;
import com.cronbay.af.util.MarshalHelper;
import com.cronbay.af.bean.NotificationStructBean;
import com.cronbay.af.bean.RestRequestStructBean;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.af.bean.ReferrerInfoStructBean;
import com.cronbay.af.bean.CronScheduleStructBean;
import com.cronbay.af.bean.CronJobBean;
import com.cronbay.ws.service.CronJobService;
import com.cronbay.af.proxy.CronJobServiceProxy;
import com.cronbay.af.proxy.remote.RemoteCronJobServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncCronJobServiceProxy extends BaseAsyncServiceProxy implements CronJobServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncCronJobServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteCronJobServiceProxy remoteProxy;

    public AsyncCronJobServiceProxy()
    {
        remoteProxy = new RemoteCronJobServiceProxy();
    }

    @Override
    public CronJob getCronJob(String guid) throws BaseException
    {
        return remoteProxy.getCronJob(guid);
    }

    @Override
    public Object getCronJob(String guid, String field) throws BaseException
    {
        return remoteProxy.getCronJob(guid, field);       
    }

    @Override
    public List<CronJob> getCronJobs(List<String> guids) throws BaseException
    {
        return remoteProxy.getCronJobs(guids);
    }

    @Override
    public List<CronJob> getAllCronJobs() throws BaseException
    {
        return getAllCronJobs(null, null, null);
    }

    @Override
    public List<CronJob> getAllCronJobs(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllCronJobs(ordering, offset, count);
    }

    @Override
    public List<String> getAllCronJobKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllCronJobKeys(ordering, offset, count);
    }

    @Override
    public List<CronJob> findCronJobs(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findCronJobs(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<CronJob> findCronJobs(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findCronJobs(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findCronJobKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findCronJobKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createCronJob(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime) throws BaseException
    {
        CronJobBean bean = new CronJobBean(null, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), ownerUser, userAcl, user, jobId, title, description, originJob, MarshalHelper.convertRestRequestStructToBean(restRequest), permalink, shortlink, status, jobStatus, extra, note, alert, MarshalHelper.convertNotificationStructToBean(notificationPref), MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), MarshalHelper.convertCronScheduleStructToBean(cronSchedule), nextRunTime);
        return createCronJob(bean);        
    }

    @Override
    public String createCronJob(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN");

        String guid = cronJob.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((CronJobBean) cronJob).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateCronJob-" + guid;
        String taskName = "RsCreateCronJob-" + guid + "-" + (new Date()).getTime();
        CronJobStub stub = MarshalHelper.convertCronJobToStub(cronJob);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(CronJobStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = cronJob.toString().length() * 2;  // 2 bytes per char
            log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    CronJobStub dummyStub = new CronJobStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    log.log(Level.FINE, "createCronJob(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "cronJobs/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                log.log(Level.INFO, "createCronJob(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "cronJobs/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateCronJob(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "CronJob guid is invalid.");
        	throw new BaseException("CronJob guid is invalid.");
        }
        CronJobBean bean = new CronJobBean(guid, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), ownerUser, userAcl, user, jobId, title, description, originJob, MarshalHelper.convertRestRequestStructToBean(restRequest), permalink, shortlink, status, jobStatus, extra, note, alert, MarshalHelper.convertNotificationStructToBean(notificationPref), MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), MarshalHelper.convertCronScheduleStructToBean(cronSchedule), nextRunTime);
        return updateCronJob(bean);        
    }

    @Override
    public Boolean updateCronJob(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN");

        String guid = cronJob.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "CronJob object is invalid.");
        	throw new BaseException("CronJob object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateCronJob-" + guid;
        String taskName = "RsUpdateCronJob-" + guid + "-" + (new Date()).getTime();
        CronJobStub stub = MarshalHelper.convertCronJobToStub(cronJob);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(CronJobStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = cronJob.toString().length() * 2;  // 2 bytes per char
            log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    CronJobStub dummyStub = new CronJobStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    log.log(Level.FINE, "updateCronJob(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "cronJobs/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                log.log(Level.INFO, "updateCronJob(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "cronJobs/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteCronJob(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteCronJob-" + guid;
        String taskName = "RsDeleteCronJob-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "cronJobs/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteCronJob(CronJob cronJob) throws BaseException
    {
        String guid = cronJob.getGuid();
        return deleteCronJob(guid);
    }

    @Override
    public Long deleteCronJobs(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteCronJobs(filter, params, values);
    }

}
