package com.cronbay.af.proxy.async;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.ApiConsumerServiceProxy;
import com.cronbay.af.proxy.UserServiceProxy;
import com.cronbay.af.proxy.CronJobServiceProxy;
import com.cronbay.af.proxy.JobOutputServiceProxy;
import com.cronbay.af.proxy.ServiceInfoServiceProxy;
import com.cronbay.af.proxy.FiveTenServiceProxy;

public class AsyncProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(AsyncProxyFactory.class.getName());

    private AsyncProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AsyncProxyFactoryHolder
    {
        private static final AsyncProxyFactory INSTANCE = new AsyncProxyFactory();
    }

    // Singleton method
    public static AsyncProxyFactory getInstance()
    {
        return AsyncProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new AsyncApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new AsyncUserServiceProxy();
    }

    @Override
    public CronJobServiceProxy getCronJobServiceProxy()
    {
        return new AsyncCronJobServiceProxy();
    }

    @Override
    public JobOutputServiceProxy getJobOutputServiceProxy()
    {
        return new AsyncJobOutputServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new AsyncServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new AsyncFiveTenServiceProxy();
    }

}
