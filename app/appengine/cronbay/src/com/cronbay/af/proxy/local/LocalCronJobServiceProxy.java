package com.cronbay.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.service.CronJobService;
import com.cronbay.af.proxy.CronJobServiceProxy;

public class LocalCronJobServiceProxy extends BaseLocalServiceProxy implements CronJobServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalCronJobServiceProxy.class.getName());

    public LocalCronJobServiceProxy()
    {
    }

    @Override
    public CronJob getCronJob(String guid) throws BaseException
    {
        return getCronJobService().getCronJob(guid);
    }

    @Override
    public Object getCronJob(String guid, String field) throws BaseException
    {
        return getCronJobService().getCronJob(guid, field);       
    }

    @Override
    public List<CronJob> getCronJobs(List<String> guids) throws BaseException
    {
        return getCronJobService().getCronJobs(guids);
    }

    @Override
    public List<CronJob> getAllCronJobs() throws BaseException
    {
        return getAllCronJobs(null, null, null);
    }

    @Override
    public List<CronJob> getAllCronJobs(String ordering, Long offset, Integer count) throws BaseException
    {
        return getCronJobService().getAllCronJobs(ordering, offset, count);
    }

    @Override
    public List<String> getAllCronJobKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getCronJobService().getAllCronJobKeys(ordering, offset, count);
    }

    @Override
    public List<CronJob> findCronJobs(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findCronJobs(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<CronJob> findCronJobs(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getCronJobService().findCronJobs(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findCronJobKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getCronJobService().findCronJobKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getCronJobService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createCronJob(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime) throws BaseException
    {
        return getCronJobService().createCronJob(managerApp, appAcl, gaeApp, ownerUser, userAcl, user, jobId, title, description, originJob, restRequest, permalink, shortlink, status, jobStatus, extra, note, alert, notificationPref, referrerInfo, cronSchedule, nextRunTime);
    }

    @Override
    public String createCronJob(CronJob cronJob) throws BaseException
    {
        return getCronJobService().createCronJob(cronJob);
    }

    @Override
    public Boolean updateCronJob(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime) throws BaseException
    {
        return getCronJobService().updateCronJob(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, jobId, title, description, originJob, restRequest, permalink, shortlink, status, jobStatus, extra, note, alert, notificationPref, referrerInfo, cronSchedule, nextRunTime);
    }

    @Override
    public Boolean updateCronJob(CronJob cronJob) throws BaseException
    {
        return getCronJobService().updateCronJob(cronJob);
    }

    @Override
    public Boolean deleteCronJob(String guid) throws BaseException
    {
        return getCronJobService().deleteCronJob(guid);
    }

    @Override
    public Boolean deleteCronJob(CronJob cronJob) throws BaseException
    {
        return getCronJobService().deleteCronJob(cronJob);
    }

    @Override
    public Long deleteCronJobs(String filter, String params, List<String> values) throws BaseException
    {
        return getCronJobService().deleteCronJobs(filter, params, values);
    }

}
