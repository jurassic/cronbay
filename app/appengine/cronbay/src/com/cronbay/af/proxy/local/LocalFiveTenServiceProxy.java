package com.cronbay.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.FiveTen;
import com.cronbay.ws.service.FiveTenService;
import com.cronbay.af.proxy.FiveTenServiceProxy;

public class LocalFiveTenServiceProxy extends BaseLocalServiceProxy implements FiveTenServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalFiveTenServiceProxy.class.getName());

    public LocalFiveTenServiceProxy()
    {
    }

    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        return getFiveTenService().getFiveTen(guid);
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        return getFiveTenService().getFiveTen(guid, field);       
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        return getFiveTenService().getFiveTens(guids);
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return getAllFiveTens(null, null, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
    {
        return getFiveTenService().getAllFiveTens(ordering, offset, count);
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getFiveTenService().getAllFiveTenKeys(ordering, offset, count);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getFiveTenService().findFiveTens(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getFiveTenService().findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getFiveTenService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException
    {
        return getFiveTenService().createFiveTen(counter, requesterIpAddress);
    }

    @Override
    public String createFiveTen(FiveTen fiveTen) throws BaseException
    {
        return getFiveTenService().createFiveTen(fiveTen);
    }

    @Override
    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException
    {
        return getFiveTenService().updateFiveTen(guid, counter, requesterIpAddress);
    }

    @Override
    public Boolean updateFiveTen(FiveTen fiveTen) throws BaseException
    {
        return getFiveTenService().updateFiveTen(fiveTen);
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        return getFiveTenService().deleteFiveTen(guid);
    }

    @Override
    public Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException
    {
        return getFiveTenService().deleteFiveTen(fiveTen);
    }

    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        return getFiveTenService().deleteFiveTens(filter, params, values);
    }

}
