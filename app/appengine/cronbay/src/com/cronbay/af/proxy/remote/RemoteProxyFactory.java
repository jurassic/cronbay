package com.cronbay.af.proxy.remote;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.ApiConsumerServiceProxy;
import com.cronbay.af.proxy.UserServiceProxy;
import com.cronbay.af.proxy.CronJobServiceProxy;
import com.cronbay.af.proxy.JobOutputServiceProxy;
import com.cronbay.af.proxy.ServiceInfoServiceProxy;
import com.cronbay.af.proxy.FiveTenServiceProxy;

public class RemoteProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(RemoteProxyFactory.class.getName());

    private RemoteProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class RemoteProxyFactoryHolder
    {
        private static final RemoteProxyFactory INSTANCE = new RemoteProxyFactory();
    }

    // Singleton method
    public static RemoteProxyFactory getInstance()
    {
        return RemoteProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new RemoteApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new RemoteUserServiceProxy();
    }

    @Override
    public CronJobServiceProxy getCronJobServiceProxy()
    {
        return new RemoteCronJobServiceProxy();
    }

    @Override
    public JobOutputServiceProxy getJobOutputServiceProxy()
    {
        return new RemoteJobOutputServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new RemoteServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new RemoteFiveTenServiceProxy();
    }

}
