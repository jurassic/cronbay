package com.cronbay.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.ws.User;
import com.cronbay.ws.stub.ErrorStub;
import com.cronbay.ws.stub.GaeAppStructStub;
import com.cronbay.ws.stub.GaeAppStructListStub;
import com.cronbay.ws.stub.GaeUserStructStub;
import com.cronbay.ws.stub.GaeUserStructListStub;
import com.cronbay.ws.stub.UserStub;
import com.cronbay.ws.stub.UserListStub;
import com.cronbay.af.util.MarshalHelper;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.af.bean.GaeUserStructBean;
import com.cronbay.af.bean.UserBean;
import com.cronbay.ws.service.UserService;
import com.cronbay.af.proxy.UserServiceProxy;
import com.cronbay.af.proxy.remote.RemoteUserServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncUserServiceProxy extends BaseAsyncServiceProxy implements UserServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncUserServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteUserServiceProxy remoteProxy;

    public AsyncUserServiceProxy()
    {
        remoteProxy = new RemoteUserServiceProxy();
    }

    @Override
    public User getUser(String guid) throws BaseException
    {
        return remoteProxy.getUser(guid);
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        return remoteProxy.getUser(guid, field);       
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        return remoteProxy.getUsers(guids);
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUsers(ordering, offset, count);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.getAllUserKeys(ordering, offset, count);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUsers(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return remoteProxy.findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime) throws BaseException
    {
        UserBean bean = new UserBean(null, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), aeryId, sessionId, username, nickname, avatar, email, openId, MarshalHelper.convertGaeUserStructToBean(gaeUser), timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime);
        return createUser(bean);        
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        String guid = user.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((UserBean) user).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateUser-" + guid;
        String taskName = "RsCreateUser-" + guid + "-" + (new Date()).getTime();
        UserStub stub = MarshalHelper.convertUserToStub(user);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = user.toString().length() * 2;  // 2 bytes per char
            log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserStub dummyStub = new UserStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    log.log(Level.FINE, "createUser(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "users/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                log.log(Level.INFO, "createUser(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "users/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "User guid is invalid.");
        	throw new BaseException("User guid is invalid.");
        }
        UserBean bean = new UserBean(guid, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), aeryId, sessionId, username, nickname, avatar, email, openId, MarshalHelper.convertGaeUserStructToBean(gaeUser), timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime);
        return updateUser(bean);        
    }

    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        String guid = user.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "User object is invalid.");
        	throw new BaseException("User object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateUser-" + guid;
        String taskName = "RsUpdateUser-" + guid + "-" + (new Date()).getTime();
        UserStub stub = MarshalHelper.convertUserToStub(user);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(UserStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = user.toString().length() * 2;  // 2 bytes per char
            log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    UserStub dummyStub = new UserStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    log.log(Level.FINE, "updateUser(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "users/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                log.log(Level.INFO, "updateUser(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "users/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteUser-" + guid;
        String taskName = "RsDeleteUser-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "users/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        String guid = user.getGuid();
        return deleteUser(guid);
    }

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteUsers(filter, params, values);
    }

}
