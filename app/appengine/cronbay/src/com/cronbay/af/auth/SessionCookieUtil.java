package com.cronbay.af.auth;

import java.util.logging.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cronbay.af.config.Config;
import com.cronbay.af.util.CookieUtil;


// SessionCookie-related convenience methods.
// It "extends" CookieUtil.
public class SessionCookieUtil
{
    private static final Logger log = Logger.getLogger(SessionCookieUtil.class.getName());
    private static final String KEY_CONFIG_SESSION_MAXAGE = "cronbayapp.session.maxage";

    private SessionCookieUtil() {}


    public static SessionCookie findSessionCookie(HttpServletRequest request)
    {
        SessionCookie sessionCookie = null;
        Cookie cookie = CookieUtil.findCookie(request, SessionCookie.SESSION_COOKIE_NAME);
        if(cookie != null) {
            sessionCookie = convertToSessionCookie(cookie);
            log.fine("findSessionCookie(): cookie = " + cookie);
            log.fine("findSessionCookie(): sessionCookie = " + sessionCookie);
        }
        return sessionCookie;
    }

    public static void addSessionCookie(HttpServletResponse response, SessionBean sessionBean)
    {
        SessionCookie sessionCookie = new SessionCookie(sessionBean);
        
        // Default value is 1 year: SessionCookie.AGE_FOREVER
        Integer maxAge = Config.getInstance().getInteger(KEY_CONFIG_SESSION_MAXAGE);
        if(maxAge != null) {
            sessionCookie.setMaxAge(maxAge);
        }

        addSessionCookie(response, sessionCookie);
    }

    public static void addSessionCookie(HttpServletResponse response, SessionCookie sessionCookie)
    {
        log.fine("addSessionCookie(): sessionCookie = " + sessionCookie);

        // TBD: Does this always work?
        response.addCookie(sessionCookie);

//        // If there is any problem with SessionCookie, use the converted Cookie.
//        Cookie cookie = convertToCookie(sessionCookie);
//        log.fine("addSessionCookie(): cookie = " + cookie);
//        response.addCookie(cookie);
    }
    
    public static void removeSessionCookie(HttpServletRequest request, HttpServletResponse response)
    {
        SessionCookie sessionCookie = findSessionCookie(request);
        if(sessionCookie != null) {
            sessionCookie.markForRemoval();
        } else {
            sessionCookie = new SessionCookie();
            sessionCookie.setMaxAge(0);
        }
        response.addCookie(sessionCookie);
    }


    // TBD:
    // The following two "convert" functions may be useful
    // in case "casting" does not work for SessionCookie type....
    // ...

    // We assume that cookie is of a SessionCookie type.
    public static SessionCookie convertToSessionCookie(Cookie cookie)
    {
        // Use encrypted value...?
        SessionBean sessionBean = SessionBean.fromEncryptedJsonString(cookie.getValue());
        SessionCookie sessionCookie = new SessionCookie(sessionBean);
        // TBD: Copy other fields...
        String domain = cookie.getDomain();
        if(domain != null) {
            sessionCookie.setDomain(domain);
        }
        String path = cookie.getPath();
        if(path != null) {
            sessionCookie.setPath(path);
        }
        String comment = cookie.getComment();
        if(comment != null) {
            sessionCookie.setComment(comment);
        }
        int version = cookie.getVersion();
        sessionCookie.setVersion(version);
        int maxAge = cookie.getMaxAge();
        sessionCookie.setMaxAge(maxAge);
        boolean secure = cookie.getSecure();
        sessionCookie.setSecure(secure);
        // ...
        return sessionCookie;
    }

    // TBD: Is this necessary?
    // Just use polymorphic behavior??? (e.g., SessionCookie inherits from Cookie.)
    public static Cookie convertToCookie(SessionCookie sessionCookie)
    {
        //SessionBean sessionBean = sessionCookie.getSessionBean();
        Cookie cookie = new Cookie(sessionCookie.getName(), sessionCookie.getValue());
        // TBD: Copy other fields...
        String domain = sessionCookie.getDomain();
        if(domain != null) {
            cookie.setDomain(domain);
        }
        String path = sessionCookie.getPath();
        if(path != null) {
            cookie.setPath(path);
        }
        String comment = sessionCookie.getComment();
        if(comment != null) {
            cookie.setComment(comment);
        }
        int version = sessionCookie.getVersion();
        cookie.setVersion(version);
        int maxAge = sessionCookie.getMaxAge();
        cookie.setMaxAge(maxAge);
        boolean secure = sessionCookie.getSecure();
        cookie.setSecure(secure);
        // ...
        return cookie;
    }

}
