package com.cronbay.af.auth;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cronbay.af.util.SimpleEncryptUtil;


// Session bean is used to store "persistent session" information in a cookie.
public class SessionBean implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SessionBean.class.getName());

    private String guid;
    private String userId;
    private String token;
    private String ipAddress;
    private String referer;
    private String status;
    private Long authenticatedTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;


    // Ctors.
    public SessionBean()
    {
        //this((String) null);
    }
    public SessionBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public SessionBean(String guid, String userId, String token, String ipAddress, String referer, String status, Long authenticatedTime, Long expirationTime)
    {
        this(guid, userId, token, ipAddress, referer, status, authenticatedTime, expirationTime, null, null);
    }
    public SessionBean(String guid, String userId, String token, String ipAddress, String referer, String status, Long authenticatedTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.userId = userId;
        this.token = token;
        this.ipAddress = ipAddress;
        this.referer = referer;
        this.status = status;
        this.authenticatedTime = authenticatedTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }


    public static SessionBean fromJsonString(String jsonStr)
    {
        SessionBean bean = null;
        try {
            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            bean = mapper.readValue(jsonStr, SessionBean.class);
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public static SessionBean fromEncryptedJsonString(String encJsonStr)
    {
        String jsonStr = SimpleEncryptUtil.decrypt(encJsonStr);
        return fromJsonString(jsonStr);
    }
    
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUserId()
    {
        return this.userId;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getToken()
    {
        return this.token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }

    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public String getReferer()
    {
        return this.referer;
    }
    public void setReferer(String referer)
    {
        this.referer = referer;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getAuthenticatedTime()
    {
        return this.authenticatedTime;
    }
    public void setAuthenticatedTime(Long authenticatedTime)
    {
        this.authenticatedTime = authenticatedTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }

    
    public String toJsonString()
    {
        String jsonStr = null;
        try {
            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    public String toEncryptedJsonString()
    {
        String jsonStr = toJsonString();
        return SimpleEncryptUtil.encrypt(jsonStr);
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("guid = " + this.guid).append(";");
        sb.append("userId = " + this.userId).append(";");
        sb.append("token = " + this.token).append(";");
        sb.append("ipAddress = " + this.ipAddress).append(";");
        sb.append("referer = " + this.referer).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("authenticatedTime = " + this.authenticatedTime).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        hash = 31 * hash + delta;
        delta = userId == null ? 0 : userId.hashCode();
        hash = 31 * hash + delta;
        delta = token == null ? 0 : token.hashCode();
        hash = 31 * hash + delta;
        delta = ipAddress == null ? 0 : ipAddress.hashCode();
        hash = 31 * hash + delta;
        delta = referer == null ? 0 : referer.hashCode();
        hash = 31 * hash + delta;
        delta = status == null ? 0 : status.hashCode();
        hash = 31 * hash + delta;
        delta = authenticatedTime == null ? 0 : authenticatedTime.hashCode();
        hash = 31 * hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        hash = 31 * hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        hash = 31 * hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        hash = 31 * hash + delta;
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
