package com.cronbay.af.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


// Whitellist-based filter is better for secure resources.
public class RefererWhitelistFilter implements Filter
{
    private static final Logger log = Logger.getLogger(RefererWhitelistFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    
    public RefererWhitelistFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // No access by default
        boolean accessAllowed = false;

        // Check whitelist.
        String referer = ((HttpServletRequest) req).getHeader("referer");
        // TBD:
        // Change accessAllowed to true if referer in the whitelist...
        // ...        

        log.log(Level.INFO, "RefererWhitelistFilter.doFilter(): accessAllowed = " + accessAllowed);

        // TBD
        // Redirect to the error or login page. ???
        config.getServletContext().getRequestDispatcher("/").forward(req, res);
    }
    
}
