package com.cronbay.af.auth;

public class AuthConstants
{
    private AuthConstants() {}

    // TBD
    public static final String PARAM_VERSION = "aeryid_version";
    public static final String PARAM_AUTH_TOKEN = "aeryid_auth_token";
    public static final String PARAM_CLIENT_KEY = "aeryid_client_key";
    public static final String PARAM_CLIENT_SECRET = "aeryid_client_secret";
    public static final String PARAM_USER_KEY = "aeryid_user_key";
    public static final String PARAM_USER_SECRET = "aeryid_user_secret";
    public static final String PARAM_TARGET_CLIENT = "aeryid_target_client";
    public static final String PARAM_TARGET_USER = "aeryid_target_user";
    public static final String PARAM_TIMESTAMP = "aeryid_timestamp";
    public static final String PARAM_REQUEST_GUID = "aeryid_request_guid";
    public static final String PARAM_SIGNATURE = "aeryid_signature";
    public static final String PARAM_SIGNATURE_METHOD = "aeryid_signature_method";
    // ...
    public static final String ATTR_AUTHORIZED = "attr_authorized";
    // ...

    // TBD
    public static final String SESSION_ATTR_SESSION_GUID = "_session_guid";
    public static final String SESSION_ATTR_SESSION_BEAN = "_session_bean";

}
