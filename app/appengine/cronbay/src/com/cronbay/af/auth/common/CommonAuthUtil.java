package com.cronbay.af.auth.common;

import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.core.GUID;
import com.cronbay.ws.util.HashUtil;
import com.cronbay.af.util.URLUtil;


public class CommonAuthUtil
{
    private static final Logger log = Logger.getLogger(CommonAuthUtil.class.getName());

    // TBD...
    public static final String CONFIG_KEY_CUSTOMCREDENTIAL_ENABLED = "cronbayapp.customcredential.enabled";

    // URL params.
    public static final String PARAM_COMEBACKURL = "comebackUrl";  // To be used when auth is successful...
    public static final String PARAM_FALLBACKURL = "fallbackUrl";  // To be used when auth has failed...
    // ...

    // To indicated whether the last/most recent auth attempt was successful or not.
    // This should be typically cleared at comeback/fallback pages...
    public static final String SESSION_ATTR_AUTHSTATUS = "com.memoany.af.auth.common.authstatus";
    // ...

    // TBD...
    public static final String AUTH_SERVICE_PROVIDERID_CUSTOM = "custom";     // Internal account (e.g., username+password based auth)
    public static final String AUTH_SERVICE_PROVIDERID_FACEBOOK = "facebook";
    public static final String AUTH_SERVICE_PROVIDERID_TWITTER = "twitter";
    public static final String AUTH_SERVICE_PROVIDERID_YAHOO = "yahoo";
    public static final String AUTH_SERVICE_PROVIDERID_GOOGLE = "google";
    public static final String AUTH_SERVICE_PROVIDERID_YAMMER = "yammer";
    public static final String AUTH_SERVICE_PROVIDERID_LINKEDIN = "linkedin";
    public static final String AUTH_SERVICE_PROVIDERID_MYSPACE = "myspace";
    public static final String AUTH_SERVICE_PROVIDERID_FOURSQUARE = "foursquare";
    // Google apps ???
    // ...


    private CommonAuthUtil() {}

    
    // Note: We implement isNonOpenId() not isOpenId().
    // isNonOpenId() not necessary equals !isOpenId().
    public static boolean isNonOpenId(String federatedIdentity)
    {
        if(federatedIdentity == null || 
                federatedIdentity.isEmpty() || 
                AUTH_SERVICE_PROVIDERID_FACEBOOK.equals(federatedIdentity) || 
                AUTH_SERVICE_PROVIDERID_TWITTER.equals(federatedIdentity)) {
            return true;
        } else {
            // It does not mean that the given federatedIdentity is open id...
            return false;
        }
    }
    
    // To be used for FB OAuth "state" param....
    public static String generateRandomNonce()
    {
        // temporary
        return HashUtil.generateSha1Hash(GUID.generate());
    }

//    public static String constructUrl(String topLevelUrl, String path)
//    {
//        if(topLevelUrl == null) {
//            return null;
//        }
//
//        StringBuffer sb = new StringBuffer();
//        sb.append(topLevelUrl);
//        if(path != null && !path.isEmpty()) {
//            if(topLevelUrl.endsWith("/")) {
//                if(path.startsWith("/")) {
//                    if(path.length() > 1) {
//                        sb.append(path.substring(1));
//                    }
//                } else {
//                    sb.append(path);
//                }
//            } else {
//                if(path.startsWith("/")) {
//                    sb.append(path);
//                } else {
//                    sb.append("/").append(path);
//                }
//            }
//        }
//
//        return sb.toString();
//    }

    // topLevelUrl: http://abc.com/
    // path: pqr/lmn
    // Note: we currently ignore contextPath....
    public static String constructUrl(String topLevelUrl, String servletPath)
    {
        return constructUrl(topLevelUrl, servletPath, (String) null);
    }
    public static String constructUrl(String topLevelUrl, String servletPath, String pathInfo)
    {
        return constructUrl(topLevelUrl, servletPath, pathInfo, null);
    }
    public static String constructUrl(String topLevelUrl, String servletPath, Map<String, Object> params)
    {
        return constructUrl(topLevelUrl, servletPath, null, params);
    }
    public static String constructUrl(String topLevelUrl, String servletPath, String pathInfo, Map<String, Object> params)
    {
        if(topLevelUrl == null) {
            //return null;
            topLevelUrl = "/";  // Use relative URL????
        }
        if(servletPath == null) {
            servletPath = "";
        }
        StringBuffer sb = new StringBuffer();
        sb.append(topLevelUrl);
        if(topLevelUrl.endsWith("/")) {
            if(servletPath.startsWith("/")) {
                if(servletPath.length() > 1) {
                    sb.append(servletPath.substring(1));
                }
            } else {
                if(servletPath.length() > 0) {
                    sb.append(servletPath);
                }
            }
        } else {
            if(servletPath.startsWith("/")) {
                sb.append(servletPath);
            } else {
                sb.append("/").append(servletPath);
            }
        }
        if(pathInfo != null && pathInfo.length() > 0) {
            if(servletPath.endsWith("/")) {
                if(pathInfo.startsWith("/")) {
                    if(pathInfo.length() > 1) {
                        sb.append(pathInfo.substring(1));
                    }
                } else {
                    sb.append(pathInfo);
                }
            } else {
                if(pathInfo.startsWith("/")) {
                    sb.append(pathInfo);
                } else {
                    sb.append("/").append(pathInfo);
                }
            }
        }
        String baseUrl = sb.toString();
        String url = URLUtil.buildUrl(baseUrl, params);
        return url;
    }

}
