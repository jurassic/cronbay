package com.cronbay.af.auth;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.oauth.client.OAuthClientFilter;
import com.sun.jersey.oauth.signature.OAuthParameters;
import com.sun.jersey.oauth.signature.OAuthSecrets;

import com.cronbay.af.core.JerseyClient;
import com.cronbay.af.cert.registry.ConsumerRegistry;


public class TwoLeggedOAuthClientUtil
{
    private static final Logger log = Logger.getLogger(TwoLeggedOAuthClientUtil.class.getName());

    private TwoLeggedOAuthClientUtil() {}

    // TBD: Get the consumer key/secret from config or registry....
    public static WebResource addClientFilter(WebResource webResource)
    {
// Note:
// Jersy-OAuth throws UnsupportedSignatureMethodException: HMAC-SHA1...
// unless com.sun.jersey.oauth.signature.OAuthSignatureMethod file is included under src/META-INF/service...

        // baseline OAuth parameters for access to resource
        String consumerKey = ConsumerRegistry.getInstance().getDefaultConsumerKey();
        OAuthParameters params = new OAuthParameters().signatureMethod("HMAC-SHA1").consumerKey(consumerKey);
//        .token("accesskey").version();

        // OAuth secrets to access resource
        String consumerSecret = ConsumerRegistry.getInstance().getDefaultConsumerSecret();
        OAuthSecrets secrets = new OAuthSecrets().consumerSecret(consumerSecret);
        //.tokenSecret("accesssecret");

        // if parameters and secrets remain static, filter can be added to each web resource
        //Client client = Client.create();  // ???
        Client client = JerseyClient.getInstance().getClient();
        OAuthClientFilter filter = new OAuthClientFilter(client.getProviders(), params, secrets);  // ???

        // filter added at the web resource level
        webResource.addFilter(filter);

        return webResource;
    }
    
//    public Providers getProviders()
//    {
//        return wsClient.getProviders();
//    }

}
