package com.cronbay.af.auth.user;

import java.util.Set;


public interface AuthUserService  // extends UserService
{
    String createLoginURL(String destinationURL);
    String createLoginURL(String destinationURL, String authDomain);
    String createLoginURL(String destinationURL, String authDomain, String federatedIdentity, Set<String> attributesRequest);
    String createLoginURL(String destinationURL, String authDomain, String federatedIdentity, Set<String> attributesRequest, String randomNonce);
    String createLogoutURL(String destinationURL);
    String createLogoutURL(String destinationURL, String authDomain);
    AuthUser getCurrentUser();
    boolean isUserAdmin();
    boolean isUserLoggedIn();

}
