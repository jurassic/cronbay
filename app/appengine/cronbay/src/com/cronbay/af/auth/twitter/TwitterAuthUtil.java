package com.cronbay.af.auth.twitter;

import java.util.logging.Logger;
import java.util.logging.Level;


public class TwitterAuthUtil
{
    private static final Logger log = Logger.getLogger(TwitterAuthUtil.class.getName());

    // Constants
    private static final String CALLBACK_URL_OOB = "oob";    // Out of band.
    // ...

    public static final String CONFIG_KEY_CONSUMERKEY = "cronbayapp.twitter.consumerkey";
    public static final String CONFIG_KEY_CONSUMERSECRET = "cronbayapp.twitter.consumersecret";
    public static final String CONFIG_KEY_DEFAULT_ACCESSTOKEN = "cronbayapp.twitter.default.accesstoken";
    public static final String CONFIG_KEY_DEFAULT_ACCESSTOKENSECRET = "cronbayapp.twitter.default.accesstokensecret";
    public static final String CONFIG_KEY_OAUTH_REDIRECTURLPATH = "cronbayapp.twitter.oauth.redirecturlpath";
    //public static final String CONFIG_KEY_REQUESTTOKEN_REDIRECTURLPATH = "cronbayapp.twitter.requesttoken.redirecturlpath";
    //public static final String CONFIG_KEY_ACCESSTOKEN_REDIRECTURLPATH = "cronbayapp.twitter.accesstoken.redirecturlpath"; 
    // ...
    
    public static final String PARAM_CALLBACK = "oauth_callback";
    public static final String PARAM_CONSUMER_KEY = "oauth_consumer_key";
    public static final String PARAM_NONCE = "oauth_nonce";
    public static final String PARAM_SIGNATURE = "oauth_signature";
    public static final String PARAM_SIGNATURE_METHOD = "oauth_signature_method";
    public static final String PARAM_TIMESTAMP = "oauth_timestamp";
    public static final String PARAM_VERSION = "oauth_version";
    public static final String PARAM_REQUEST_TOKEN = "oauth_token";
    public static final String PARAM_TOKEN_VERIFIER = "oauth_verifier";
    // ...
    
    public static final String BASE_URL_AUTHORIZE = "https://api.twitter.com/oauth/authorize";         // ???
    public static final String BASE_URL_AUTHENTICATE = "https://api.twitter.com/oauth/authenticate";   // ???
    public static final String BASE_URL_REQUEST_TOKEN_FETCH = "https://api.twitter.com/oauth/request_token";
    public static final String BASE_URL_ACCESS_TOKEN_EXCHANGE = "https://api.twitter.com/oauth/access_token";
    // ...

    public static final String SESSION_ATTR_REQUESTTOKEN = "com.cronbay.af.auth.twitter.oauth.requesttoken";
    // ...
    
    private TwitterAuthUtil() {}

    
    // TBD:
    // ...
    
}
