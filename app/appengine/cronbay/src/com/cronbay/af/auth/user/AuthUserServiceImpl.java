package com.cronbay.af.auth.user;

import java.util.Set;
import java.util.logging.Logger;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;

import com.cronbay.ws.core.CacheMap;
import com.cronbay.af.auth.common.CommonAuthUtil;
import com.cronbay.af.auth.facebook.FacebookAuthHelper;
import com.cronbay.af.auth.twitter.TwitterAuthHelper;


// TBD: ....
public class AuthUserServiceImpl implements AuthUserService
{
    private static final Logger log = Logger.getLogger(AuthUserServiceImpl.class.getName());

    // Embedded object
    private UserService mGaeUserService = null;
    
    // AuthUser cache
    // Map of {openId -> authUser}. (Is there a better key?)
    private CacheMap<String,AuthUser> mUserMap = null;

    public AuthUserServiceImpl(UserService gaeUserService)
    {
        // TBD: what to do if gaeUserService == null????
        mGaeUserService = gaeUserService;
        initCacheMap();
    }
    
    private void initCacheMap()
    {
        mUserMap = new CacheMap<String,AuthUser>();
        // TBD:
        int maxSize = 1000;   // testing......
        mUserMap.setMaxSize(maxSize);
        // ...
    }
    
    private UserService getGaeUserService()
    {
        if(mGaeUserService == null) {
            // TBD: what to do if gaeUserService == null????
        }
        return mGaeUserService;
    }
    
    
    @Override
    public String createLoginURL(String destinationURL)
    {
        return getGaeUserService().createLoginURL(destinationURL);
    }

    @Override
    public String createLoginURL(String destinationURL, String authDomain)
    {
        return getGaeUserService().createLoginURL(destinationURL, authDomain);
    }

    @Override
    public String createLoginURL(String destinationURL, String authDomain, String federatedIdentity, Set<String> attributesRequest)
    {
        return getGaeUserService().createLoginURL(destinationURL, authDomain, federatedIdentity, attributesRequest);
    }

    @Override
    public String createLoginURL(String destinationURL, String authDomain, String federatedIdentity, Set<String> attributesRequest, String randomNonce)
    {
        log.info("createLoginURL() called: destinationURL = " + destinationURL + "; federatedIdentity = " + federatedIdentity);
        
        String loginUrl = null;
        if(CommonAuthUtil.isNonOpenId(federatedIdentity)) {
            if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_FACEBOOK.equals(federatedIdentity)) {
                loginUrl = FacebookAuthHelper.getInstance().getOAuthLoginUrl(destinationURL, randomNonce);
                
                
            } else if(CommonAuthUtil.AUTH_SERVICE_PROVIDERID_TWITTER.equals(federatedIdentity)) {
                // TBD: ...
                
            } else {
                // ????
            }
        } else {
            loginUrl = getGaeUserService().createLoginURL(destinationURL, authDomain, federatedIdentity, attributesRequest);
        }

        log.info("loginUrl = " + loginUrl);
        return loginUrl;
    }

    @Override
    public String createLogoutURL(String destinationURL)
    {
        return getGaeUserService().createLogoutURL(destinationURL);
    }

    @Override
    public String createLogoutURL(String destinationURL, String authDomain)
    {
        return getGaeUserService().createLogoutURL(destinationURL, authDomain);
    }

    
    @Override
    public AuthUser getCurrentUser()
    {
        // TBD: Cache authUser list???
        // E.g., map of {openId -> authUser}
        // and use LRU clean up???
        // Note: we can store some extra (temporary) info in authUser objects..
        // but, without authUser object caching here, that's not very useful....
        // Unfortunately, we still have to call getGaeUserService().getCurrentUser()
        //    because we don't have a "key"....
        AuthUser authUser = null;
        User gaeUser = getGaeUserService().getCurrentUser();
        if(gaeUser != null) {
            String gaeOpenId = gaeUser.getFederatedIdentity();
            authUser = mUserMap.get(gaeOpenId);
            if(authUser == null) {
                authUser = new AuthUser(gaeUser);
                mUserMap.put(gaeOpenId, authUser);
            }
        }
        return authUser;
    }

    
    @Override
    public boolean isUserAdmin()
    {
        return getGaeUserService().isUserAdmin();
    }

    @Override
    public boolean isUserLoggedIn()
    {
        return getGaeUserService().isUserLoggedIn();
    }

    
}
