package com.cronbay.af.auth;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;


public class UserAuthManager
{
    private static final Logger log = Logger.getLogger(UserAuthManager.class.getName());


    private UserService mUserService = null;
    
    private UserAuthManager()
    {
        // Can we keep this object throughout the lifetime of the singleton????
        mUserService = UserServiceFactory.getUserService();
    }

    
    // Initialization-on-demand holder.
    private static final class UserAuthManagerHolder
    {
        private static final UserAuthManager INSTANCE = new UserAuthManager();
    }

    // Singleton method
    public static UserAuthManager getInstance()
    {
        return UserAuthManagerHolder.INSTANCE;
    }


    public boolean isUserAuthenticated()
    {
        // TBD
        return false;
    }

    public boolean isUserLoggedInForApp()
    {
        // TBD
        return false;
    }

    public boolean isUserLoggedInForApp(String user)
    {
        // TBD
        return false;
    }

    public String loginUserForApp()
    {
        // TBD
        return null;
    }

    public String loginUserForApp(String user)
    {
        // TBD
        return null;
    }

    public String logoutUserForApp(String user)
    {
        // TBD
        return null;
    }

    // "Single sign out"
    public String logoutUserFromAll(String user)
    {
        // TBD
        return null;
    }

    
    
    
}
