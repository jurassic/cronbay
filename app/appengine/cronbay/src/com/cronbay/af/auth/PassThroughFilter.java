package com.cronbay.af.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class PassThroughFilter implements Filter
{
    private static final Logger log = Logger.getLogger(PassThroughFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    
    public PassThroughFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // Pass through.
        boolean accessAllowed = true;
        log.log(Level.INFO, "PassThroughFilter.doFilter(): accessAllowed = " + accessAllowed);

        // Continue through filter chain.
        chain.doFilter(req, res);
    }
    
}
