package com.cronbay.af.auth.twitter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.config.Config;
import com.cronbay.af.util.URLUtil;
import com.cronbay.af.auth.common.CommonAuthUtil;


// Note:
public class TwitterAuthHelper
{
    private static final Logger log = Logger.getLogger(TwitterAuthHelper.class.getName());

    // "Lazy initialization"
    private String mConsumerKey = null;
    private String mConsumerSecret = null;
    private String mDefaultAccessToken = null;
    private String mDefaultAccessTokenSecret = null;
    private String mOAuthRedirectUrlPath = null;
    //private String mRequestTokenRedirectUrlPath = null;
    //private String mAccessTokenRedirectUrlPath = null;
    

    private TwitterAuthHelper() {}

    // Initialization-on-demand holder.
    private static final class TwitterAuthHelperHolder
    {
        private static final TwitterAuthHelper INSTANCE = new TwitterAuthHelper();
    }

    // Singleton method
    public static TwitterAuthHelper getInstance()
    {
        return TwitterAuthHelperHolder.INSTANCE;
    }
    
    
    public String getConsumerKey()
    {
        if(mConsumerKey == null) {
            mConsumerKey = Config.getInstance().getString(TwitterAuthUtil.CONFIG_KEY_CONSUMERKEY);
        }
        return mConsumerKey;
    }
    public String getConsumerSecret()
    {
        if(mConsumerSecret == null) {
            mConsumerSecret = Config.getInstance().getString(TwitterAuthUtil.CONFIG_KEY_CONSUMERSECRET);
        }
        return mConsumerSecret;
    }
    public String getDefaultAccessToken()
    {
        if(mDefaultAccessToken == null) {
            mDefaultAccessToken = Config.getInstance().getString(TwitterAuthUtil.CONFIG_KEY_DEFAULT_ACCESSTOKEN);
        }
        return mDefaultAccessToken;
    }
    public String getDefaultAccessTokenSecret()
    {
        if(mDefaultAccessTokenSecret == null) {
            mDefaultAccessTokenSecret = Config.getInstance().getString(TwitterAuthUtil.CONFIG_KEY_DEFAULT_ACCESSTOKENSECRET);
        }
        return mDefaultAccessTokenSecret;
    }
    private String getOAuthRedirectUrlPath()
    {
        if(mOAuthRedirectUrlPath == null) {
            mOAuthRedirectUrlPath = Config.getInstance().getString(TwitterAuthUtil.CONFIG_KEY_OAUTH_REDIRECTURLPATH);
        }
        return mOAuthRedirectUrlPath;
    }
//    private String getRequestTokenRedirectUrlPath()
//    {
//        if(mRequestTokenRedirectUrlPath == null) {
//            mRequestTokenRedirectUrlPath = Config.getInstance().getString(TwitterAuthUtil.CONFIG_KEY_REQUESTTOKEN_REDIRECTURLPATH);
//        }
//        return mRequestTokenRedirectUrlPath;
//    }
//    private String getAccessTokenRedirectUrlPath()
//    {
//        if(mAccessTokenRedirectUrlPath == null) {
//            mAccessTokenRedirectUrlPath = Config.getInstance().getString(TwitterAuthUtil.CONFIG_KEY_ACCESSTOKEN_REDIRECTURLPATH);
//        }
//        return mAccessTokenRedirectUrlPath;
//    }


    public String getDefaultOAuthRedirectUri(String topLevelUrl)
    {
        return CommonAuthUtil.constructUrl(topLevelUrl, getOAuthRedirectUrlPath());
    }
//    public String getDefaultRequestTokenRedirectUri(String topLevelUrl)
//    {
//        return CommonAuthUtil.constructUrl(topLevelUrl, getRequestTokenRedirectUrlPath());
//    }
//    public String getDefaultAccessTokenRedirectUri(String topLevelUrl)
//    {
//        return CommonAuthUtil.constructUrl(topLevelUrl, getAccessTokenRedirectUrlPath());
//    }

    public String getOAuthAuthenticateUrl(String topLevelUrl, String requestToken)
    {
        return getOAuthAuthenticateUrl(topLevelUrl, requestToken, null);
    }
    public String getOAuthAuthenticateUrl(String topLevelUrl, String requestToken, String callbackUrl)
    {
        String baseUrl = TwitterAuthUtil.BASE_URL_AUTHENTICATE;
        Map<String,Object> params = new LinkedHashMap<String,Object>();  // Ordered, for debugging purposes.
        params.put(TwitterAuthUtil.PARAM_REQUEST_TOKEN, requestToken);
        if(callbackUrl == null) {
            callbackUrl = getDefaultOAuthRedirectUri(topLevelUrl);
        }
        params.put(TwitterAuthUtil.PARAM_CALLBACK, callbackUrl);
        // etc...
        String url = URLUtil.buildUrl(baseUrl, params);
        return url;
    }
    public String getOAuthAuthorizeUrl(String topLevelUrl, String requestToken)
    {
        return getOAuthAuthorizeUrl(topLevelUrl, requestToken, null);
    }
    public String getOAuthAuthorizeUrl(String topLevelUrl, String requestToken, String callbackUrl)
    {
        String baseUrl = TwitterAuthUtil.BASE_URL_AUTHORIZE;
        Map<String,Object> params = new LinkedHashMap<String,Object>();  // Ordered, for debugging purposes.
        params.put(TwitterAuthUtil.PARAM_REQUEST_TOKEN, requestToken);
        if(callbackUrl == null) {
            callbackUrl = getDefaultOAuthRedirectUri(topLevelUrl);
        }
        params.put(TwitterAuthUtil.PARAM_CALLBACK, callbackUrl);
        // etc...
        String url = URLUtil.buildUrl(baseUrl, params);
        return url;
    }

    // TBD:
    // ...

    public String getRequestTokenFetchUrl(String topLevelUrl, String consumerKey, String callbackUrl)
    {
        String baseUrl = TwitterAuthUtil.BASE_URL_REQUEST_TOKEN_FETCH;
        Map<String,Object> params = new LinkedHashMap<String,Object>();  // Ordered, for debugging purposes.
        // TBD...
        // ...
        String url = URLUtil.buildUrl(baseUrl, params);
        return url;
    }

    public String getAcessTokenExchangeUrl(String consumerKey, String requestToken, String tokenVerifier)
    {
        String baseUrl = TwitterAuthUtil.BASE_URL_ACCESS_TOKEN_EXCHANGE;
        Map<String,Object> params = new LinkedHashMap<String,Object>();  // Ordered, for debugging purposes.
        // TBD...
        // ...
        String url = URLUtil.buildUrl(baseUrl, params);
        return url;
    }

}
