package com.cronbay.af.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class SessionFilter implements Filter
{
    private static final Logger log = Logger.getLogger(SessionFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    
    public SessionFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
        // TBD: 
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // Add a sessionBean to the user session.
        SessionBean sessionBean = null;
        if(req != null && req instanceof HttpServletRequest) {
            sessionBean = UserSessionManager.getInstance().setSessionBean((HttpServletRequest) req, (HttpServletResponse) res);
            if(sessionBean != null) {
                String sessionId = sessionBean.getGuid();
                String sessionToken = sessionBean.getToken();
                log.info("Session: sessionId = " + sessionId + "; sessionToken = " + sessionToken);
            } else {
                log.warning("Failed to create/retrieve a sessionBean from the current session.");
            }
        } else {
            // This cannot happen.
            log.warning("Failed to create a session due to unknown reasons.");
        }
        
        // TBD: This should probably be done in an authentication filter....
        if(sessionBean != null) {
            String userId = sessionBean.getUserId();
            if(userId != null) {
                // TBD: Retrieve the user object, and
                //      authenticate the user ????
                // ...
            } else {
                // TBD: Create a user object,
                //      save it to the data store, and
                //      call sessionBean.setUserId(userId).
                // ...
            }
        }

        // Continue through filter chain.
        chain.doFilter(req, res);
    }
    
}
