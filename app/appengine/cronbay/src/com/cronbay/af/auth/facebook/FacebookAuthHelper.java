package com.cronbay.af.auth.facebook;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.config.Config;
import com.cronbay.af.util.URLUtil;
import com.cronbay.af.auth.common.CommonAuthUtil;


public class FacebookAuthHelper
{
    private static final Logger log = Logger.getLogger(FacebookAuthHelper.class.getName());

    // "Lazy initialization"
    private String mAppId = null;
    private String mAppSecret = null;
    private String mOAuthRedirectUrlPath = null;
    //private String mAfterLoginRedirectUrlPath = null;
    //private String mAccessTokenRedirectUrlPath = null;
    private List<String> mDefaultPermissionList = null;
    

    private FacebookAuthHelper() {}

    // Initialization-on-demand holder.
    private static final class FacebookAuthHelperHolder
    {
        private static final FacebookAuthHelper INSTANCE = new FacebookAuthHelper();
    }

    // Singleton method
    public static FacebookAuthHelper getInstance()
    {
        return FacebookAuthHelperHolder.INSTANCE;
    }
    
    
    public String getAppId()
    {
        if(mAppId == null) {
            mAppId = Config.getInstance().getString(FacebookAuthUtil.CONFIG_KEY_APPID);
        }
        return mAppId;
    }
    public String getAppSecret()
    {
        if(mAppSecret == null) {
            mAppSecret = Config.getInstance().getString(FacebookAuthUtil.CONFIG_KEY_APPSECRET);
        }
        return mAppSecret;
    }
    public String getOAuthRedirectUrlPath()
    {
        if(mOAuthRedirectUrlPath == null) {
            mOAuthRedirectUrlPath = Config.getInstance().getString(FacebookAuthUtil.CONFIG_KEY_OAUTH_REDIRECTURLPATH);
        }
        return mOAuthRedirectUrlPath;
    }
//    public String getAfterLoginRedirectUrlPath()
//    {
//        if(mAfterLoginRedirectUrlPath == null) {
//            mAfterLoginRedirectUrlPath = Config.getInstance().getString(FacebookAuthUtil.CONFIG_KEY_AFTERLOGIN_REDIRECTURLPATH);
//        }
//        return mAfterLoginRedirectUrlPath;
//    }
//    public String getAccessTokenRedirectUrlPath()
//    {
//        if(mAccessTokenRedirectUrlPath == null) {
//            mAccessTokenRedirectUrlPath = Config.getInstance().getString(FacebookAuthUtil.CONFIG_KEY_ACCESSTOKEN_REDIRECTURLPATH);
//        }
//        return mAccessTokenRedirectUrlPath;
//    }
    public List<String> getDefaultPermissionList()
    {
        if(mDefaultPermissionList == null) {
            mDefaultPermissionList = new ArrayList<String>();
            String permissions = Config.getInstance().getString(FacebookAuthUtil.CONFIG_KEY_DEFAULT_PERMISSIONS);
            if(permissions != null && !permissions.isEmpty()) {
                String[] permissionArr = permissions.split("\\s*,\\s*");
                mDefaultPermissionList.addAll(Arrays.asList(permissionArr));
            }
        }
        return mDefaultPermissionList;
    }


    public String getDefaultOAuthRedirectUri(String topLevelUrl)
    {
        return CommonAuthUtil.constructUrl(topLevelUrl, getOAuthRedirectUrlPath());
    }
//    public String getDefaultAfterLoginRedirectUri(String topLevelUrl)
//    {
//        return CommonAuthUtil.constructUrl(topLevelUrl, getAfterLoginRedirectUrlPath());
//    }
//    public String getDefaultAccessTokenRedirectUri(String topLevelUrl)
//    {
//        return CommonAuthUtil.constructUrl(topLevelUrl, getAccessTokenRedirectUrlPath());
//    }

    public String getOAuthLoginUrl(String destinationURL, String state)
    {
        String baseUrl = FacebookAuthUtil.BASE_URL_OAUTH_LOGIN;
        Map<String,Object> params = new LinkedHashMap<String,Object>();  // Ordered, for debugging purposes.
        params.put(FacebookAuthUtil.PARAM_CLIENT_ID, getAppId());
        params.put(FacebookAuthUtil.PARAM_REDIRECT_URI, destinationURL);
        List<String> permissions = getDefaultPermissionList();
        if(permissions != null && !permissions.isEmpty()) {
            params.put(FacebookAuthUtil.PARAM_SCOPE, permissions);
        }
        if(state == null || state.isEmpty()) {
            state = CommonAuthUtil.generateRandomNonce();
        }
        params.put(FacebookAuthUtil.PARAM_STATE, state);
        String url = URLUtil.buildUrl(baseUrl, params);
        return url;
    }

    public String getAcessTokenExchangeUrl(String destinationURL, String code)
    {
        String baseUrl = FacebookAuthUtil.BASE_URL_ACCESS_TOKEN_EXCHANGE;
        Map<String,Object> params = new LinkedHashMap<String,Object>();  // Ordered, for debugging purposes.
        params.put(FacebookAuthUtil.PARAM_CLIENT_ID, getAppId());
        params.put(FacebookAuthUtil.PARAM_CLIENT_SECRET, getAppSecret());
        params.put(FacebookAuthUtil.PARAM_REDIRECT_URI, destinationURL);
        params.put(FacebookAuthUtil.PARAM_CODE, code);
        String url = URLUtil.buildUrl(baseUrl, params);
        return url;
    }

    public Map<String,Object> parseAccessTokenContent(String content)
    {
        if(content == null) {
            return null;
        }
        Map<String,Object> map = new HashMap<String,Object>();
        if(!content.isEmpty()) {
            String[] parts = content.split("&");
            if(parts != null && parts.length > 0) {
                for(String part : parts) {
                    String[] pair = part.split("=", 2);
                    if(pair != null && pair.length > 0) {
                        String key = pair[0];
                        Object val = null;
                        if(pair.length > 1) {
                            val = pair[1];
                            if(key.equals(FacebookAuthUtil.RESPONSE_VAR_EXPIRES)) {
                                try {
                                    val = Integer.parseInt((String) val);
                                } catch(Exception e) {
                                    log.log(Level.WARNING, "Failed to parse the expires param.", e);
                                    val = null;
                                }
                            } else {
                                try {
                                    val = URLDecoder.decode((String) val, "UTF-8");
                                } catch(Exception e) {
                                    log.log(Level.WARNING, "Failed to url decode a param value. val = " + val, e);
                                    //val = null;
                                }
                            }
                        }
                        map.put(key, val);
                    }
                }
            }
        }
        return map;
    }

}
