package com.cronbay.af.auth.facebook;

import java.util.logging.Logger;
import java.util.logging.Level;


public class FacebookAuthUtil
{
    private static final Logger log = Logger.getLogger(FacebookAuthUtil.class.getName());

    public static final String CONFIG_KEY_APIKEY = "cronbayapp.facebook.apikey";
    public static final String CONFIG_KEY_APPID = "cronbayapp.facebook.appid";  // client-id
    public static final String CONFIG_KEY_APPSECRET = "cronbayapp.facebook.appsecret";
    public static final String CONFIG_KEY_OAUTH_REDIRECTURLPATH = "cronbayapp.facebook.oauth.redirecturlpath";
    //public static final String CONFIG_KEY_AFTERLOGIN_REDIRECTURLPATH = "cronbayapp.facebook.afterlogin.redirecturlpath";
    //public static final String CONFIG_KEY_ACCESSTOKEN_REDIRECTURLPATH = "cronbayapp.facebook.accesstoken.redirecturlpath"; 
    public static final String CONFIG_KEY_DEFAULT_PERMISSIONS = "cronbayapp.facebook.default.permissions";
    // ...
    
    public static final String PARAM_CODE = "code";
    public static final String PARAM_SCOPE = "scope";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_CLIENT_ID = "client_id";
    public static final String PARAM_CLIENT_SECRET = "client_secret";
    public static final String PARAM_REDIRECT_URI = "redirect_uri";
    public static final String PARAM_ERROR = "error";
    public static final String PARAM_ERROR_REASON = "error_reason";
    public static final String PARAM_ERROR_DESCRIPTION = "error_description";
    // ...
    public static final String RESPONSE_VAR_ACCESS_TOKEN = "access_token";
    public static final String RESPONSE_VAR_EXPIRES = "expires";
    // ...
    
    public static final String BASE_URL_OAUTH_LOGIN = "https://www.facebook.com/dialog/oauth";
    public static final String BASE_URL_ACCESS_TOKEN_EXCHANGE = "https://graph.facebook.com/oauth/access_token";
    // ...

    public static final String SESSION_ATTR_STATE = "com.cronbay.af.auth.facebook.oauth.state";
    // ...
    public static final String REQUEST_ATTR_ACCESS_TOKEN = "com.cronbay.af.auth.facebook.oauth.access_token";
    public static final String REQUEST_ATTR_EXPIRES = "com.cronbay.af.auth.facebook.oauth.expires";
    // ...

    
    private FacebookAuthUtil() {}

    
    // TBD:
    // ...

}
