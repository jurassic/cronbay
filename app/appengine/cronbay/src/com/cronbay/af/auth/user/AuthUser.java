package com.cronbay.af.auth.user;

import java.util.logging.Logger;
import com.google.appengine.api.users.User;


// Wrapper for com.google.appengine.api.users.User
public class AuthUser implements java.io.Serializable, java.lang.Comparable<AuthUser>
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AuthUser.class.getName());

    // Embedded object
    private User mGaeUser = null;
    
    // This is a "temporary" variable. Not backed by database.
    private String mUserGuid = null;
    // ....

    public AuthUser(User gaeUser)
    {
        this(gaeUser, null);
    }
    public AuthUser(User gaeUser, String userGuid)
    {
        // TBD: What to do if geeUser == null????
        mGaeUser = gaeUser;
        mUserGuid = userGuid;
    }

    public AuthUser(String email, String authDomain) 
    {
        mGaeUser = new User(email, authDomain);
        mUserGuid = null;
    }

    public AuthUser(String email, String authDomain, String userId) 
    {
        mGaeUser = new User(email, authDomain, userId);
        mUserGuid = null;
    }

    public AuthUser(String email, String authDomain, String userId, String federatedIdentity) 
    {
        mGaeUser = new User(email, authDomain, userId, federatedIdentity);
        mUserGuid = null;
    }

    
    public User getGaeUser()
    {
        if(mGaeUser == null) {
            // TBD:...
            // mGaeUser = ... ;
        }
        return mGaeUser;
    }
    public void setUser(User gaeUser)
    {
        mGaeUser = gaeUser;
        mUserGuid = null;
    }

    
    public String getAuthDomain()
    {
        return getGaeUser().getAuthDomain();
    }

    public String getEmail() 
    {
        return getGaeUser().getEmail();
    }

    public String getFederatedIdentity() 
    {
        return getGaeUser().getFederatedIdentity();
    }
               
    public String getNickname() 
    {
        return getGaeUser().getNickname();
    }

    public String getUserId() 
    {
        return getGaeUser().getUserId();
    }


    public String getUserGuid()
    {
        return mUserGuid;
    }

    public void setUserGuid(String userGuid)
    {
        this.mUserGuid = userGuid;
    }

    
    @Override
    public int compareTo(AuthUser authUser)
    {
        return getGaeUser().compareTo(authUser.getGaeUser());
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((mGaeUser == null) ? 0 : mGaeUser.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AuthUser other = (AuthUser) obj;
        if (mGaeUser == null) {
            if (other.mGaeUser != null)
                return false;
        } else if (!mGaeUser.equals(other.mGaeUser))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "AuthUser [mGaeUser=" + mGaeUser + "]";
    }

    
}
