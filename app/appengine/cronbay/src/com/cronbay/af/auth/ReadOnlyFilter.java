package com.cronbay.af.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


public class ReadOnlyFilter implements Filter
{
    private static final Logger log = Logger.getLogger(ReadOnlyFilter.class.getName());

    // TBD
    private FilterConfig config = null;
    
    public ReadOnlyFilter()
    {
    }

    @Override
    public void destroy()
    {
        this.config = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.config = config;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException
    {
        // GET-only access
        boolean accessAllowed = false;
        if(req instanceof HttpServletRequest) {
            if("GET".equals(((HttpServletRequest) req).getMethod())) {
                accessAllowed = true;
            }
        }
        log.log(Level.INFO, "ReadOnlyFilter.doFilter(): accessAllowed = " + accessAllowed);

        // TBD
        if (accessAllowed == true) {
            // Continue through filter chain.
            chain.doFilter(req, res);
        } else {
            // Redirect to the error or login page. ???
            config.getServletContext().getRequestDispatcher("/").forward(req, res);
            return;
        }
    }
    
}
