package com.cronbay.af.task;

import java.util.Map;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

public class TaskManager
{
    // Singleton.
    private TaskManager() {}

    // Initialization-on-demand holder.
    private static class TaskManagerHolder
    {
        private static final TaskManager INSTANCE = new TaskManager();
    }

    // Singleton method
    public static TaskManager getInstance()
    {
        return TaskManagerHolder.INSTANCE;
    }

    /**
     * @param queue Queue name.
     * @param task GenericTask.
     */
    public void createTask(String queue, GenericTask task)
    {
        Queue q = QueueFactory.getQueue(queue); 

        TaskOptions options = TaskOptions.Builder.withUrl(task.getUrl()).method(Method.POST);
        
        Map<String, String> params = task.getParams();
        for(Map.Entry<String, String> e : params.entrySet()) {
            options = options.param(e.getKey(), e.getValue());
        }

        // TBD: 
        // Add POST data.
        
        q.add(options);
        
    }
    
}
