package com.cronbay.af.task;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cronbay.ws.BaseException;

// TBD
@Path("/_task/")
public class UniversalTaskResource
{
    private static final Logger log = Logger.getLogger(UniversalTaskResource.class.getName());

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response performTask(@QueryParam("action") String action, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseException
    {
        // TBD:
        // Parse action param
        // and excute a method corresponding to the action
        //     with the params/values.
        // ...
        
        return Response.ok().build();       
    }


}
