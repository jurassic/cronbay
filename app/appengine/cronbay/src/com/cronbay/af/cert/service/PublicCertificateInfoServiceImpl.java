package com.cronbay.af.cert.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.cert.PublicCertificateInfo;
import com.cronbay.af.cert.bean.PublicCertificateInfoBean;
import com.cronbay.af.cert.proxy.AbstractProxyFactory;
import com.cronbay.af.cert.proxy.ProxyFactoryManager;
import com.cronbay.af.cert.service.PublicCertificateInfoService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class PublicCertificateInfoServiceImpl implements PublicCertificateInfoService
{
    private static final Logger log = Logger.getLogger(PublicCertificateInfoServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;
    
    public PublicCertificateInfoServiceImpl()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            //props.put(GCacheFactory.EXPIRATION_DELTA, ServiceConstants.CACHE_EXPIRATION_DURATION);
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }


    //////////////////////////////////////////////////////////////////////////
    // PublicCertificateInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public PublicCertificateInfo getPublicCertificateInfo(String guid) throws BaseException
    {
        PublicCertificateInfoBean bean = null;
        if(mCache != null) {
            bean = (PublicCertificateInfoBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (PublicCertificateInfoBean) getProxyFactory().getPublicCertificateInfoServiceProxy().getPublicCertificateInfo(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "PublicCertificateInfoBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve PublicCertificateInfoBean for guid = " + guid);
        }
        return bean;
    }

    @Override
    public Object getPublicCertificateInfo(String guid, String field) throws BaseException
    {
        PublicCertificateInfoBean bean = null;
        if(mCache != null) {
            bean = (PublicCertificateInfoBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (PublicCertificateInfoBean) getProxyFactory().getPublicCertificateInfoServiceProxy().getPublicCertificateInfo(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "PublicCertificateInfoBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve PublicCertificateInfoBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appId")) {
            return bean.getAppId();
        } else if(field.equals("appUrl")) {
            return bean.getAppUrl();
        } else if(field.equals("certName")) {
            return bean.getCertName();
        } else if(field.equals("certInPemFormat")) {
            return bean.getCertInPemFormat();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<PublicCertificateInfo> getAllPublicCertificateInfos() throws BaseException
    {
        return getAllPublicCertificateInfos(null, null, null);
    }

    @Override
    public List<PublicCertificateInfo> getAllPublicCertificateInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        // TBD: Is there a better way????
        List<PublicCertificateInfo> publicCertificateInfos = getProxyFactory().getPublicCertificateInfoServiceProxy().getAllPublicCertificateInfos(ordering, offset, count);
        if(publicCertificateInfos == null) {
            log.log(Level.WARNING, "Failed to retrieve PublicCertificateInfoBean list.");
        }
        return publicCertificateInfos;
    }

    @Override
    public List<PublicCertificateInfo> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findPublicCertificateInfos(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<PublicCertificateInfo> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("PublicCertificateInfoServiceImpl.findPublicCertificateInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<PublicCertificateInfo> publicCertificateInfos = getProxyFactory().getPublicCertificateInfoServiceProxy().findPublicCertificateInfos(filter, ordering, params, values, grouping, unique, offset, count);
        if(publicCertificateInfos == null) {
            log.log(Level.WARNING, "Failed to find publicCertificateInfos for the given criterion.");
        }
        return publicCertificateInfos;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("PublicCertificateInfoServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = getProxyFactory().getPublicCertificateInfoServiceProxy().getCount(filter, params, values, aggregate);
        return count;
    }

    @Override
    public String createPublicCertificateInfo(String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        PublicCertificateInfoBean bean = new PublicCertificateInfoBean(null, appId, appUrl, certName, certInPemFormat, note, status, expirationTime);
        return createPublicCertificateInfo(bean);
    }

    @Override
    public String createPublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        // TBD
        //PublicCertificateInfo bean = constructPublicCertificateInfo(publicCertificateInfo);
        //return bean.getGuid();

        // Param publicCertificateInfo cannot be null.....
        if(publicCertificateInfo == null) {
            log.log(Level.INFO, "Param publicCertificateInfo is null!");
            throw new BadRequestException("Param publicCertificateInfo object is null!");
        }
        PublicCertificateInfoBean bean = null;
        if(publicCertificateInfo instanceof PublicCertificateInfoBean) {
            bean = (PublicCertificateInfoBean) publicCertificateInfo;
        } else if(publicCertificateInfo instanceof PublicCertificateInfo) {
            // bean = new PublicCertificateInfoBean(null, publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new PublicCertificateInfoBean(publicCertificateInfo.getGuid(), publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createPublicCertificateInfo(): Arg publicCertificateInfo is of an unknown type.");
            //bean = new PublicCertificateInfoBean();
            bean = new PublicCertificateInfoBean(publicCertificateInfo.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getPublicCertificateInfoServiceProxy().createPublicCertificateInfo(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }
        return guid;
    }

    @Override
    public PublicCertificateInfo constructPublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        // Param publicCertificateInfo cannot be null.....
        if(publicCertificateInfo == null) {
            log.log(Level.INFO, "Param publicCertificateInfo is null!");
            throw new BadRequestException("Param publicCertificateInfo object is null!");
        }
        PublicCertificateInfoBean bean = null;
        if(publicCertificateInfo instanceof PublicCertificateInfoBean) {
            bean = (PublicCertificateInfoBean) publicCertificateInfo;
        } else if(publicCertificateInfo instanceof PublicCertificateInfo) {
            // bean = new PublicCertificateInfoBean(null, publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new PublicCertificateInfoBean(publicCertificateInfo.getGuid(), publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createPublicCertificateInfo(): Arg publicCertificateInfo is of an unknown type.");
            //bean = new PublicCertificateInfoBean();
            bean = new PublicCertificateInfoBean(publicCertificateInfo.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getPublicCertificateInfoServiceProxy().createPublicCertificateInfo(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }
        return bean;
    }

    @Override
    public Boolean updatePublicCertificateInfo(String guid, String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        PublicCertificateInfoBean bean = new PublicCertificateInfoBean(guid, appId, appUrl, certName, certInPemFormat, note, status, expirationTime);
        return updatePublicCertificateInfo(bean);
    }
        
    @Override
    public Boolean updatePublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        // TBD
        //PublicCertificateInfo bean = refreshPublicCertificateInfo(publicCertificateInfo);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param publicCertificateInfo cannot be null.....
        if(publicCertificateInfo == null || publicCertificateInfo.getGuid() == null) {
            log.log(Level.INFO, "Param publicCertificateInfo or its guid is null!");
            throw new BadRequestException("Param publicCertificateInfo object or its guid is null!");
        }
        PublicCertificateInfoBean bean = null;
        if(publicCertificateInfo instanceof PublicCertificateInfoBean) {
            bean = (PublicCertificateInfoBean) publicCertificateInfo;
        } else if(publicCertificateInfo instanceof PublicCertificateInfoBean) {
            bean = (PublicCertificateInfoBean) publicCertificateInfo;
        } else {  // if(publicCertificateInfo instanceof PublicCertificateInfo)
            bean = new PublicCertificateInfoBean(publicCertificateInfo.getGuid(), publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getPublicCertificateInfoServiceProxy().updatePublicCertificateInfo(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }
        return suc;
    }
        
    @Override
    public PublicCertificateInfo refreshPublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        // Param publicCertificateInfo cannot be null.....
        if(publicCertificateInfo == null || publicCertificateInfo.getGuid() == null) {
            log.log(Level.INFO, "Param publicCertificateInfo or its guid is null!");
            throw new BadRequestException("Param publicCertificateInfo object or its guid is null!");
        }
        PublicCertificateInfoBean bean = null;
        if(publicCertificateInfo instanceof PublicCertificateInfoBean) {
            bean = (PublicCertificateInfoBean) publicCertificateInfo;
        } else if(publicCertificateInfo instanceof PublicCertificateInfoBean) {
            bean = (PublicCertificateInfoBean) publicCertificateInfo;
        } else {  // if(publicCertificateInfo instanceof PublicCertificateInfo)
            bean = new PublicCertificateInfoBean(publicCertificateInfo.getGuid(), publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getPublicCertificateInfoServiceProxy().updatePublicCertificateInfo(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }
        //if(suc == true)  // else ???
        return bean;
    }

    @Override
    public Boolean deletePublicCertificateInfo(String guid) throws BaseException
    {
        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getProxyFactory().getPublicCertificateInfoServiceProxy().deletePublicCertificateInfo(guid);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(guid);
            //}
        }
        return suc;
    }

    // ???
    @Override
    public Boolean deletePublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        // Param publicCertificateInfo cannot be null.....
        if(publicCertificateInfo == null || publicCertificateInfo.getGuid() == null) {
            log.log(Level.INFO, "Param publicCertificateInfo or its guid is null!");
            throw new BadRequestException("Param publicCertificateInfo object or its guid is null!");
        }
        PublicCertificateInfoBean bean = null;
        if(publicCertificateInfo instanceof PublicCertificateInfoBean) {
            bean = (PublicCertificateInfoBean) publicCertificateInfo;
        } else {  // if(publicCertificateInfo instanceof PublicCertificateInfo)
            bean = new PublicCertificateInfoBean(publicCertificateInfo.getGuid(), publicCertificateInfo.getAppId(), publicCertificateInfo.getAppUrl(), publicCertificateInfo.getCertName(), publicCertificateInfo.getCertInPemFormat(), publicCertificateInfo.getNote(), publicCertificateInfo.getStatus(), publicCertificateInfo.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getPublicCertificateInfoServiceProxy().deletePublicCertificateInfo(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }
        return suc;
    }

    // TBD
    @Override
    public Long deletePublicCertificateInfos(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getPublicCertificateInfoServiceProxy().deletePublicCertificateInfos(filter, params, values);
        if(mCache != null) {
            //if(count > 0) {
            //    mCache.remove(...);
            //}
        }
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createPublicCertificateInfos(List<PublicCertificateInfo> publicCertificateInfos) throws BaseException
    {
        if(publicCertificateInfos == null) {
            log.log(Level.WARNING, "createPublicCertificateInfos() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = publicCertificateInfos.size();
        if(size == 0) {
            log.log(Level.WARNING, "createPublicCertificateInfos() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(PublicCertificateInfo publicCertificateInfo : publicCertificateInfos) {
            String guid = createPublicCertificateInfo(publicCertificateInfo);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                log.log(Level.WARNING, "createPublicCertificateInfos() failed for at least one publicCertificateInfo. Index = " + count);
            }
        }
        return count;
    }

    // TBD
    //@Override
    //public Boolean updatePublicCertificateInfos(List<PublicCertificateInfo> publicCertificateInfos) throws BaseException
    //{
    //}

}
