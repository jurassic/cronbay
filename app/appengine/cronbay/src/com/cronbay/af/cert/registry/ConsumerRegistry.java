package com.cronbay.af.cert.registry;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.config.Config;


// OAuth consumer key registry.
// To be used for authenticating itself against WS service.
public class ConsumerRegistry
{
    private static final Logger log = Logger.getLogger(ConsumerRegistry.class.getName());

    // temporary
    private static final String CONFIG_KEY_DEFAULT_CONSUMER_KEY = "cronbayapp.oauth.consumerkey";
    private static final String CONFIG_KEY_DEFAULT_CONSUMER_SECRET = "cronbayapp.oauth.consumersecret";
    
    private String defaultConsumerKey;
    private String defaultConsumerSecret;

    private ConsumerRegistry()
    {
        // TBD:
        // Read these from a private file/DB, etc...
//        defaultConsumerKey = "ac567b5c-b8a0-4a54-92b4-78033dafff1b";
//        defaultConsumerSecret = "9db30167-5233-42e8-b277-e73695d3167c";
        defaultConsumerKey = Config.getInstance().getString(CONFIG_KEY_DEFAULT_CONSUMER_KEY, "");  // ???
        defaultConsumerSecret = Config.getInstance().getString(CONFIG_KEY_DEFAULT_CONSUMER_SECRET, "");  // ???
    }

    // Initialization-on-demand holder.
    private static class ConsumerRegistryHolder
    {
        private static final ConsumerRegistry INSTANCE = new ConsumerRegistry();
    }

    // Singleton method
    public static ConsumerRegistry getInstance()
    {
        return ConsumerRegistryHolder.INSTANCE;
    }


    public String getDefaultConsumerKey()
    {
        // TBD.
        return defaultConsumerKey;
    }

    public String getDefaultConsumerSecret()
    {
        // TBD.
        return defaultConsumerSecret;
    }

}
