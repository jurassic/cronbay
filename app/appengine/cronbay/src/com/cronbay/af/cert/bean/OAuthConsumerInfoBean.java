package com.cronbay.af.cert.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.cert.OAuthConsumerInfo;

// Not a stub wrapper.
public class OAuthConsumerInfoBean implements OAuthConsumerInfo, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OAuthConsumerInfoBean.class.getName());

    // [2] Or, without an embedded object.
    private String guid;
    private String serviceName;
    private String appId;
    private String appUrl;
    private String consumerKey;
    private String consumerSecret;
    private String note;
    private String status;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public OAuthConsumerInfoBean()
    {
        //this((String) null);
    }
    public OAuthConsumerInfoBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public OAuthConsumerInfoBean(String guid, String serviceName, String appId, String appUrl, String consumerKey, String consumerSecret, String note, String status, Long expirationTime)
    {
        this(guid, serviceName, appId, appUrl, consumerKey, consumerSecret, note, status, expirationTime, null, null);
    }
    public OAuthConsumerInfoBean(String guid, String serviceName, String appId, String appUrl, String consumerKey, String consumerSecret, String note, String status, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.serviceName = serviceName;
        this.appId = appId;
        this.appUrl = appUrl;
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
        this.note = note;
        this.status = status;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getServiceName()
    {
        return this.serviceName;
    }
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getAppId()
    {
        return this.appId;
    }
    public void setAppId(String appId)
    {
        this.appId = appId;
    }

    public String getAppUrl()
    {
        return this.appUrl;
    }
    public void setAppUrl(String appUrl)
    {
        this.appUrl = appUrl;
    }

    public String getConsumerKey()
    {
        return this.consumerKey;
    }
    public void setConsumerKey(String consumerKey)
    {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret()
    {
        return this.consumerSecret;
    }
    public void setConsumerSecret(String consumerSecret)
    {
        this.consumerSecret = consumerSecret;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("guid = " + this.guid).append(";");
        sb.append("serviceName = " + this.serviceName).append(";");
        sb.append("appId = " + this.appId).append(";");
        sb.append("appUrl = " + this.appUrl).append(";");
        sb.append("consumerKey = " + this.consumerKey).append(";");
        sb.append("consumerSecret = " + this.consumerSecret).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        hash = 31 * hash + delta;
        delta = serviceName == null ? 0 : serviceName.hashCode();
        hash = 31 * hash + delta;
        delta = appId == null ? 0 : appId.hashCode();
        hash = 31 * hash + delta;
        delta = appUrl == null ? 0 : appUrl.hashCode();
        hash = 31 * hash + delta;
        delta = consumerKey == null ? 0 : consumerKey.hashCode();
        hash = 31 * hash + delta;
        delta = consumerSecret == null ? 0 : consumerSecret.hashCode();
        hash = 31 * hash + delta;
        delta = note == null ? 0 : note.hashCode();
        hash = 31 * hash + delta;
        delta = status == null ? 0 : status.hashCode();
        hash = 31 * hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        hash = 31 * hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        hash = 31 * hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        hash = 31 * hash + delta;
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
