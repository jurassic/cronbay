package com.cronbay.af.cert.registry;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;


// Public certificate registry.
public class CertRegistry
{
    private static final Logger log = Logger.getLogger(CertRegistry.class.getName());

    private CertRegistry()
    {
    }

    // Initialization-on-demand holder.
    private static class CertRegistryHolder
    {
        private static final CertRegistry INSTANCE = new CertRegistry();
    }

    // Singleton method
    public static CertRegistry getInstance()
    {
        return CertRegistryHolder.INSTANCE;
    }


    public String getPublicCert(String appId)
    {
        // TBD.
        return null;
    }

}
