package com.cronbay.af.cert.proxy;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.cert.PublicCertificateInfo;
import com.cronbay.ws.cert.service.PublicCertificateInfoService;
import com.cronbay.af.cert.proxy.PublicCertificateInfoServiceProxy;

public class LocalPublicCertificateInfoServiceProxy extends BaseLocalServiceProxy implements PublicCertificateInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalPublicCertificateInfoServiceProxy.class.getName());

    public LocalPublicCertificateInfoServiceProxy()
    {
    }

    @Override
    public PublicCertificateInfo getPublicCertificateInfo(String guid) throws BaseException
    {
        return getPublicCertificateInfoService().getPublicCertificateInfo(guid);
    }

    @Override
    public Object getPublicCertificateInfo(String guid, String field) throws BaseException
    {
        return getPublicCertificateInfoService().getPublicCertificateInfo(guid, field);       
    }

    @Override
    public List<PublicCertificateInfo> getAllPublicCertificateInfos() throws BaseException
    {
        return getAllPublicCertificateInfos(null, null, null);
    }

    @Override
    public List<PublicCertificateInfo> getAllPublicCertificateInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getPublicCertificateInfoService().getAllPublicCertificateInfos(ordering, offset, count);
    }

    @Override
    public List<PublicCertificateInfo> findPublicCertificateInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findPublicCertificateInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<PublicCertificateInfo> findPublicCertificateInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return getPublicCertificateInfoService().findPublicCertificateInfos(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getPublicCertificateInfoService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createPublicCertificateInfo(String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime) throws BaseException
    {
        return getPublicCertificateInfoService().createPublicCertificateInfo(appId, appUrl, certName, certInPemFormat, note, status, expirationTime);
    }

    @Override
    public String createPublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        return getPublicCertificateInfoService().createPublicCertificateInfo(publicCertificateInfo);
    }

    @Override
    public Boolean updatePublicCertificateInfo(String guid, String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime) throws BaseException
    {
        return getPublicCertificateInfoService().updatePublicCertificateInfo(guid, appId, appUrl, certName, certInPemFormat, note, status, expirationTime);
    }

    @Override
    public Boolean updatePublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        return getPublicCertificateInfoService().updatePublicCertificateInfo(publicCertificateInfo);
    }

    @Override
    public Boolean deletePublicCertificateInfo(String guid) throws BaseException
    {
        return getPublicCertificateInfoService().deletePublicCertificateInfo(guid);
    }

    @Override
    public Boolean deletePublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException
    {
        return getPublicCertificateInfoService().deletePublicCertificateInfo(publicCertificateInfo);
    }

    @Override
    public Long deletePublicCertificateInfos(String filter, String params, List<String> values) throws BaseException
    {
        return getPublicCertificateInfoService().deletePublicCertificateInfos(filter, params, values);
    }

}
