package com.cronbay.af.cert.proxy;

public abstract class AbstractProxyFactory
{
    public abstract PublicCertificateInfoServiceProxy getPublicCertificateInfoServiceProxy();
}
