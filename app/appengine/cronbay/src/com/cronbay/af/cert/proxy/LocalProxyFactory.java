package com.cronbay.af.cert.proxy;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.cert.proxy.PublicCertificateInfoServiceProxy;


public class LocalProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(LocalProxyFactory.class.getName());

    private LocalProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class LocalProxyFactoryHolder
    {
        private static final LocalProxyFactory INSTANCE = new LocalProxyFactory();
    }

    // Singleton method
    public static LocalProxyFactory getInstance()
    {
        return LocalProxyFactoryHolder.INSTANCE;
    }

    @Override
    public PublicCertificateInfoServiceProxy getPublicCertificateInfoServiceProxy()
    {
        return new LocalPublicCertificateInfoServiceProxy();
    }

}
