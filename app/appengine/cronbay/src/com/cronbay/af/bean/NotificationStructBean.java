package com.cronbay.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.stub.NotificationStructStub;

// Wrapper class + bean combo.
public class NotificationStructBean implements NotificationStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(NotificationStructBean.class.getName());

    // [1] With an embedded object.
    private NotificationStructStub stub = null;

    // [2] Or, without an embedded object.
    private String preferredMode;
    private String mobileNumber;
    private String emailAddress;
    private String callbackUrl;
    private String note;

    // Ctors.
    public NotificationStructBean()
    {
        //this((String) null);
    }
    public NotificationStructBean(String preferredMode, String mobileNumber, String emailAddress, String callbackUrl, String note)
    {
        this.preferredMode = preferredMode;
        this.mobileNumber = mobileNumber;
        this.emailAddress = emailAddress;
        this.callbackUrl = callbackUrl;
        this.note = note;
    }
    public NotificationStructBean(NotificationStruct stub)
    {
        if(stub instanceof NotificationStructStub) {
            this.stub = (NotificationStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setPreferredMode(stub.getPreferredMode());   
            setMobileNumber(stub.getMobileNumber());   
            setEmailAddress(stub.getEmailAddress());   
            setCallbackUrl(stub.getCallbackUrl());   
            setNote(stub.getNote());   
        } else {
            log.log(Level.WARNING, "The arg stub object is null.");
        }
    }

    public String getPreferredMode()
    {
        if(getStub() != null) {
            return getStub().getPreferredMode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.preferredMode;
        }
    }
    public void setPreferredMode(String preferredMode)
    {
        if(getStub() != null) {
            getStub().setPreferredMode(preferredMode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.preferredMode = preferredMode;
        }
    }

    public String getMobileNumber()
    {
        if(getStub() != null) {
            return getStub().getMobileNumber();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.mobileNumber;
        }
    }
    public void setMobileNumber(String mobileNumber)
    {
        if(getStub() != null) {
            getStub().setMobileNumber(mobileNumber);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.mobileNumber = mobileNumber;
        }
    }

    public String getEmailAddress()
    {
        if(getStub() != null) {
            return getStub().getEmailAddress();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.emailAddress;
        }
    }
    public void setEmailAddress(String emailAddress)
    {
        if(getStub() != null) {
            getStub().setEmailAddress(emailAddress);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.emailAddress = emailAddress;
        }
    }

    public String getCallbackUrl()
    {
        if(getStub() != null) {
            return getStub().getCallbackUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.callbackUrl;
        }
    }
    public void setCallbackUrl(String callbackUrl)
    {
        if(getStub() != null) {
            getStub().setCallbackUrl(callbackUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.callbackUrl = callbackUrl;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public NotificationStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(NotificationStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("preferredMode = " + this.preferredMode).append(";");
            sb.append("mobileNumber = " + this.mobileNumber).append(";");
            sb.append("emailAddress = " + this.emailAddress).append(";");
            sb.append("callbackUrl = " + this.callbackUrl).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = preferredMode == null ? 0 : preferredMode.hashCode();
            _hash = 31 * _hash + delta;
            delta = mobileNumber == null ? 0 : mobileNumber.hashCode();
            _hash = 31 * _hash + delta;
            delta = emailAddress == null ? 0 : emailAddress.hashCode();
            _hash = 31 * _hash + delta;
            delta = callbackUrl == null ? 0 : callbackUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
