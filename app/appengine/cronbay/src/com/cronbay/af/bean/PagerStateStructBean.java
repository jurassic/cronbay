package com.cronbay.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.PagerStateStruct;
import com.cronbay.ws.stub.PagerStateStructStub;

// Wrapper class + bean combo.
public class PagerStateStructBean implements PagerStateStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PagerStateStructBean.class.getName());

    // [1] With an embedded object.
    private PagerStateStructStub stub = null;

    // [2] Or, without an embedded object.
    private String pagerMode;
    private String primaryOrdering;
    private String secondaryOrdering;
    private Long currentOffset;
    private Long currentPage;
    private Integer pageSize;
    private Long totalCount;
    private Long lowerBoundTotalCount;
    private Long previousPageOffset;
    private Long nextPageOffset;
    private Long lastPageOffset;
    private Long lastPageIndex;
    private Boolean firstActionEnabled;
    private Boolean previousActionEnabled;
    private Boolean nextActionEnabled;
    private Boolean lastActionEnabled;
    private String note;

    // Ctors.
    public PagerStateStructBean()
    {
        //this((String) null);
    }
    public PagerStateStructBean(String pagerMode, String primaryOrdering, String secondaryOrdering, Long currentOffset, Long currentPage, Integer pageSize, Long totalCount, Long lowerBoundTotalCount, Long previousPageOffset, Long nextPageOffset, Long lastPageOffset, Long lastPageIndex, Boolean firstActionEnabled, Boolean previousActionEnabled, Boolean nextActionEnabled, Boolean lastActionEnabled, String note)
    {
        this.pagerMode = pagerMode;
        this.primaryOrdering = primaryOrdering;
        this.secondaryOrdering = secondaryOrdering;
        this.currentOffset = currentOffset;
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.lowerBoundTotalCount = lowerBoundTotalCount;
        this.previousPageOffset = previousPageOffset;
        this.nextPageOffset = nextPageOffset;
        this.lastPageOffset = lastPageOffset;
        this.lastPageIndex = lastPageIndex;
        this.firstActionEnabled = firstActionEnabled;
        this.previousActionEnabled = previousActionEnabled;
        this.nextActionEnabled = nextActionEnabled;
        this.lastActionEnabled = lastActionEnabled;
        this.note = note;
    }
    public PagerStateStructBean(PagerStateStruct stub)
    {
        if(stub instanceof PagerStateStructStub) {
            this.stub = (PagerStateStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setPagerMode(stub.getPagerMode());   
            setPrimaryOrdering(stub.getPrimaryOrdering());   
            setSecondaryOrdering(stub.getSecondaryOrdering());   
            setCurrentOffset(stub.getCurrentOffset());   
            setCurrentPage(stub.getCurrentPage());   
            setPageSize(stub.getPageSize());   
            setTotalCount(stub.getTotalCount());   
            setLowerBoundTotalCount(stub.getLowerBoundTotalCount());   
            setPreviousPageOffset(stub.getPreviousPageOffset());   
            setNextPageOffset(stub.getNextPageOffset());   
            setLastPageOffset(stub.getLastPageOffset());   
            setLastPageIndex(stub.getLastPageIndex());   
            setFirstActionEnabled(stub.isFirstActionEnabled());   
            setPreviousActionEnabled(stub.isPreviousActionEnabled());   
            setNextActionEnabled(stub.isNextActionEnabled());   
            setLastActionEnabled(stub.isLastActionEnabled());   
            setNote(stub.getNote());   
        } else {
            log.log(Level.WARNING, "The arg stub object is null.");
        }
    }

    public String getPagerMode()
    {
        if(getStub() != null) {
            return getStub().getPagerMode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pagerMode;
        }
    }
    public void setPagerMode(String pagerMode)
    {
        if(getStub() != null) {
            getStub().setPagerMode(pagerMode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pagerMode = pagerMode;
        }
    }

    public String getPrimaryOrdering()
    {
        if(getStub() != null) {
            return getStub().getPrimaryOrdering();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.primaryOrdering;
        }
    }
    public void setPrimaryOrdering(String primaryOrdering)
    {
        if(getStub() != null) {
            getStub().setPrimaryOrdering(primaryOrdering);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.primaryOrdering = primaryOrdering;
        }
    }

    public String getSecondaryOrdering()
    {
        if(getStub() != null) {
            return getStub().getSecondaryOrdering();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.secondaryOrdering;
        }
    }
    public void setSecondaryOrdering(String secondaryOrdering)
    {
        if(getStub() != null) {
            getStub().setSecondaryOrdering(secondaryOrdering);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.secondaryOrdering = secondaryOrdering;
        }
    }

    public Long getCurrentOffset()
    {
        if(getStub() != null) {
            return getStub().getCurrentOffset();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.currentOffset;
        }
    }
    public void setCurrentOffset(Long currentOffset)
    {
        if(getStub() != null) {
            getStub().setCurrentOffset(currentOffset);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.currentOffset = currentOffset;
        }
    }

    public Long getCurrentPage()
    {
        if(getStub() != null) {
            return getStub().getCurrentPage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.currentPage;
        }
    }
    public void setCurrentPage(Long currentPage)
    {
        if(getStub() != null) {
            getStub().setCurrentPage(currentPage);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.currentPage = currentPage;
        }
    }

    public Integer getPageSize()
    {
        if(getStub() != null) {
            return getStub().getPageSize();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pageSize;
        }
    }
    public void setPageSize(Integer pageSize)
    {
        if(getStub() != null) {
            getStub().setPageSize(pageSize);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pageSize = pageSize;
        }
    }

    public Long getTotalCount()
    {
        if(getStub() != null) {
            return getStub().getTotalCount();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.totalCount;
        }
    }
    public void setTotalCount(Long totalCount)
    {
        if(getStub() != null) {
            getStub().setTotalCount(totalCount);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.totalCount = totalCount;
        }
    }

    public Long getLowerBoundTotalCount()
    {
        if(getStub() != null) {
            return getStub().getLowerBoundTotalCount();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lowerBoundTotalCount;
        }
    }
    public void setLowerBoundTotalCount(Long lowerBoundTotalCount)
    {
        if(getStub() != null) {
            getStub().setLowerBoundTotalCount(lowerBoundTotalCount);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lowerBoundTotalCount = lowerBoundTotalCount;
        }
    }

    public Long getPreviousPageOffset()
    {
        if(getStub() != null) {
            return getStub().getPreviousPageOffset();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.previousPageOffset;
        }
    }
    public void setPreviousPageOffset(Long previousPageOffset)
    {
        if(getStub() != null) {
            getStub().setPreviousPageOffset(previousPageOffset);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.previousPageOffset = previousPageOffset;
        }
    }

    public Long getNextPageOffset()
    {
        if(getStub() != null) {
            return getStub().getNextPageOffset();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.nextPageOffset;
        }
    }
    public void setNextPageOffset(Long nextPageOffset)
    {
        if(getStub() != null) {
            getStub().setNextPageOffset(nextPageOffset);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.nextPageOffset = nextPageOffset;
        }
    }

    public Long getLastPageOffset()
    {
        if(getStub() != null) {
            return getStub().getLastPageOffset();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastPageOffset;
        }
    }
    public void setLastPageOffset(Long lastPageOffset)
    {
        if(getStub() != null) {
            getStub().setLastPageOffset(lastPageOffset);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastPageOffset = lastPageOffset;
        }
    }

    public Long getLastPageIndex()
    {
        if(getStub() != null) {
            return getStub().getLastPageIndex();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastPageIndex;
        }
    }
    public void setLastPageIndex(Long lastPageIndex)
    {
        if(getStub() != null) {
            getStub().setLastPageIndex(lastPageIndex);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastPageIndex = lastPageIndex;
        }
    }

    public Boolean isFirstActionEnabled()
    {
        if(getStub() != null) {
            return getStub().isFirstActionEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.firstActionEnabled;
        }
    }
    public void setFirstActionEnabled(Boolean firstActionEnabled)
    {
        if(getStub() != null) {
            getStub().setFirstActionEnabled(firstActionEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.firstActionEnabled = firstActionEnabled;
        }
    }

    public Boolean isPreviousActionEnabled()
    {
        if(getStub() != null) {
            return getStub().isPreviousActionEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.previousActionEnabled;
        }
    }
    public void setPreviousActionEnabled(Boolean previousActionEnabled)
    {
        if(getStub() != null) {
            getStub().setPreviousActionEnabled(previousActionEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.previousActionEnabled = previousActionEnabled;
        }
    }

    public Boolean isNextActionEnabled()
    {
        if(getStub() != null) {
            return getStub().isNextActionEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.nextActionEnabled;
        }
    }
    public void setNextActionEnabled(Boolean nextActionEnabled)
    {
        if(getStub() != null) {
            getStub().setNextActionEnabled(nextActionEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.nextActionEnabled = nextActionEnabled;
        }
    }

    public Boolean isLastActionEnabled()
    {
        if(getStub() != null) {
            return getStub().isLastActionEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastActionEnabled;
        }
    }
    public void setLastActionEnabled(Boolean lastActionEnabled)
    {
        if(getStub() != null) {
            getStub().setLastActionEnabled(lastActionEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastActionEnabled = lastActionEnabled;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public PagerStateStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(PagerStateStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("pagerMode = " + this.pagerMode).append(";");
            sb.append("primaryOrdering = " + this.primaryOrdering).append(";");
            sb.append("secondaryOrdering = " + this.secondaryOrdering).append(";");
            sb.append("currentOffset = " + this.currentOffset).append(";");
            sb.append("currentPage = " + this.currentPage).append(";");
            sb.append("pageSize = " + this.pageSize).append(";");
            sb.append("totalCount = " + this.totalCount).append(";");
            sb.append("lowerBoundTotalCount = " + this.lowerBoundTotalCount).append(";");
            sb.append("previousPageOffset = " + this.previousPageOffset).append(";");
            sb.append("nextPageOffset = " + this.nextPageOffset).append(";");
            sb.append("lastPageOffset = " + this.lastPageOffset).append(";");
            sb.append("lastPageIndex = " + this.lastPageIndex).append(";");
            sb.append("firstActionEnabled = " + this.firstActionEnabled).append(";");
            sb.append("previousActionEnabled = " + this.previousActionEnabled).append(";");
            sb.append("nextActionEnabled = " + this.nextActionEnabled).append(";");
            sb.append("lastActionEnabled = " + this.lastActionEnabled).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = pagerMode == null ? 0 : pagerMode.hashCode();
            _hash = 31 * _hash + delta;
            delta = primaryOrdering == null ? 0 : primaryOrdering.hashCode();
            _hash = 31 * _hash + delta;
            delta = secondaryOrdering == null ? 0 : secondaryOrdering.hashCode();
            _hash = 31 * _hash + delta;
            delta = currentOffset == null ? 0 : currentOffset.hashCode();
            _hash = 31 * _hash + delta;
            delta = currentPage == null ? 0 : currentPage.hashCode();
            _hash = 31 * _hash + delta;
            delta = pageSize == null ? 0 : pageSize.hashCode();
            _hash = 31 * _hash + delta;
            delta = totalCount == null ? 0 : totalCount.hashCode();
            _hash = 31 * _hash + delta;
            delta = lowerBoundTotalCount == null ? 0 : lowerBoundTotalCount.hashCode();
            _hash = 31 * _hash + delta;
            delta = previousPageOffset == null ? 0 : previousPageOffset.hashCode();
            _hash = 31 * _hash + delta;
            delta = nextPageOffset == null ? 0 : nextPageOffset.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastPageOffset == null ? 0 : lastPageOffset.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastPageIndex == null ? 0 : lastPageIndex.hashCode();
            _hash = 31 * _hash + delta;
            delta = firstActionEnabled == null ? 0 : firstActionEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = previousActionEnabled == null ? 0 : previousActionEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = nextActionEnabled == null ? 0 : nextActionEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastActionEnabled == null ? 0 : lastActionEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
