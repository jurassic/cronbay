package com.cronbay.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.stub.GaeAppStructStub;

// Wrapper class + bean combo.
public class GaeAppStructBean implements GaeAppStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GaeAppStructBean.class.getName());

    // [1] With an embedded object.
    private GaeAppStructStub stub = null;

    // [2] Or, without an embedded object.
    private String groupId;
    private String appId;
    private String appDomain;
    private String namespace;
    private Long acl;
    private String note;

    // Ctors.
    public GaeAppStructBean()
    {
        //this((String) null);
    }
    public GaeAppStructBean(String groupId, String appId, String appDomain, String namespace, Long acl, String note)
    {
        this.groupId = groupId;
        this.appId = appId;
        this.appDomain = appDomain;
        this.namespace = namespace;
        this.acl = acl;
        this.note = note;
    }
    public GaeAppStructBean(GaeAppStruct stub)
    {
        if(stub instanceof GaeAppStructStub) {
            this.stub = (GaeAppStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGroupId(stub.getGroupId());   
            setAppId(stub.getAppId());   
            setAppDomain(stub.getAppDomain());   
            setNamespace(stub.getNamespace());   
            setAcl(stub.getAcl());   
            setNote(stub.getNote());   
        } else {
            log.log(Level.WARNING, "The arg stub object is null.");
        }
    }

    public String getGroupId()
    {
        if(getStub() != null) {
            return getStub().getGroupId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.groupId;
        }
    }
    public void setGroupId(String groupId)
    {
        if(getStub() != null) {
            getStub().setGroupId(groupId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.groupId = groupId;
        }
    }

    public String getAppId()
    {
        if(getStub() != null) {
            return getStub().getAppId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appId;
        }
    }
    public void setAppId(String appId)
    {
        if(getStub() != null) {
            getStub().setAppId(appId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appId = appId;
        }
    }

    public String getAppDomain()
    {
        if(getStub() != null) {
            return getStub().getAppDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appDomain;
        }
    }
    public void setAppDomain(String appDomain)
    {
        if(getStub() != null) {
            getStub().setAppDomain(appDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appDomain = appDomain;
        }
    }

    public String getNamespace()
    {
        if(getStub() != null) {
            return getStub().getNamespace();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.namespace;
        }
    }
    public void setNamespace(String namespace)
    {
        if(getStub() != null) {
            getStub().setNamespace(namespace);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.namespace = namespace;
        }
    }

    public Long getAcl()
    {
        if(getStub() != null) {
            return getStub().getAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.acl;
        }
    }
    public void setAcl(Long acl)
    {
        if(getStub() != null) {
            getStub().setAcl(acl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.acl = acl;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public GaeAppStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(GaeAppStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("groupId = " + this.groupId).append(";");
            sb.append("appId = " + this.appId).append(";");
            sb.append("appDomain = " + this.appDomain).append(";");
            sb.append("namespace = " + this.namespace).append(";");
            sb.append("acl = " + this.acl).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = groupId == null ? 0 : groupId.hashCode();
            _hash = 31 * _hash + delta;
            delta = appId == null ? 0 : appId.hashCode();
            _hash = 31 * _hash + delta;
            delta = appDomain == null ? 0 : appDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = namespace == null ? 0 : namespace.hashCode();
            _hash = 31 * _hash + delta;
            delta = acl == null ? 0 : acl.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
