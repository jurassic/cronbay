package com.cronbay.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.stub.CronScheduleStructStub;

// Wrapper class + bean combo.
public class CronScheduleStructBean implements CronScheduleStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CronScheduleStructBean.class.getName());

    // [1] With an embedded object.
    private CronScheduleStructStub stub = null;

    // [2] Or, without an embedded object.
    private String type;
    private String schedule;
    private String timezone;
    private Integer maxIterations;
    private Integer repeatInterval;
    private Long firtRunTime;
    private Long startTime;
    private Long endTime;
    private String note;

    // Ctors.
    public CronScheduleStructBean()
    {
        //this((String) null);
    }
    public CronScheduleStructBean(String type, String schedule, String timezone, Integer maxIterations, Integer repeatInterval, Long firtRunTime, Long startTime, Long endTime, String note)
    {
        this.type = type;
        this.schedule = schedule;
        this.timezone = timezone;
        this.maxIterations = maxIterations;
        this.repeatInterval = repeatInterval;
        this.firtRunTime = firtRunTime;
        this.startTime = startTime;
        this.endTime = endTime;
        this.note = note;
    }
    public CronScheduleStructBean(CronScheduleStruct stub)
    {
        if(stub instanceof CronScheduleStructStub) {
            this.stub = (CronScheduleStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setType(stub.getType());   
            setSchedule(stub.getSchedule());   
            setTimezone(stub.getTimezone());   
            setMaxIterations(stub.getMaxIterations());   
            setRepeatInterval(stub.getRepeatInterval());   
            setFirtRunTime(stub.getFirtRunTime());   
            setStartTime(stub.getStartTime());   
            setEndTime(stub.getEndTime());   
            setNote(stub.getNote());   
        } else {
            log.log(Level.WARNING, "The arg stub object is null.");
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }

    public String getSchedule()
    {
        if(getStub() != null) {
            return getStub().getSchedule();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.schedule;
        }
    }
    public void setSchedule(String schedule)
    {
        if(getStub() != null) {
            getStub().setSchedule(schedule);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.schedule = schedule;
        }
    }

    public String getTimezone()
    {
        if(getStub() != null) {
            return getStub().getTimezone();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.timezone;
        }
    }
    public void setTimezone(String timezone)
    {
        if(getStub() != null) {
            getStub().setTimezone(timezone);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.timezone = timezone;
        }
    }

    public Integer getMaxIterations()
    {
        if(getStub() != null) {
            return getStub().getMaxIterations();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.maxIterations;
        }
    }
    public void setMaxIterations(Integer maxIterations)
    {
        if(getStub() != null) {
            getStub().setMaxIterations(maxIterations);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.maxIterations = maxIterations;
        }
    }

    public Integer getRepeatInterval()
    {
        if(getStub() != null) {
            return getStub().getRepeatInterval();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.repeatInterval;
        }
    }
    public void setRepeatInterval(Integer repeatInterval)
    {
        if(getStub() != null) {
            getStub().setRepeatInterval(repeatInterval);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.repeatInterval = repeatInterval;
        }
    }

    public Long getFirtRunTime()
    {
        if(getStub() != null) {
            return getStub().getFirtRunTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.firtRunTime;
        }
    }
    public void setFirtRunTime(Long firtRunTime)
    {
        if(getStub() != null) {
            getStub().setFirtRunTime(firtRunTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.firtRunTime = firtRunTime;
        }
    }

    public Long getStartTime()
    {
        if(getStub() != null) {
            return getStub().getStartTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.startTime;
        }
    }
    public void setStartTime(Long startTime)
    {
        if(getStub() != null) {
            getStub().setStartTime(startTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.startTime = startTime;
        }
    }

    public Long getEndTime()
    {
        if(getStub() != null) {
            return getStub().getEndTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.endTime;
        }
    }
    public void setEndTime(Long endTime)
    {
        if(getStub() != null) {
            getStub().setEndTime(endTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.endTime = endTime;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public CronScheduleStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(CronScheduleStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("type = " + this.type).append(";");
            sb.append("schedule = " + this.schedule).append(";");
            sb.append("timezone = " + this.timezone).append(";");
            sb.append("maxIterations = " + this.maxIterations).append(";");
            sb.append("repeatInterval = " + this.repeatInterval).append(";");
            sb.append("firtRunTime = " + this.firtRunTime).append(";");
            sb.append("startTime = " + this.startTime).append(";");
            sb.append("endTime = " + this.endTime).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            delta = schedule == null ? 0 : schedule.hashCode();
            _hash = 31 * _hash + delta;
            delta = timezone == null ? 0 : timezone.hashCode();
            _hash = 31 * _hash + delta;
            delta = maxIterations == null ? 0 : maxIterations.hashCode();
            _hash = 31 * _hash + delta;
            delta = repeatInterval == null ? 0 : repeatInterval.hashCode();
            _hash = 31 * _hash + delta;
            delta = firtRunTime == null ? 0 : firtRunTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = startTime == null ? 0 : startTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = endTime == null ? 0 : endTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
