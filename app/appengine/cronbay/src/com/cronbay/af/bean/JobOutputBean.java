package com.cronbay.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.stub.JobOutputStub;

// Wrapper class + bean combo.
public class JobOutputBean implements JobOutput, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(JobOutputBean.class.getName());

    // [1] With an embedded object.
    private JobOutputStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private String cronJob;
    private String previousRun;
    private String previousTry;
    private String responseCode;
    private String outputFormat;
    private String outputText;
    private List<String> outputData;
    private String outputHash;
    private String permalink;
    private String shortlink;
    private String result;
    private String status;
    private String extra;
    private String note;
    private Integer retryCounter;
    private Long scheduledTime;
    private Long processedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public JobOutputBean()
    {
        //this((String) null);
    }
    public JobOutputBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public JobOutputBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime, null, null);
    }
    public JobOutputBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.cronJob = cronJob;
        this.previousRun = previousRun;
        this.previousTry = previousTry;
        this.responseCode = responseCode;
        this.outputFormat = outputFormat;
        this.outputText = outputText;
        this.outputData = outputData;
        this.outputHash = outputHash;
        this.permalink = permalink;
        this.shortlink = shortlink;
        this.result = result;
        this.status = status;
        this.extra = extra;
        this.note = note;
        this.retryCounter = retryCounter;
        this.scheduledTime = scheduledTime;
        this.processedTime = processedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public JobOutputBean(JobOutput stub)
    {
        if(stub instanceof JobOutputStub) {
            this.stub = (JobOutputStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setManagerApp(stub.getManagerApp());   
            setAppAcl(stub.getAppAcl());   
            GaeAppStruct gaeApp = stub.getGaeApp();
            if(gaeApp instanceof GaeAppStructBean) {
                setGaeApp((GaeAppStructBean) gaeApp);   
            } else {
                setGaeApp(new GaeAppStructBean(gaeApp));   
            }
            setOwnerUser(stub.getOwnerUser());   
            setUserAcl(stub.getUserAcl());   
            setUser(stub.getUser());   
            setCronJob(stub.getCronJob());   
            setPreviousRun(stub.getPreviousRun());   
            setPreviousTry(stub.getPreviousTry());   
            setResponseCode(stub.getResponseCode());   
            setOutputFormat(stub.getOutputFormat());   
            setOutputText(stub.getOutputText());   
            setOutputData(stub.getOutputData());   
            setOutputHash(stub.getOutputHash());   
            setPermalink(stub.getPermalink());   
            setShortlink(stub.getShortlink());   
            setResult(stub.getResult());   
            setStatus(stub.getStatus());   
            setExtra(stub.getExtra());   
            setNote(stub.getNote());   
            setRetryCounter(stub.getRetryCounter());   
            setScheduledTime(stub.getScheduledTime());   
            setProcessedTime(stub.getProcessedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            log.log(Level.WARNING, "The arg stub object is null.");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getManagerApp()
    {
        if(getStub() != null) {
            return getStub().getManagerApp();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.managerApp;
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getStub() != null) {
            getStub().setManagerApp(managerApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.managerApp = managerApp;
        }
    }

    public Long getAppAcl()
    {
        if(getStub() != null) {
            return getStub().getAppAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appAcl;
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getStub() != null) {
            getStub().setAppAcl(appAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appAcl = appAcl;
        }
    }

    public GaeAppStruct getGaeApp()
    {  
        if(getStub() != null) {
            // Note the object type.
            return new GaeAppStructBean(getStub().getGaeApp());
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.gaeApp;
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGaeApp(gaeApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(gaeApp instanceof GaeAppStructBean) {
                this.gaeApp = (GaeAppStructBean) gaeApp;
            } else {
                this.gaeApp = new GaeAppStructBean(gaeApp);
            }
        }
    }

    public String getOwnerUser()
    {
        if(getStub() != null) {
            return getStub().getOwnerUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ownerUser;
        }
    }
    public void setOwnerUser(String ownerUser)
    {
        if(getStub() != null) {
            getStub().setOwnerUser(ownerUser);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ownerUser = ownerUser;
        }
    }

    public Long getUserAcl()
    {
        if(getStub() != null) {
            return getStub().getUserAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userAcl;
        }
    }
    public void setUserAcl(Long userAcl)
    {
        if(getStub() != null) {
            getStub().setUserAcl(userAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userAcl = userAcl;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getCronJob()
    {
        if(getStub() != null) {
            return getStub().getCronJob();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.cronJob;
        }
    }
    public void setCronJob(String cronJob)
    {
        if(getStub() != null) {
            getStub().setCronJob(cronJob);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.cronJob = cronJob;
        }
    }

    public String getPreviousRun()
    {
        if(getStub() != null) {
            return getStub().getPreviousRun();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.previousRun;
        }
    }
    public void setPreviousRun(String previousRun)
    {
        if(getStub() != null) {
            getStub().setPreviousRun(previousRun);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.previousRun = previousRun;
        }
    }

    public String getPreviousTry()
    {
        if(getStub() != null) {
            return getStub().getPreviousTry();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.previousTry;
        }
    }
    public void setPreviousTry(String previousTry)
    {
        if(getStub() != null) {
            getStub().setPreviousTry(previousTry);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.previousTry = previousTry;
        }
    }

    public String getResponseCode()
    {
        if(getStub() != null) {
            return getStub().getResponseCode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.responseCode;
        }
    }
    public void setResponseCode(String responseCode)
    {
        if(getStub() != null) {
            getStub().setResponseCode(responseCode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.responseCode = responseCode;
        }
    }

    public String getOutputFormat()
    {
        if(getStub() != null) {
            return getStub().getOutputFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputFormat;
        }
    }
    public void setOutputFormat(String outputFormat)
    {
        if(getStub() != null) {
            getStub().setOutputFormat(outputFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputFormat = outputFormat;
        }
    }

    public String getOutputText()
    {
        if(getStub() != null) {
            return getStub().getOutputText();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputText;
        }
    }
    public void setOutputText(String outputText)
    {
        if(getStub() != null) {
            getStub().setOutputText(outputText);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputText = outputText;
        }
    }

    public List<String> getOutputData()
    {
        if(getStub() != null) {
            return getStub().getOutputData();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputData;
        }
    }
    public void setOutputData(List<String> outputData)
    {
        if(getStub() != null) {
            getStub().setOutputData(outputData);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputData = outputData;
        }
    }

    public String getOutputHash()
    {
        if(getStub() != null) {
            return getStub().getOutputHash();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputHash;
        }
    }
    public void setOutputHash(String outputHash)
    {
        if(getStub() != null) {
            getStub().setOutputHash(outputHash);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputHash = outputHash;
        }
    }

    public String getPermalink()
    {
        if(getStub() != null) {
            return getStub().getPermalink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.permalink;
        }
    }
    public void setPermalink(String permalink)
    {
        if(getStub() != null) {
            getStub().setPermalink(permalink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.permalink = permalink;
        }
    }

    public String getShortlink()
    {
        if(getStub() != null) {
            return getStub().getShortlink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortlink;
        }
    }
    public void setShortlink(String shortlink)
    {
        if(getStub() != null) {
            getStub().setShortlink(shortlink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortlink = shortlink;
        }
    }

    public String getResult()
    {
        if(getStub() != null) {
            return getStub().getResult();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.result;
        }
    }
    public void setResult(String result)
    {
        if(getStub() != null) {
            getStub().setResult(result);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.result = result;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getExtra()
    {
        if(getStub() != null) {
            return getStub().getExtra();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.extra;
        }
    }
    public void setExtra(String extra)
    {
        if(getStub() != null) {
            getStub().setExtra(extra);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.extra = extra;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Integer getRetryCounter()
    {
        if(getStub() != null) {
            return getStub().getRetryCounter();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.retryCounter;
        }
    }
    public void setRetryCounter(Integer retryCounter)
    {
        if(getStub() != null) {
            getStub().setRetryCounter(retryCounter);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.retryCounter = retryCounter;
        }
    }

    public Long getScheduledTime()
    {
        if(getStub() != null) {
            return getStub().getScheduledTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.scheduledTime;
        }
    }
    public void setScheduledTime(Long scheduledTime)
    {
        if(getStub() != null) {
            getStub().setScheduledTime(scheduledTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.scheduledTime = scheduledTime;
        }
    }

    public Long getProcessedTime()
    {
        if(getStub() != null) {
            return getStub().getProcessedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.processedTime;
        }
    }
    public void setProcessedTime(Long processedTime)
    {
        if(getStub() != null) {
            getStub().setProcessedTime(processedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.processedTime = processedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public JobOutputStub getStub()
    {
        return this.stub;
    }
    protected void setStub(JobOutputStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("managerApp = " + this.managerApp).append(";");
            sb.append("appAcl = " + this.appAcl).append(";");
            sb.append("gaeApp = " + this.gaeApp).append(";");
            sb.append("ownerUser = " + this.ownerUser).append(";");
            sb.append("userAcl = " + this.userAcl).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("cronJob = " + this.cronJob).append(";");
            sb.append("previousRun = " + this.previousRun).append(";");
            sb.append("previousTry = " + this.previousTry).append(";");
            sb.append("responseCode = " + this.responseCode).append(";");
            sb.append("outputFormat = " + this.outputFormat).append(";");
            sb.append("outputText = " + this.outputText).append(";");
            sb.append("outputData = " + this.outputData).append(";");
            sb.append("outputHash = " + this.outputHash).append(";");
            sb.append("permalink = " + this.permalink).append(";");
            sb.append("shortlink = " + this.shortlink).append(";");
            sb.append("result = " + this.result).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("extra = " + this.extra).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("retryCounter = " + this.retryCounter).append(";");
            sb.append("scheduledTime = " + this.scheduledTime).append(";");
            sb.append("processedTime = " + this.processedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = managerApp == null ? 0 : managerApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = appAcl == null ? 0 : appAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = gaeApp == null ? 0 : gaeApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = ownerUser == null ? 0 : ownerUser.hashCode();
            _hash = 31 * _hash + delta;
            delta = userAcl == null ? 0 : userAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = cronJob == null ? 0 : cronJob.hashCode();
            _hash = 31 * _hash + delta;
            delta = previousRun == null ? 0 : previousRun.hashCode();
            _hash = 31 * _hash + delta;
            delta = previousTry == null ? 0 : previousTry.hashCode();
            _hash = 31 * _hash + delta;
            delta = responseCode == null ? 0 : responseCode.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputFormat == null ? 0 : outputFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputText == null ? 0 : outputText.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputData == null ? 0 : outputData.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputHash == null ? 0 : outputHash.hashCode();
            _hash = 31 * _hash + delta;
            delta = permalink == null ? 0 : permalink.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortlink == null ? 0 : shortlink.hashCode();
            _hash = 31 * _hash + delta;
            delta = result == null ? 0 : result.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = extra == null ? 0 : extra.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = retryCounter == null ? 0 : retryCounter.hashCode();
            _hash = 31 * _hash + delta;
            delta = scheduledTime == null ? 0 : scheduledTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = processedTime == null ? 0 : processedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
