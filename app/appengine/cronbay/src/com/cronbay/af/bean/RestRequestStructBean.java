package com.cronbay.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.stub.RestRequestStructStub;

// Wrapper class + bean combo.
public class RestRequestStructBean implements RestRequestStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RestRequestStructBean.class.getName());

    // [1] With an embedded object.
    private RestRequestStructStub stub = null;

    // [2] Or, without an embedded object.
    private String serviceName;
    private String serviceUrl;
    private String requestMethod;
    private String requestUrl;
    private String targetEntity;
    private String queryString;
    private List<String> queryParams;
    private String inputFormat;
    private String inputContent;
    private String outputFormat;
    private Integer maxRetries;
    private Integer retryInterval;
    private String note;

    // Ctors.
    public RestRequestStructBean()
    {
        //this((String) null);
    }
    public RestRequestStructBean(String serviceName, String serviceUrl, String requestMethod, String requestUrl, String targetEntity, String queryString, List<String> queryParams, String inputFormat, String inputContent, String outputFormat, Integer maxRetries, Integer retryInterval, String note)
    {
        this.serviceName = serviceName;
        this.serviceUrl = serviceUrl;
        this.requestMethod = requestMethod;
        this.requestUrl = requestUrl;
        this.targetEntity = targetEntity;
        this.queryString = queryString;
        this.queryParams = queryParams;
        this.inputFormat = inputFormat;
        this.inputContent = inputContent;
        this.outputFormat = outputFormat;
        this.maxRetries = maxRetries;
        this.retryInterval = retryInterval;
        this.note = note;
    }
    public RestRequestStructBean(RestRequestStruct stub)
    {
        if(stub instanceof RestRequestStructStub) {
            this.stub = (RestRequestStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setServiceName(stub.getServiceName());   
            setServiceUrl(stub.getServiceUrl());   
            setRequestMethod(stub.getRequestMethod());   
            setRequestUrl(stub.getRequestUrl());   
            setTargetEntity(stub.getTargetEntity());   
            setQueryString(stub.getQueryString());   
            setQueryParams(stub.getQueryParams());   
            setInputFormat(stub.getInputFormat());   
            setInputContent(stub.getInputContent());   
            setOutputFormat(stub.getOutputFormat());   
            setMaxRetries(stub.getMaxRetries());   
            setRetryInterval(stub.getRetryInterval());   
            setNote(stub.getNote());   
        } else {
            log.log(Level.WARNING, "The arg stub object is null.");
        }
    }

    public String getServiceName()
    {
        if(getStub() != null) {
            return getStub().getServiceName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceName;
        }
    }
    public void setServiceName(String serviceName)
    {
        if(getStub() != null) {
            getStub().setServiceName(serviceName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceName = serviceName;
        }
    }

    public String getServiceUrl()
    {
        if(getStub() != null) {
            return getStub().getServiceUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceUrl;
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getStub() != null) {
            getStub().setServiceUrl(serviceUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceUrl = serviceUrl;
        }
    }

    public String getRequestMethod()
    {
        if(getStub() != null) {
            return getStub().getRequestMethod();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.requestMethod;
        }
    }
    public void setRequestMethod(String requestMethod)
    {
        if(getStub() != null) {
            getStub().setRequestMethod(requestMethod);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.requestMethod = requestMethod;
        }
    }

    public String getRequestUrl()
    {
        if(getStub() != null) {
            return getStub().getRequestUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.requestUrl;
        }
    }
    public void setRequestUrl(String requestUrl)
    {
        if(getStub() != null) {
            getStub().setRequestUrl(requestUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.requestUrl = requestUrl;
        }
    }

    public String getTargetEntity()
    {
        if(getStub() != null) {
            return getStub().getTargetEntity();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.targetEntity;
        }
    }
    public void setTargetEntity(String targetEntity)
    {
        if(getStub() != null) {
            getStub().setTargetEntity(targetEntity);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.targetEntity = targetEntity;
        }
    }

    public String getQueryString()
    {
        if(getStub() != null) {
            return getStub().getQueryString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.queryString;
        }
    }
    public void setQueryString(String queryString)
    {
        if(getStub() != null) {
            getStub().setQueryString(queryString);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.queryString = queryString;
        }
    }

    public List<String> getQueryParams()
    {
        if(getStub() != null) {
            return getStub().getQueryParams();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.queryParams;
        }
    }
    public void setQueryParams(List<String> queryParams)
    {
        if(getStub() != null) {
            getStub().setQueryParams(queryParams);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.queryParams = queryParams;
        }
    }

    public String getInputFormat()
    {
        if(getStub() != null) {
            return getStub().getInputFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.inputFormat;
        }
    }
    public void setInputFormat(String inputFormat)
    {
        if(getStub() != null) {
            getStub().setInputFormat(inputFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.inputFormat = inputFormat;
        }
    }

    public String getInputContent()
    {
        if(getStub() != null) {
            return getStub().getInputContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.inputContent;
        }
    }
    public void setInputContent(String inputContent)
    {
        if(getStub() != null) {
            getStub().setInputContent(inputContent);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.inputContent = inputContent;
        }
    }

    public String getOutputFormat()
    {
        if(getStub() != null) {
            return getStub().getOutputFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputFormat;
        }
    }
    public void setOutputFormat(String outputFormat)
    {
        if(getStub() != null) {
            getStub().setOutputFormat(outputFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputFormat = outputFormat;
        }
    }

    public Integer getMaxRetries()
    {
        if(getStub() != null) {
            return getStub().getMaxRetries();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.maxRetries;
        }
    }
    public void setMaxRetries(Integer maxRetries)
    {
        if(getStub() != null) {
            getStub().setMaxRetries(maxRetries);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.maxRetries = maxRetries;
        }
    }

    public Integer getRetryInterval()
    {
        if(getStub() != null) {
            return getStub().getRetryInterval();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.retryInterval;
        }
    }
    public void setRetryInterval(Integer retryInterval)
    {
        if(getStub() != null) {
            getStub().setRetryInterval(retryInterval);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.retryInterval = retryInterval;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public RestRequestStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(RestRequestStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("serviceName = " + this.serviceName).append(";");
            sb.append("serviceUrl = " + this.serviceUrl).append(";");
            sb.append("requestMethod = " + this.requestMethod).append(";");
            sb.append("requestUrl = " + this.requestUrl).append(";");
            sb.append("targetEntity = " + this.targetEntity).append(";");
            sb.append("queryString = " + this.queryString).append(";");
            sb.append("queryParams = " + this.queryParams).append(";");
            sb.append("inputFormat = " + this.inputFormat).append(";");
            sb.append("inputContent = " + this.inputContent).append(";");
            sb.append("outputFormat = " + this.outputFormat).append(";");
            sb.append("maxRetries = " + this.maxRetries).append(";");
            sb.append("retryInterval = " + this.retryInterval).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = serviceName == null ? 0 : serviceName.hashCode();
            _hash = 31 * _hash + delta;
            delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = requestMethod == null ? 0 : requestMethod.hashCode();
            _hash = 31 * _hash + delta;
            delta = requestUrl == null ? 0 : requestUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = targetEntity == null ? 0 : targetEntity.hashCode();
            _hash = 31 * _hash + delta;
            delta = queryString == null ? 0 : queryString.hashCode();
            _hash = 31 * _hash + delta;
            delta = queryParams == null ? 0 : queryParams.hashCode();
            _hash = 31 * _hash + delta;
            delta = inputFormat == null ? 0 : inputFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = inputContent == null ? 0 : inputContent.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputFormat == null ? 0 : outputFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = maxRetries == null ? 0 : maxRetries.hashCode();
            _hash = 31 * _hash + delta;
            delta = retryInterval == null ? 0 : retryInterval.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
