package com.cronbay.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.stub.CronJobStub;

// Wrapper class + bean combo.
public class CronJobBean implements CronJob, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CronJobBean.class.getName());

    // [1] With an embedded object.
    private CronJobStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private Integer jobId;
    private String title;
    private String description;
    private String originJob;
    private RestRequestStructBean restRequest;
    private String permalink;
    private String shortlink;
    private String status;
    private Integer jobStatus;
    private String extra;
    private String note;
    private Boolean alert;
    private NotificationStructBean notificationPref;
    private ReferrerInfoStructBean referrerInfo;
    private CronScheduleStructBean cronSchedule;
    private Long nextRunTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public CronJobBean()
    {
        //this((String) null);
    }
    public CronJobBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public CronJobBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStructBean restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStructBean notificationPref, ReferrerInfoStructBean referrerInfo, CronScheduleStructBean cronSchedule, Long nextRunTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, jobId, title, description, originJob, restRequest, permalink, shortlink, status, jobStatus, extra, note, alert, notificationPref, referrerInfo, cronSchedule, nextRunTime, null, null);
    }
    public CronJobBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStructBean restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStructBean notificationPref, ReferrerInfoStructBean referrerInfo, CronScheduleStructBean cronSchedule, Long nextRunTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.jobId = jobId;
        this.title = title;
        this.description = description;
        this.originJob = originJob;
        this.restRequest = restRequest;
        this.permalink = permalink;
        this.shortlink = shortlink;
        this.status = status;
        this.jobStatus = jobStatus;
        this.extra = extra;
        this.note = note;
        this.alert = alert;
        this.notificationPref = notificationPref;
        this.referrerInfo = referrerInfo;
        this.cronSchedule = cronSchedule;
        this.nextRunTime = nextRunTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public CronJobBean(CronJob stub)
    {
        if(stub instanceof CronJobStub) {
            this.stub = (CronJobStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setManagerApp(stub.getManagerApp());   
            setAppAcl(stub.getAppAcl());   
            GaeAppStruct gaeApp = stub.getGaeApp();
            if(gaeApp instanceof GaeAppStructBean) {
                setGaeApp((GaeAppStructBean) gaeApp);   
            } else {
                setGaeApp(new GaeAppStructBean(gaeApp));   
            }
            setOwnerUser(stub.getOwnerUser());   
            setUserAcl(stub.getUserAcl());   
            setUser(stub.getUser());   
            setJobId(stub.getJobId());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setOriginJob(stub.getOriginJob());   
            RestRequestStruct restRequest = stub.getRestRequest();
            if(restRequest instanceof RestRequestStructBean) {
                setRestRequest((RestRequestStructBean) restRequest);   
            } else {
                setRestRequest(new RestRequestStructBean(restRequest));   
            }
            setPermalink(stub.getPermalink());   
            setShortlink(stub.getShortlink());   
            setStatus(stub.getStatus());   
            setJobStatus(stub.getJobStatus());   
            setExtra(stub.getExtra());   
            setNote(stub.getNote());   
            setAlert(stub.isAlert());   
            NotificationStruct notificationPref = stub.getNotificationPref();
            if(notificationPref instanceof NotificationStructBean) {
                setNotificationPref((NotificationStructBean) notificationPref);   
            } else {
                setNotificationPref(new NotificationStructBean(notificationPref));   
            }
            ReferrerInfoStruct referrerInfo = stub.getReferrerInfo();
            if(referrerInfo instanceof ReferrerInfoStructBean) {
                setReferrerInfo((ReferrerInfoStructBean) referrerInfo);   
            } else {
                setReferrerInfo(new ReferrerInfoStructBean(referrerInfo));   
            }
            CronScheduleStruct cronSchedule = stub.getCronSchedule();
            if(cronSchedule instanceof CronScheduleStructBean) {
                setCronSchedule((CronScheduleStructBean) cronSchedule);   
            } else {
                setCronSchedule(new CronScheduleStructBean(cronSchedule));   
            }
            setNextRunTime(stub.getNextRunTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            log.log(Level.WARNING, "The arg stub object is null.");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getManagerApp()
    {
        if(getStub() != null) {
            return getStub().getManagerApp();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.managerApp;
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getStub() != null) {
            getStub().setManagerApp(managerApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.managerApp = managerApp;
        }
    }

    public Long getAppAcl()
    {
        if(getStub() != null) {
            return getStub().getAppAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appAcl;
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getStub() != null) {
            getStub().setAppAcl(appAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appAcl = appAcl;
        }
    }

    public GaeAppStruct getGaeApp()
    {  
        if(getStub() != null) {
            // Note the object type.
            return new GaeAppStructBean(getStub().getGaeApp());
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.gaeApp;
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGaeApp(gaeApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(gaeApp instanceof GaeAppStructBean) {
                this.gaeApp = (GaeAppStructBean) gaeApp;
            } else {
                this.gaeApp = new GaeAppStructBean(gaeApp);
            }
        }
    }

    public String getOwnerUser()
    {
        if(getStub() != null) {
            return getStub().getOwnerUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ownerUser;
        }
    }
    public void setOwnerUser(String ownerUser)
    {
        if(getStub() != null) {
            getStub().setOwnerUser(ownerUser);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ownerUser = ownerUser;
        }
    }

    public Long getUserAcl()
    {
        if(getStub() != null) {
            return getStub().getUserAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userAcl;
        }
    }
    public void setUserAcl(Long userAcl)
    {
        if(getStub() != null) {
            getStub().setUserAcl(userAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userAcl = userAcl;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public Integer getJobId()
    {
        if(getStub() != null) {
            return getStub().getJobId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.jobId;
        }
    }
    public void setJobId(Integer jobId)
    {
        if(getStub() != null) {
            getStub().setJobId(jobId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.jobId = jobId;
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public String getOriginJob()
    {
        if(getStub() != null) {
            return getStub().getOriginJob();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.originJob;
        }
    }
    public void setOriginJob(String originJob)
    {
        if(getStub() != null) {
            getStub().setOriginJob(originJob);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.originJob = originJob;
        }
    }

    public RestRequestStruct getRestRequest()
    {  
        if(getStub() != null) {
            // Note the object type.
            return new RestRequestStructBean(getStub().getRestRequest());
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.restRequest;
        }
    }
    public void setRestRequest(RestRequestStruct restRequest)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setRestRequest(restRequest);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(restRequest instanceof RestRequestStructBean) {
                this.restRequest = (RestRequestStructBean) restRequest;
            } else {
                this.restRequest = new RestRequestStructBean(restRequest);
            }
        }
    }

    public String getPermalink()
    {
        if(getStub() != null) {
            return getStub().getPermalink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.permalink;
        }
    }
    public void setPermalink(String permalink)
    {
        if(getStub() != null) {
            getStub().setPermalink(permalink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.permalink = permalink;
        }
    }

    public String getShortlink()
    {
        if(getStub() != null) {
            return getStub().getShortlink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortlink;
        }
    }
    public void setShortlink(String shortlink)
    {
        if(getStub() != null) {
            getStub().setShortlink(shortlink);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortlink = shortlink;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Integer getJobStatus()
    {
        if(getStub() != null) {
            return getStub().getJobStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.jobStatus;
        }
    }
    public void setJobStatus(Integer jobStatus)
    {
        if(getStub() != null) {
            getStub().setJobStatus(jobStatus);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.jobStatus = jobStatus;
        }
    }

    public String getExtra()
    {
        if(getStub() != null) {
            return getStub().getExtra();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.extra;
        }
    }
    public void setExtra(String extra)
    {
        if(getStub() != null) {
            getStub().setExtra(extra);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.extra = extra;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Boolean isAlert()
    {
        if(getStub() != null) {
            return getStub().isAlert();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.alert;
        }
    }
    public void setAlert(Boolean alert)
    {
        if(getStub() != null) {
            getStub().setAlert(alert);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.alert = alert;
        }
    }

    public NotificationStruct getNotificationPref()
    {  
        if(getStub() != null) {
            // Note the object type.
            return new NotificationStructBean(getStub().getNotificationPref());
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.notificationPref;
        }
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setNotificationPref(notificationPref);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(notificationPref instanceof NotificationStructBean) {
                this.notificationPref = (NotificationStructBean) notificationPref;
            } else {
                this.notificationPref = new NotificationStructBean(notificationPref);
            }
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {  
        if(getStub() != null) {
            // Note the object type.
            return new ReferrerInfoStructBean(getStub().getReferrerInfo());
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referrerInfo;
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setReferrerInfo(referrerInfo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(referrerInfo instanceof ReferrerInfoStructBean) {
                this.referrerInfo = (ReferrerInfoStructBean) referrerInfo;
            } else {
                this.referrerInfo = new ReferrerInfoStructBean(referrerInfo);
            }
        }
    }

    public CronScheduleStruct getCronSchedule()
    {  
        if(getStub() != null) {
            // Note the object type.
            return new CronScheduleStructBean(getStub().getCronSchedule());
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.cronSchedule;
        }
    }
    public void setCronSchedule(CronScheduleStruct cronSchedule)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setCronSchedule(cronSchedule);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(cronSchedule instanceof CronScheduleStructBean) {
                this.cronSchedule = (CronScheduleStructBean) cronSchedule;
            } else {
                this.cronSchedule = new CronScheduleStructBean(cronSchedule);
            }
        }
    }

    public Long getNextRunTime()
    {
        if(getStub() != null) {
            return getStub().getNextRunTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.nextRunTime;
        }
    }
    public void setNextRunTime(Long nextRunTime)
    {
        if(getStub() != null) {
            getStub().setNextRunTime(nextRunTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.nextRunTime = nextRunTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public CronJobStub getStub()
    {
        return this.stub;
    }
    protected void setStub(CronJobStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("managerApp = " + this.managerApp).append(";");
            sb.append("appAcl = " + this.appAcl).append(";");
            sb.append("gaeApp = " + this.gaeApp).append(";");
            sb.append("ownerUser = " + this.ownerUser).append(";");
            sb.append("userAcl = " + this.userAcl).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("jobId = " + this.jobId).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("originJob = " + this.originJob).append(";");
            sb.append("restRequest = " + this.restRequest).append(";");
            sb.append("permalink = " + this.permalink).append(";");
            sb.append("shortlink = " + this.shortlink).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("jobStatus = " + this.jobStatus).append(";");
            sb.append("extra = " + this.extra).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("alert = " + this.alert).append(";");
            sb.append("notificationPref = " + this.notificationPref).append(";");
            sb.append("referrerInfo = " + this.referrerInfo).append(";");
            sb.append("cronSchedule = " + this.cronSchedule).append(";");
            sb.append("nextRunTime = " + this.nextRunTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = managerApp == null ? 0 : managerApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = appAcl == null ? 0 : appAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = gaeApp == null ? 0 : gaeApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = ownerUser == null ? 0 : ownerUser.hashCode();
            _hash = 31 * _hash + delta;
            delta = userAcl == null ? 0 : userAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = jobId == null ? 0 : jobId.hashCode();
            _hash = 31 * _hash + delta;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = originJob == null ? 0 : originJob.hashCode();
            _hash = 31 * _hash + delta;
            delta = restRequest == null ? 0 : restRequest.hashCode();
            _hash = 31 * _hash + delta;
            delta = permalink == null ? 0 : permalink.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortlink == null ? 0 : shortlink.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = jobStatus == null ? 0 : jobStatus.hashCode();
            _hash = 31 * _hash + delta;
            delta = extra == null ? 0 : extra.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = alert == null ? 0 : alert.hashCode();
            _hash = 31 * _hash + delta;
            delta = notificationPref == null ? 0 : notificationPref.hashCode();
            _hash = 31 * _hash + delta;
            delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
            _hash = 31 * _hash + delta;
            delta = cronSchedule == null ? 0 : cronSchedule.hashCode();
            _hash = 31 * _hash + delta;
            delta = nextRunTime == null ? 0 : nextRunTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
