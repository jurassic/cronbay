package com.cronbay.af.analytics;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.config.Config;


// Place-holder.
// TBD: Use page/URL dependent analytics code?
public class AnalyticsManager
{
    private static final Logger log = Logger.getLogger(AnalyticsManager.class.getName());

    // temporary
    private static final String CONFIG_KEY_ANALYTICS_CODE_GOOGLE = "cronbayapp.analyticscode.google";
    
    private String googleAnalyticsCode;

    private AnalyticsManager()
    {
        // TBD:
        // Read these from a private file/DB, etc... ???
        googleAnalyticsCode = Config.getInstance().getString(CONFIG_KEY_ANALYTICS_CODE_GOOGLE, "");  // ???
    }

    // Initialization-on-demand holder.
    private static class AnalyticsManagerHolder
    {
        private static final AnalyticsManager INSTANCE = new AnalyticsManager();
    }

    // Singleton method
    public static AnalyticsManager getInstance()
    {
        return AnalyticsManagerHolder.INSTANCE;
    }


    public String getGoogleAnalyticsCode()
    {
        // TBD.
        return googleAnalyticsCode;
    }

}
