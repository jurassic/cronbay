package com.cronbay.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.ServiceInfo;
import com.cronbay.af.bean.ServiceInfoBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.ServiceInfoService;
import com.cronbay.af.service.impl.ServiceInfoServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ServiceInfoProtoService extends ServiceInfoServiceImpl implements ServiceInfoService
{
    private static final Logger log = Logger.getLogger(ServiceInfoProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ServiceInfoProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ServiceInfo related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ServiceInfo getServiceInfo(String guid) throws BaseException
    {
        return super.getServiceInfo(guid);
    }

    @Override
    public Object getServiceInfo(String guid, String field) throws BaseException
    {
        return super.getServiceInfo(guid, field);
    }

    @Override
    public List<ServiceInfo> getServiceInfos(List<String> guids) throws BaseException
    {
        return super.getServiceInfos(guids);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos() throws BaseException
    {
        return super.getAllServiceInfos();
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllServiceInfoKeys(ordering, offset, count);
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        return super.createServiceInfo(serviceInfo);
    }

    @Override
    public ServiceInfo constructServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        return super.constructServiceInfo(serviceInfo);
    }


    @Override
    public Boolean updateServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        return super.updateServiceInfo(serviceInfo);
    }
        
    @Override
    public ServiceInfo refreshServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        return super.refreshServiceInfo(serviceInfo);
    }

    @Override
    public Boolean deleteServiceInfo(String guid) throws BaseException
    {
        return super.deleteServiceInfo(guid);
    }

    @Override
    public Boolean deleteServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        return super.deleteServiceInfo(serviceInfo);
    }

    @Override
    public Integer createServiceInfos(List<ServiceInfo> serviceInfos) throws BaseException
    {
        return super.createServiceInfos(serviceInfos);
    }

    // TBD
    //@Override
    //public Boolean updateServiceInfos(List<ServiceInfo> serviceInfos) throws BaseException
    //{
    //}

}
