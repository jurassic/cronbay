package com.cronbay.af.service;

import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.ws.User;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UserService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    User getUser(String guid) throws BaseException;
    Object getUser(String guid, String field) throws BaseException;
    List<User> getUsers(List<String> guids) throws BaseException;
    List<User> getAllUsers() throws BaseException;
    List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<User> findUsers(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime) throws BaseException;
    //String createUser(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return User?)
    String createUser(User user) throws BaseException;
    User constructUser(User user) throws BaseException;
    Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime) throws BaseException;
    //Boolean updateUser(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUser(User user) throws BaseException;
    User refreshUser(User user) throws BaseException;
    Boolean deleteUser(String guid) throws BaseException;
    Boolean deleteUser(User user) throws BaseException;
    Long deleteUsers(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createUsers(List<User> users) throws BaseException;
//    Boolean updateUsers(List<User> users) throws BaseException;

}
