package com.cronbay.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.af.bean.JobOutputBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.JobOutputService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class JobOutputServiceImpl implements JobOutputService
{
    private static final Logger log = Logger.getLogger(JobOutputServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;
    
    public JobOutputServiceImpl()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            //props.put(GCacheFactory.EXPIRATION_DELTA, ServiceConstants.CACHE_EXPIRATION_DURATION);
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }


    //////////////////////////////////////////////////////////////////////////
    // JobOutput related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public JobOutput getJobOutput(String guid) throws BaseException
    {
        log.fine("getJobOutput(): guid = " + guid);

        JobOutputBean bean = null;
        if(mCache != null) {
            bean = (JobOutputBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (JobOutputBean) getProxyFactory().getJobOutputServiceProxy().getJobOutput(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "JobOutputBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve JobOutputBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getJobOutput(String guid, String field) throws BaseException
    {
        JobOutputBean bean = null;
        if(mCache != null) {
            bean = (JobOutputBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (JobOutputBean) getProxyFactory().getJobOutputServiceProxy().getJobOutput(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "JobOutputBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve JobOutputBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return bean.getManagerApp();
        } else if(field.equals("appAcl")) {
            return bean.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return bean.getGaeApp();
        } else if(field.equals("ownerUser")) {
            return bean.getOwnerUser();
        } else if(field.equals("userAcl")) {
            return bean.getUserAcl();
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("cronJob")) {
            return bean.getCronJob();
        } else if(field.equals("previousRun")) {
            return bean.getPreviousRun();
        } else if(field.equals("previousTry")) {
            return bean.getPreviousTry();
        } else if(field.equals("responseCode")) {
            return bean.getResponseCode();
        } else if(field.equals("outputFormat")) {
            return bean.getOutputFormat();
        } else if(field.equals("outputText")) {
            return bean.getOutputText();
        } else if(field.equals("outputData")) {
            return bean.getOutputData();
        } else if(field.equals("outputHash")) {
            return bean.getOutputHash();
        } else if(field.equals("permalink")) {
            return bean.getPermalink();
        } else if(field.equals("shortlink")) {
            return bean.getShortlink();
        } else if(field.equals("result")) {
            return bean.getResult();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("extra")) {
            return bean.getExtra();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("retryCounter")) {
            return bean.getRetryCounter();
        } else if(field.equals("scheduledTime")) {
            return bean.getScheduledTime();
        } else if(field.equals("processedTime")) {
            return bean.getProcessedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<JobOutput> getJobOutputs(List<String> guids) throws BaseException
    {
        log.fine("getJobOutputs()");

        // TBD: Is there a better way????
        List<JobOutput> jobOutputs = getProxyFactory().getJobOutputServiceProxy().getJobOutputs(guids);
        if(jobOutputs == null) {
            log.log(Level.WARNING, "Failed to retrieve JobOutputBean list.");
        }

        log.finer("END");
        return jobOutputs;
    }

    @Override
    public List<JobOutput> getAllJobOutputs() throws BaseException
    {
        return getAllJobOutputs(null, null, null);
    }

    @Override
    public List<JobOutput> getAllJobOutputs(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllJobOutputs(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<JobOutput> jobOutputs = getProxyFactory().getJobOutputServiceProxy().getAllJobOutputs(ordering, offset, count);
        if(jobOutputs == null) {
            log.log(Level.WARNING, "Failed to retrieve JobOutputBean list.");
        }

        log.finer("END");
        return jobOutputs;
    }

    @Override
    public List<String> getAllJobOutputKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllJobOutputKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getJobOutputServiceProxy().getAllJobOutputKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve JobOutputBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<JobOutput> findJobOutputs(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findJobOutputs(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<JobOutput> findJobOutputs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("JobOutputServiceImpl.findJobOutputs(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<JobOutput> jobOutputs = getProxyFactory().getJobOutputServiceProxy().findJobOutputs(filter, ordering, params, values, grouping, unique, offset, count);
        if(jobOutputs == null) {
            log.log(Level.WARNING, "Failed to find jobOutputs for the given criterion.");
        }

        log.finer("END");
        return jobOutputs;
    }

    @Override
    public List<String> findJobOutputKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("JobOutputServiceImpl.findJobOutputKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getJobOutputServiceProxy().findJobOutputKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find JobOutput keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("JobOutputServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getJobOutputServiceProxy().getCount(filter, params, values, aggregate);

        log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createJobOutput(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        JobOutputBean bean = new JobOutputBean(null, managerApp, appAcl, gaeAppBean, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
        return createJobOutput(bean);
    }

    @Override
    public String createJobOutput(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //JobOutput bean = constructJobOutput(jobOutput);
        //return bean.getGuid();

        // Param jobOutput cannot be null.....
        if(jobOutput == null) {
            log.log(Level.INFO, "Param jobOutput is null!");
            throw new BadRequestException("Param jobOutput object is null!");
        }
        JobOutputBean bean = null;
        if(jobOutput instanceof JobOutputBean) {
            bean = (JobOutputBean) jobOutput;
        } else if(jobOutput instanceof JobOutput) {
            // bean = new JobOutputBean(null, jobOutput.getManagerApp(), jobOutput.getAppAcl(), (GaeAppStructBean) jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new JobOutputBean(jobOutput.getGuid(), jobOutput.getManagerApp(), jobOutput.getAppAcl(), (GaeAppStructBean) jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
        } else {
            log.log(Level.WARNING, "createJobOutput(): Arg jobOutput is of an unknown type.");
            //bean = new JobOutputBean();
            bean = new JobOutputBean(jobOutput.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getJobOutputServiceProxy().createJobOutput(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public JobOutput constructJobOutput(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        // Param jobOutput cannot be null.....
        if(jobOutput == null) {
            log.log(Level.INFO, "Param jobOutput is null!");
            throw new BadRequestException("Param jobOutput object is null!");
        }
        JobOutputBean bean = null;
        if(jobOutput instanceof JobOutputBean) {
            bean = (JobOutputBean) jobOutput;
        } else if(jobOutput instanceof JobOutput) {
            // bean = new JobOutputBean(null, jobOutput.getManagerApp(), jobOutput.getAppAcl(), (GaeAppStructBean) jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new JobOutputBean(jobOutput.getGuid(), jobOutput.getManagerApp(), jobOutput.getAppAcl(), (GaeAppStructBean) jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
        } else {
            log.log(Level.WARNING, "createJobOutput(): Arg jobOutput is of an unknown type.");
            //bean = new JobOutputBean();
            bean = new JobOutputBean(jobOutput.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getJobOutputServiceProxy().createJobOutput(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateJobOutput(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        JobOutputBean bean = new JobOutputBean(guid, managerApp, appAcl, gaeAppBean, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
        return updateJobOutput(bean);
    }
        
    @Override
    public Boolean updateJobOutput(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //JobOutput bean = refreshJobOutput(jobOutput);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param jobOutput cannot be null.....
        if(jobOutput == null || jobOutput.getGuid() == null) {
            log.log(Level.INFO, "Param jobOutput or its guid is null!");
            throw new BadRequestException("Param jobOutput object or its guid is null!");
        }
        JobOutputBean bean = null;
        if(jobOutput instanceof JobOutputBean) {
            bean = (JobOutputBean) jobOutput;
        } else if(jobOutput instanceof JobOutputBean) {
            bean = (JobOutputBean) jobOutput;
        } else {  // if(jobOutput instanceof JobOutput)
            bean = new JobOutputBean(jobOutput.getGuid(), jobOutput.getManagerApp(), jobOutput.getAppAcl(), (GaeAppStructBean) jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
        }
        Boolean suc = getProxyFactory().getJobOutputServiceProxy().updateJobOutput(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }
        
    @Override
    public JobOutput refreshJobOutput(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        // Param jobOutput cannot be null.....
        if(jobOutput == null || jobOutput.getGuid() == null) {
            log.log(Level.INFO, "Param jobOutput or its guid is null!");
            throw new BadRequestException("Param jobOutput object or its guid is null!");
        }
        JobOutputBean bean = null;
        if(jobOutput instanceof JobOutputBean) {
            bean = (JobOutputBean) jobOutput;
        } else if(jobOutput instanceof JobOutputBean) {
            bean = (JobOutputBean) jobOutput;
        } else {  // if(jobOutput instanceof JobOutput)
            bean = new JobOutputBean(jobOutput.getGuid(), jobOutput.getManagerApp(), jobOutput.getAppAcl(), (GaeAppStructBean) jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
        }
        Boolean suc = getProxyFactory().getJobOutputServiceProxy().updateJobOutput(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }
        //if(suc == true)  // else ???

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteJobOutput(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getProxyFactory().getJobOutputServiceProxy().deleteJobOutput(guid);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(guid);
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // ???
    @Override
    public Boolean deleteJobOutput(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        // Param jobOutput cannot be null.....
        if(jobOutput == null || jobOutput.getGuid() == null) {
            log.log(Level.INFO, "Param jobOutput or its guid is null!");
            throw new BadRequestException("Param jobOutput object or its guid is null!");
        }
        JobOutputBean bean = null;
        if(jobOutput instanceof JobOutputBean) {
            bean = (JobOutputBean) jobOutput;
        } else {  // if(jobOutput instanceof JobOutput)
            bean = new JobOutputBean(jobOutput.getGuid(), jobOutput.getManagerApp(), jobOutput.getAppAcl(), (GaeAppStructBean) jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
        }
        Boolean suc = getProxyFactory().getJobOutputServiceProxy().deleteJobOutput(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteJobOutputs(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getJobOutputServiceProxy().deleteJobOutputs(filter, params, values);
        if(mCache != null) {
            //if(count > 0) {
            //    mCache.remove(...);
            //}
        }
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createJobOutputs(List<JobOutput> jobOutputs) throws BaseException
    {
        log.finer("BEGIN");

        if(jobOutputs == null) {
            log.log(Level.WARNING, "createJobOutputs() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = jobOutputs.size();
        if(size == 0) {
            log.log(Level.WARNING, "createJobOutputs() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(JobOutput jobOutput : jobOutputs) {
            String guid = createJobOutput(jobOutput);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                log.log(Level.WARNING, "createJobOutputs() failed for at least one jobOutput. Index = " + count);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateJobOutputs(List<JobOutput> jobOutputs) throws BaseException
    //{
    //}

}
