package com.cronbay.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.ApiConsumer;
import com.cronbay.af.bean.ApiConsumerBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.ApiConsumerService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ApiConsumerServiceImpl implements ApiConsumerService
{
    private static final Logger log = Logger.getLogger(ApiConsumerServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;
    
    public ApiConsumerServiceImpl()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            //props.put(GCacheFactory.EXPIRATION_DELTA, ServiceConstants.CACHE_EXPIRATION_DURATION);
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }


    //////////////////////////////////////////////////////////////////////////
    // ApiConsumer related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ApiConsumer getApiConsumer(String guid) throws BaseException
    {
        log.fine("getApiConsumer(): guid = " + guid);

        ApiConsumerBean bean = null;
        if(mCache != null) {
            bean = (ApiConsumerBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (ApiConsumerBean) getProxyFactory().getApiConsumerServiceProxy().getApiConsumer(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "ApiConsumerBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve ApiConsumerBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getApiConsumer(String guid, String field) throws BaseException
    {
        ApiConsumerBean bean = null;
        if(mCache != null) {
            bean = (ApiConsumerBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (ApiConsumerBean) getProxyFactory().getApiConsumerServiceProxy().getApiConsumer(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "ApiConsumerBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve ApiConsumerBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("aeryId")) {
            return bean.getAeryId();
        } else if(field.equals("name")) {
            return bean.getName();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("appKey")) {
            return bean.getAppKey();
        } else if(field.equals("appSecret")) {
            return bean.getAppSecret();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ApiConsumer> getApiConsumers(List<String> guids) throws BaseException
    {
        log.fine("getApiConsumers()");

        // TBD: Is there a better way????
        List<ApiConsumer> apiConsumers = getProxyFactory().getApiConsumerServiceProxy().getApiConsumers(guids);
        if(apiConsumers == null) {
            log.log(Level.WARNING, "Failed to retrieve ApiConsumerBean list.");
        }

        log.finer("END");
        return apiConsumers;
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers() throws BaseException
    {
        return getAllApiConsumers(null, null, null);
    }

    @Override
    public List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllApiConsumers(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<ApiConsumer> apiConsumers = getProxyFactory().getApiConsumerServiceProxy().getAllApiConsumers(ordering, offset, count);
        if(apiConsumers == null) {
            log.log(Level.WARNING, "Failed to retrieve ApiConsumerBean list.");
        }

        log.finer("END");
        return apiConsumers;
    }

    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllApiConsumerKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getApiConsumerServiceProxy().getAllApiConsumerKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ApiConsumerBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findApiConsumers(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("ApiConsumerServiceImpl.findApiConsumers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<ApiConsumer> apiConsumers = getProxyFactory().getApiConsumerServiceProxy().findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count);
        if(apiConsumers == null) {
            log.log(Level.WARNING, "Failed to find apiConsumers for the given criterion.");
        }

        log.finer("END");
        return apiConsumers;
    }

    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("ApiConsumerServiceImpl.findApiConsumerKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getApiConsumerServiceProxy().findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ApiConsumer keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("ApiConsumerServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getApiConsumerServiceProxy().getCount(filter, params, values, aggregate);

        log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createApiConsumer(String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ApiConsumerBean bean = new ApiConsumerBean(null, aeryId, name, description, appKey, appSecret, status);
        return createApiConsumer(bean);
    }

    @Override
    public String createApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ApiConsumer bean = constructApiConsumer(apiConsumer);
        //return bean.getGuid();

        // Param apiConsumer cannot be null.....
        if(apiConsumer == null) {
            log.log(Level.INFO, "Param apiConsumer is null!");
            throw new BadRequestException("Param apiConsumer object is null!");
        }
        ApiConsumerBean bean = null;
        if(apiConsumer instanceof ApiConsumerBean) {
            bean = (ApiConsumerBean) apiConsumer;
        } else if(apiConsumer instanceof ApiConsumer) {
            // bean = new ApiConsumerBean(null, apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ApiConsumerBean(apiConsumer.getGuid(), apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
        } else {
            log.log(Level.WARNING, "createApiConsumer(): Arg apiConsumer is of an unknown type.");
            //bean = new ApiConsumerBean();
            bean = new ApiConsumerBean(apiConsumer.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getApiConsumerServiceProxy().createApiConsumer(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ApiConsumer constructApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("BEGIN");

        // Param apiConsumer cannot be null.....
        if(apiConsumer == null) {
            log.log(Level.INFO, "Param apiConsumer is null!");
            throw new BadRequestException("Param apiConsumer object is null!");
        }
        ApiConsumerBean bean = null;
        if(apiConsumer instanceof ApiConsumerBean) {
            bean = (ApiConsumerBean) apiConsumer;
        } else if(apiConsumer instanceof ApiConsumer) {
            // bean = new ApiConsumerBean(null, apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ApiConsumerBean(apiConsumer.getGuid(), apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
        } else {
            log.log(Level.WARNING, "createApiConsumer(): Arg apiConsumer is of an unknown type.");
            //bean = new ApiConsumerBean();
            bean = new ApiConsumerBean(apiConsumer.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getApiConsumerServiceProxy().createApiConsumer(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateApiConsumer(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ApiConsumerBean bean = new ApiConsumerBean(guid, aeryId, name, description, appKey, appSecret, status);
        return updateApiConsumer(bean);
    }
        
    @Override
    public Boolean updateApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ApiConsumer bean = refreshApiConsumer(apiConsumer);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param apiConsumer cannot be null.....
        if(apiConsumer == null || apiConsumer.getGuid() == null) {
            log.log(Level.INFO, "Param apiConsumer or its guid is null!");
            throw new BadRequestException("Param apiConsumer object or its guid is null!");
        }
        ApiConsumerBean bean = null;
        if(apiConsumer instanceof ApiConsumerBean) {
            bean = (ApiConsumerBean) apiConsumer;
        } else if(apiConsumer instanceof ApiConsumerBean) {
            bean = (ApiConsumerBean) apiConsumer;
        } else {  // if(apiConsumer instanceof ApiConsumer)
            bean = new ApiConsumerBean(apiConsumer.getGuid(), apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
        }
        Boolean suc = getProxyFactory().getApiConsumerServiceProxy().updateApiConsumer(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }
        
    @Override
    public ApiConsumer refreshApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("BEGIN");

        // Param apiConsumer cannot be null.....
        if(apiConsumer == null || apiConsumer.getGuid() == null) {
            log.log(Level.INFO, "Param apiConsumer or its guid is null!");
            throw new BadRequestException("Param apiConsumer object or its guid is null!");
        }
        ApiConsumerBean bean = null;
        if(apiConsumer instanceof ApiConsumerBean) {
            bean = (ApiConsumerBean) apiConsumer;
        } else if(apiConsumer instanceof ApiConsumerBean) {
            bean = (ApiConsumerBean) apiConsumer;
        } else {  // if(apiConsumer instanceof ApiConsumer)
            bean = new ApiConsumerBean(apiConsumer.getGuid(), apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
        }
        Boolean suc = getProxyFactory().getApiConsumerServiceProxy().updateApiConsumer(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }
        //if(suc == true)  // else ???

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteApiConsumer(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getProxyFactory().getApiConsumerServiceProxy().deleteApiConsumer(guid);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(guid);
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // ???
    @Override
    public Boolean deleteApiConsumer(ApiConsumer apiConsumer) throws BaseException
    {
        log.finer("BEGIN");

        // Param apiConsumer cannot be null.....
        if(apiConsumer == null || apiConsumer.getGuid() == null) {
            log.log(Level.INFO, "Param apiConsumer or its guid is null!");
            throw new BadRequestException("Param apiConsumer object or its guid is null!");
        }
        ApiConsumerBean bean = null;
        if(apiConsumer instanceof ApiConsumerBean) {
            bean = (ApiConsumerBean) apiConsumer;
        } else {  // if(apiConsumer instanceof ApiConsumer)
            bean = new ApiConsumerBean(apiConsumer.getGuid(), apiConsumer.getAeryId(), apiConsumer.getName(), apiConsumer.getDescription(), apiConsumer.getAppKey(), apiConsumer.getAppSecret(), apiConsumer.getStatus());
        }
        Boolean suc = getProxyFactory().getApiConsumerServiceProxy().deleteApiConsumer(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteApiConsumers(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getApiConsumerServiceProxy().deleteApiConsumers(filter, params, values);
        if(mCache != null) {
            //if(count > 0) {
            //    mCache.remove(...);
            //}
        }
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createApiConsumers(List<ApiConsumer> apiConsumers) throws BaseException
    {
        log.finer("BEGIN");

        if(apiConsumers == null) {
            log.log(Level.WARNING, "createApiConsumers() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = apiConsumers.size();
        if(size == 0) {
            log.log(Level.WARNING, "createApiConsumers() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ApiConsumer apiConsumer : apiConsumers) {
            String guid = createApiConsumer(apiConsumer);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                log.log(Level.WARNING, "createApiConsumers() failed for at least one apiConsumer. Index = " + count);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateApiConsumers(List<ApiConsumer> apiConsumers) throws BaseException
    //{
    //}

}
