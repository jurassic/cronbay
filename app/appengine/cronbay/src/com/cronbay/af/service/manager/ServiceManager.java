package com.cronbay.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.service.AbstractServiceFactory;
import com.cronbay.af.service.ApiConsumerService;
import com.cronbay.af.service.UserService;
import com.cronbay.af.service.CronJobService;
import com.cronbay.af.service.JobOutputService;
import com.cronbay.af.service.ServiceInfoService;
import com.cronbay.af.service.FiveTenService;

// TBD:
// Factory? DI? (Does the current "dual" approach make sense?)
// Make it a singleton? Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

    // Reference to the Abstract factory.
    private static AbstractServiceFactory serviceFactory = ServiceFactoryManager.getServiceFactory();

    // All service getters/setters are delegated to ServiceController.
    // TBD: Use a DI framework such as Spring or Guice....
    private static ServiceController serviceController = new ServiceController(null, null, null, null, null, null);

    // Static methods only.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(serviceController.getApiConsumerService() == null) {
            serviceController.setApiConsumerService(serviceFactory.getApiConsumerService());
        }
        return serviceController.getApiConsumerService();
    }
    // Injects a ApiConsumerService instance.
	public static void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        serviceController.setApiConsumerService(apiConsumerService);
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(serviceController.getUserService() == null) {
            serviceController.setUserService(serviceFactory.getUserService());
        }
        return serviceController.getUserService();
    }
    // Injects a UserService instance.
	public static void setUserService(UserService userService) 
    {
        serviceController.setUserService(userService);
    }

    // Returns a CronJobService instance.
	public static CronJobService getCronJobService() 
    {
        if(serviceController.getCronJobService() == null) {
            serviceController.setCronJobService(serviceFactory.getCronJobService());
        }
        return serviceController.getCronJobService();
    }
    // Injects a CronJobService instance.
	public static void setCronJobService(CronJobService cronJobService) 
    {
        serviceController.setCronJobService(cronJobService);
    }

    // Returns a JobOutputService instance.
	public static JobOutputService getJobOutputService() 
    {
        if(serviceController.getJobOutputService() == null) {
            serviceController.setJobOutputService(serviceFactory.getJobOutputService());
        }
        return serviceController.getJobOutputService();
    }
    // Injects a JobOutputService instance.
	public static void setJobOutputService(JobOutputService jobOutputService) 
    {
        serviceController.setJobOutputService(jobOutputService);
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(serviceController.getServiceInfoService() == null) {
            serviceController.setServiceInfoService(serviceFactory.getServiceInfoService());
        }
        return serviceController.getServiceInfoService();
    }
    // Injects a ServiceInfoService instance.
	public static void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        serviceController.setServiceInfoService(serviceInfoService);
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(serviceController.getFiveTenService() == null) {
            serviceController.setFiveTenService(serviceFactory.getFiveTenService());
        }
        return serviceController.getFiveTenService();
    }
    // Injects a FiveTenService instance.
	public static void setFiveTenService(FiveTenService fiveTenService) 
    {
        serviceController.setFiveTenService(fiveTenService);
    }

}
