package com.cronbay.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.ServiceInfo;
import com.cronbay.af.bean.ServiceInfoBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.ServiceInfoService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ServiceInfoServiceImpl implements ServiceInfoService
{
    private static final Logger log = Logger.getLogger(ServiceInfoServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;
    
    public ServiceInfoServiceImpl()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            //props.put(GCacheFactory.EXPIRATION_DELTA, ServiceConstants.CACHE_EXPIRATION_DURATION);
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }


    //////////////////////////////////////////////////////////////////////////
    // ServiceInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ServiceInfo getServiceInfo(String guid) throws BaseException
    {
        log.fine("getServiceInfo(): guid = " + guid);

        ServiceInfoBean bean = null;
        if(mCache != null) {
            bean = (ServiceInfoBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (ServiceInfoBean) getProxyFactory().getServiceInfoServiceProxy().getServiceInfo(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "ServiceInfoBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceInfoBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getServiceInfo(String guid, String field) throws BaseException
    {
        ServiceInfoBean bean = null;
        if(mCache != null) {
            bean = (ServiceInfoBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (ServiceInfoBean) getProxyFactory().getServiceInfoServiceProxy().getServiceInfo(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "ServiceInfoBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceInfoBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("content")) {
            return bean.getContent();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("scheduledTime")) {
            return bean.getScheduledTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ServiceInfo> getServiceInfos(List<String> guids) throws BaseException
    {
        log.fine("getServiceInfos()");

        // TBD: Is there a better way????
        List<ServiceInfo> serviceInfos = getProxyFactory().getServiceInfoServiceProxy().getServiceInfos(guids);
        if(serviceInfos == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceInfoBean list.");
        }

        log.finer("END");
        return serviceInfos;
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos() throws BaseException
    {
        return getAllServiceInfos(null, null, null);
    }

    @Override
    public List<ServiceInfo> getAllServiceInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllServiceInfos(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<ServiceInfo> serviceInfos = getProxyFactory().getServiceInfoServiceProxy().getAllServiceInfos(ordering, offset, count);
        if(serviceInfos == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceInfoBean list.");
        }

        log.finer("END");
        return serviceInfos;
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllServiceInfoKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getServiceInfoServiceProxy().getAllServiceInfoKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ServiceInfoBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findServiceInfos(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ServiceInfo> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("ServiceInfoServiceImpl.findServiceInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<ServiceInfo> serviceInfos = getProxyFactory().getServiceInfoServiceProxy().findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count);
        if(serviceInfos == null) {
            log.log(Level.WARNING, "Failed to find serviceInfos for the given criterion.");
        }

        log.finer("END");
        return serviceInfos;
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("ServiceInfoServiceImpl.findServiceInfoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getServiceInfoServiceProxy().findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ServiceInfo keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("ServiceInfoServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getServiceInfoServiceProxy().getCount(filter, params, values, aggregate);

        log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createServiceInfo(String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ServiceInfoBean bean = new ServiceInfoBean(null, title, content, type, status, scheduledTime);
        return createServiceInfo(bean);
    }

    @Override
    public String createServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ServiceInfo bean = constructServiceInfo(serviceInfo);
        //return bean.getGuid();

        // Param serviceInfo cannot be null.....
        if(serviceInfo == null) {
            log.log(Level.INFO, "Param serviceInfo is null!");
            throw new BadRequestException("Param serviceInfo object is null!");
        }
        ServiceInfoBean bean = null;
        if(serviceInfo instanceof ServiceInfoBean) {
            bean = (ServiceInfoBean) serviceInfo;
        } else if(serviceInfo instanceof ServiceInfo) {
            // bean = new ServiceInfoBean(null, serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ServiceInfoBean(serviceInfo.getGuid(), serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
        } else {
            log.log(Level.WARNING, "createServiceInfo(): Arg serviceInfo is of an unknown type.");
            //bean = new ServiceInfoBean();
            bean = new ServiceInfoBean(serviceInfo.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getServiceInfoServiceProxy().createServiceInfo(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ServiceInfo constructServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceInfo cannot be null.....
        if(serviceInfo == null) {
            log.log(Level.INFO, "Param serviceInfo is null!");
            throw new BadRequestException("Param serviceInfo object is null!");
        }
        ServiceInfoBean bean = null;
        if(serviceInfo instanceof ServiceInfoBean) {
            bean = (ServiceInfoBean) serviceInfo;
        } else if(serviceInfo instanceof ServiceInfo) {
            // bean = new ServiceInfoBean(null, serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ServiceInfoBean(serviceInfo.getGuid(), serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
        } else {
            log.log(Level.WARNING, "createServiceInfo(): Arg serviceInfo is of an unknown type.");
            //bean = new ServiceInfoBean();
            bean = new ServiceInfoBean(serviceInfo.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getServiceInfoServiceProxy().createServiceInfo(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ServiceInfoBean bean = new ServiceInfoBean(guid, title, content, type, status, scheduledTime);
        return updateServiceInfo(bean);
    }
        
    @Override
    public Boolean updateServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ServiceInfo bean = refreshServiceInfo(serviceInfo);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param serviceInfo cannot be null.....
        if(serviceInfo == null || serviceInfo.getGuid() == null) {
            log.log(Level.INFO, "Param serviceInfo or its guid is null!");
            throw new BadRequestException("Param serviceInfo object or its guid is null!");
        }
        ServiceInfoBean bean = null;
        if(serviceInfo instanceof ServiceInfoBean) {
            bean = (ServiceInfoBean) serviceInfo;
        } else if(serviceInfo instanceof ServiceInfoBean) {
            bean = (ServiceInfoBean) serviceInfo;
        } else {  // if(serviceInfo instanceof ServiceInfo)
            bean = new ServiceInfoBean(serviceInfo.getGuid(), serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
        }
        Boolean suc = getProxyFactory().getServiceInfoServiceProxy().updateServiceInfo(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }
        
    @Override
    public ServiceInfo refreshServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceInfo cannot be null.....
        if(serviceInfo == null || serviceInfo.getGuid() == null) {
            log.log(Level.INFO, "Param serviceInfo or its guid is null!");
            throw new BadRequestException("Param serviceInfo object or its guid is null!");
        }
        ServiceInfoBean bean = null;
        if(serviceInfo instanceof ServiceInfoBean) {
            bean = (ServiceInfoBean) serviceInfo;
        } else if(serviceInfo instanceof ServiceInfoBean) {
            bean = (ServiceInfoBean) serviceInfo;
        } else {  // if(serviceInfo instanceof ServiceInfo)
            bean = new ServiceInfoBean(serviceInfo.getGuid(), serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
        }
        Boolean suc = getProxyFactory().getServiceInfoServiceProxy().updateServiceInfo(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }
        //if(suc == true)  // else ???

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteServiceInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getProxyFactory().getServiceInfoServiceProxy().deleteServiceInfo(guid);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(guid);
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // ???
    @Override
    public Boolean deleteServiceInfo(ServiceInfo serviceInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param serviceInfo cannot be null.....
        if(serviceInfo == null || serviceInfo.getGuid() == null) {
            log.log(Level.INFO, "Param serviceInfo or its guid is null!");
            throw new BadRequestException("Param serviceInfo object or its guid is null!");
        }
        ServiceInfoBean bean = null;
        if(serviceInfo instanceof ServiceInfoBean) {
            bean = (ServiceInfoBean) serviceInfo;
        } else {  // if(serviceInfo instanceof ServiceInfo)
            bean = new ServiceInfoBean(serviceInfo.getGuid(), serviceInfo.getTitle(), serviceInfo.getContent(), serviceInfo.getType(), serviceInfo.getStatus(), serviceInfo.getScheduledTime());
        }
        Boolean suc = getProxyFactory().getServiceInfoServiceProxy().deleteServiceInfo(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteServiceInfos(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getServiceInfoServiceProxy().deleteServiceInfos(filter, params, values);
        if(mCache != null) {
            //if(count > 0) {
            //    mCache.remove(...);
            //}
        }
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createServiceInfos(List<ServiceInfo> serviceInfos) throws BaseException
    {
        log.finer("BEGIN");

        if(serviceInfos == null) {
            log.log(Level.WARNING, "createServiceInfos() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = serviceInfos.size();
        if(size == 0) {
            log.log(Level.WARNING, "createServiceInfos() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ServiceInfo serviceInfo : serviceInfos) {
            String guid = createServiceInfo(serviceInfo);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                log.log(Level.WARNING, "createServiceInfos() failed for at least one serviceInfo. Index = " + count);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateServiceInfos(List<ServiceInfo> serviceInfos) throws BaseException
    //{
    //}

}
