package com.cronbay.af.service.impl;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.service.AbstractServiceFactory;
import com.cronbay.af.service.ApiConsumerService;
import com.cronbay.af.service.UserService;
import com.cronbay.af.service.CronJobService;
import com.cronbay.af.service.JobOutputService;
import com.cronbay.af.service.ServiceInfoService;
import com.cronbay.af.service.FiveTenService;

public class ImplServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ImplServiceFactory.class.getName());

    private ImplServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ImplServiceFactoryHolder
    {
        private static final ImplServiceFactory INSTANCE = new ImplServiceFactory();
    }

    // Singleton method
    public static ImplServiceFactory getInstance()
    {
        return ImplServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerServiceImpl();
    }

    @Override
    public UserService getUserService()
    {
        return new UserServiceImpl();
    }

    @Override
    public CronJobService getCronJobService()
    {
        return new CronJobServiceImpl();
    }

    @Override
    public JobOutputService getJobOutputService()
    {
        return new JobOutputServiceImpl();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoServiceImpl();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenServiceImpl();
    }


}
