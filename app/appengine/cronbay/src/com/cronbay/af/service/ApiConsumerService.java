package com.cronbay.af.service;

import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.ApiConsumer;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ApiConsumerService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ApiConsumer getApiConsumer(String guid) throws BaseException;
    Object getApiConsumer(String guid, String field) throws BaseException;
    List<ApiConsumer> getApiConsumers(List<String> guids) throws BaseException;
    List<ApiConsumer> getAllApiConsumers() throws BaseException;
    List<ApiConsumer> getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ApiConsumer> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createApiConsumer(String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException;
    //String createApiConsumer(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ApiConsumer?)
    String createApiConsumer(ApiConsumer apiConsumer) throws BaseException;
    ApiConsumer constructApiConsumer(ApiConsumer apiConsumer) throws BaseException;
    Boolean updateApiConsumer(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseException;
    //Boolean updateApiConsumer(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateApiConsumer(ApiConsumer apiConsumer) throws BaseException;
    ApiConsumer refreshApiConsumer(ApiConsumer apiConsumer) throws BaseException;
    Boolean deleteApiConsumer(String guid) throws BaseException;
    Boolean deleteApiConsumer(ApiConsumer apiConsumer) throws BaseException;
    Long deleteApiConsumers(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createApiConsumers(List<ApiConsumer> apiConsumers) throws BaseException;
//    Boolean updateApiConsumers(List<ApiConsumer> apiConsumers) throws BaseException;

}
