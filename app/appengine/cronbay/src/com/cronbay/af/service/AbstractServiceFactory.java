package com.cronbay.af.service;

public abstract class AbstractServiceFactory
{
    public abstract ApiConsumerService getApiConsumerService();
    public abstract UserService getUserService();
    public abstract CronJobService getCronJobService();
    public abstract JobOutputService getJobOutputService();
    public abstract ServiceInfoService getServiceInfoService();
    public abstract FiveTenService getFiveTenService();

}
