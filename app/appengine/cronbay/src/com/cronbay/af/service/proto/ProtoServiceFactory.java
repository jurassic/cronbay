package com.cronbay.af.service.proto;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.service.AbstractServiceFactory;
import com.cronbay.af.service.ApiConsumerService;
import com.cronbay.af.service.UserService;
import com.cronbay.af.service.CronJobService;
import com.cronbay.af.service.JobOutputService;
import com.cronbay.af.service.ServiceInfoService;
import com.cronbay.af.service.FiveTenService;

public class ProtoServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ProtoServiceFactory.class.getName());

    private ProtoServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ProtoServiceFactoryHolder
    {
        private static final ProtoServiceFactory INSTANCE = new ProtoServiceFactory();
    }

    // Singleton method
    public static ProtoServiceFactory getInstance()
    {
        return ProtoServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerProtoService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserProtoService();
    }

    @Override
    public CronJobService getCronJobService()
    {
        return new CronJobProtoService();
    }

    @Override
    public JobOutputService getJobOutputService()
    {
        return new JobOutputProtoService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoProtoService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenProtoService();
    }


}
