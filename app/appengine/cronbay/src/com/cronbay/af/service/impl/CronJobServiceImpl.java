package com.cronbay.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.af.bean.NotificationStructBean;
import com.cronbay.af.bean.RestRequestStructBean;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.af.bean.ReferrerInfoStructBean;
import com.cronbay.af.bean.CronScheduleStructBean;
import com.cronbay.af.bean.CronJobBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.CronJobService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class CronJobServiceImpl implements CronJobService
{
    private static final Logger log = Logger.getLogger(CronJobServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;
    
    public CronJobServiceImpl()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            //props.put(GCacheFactory.EXPIRATION_DELTA, ServiceConstants.CACHE_EXPIRATION_DURATION);
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }


    //////////////////////////////////////////////////////////////////////////
    // CronJob related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public CronJob getCronJob(String guid) throws BaseException
    {
        log.fine("getCronJob(): guid = " + guid);

        CronJobBean bean = null;
        if(mCache != null) {
            bean = (CronJobBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (CronJobBean) getProxyFactory().getCronJobServiceProxy().getCronJob(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "CronJobBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve CronJobBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getCronJob(String guid, String field) throws BaseException
    {
        CronJobBean bean = null;
        if(mCache != null) {
            bean = (CronJobBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (CronJobBean) getProxyFactory().getCronJobServiceProxy().getCronJob(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "CronJobBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve CronJobBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return bean.getManagerApp();
        } else if(field.equals("appAcl")) {
            return bean.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return bean.getGaeApp();
        } else if(field.equals("ownerUser")) {
            return bean.getOwnerUser();
        } else if(field.equals("userAcl")) {
            return bean.getUserAcl();
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("jobId")) {
            return bean.getJobId();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("originJob")) {
            return bean.getOriginJob();
        } else if(field.equals("restRequest")) {
            return bean.getRestRequest();
        } else if(field.equals("permalink")) {
            return bean.getPermalink();
        } else if(field.equals("shortlink")) {
            return bean.getShortlink();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("jobStatus")) {
            return bean.getJobStatus();
        } else if(field.equals("extra")) {
            return bean.getExtra();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("alert")) {
            return bean.isAlert();
        } else if(field.equals("notificationPref")) {
            return bean.getNotificationPref();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("cronSchedule")) {
            return bean.getCronSchedule();
        } else if(field.equals("nextRunTime")) {
            return bean.getNextRunTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<CronJob> getCronJobs(List<String> guids) throws BaseException
    {
        log.fine("getCronJobs()");

        // TBD: Is there a better way????
        List<CronJob> cronJobs = getProxyFactory().getCronJobServiceProxy().getCronJobs(guids);
        if(cronJobs == null) {
            log.log(Level.WARNING, "Failed to retrieve CronJobBean list.");
        }

        log.finer("END");
        return cronJobs;
    }

    @Override
    public List<CronJob> getAllCronJobs() throws BaseException
    {
        return getAllCronJobs(null, null, null);
    }

    @Override
    public List<CronJob> getAllCronJobs(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllCronJobs(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<CronJob> cronJobs = getProxyFactory().getCronJobServiceProxy().getAllCronJobs(ordering, offset, count);
        if(cronJobs == null) {
            log.log(Level.WARNING, "Failed to retrieve CronJobBean list.");
        }

        log.finer("END");
        return cronJobs;
    }

    @Override
    public List<String> getAllCronJobKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllCronJobKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getCronJobServiceProxy().getAllCronJobKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve CronJobBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<CronJob> findCronJobs(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findCronJobs(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<CronJob> findCronJobs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("CronJobServiceImpl.findCronJobs(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<CronJob> cronJobs = getProxyFactory().getCronJobServiceProxy().findCronJobs(filter, ordering, params, values, grouping, unique, offset, count);
        if(cronJobs == null) {
            log.log(Level.WARNING, "Failed to find cronJobs for the given criterion.");
        }

        log.finer("END");
        return cronJobs;
    }

    @Override
    public List<String> findCronJobKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("CronJobServiceImpl.findCronJobKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getCronJobServiceProxy().findCronJobKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find CronJob keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("CronJobServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getCronJobServiceProxy().getCount(filter, params, values, aggregate);

        log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createCronJob(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        RestRequestStructBean restRequestBean = null;
        if(restRequest instanceof RestRequestStructBean) {
            restRequestBean = (RestRequestStructBean) restRequest;
        } else if(restRequest instanceof RestRequestStruct) {
            restRequestBean = new RestRequestStructBean(restRequest.getServiceName(), restRequest.getServiceUrl(), restRequest.getRequestMethod(), restRequest.getRequestUrl(), restRequest.getTargetEntity(), restRequest.getQueryString(), restRequest.getQueryParams(), restRequest.getInputFormat(), restRequest.getInputContent(), restRequest.getOutputFormat(), restRequest.getMaxRetries(), restRequest.getRetryInterval(), restRequest.getNote());
        } else {
            restRequestBean = null;   // ????
        }
        NotificationStructBean notificationPrefBean = null;
        if(notificationPref instanceof NotificationStructBean) {
            notificationPrefBean = (NotificationStructBean) notificationPref;
        } else if(notificationPref instanceof NotificationStruct) {
            notificationPrefBean = new NotificationStructBean(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getCallbackUrl(), notificationPref.getNote());
        } else {
            notificationPrefBean = null;   // ????
        }
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        CronScheduleStructBean cronScheduleBean = null;
        if(cronSchedule instanceof CronScheduleStructBean) {
            cronScheduleBean = (CronScheduleStructBean) cronSchedule;
        } else if(cronSchedule instanceof CronScheduleStruct) {
            cronScheduleBean = new CronScheduleStructBean(cronSchedule.getType(), cronSchedule.getSchedule(), cronSchedule.getTimezone(), cronSchedule.getMaxIterations(), cronSchedule.getRepeatInterval(), cronSchedule.getFirtRunTime(), cronSchedule.getStartTime(), cronSchedule.getEndTime(), cronSchedule.getNote());
        } else {
            cronScheduleBean = null;   // ????
        }
        CronJobBean bean = new CronJobBean(null, managerApp, appAcl, gaeAppBean, ownerUser, userAcl, user, jobId, title, description, originJob, restRequestBean, permalink, shortlink, status, jobStatus, extra, note, alert, notificationPrefBean, referrerInfoBean, cronScheduleBean, nextRunTime);
        return createCronJob(bean);
    }

    @Override
    public String createCronJob(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //CronJob bean = constructCronJob(cronJob);
        //return bean.getGuid();

        // Param cronJob cannot be null.....
        if(cronJob == null) {
            log.log(Level.INFO, "Param cronJob is null!");
            throw new BadRequestException("Param cronJob object is null!");
        }
        CronJobBean bean = null;
        if(cronJob instanceof CronJobBean) {
            bean = (CronJobBean) cronJob;
        } else if(cronJob instanceof CronJob) {
            // bean = new CronJobBean(null, cronJob.getManagerApp(), cronJob.getAppAcl(), (GaeAppStructBean) cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), (RestRequestStructBean) cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), (NotificationStructBean) cronJob.getNotificationPref(), (ReferrerInfoStructBean) cronJob.getReferrerInfo(), (CronScheduleStructBean) cronJob.getCronSchedule(), cronJob.getNextRunTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new CronJobBean(cronJob.getGuid(), cronJob.getManagerApp(), cronJob.getAppAcl(), (GaeAppStructBean) cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), (RestRequestStructBean) cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), (NotificationStructBean) cronJob.getNotificationPref(), (ReferrerInfoStructBean) cronJob.getReferrerInfo(), (CronScheduleStructBean) cronJob.getCronSchedule(), cronJob.getNextRunTime());
        } else {
            log.log(Level.WARNING, "createCronJob(): Arg cronJob is of an unknown type.");
            //bean = new CronJobBean();
            bean = new CronJobBean(cronJob.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getCronJobServiceProxy().createCronJob(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public CronJob constructCronJob(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN");

        // Param cronJob cannot be null.....
        if(cronJob == null) {
            log.log(Level.INFO, "Param cronJob is null!");
            throw new BadRequestException("Param cronJob object is null!");
        }
        CronJobBean bean = null;
        if(cronJob instanceof CronJobBean) {
            bean = (CronJobBean) cronJob;
        } else if(cronJob instanceof CronJob) {
            // bean = new CronJobBean(null, cronJob.getManagerApp(), cronJob.getAppAcl(), (GaeAppStructBean) cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), (RestRequestStructBean) cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), (NotificationStructBean) cronJob.getNotificationPref(), (ReferrerInfoStructBean) cronJob.getReferrerInfo(), (CronScheduleStructBean) cronJob.getCronSchedule(), cronJob.getNextRunTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new CronJobBean(cronJob.getGuid(), cronJob.getManagerApp(), cronJob.getAppAcl(), (GaeAppStructBean) cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), (RestRequestStructBean) cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), (NotificationStructBean) cronJob.getNotificationPref(), (ReferrerInfoStructBean) cronJob.getReferrerInfo(), (CronScheduleStructBean) cronJob.getCronSchedule(), cronJob.getNextRunTime());
        } else {
            log.log(Level.WARNING, "createCronJob(): Arg cronJob is of an unknown type.");
            //bean = new CronJobBean();
            bean = new CronJobBean(cronJob.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getCronJobServiceProxy().createCronJob(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateCronJob(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        RestRequestStructBean restRequestBean = null;
        if(restRequest instanceof RestRequestStructBean) {
            restRequestBean = (RestRequestStructBean) restRequest;            
        } else if(restRequest instanceof RestRequestStruct) {
            restRequestBean = new RestRequestStructBean(restRequest.getServiceName(), restRequest.getServiceUrl(), restRequest.getRequestMethod(), restRequest.getRequestUrl(), restRequest.getTargetEntity(), restRequest.getQueryString(), restRequest.getQueryParams(), restRequest.getInputFormat(), restRequest.getInputContent(), restRequest.getOutputFormat(), restRequest.getMaxRetries(), restRequest.getRetryInterval(), restRequest.getNote());
        } else {
            restRequestBean = null;   // ????
        }
        NotificationStructBean notificationPrefBean = null;
        if(notificationPref instanceof NotificationStructBean) {
            notificationPrefBean = (NotificationStructBean) notificationPref;            
        } else if(notificationPref instanceof NotificationStruct) {
            notificationPrefBean = new NotificationStructBean(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getCallbackUrl(), notificationPref.getNote());
        } else {
            notificationPrefBean = null;   // ????
        }
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;            
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        CronScheduleStructBean cronScheduleBean = null;
        if(cronSchedule instanceof CronScheduleStructBean) {
            cronScheduleBean = (CronScheduleStructBean) cronSchedule;            
        } else if(cronSchedule instanceof CronScheduleStruct) {
            cronScheduleBean = new CronScheduleStructBean(cronSchedule.getType(), cronSchedule.getSchedule(), cronSchedule.getTimezone(), cronSchedule.getMaxIterations(), cronSchedule.getRepeatInterval(), cronSchedule.getFirtRunTime(), cronSchedule.getStartTime(), cronSchedule.getEndTime(), cronSchedule.getNote());
        } else {
            cronScheduleBean = null;   // ????
        }
        CronJobBean bean = new CronJobBean(guid, managerApp, appAcl, gaeAppBean, ownerUser, userAcl, user, jobId, title, description, originJob, restRequestBean, permalink, shortlink, status, jobStatus, extra, note, alert, notificationPrefBean, referrerInfoBean, cronScheduleBean, nextRunTime);
        return updateCronJob(bean);
    }
        
    @Override
    public Boolean updateCronJob(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //CronJob bean = refreshCronJob(cronJob);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param cronJob cannot be null.....
        if(cronJob == null || cronJob.getGuid() == null) {
            log.log(Level.INFO, "Param cronJob or its guid is null!");
            throw new BadRequestException("Param cronJob object or its guid is null!");
        }
        CronJobBean bean = null;
        if(cronJob instanceof CronJobBean) {
            bean = (CronJobBean) cronJob;
        } else if(cronJob instanceof CronJobBean) {
            bean = (CronJobBean) cronJob;
        } else {  // if(cronJob instanceof CronJob)
            bean = new CronJobBean(cronJob.getGuid(), cronJob.getManagerApp(), cronJob.getAppAcl(), (GaeAppStructBean) cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), (RestRequestStructBean) cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), (NotificationStructBean) cronJob.getNotificationPref(), (ReferrerInfoStructBean) cronJob.getReferrerInfo(), (CronScheduleStructBean) cronJob.getCronSchedule(), cronJob.getNextRunTime());
        }
        Boolean suc = getProxyFactory().getCronJobServiceProxy().updateCronJob(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }
        
    @Override
    public CronJob refreshCronJob(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN");

        // Param cronJob cannot be null.....
        if(cronJob == null || cronJob.getGuid() == null) {
            log.log(Level.INFO, "Param cronJob or its guid is null!");
            throw new BadRequestException("Param cronJob object or its guid is null!");
        }
        CronJobBean bean = null;
        if(cronJob instanceof CronJobBean) {
            bean = (CronJobBean) cronJob;
        } else if(cronJob instanceof CronJobBean) {
            bean = (CronJobBean) cronJob;
        } else {  // if(cronJob instanceof CronJob)
            bean = new CronJobBean(cronJob.getGuid(), cronJob.getManagerApp(), cronJob.getAppAcl(), (GaeAppStructBean) cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), (RestRequestStructBean) cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), (NotificationStructBean) cronJob.getNotificationPref(), (ReferrerInfoStructBean) cronJob.getReferrerInfo(), (CronScheduleStructBean) cronJob.getCronSchedule(), cronJob.getNextRunTime());
        }
        Boolean suc = getProxyFactory().getCronJobServiceProxy().updateCronJob(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }
        //if(suc == true)  // else ???

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteCronJob(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getProxyFactory().getCronJobServiceProxy().deleteCronJob(guid);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(guid);
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // ???
    @Override
    public Boolean deleteCronJob(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN");

        // Param cronJob cannot be null.....
        if(cronJob == null || cronJob.getGuid() == null) {
            log.log(Level.INFO, "Param cronJob or its guid is null!");
            throw new BadRequestException("Param cronJob object or its guid is null!");
        }
        CronJobBean bean = null;
        if(cronJob instanceof CronJobBean) {
            bean = (CronJobBean) cronJob;
        } else {  // if(cronJob instanceof CronJob)
            bean = new CronJobBean(cronJob.getGuid(), cronJob.getManagerApp(), cronJob.getAppAcl(), (GaeAppStructBean) cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), (RestRequestStructBean) cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), (NotificationStructBean) cronJob.getNotificationPref(), (ReferrerInfoStructBean) cronJob.getReferrerInfo(), (CronScheduleStructBean) cronJob.getCronSchedule(), cronJob.getNextRunTime());
        }
        Boolean suc = getProxyFactory().getCronJobServiceProxy().deleteCronJob(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteCronJobs(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getCronJobServiceProxy().deleteCronJobs(filter, params, values);
        if(mCache != null) {
            //if(count > 0) {
            //    mCache.remove(...);
            //}
        }
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createCronJobs(List<CronJob> cronJobs) throws BaseException
    {
        log.finer("BEGIN");

        if(cronJobs == null) {
            log.log(Level.WARNING, "createCronJobs() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = cronJobs.size();
        if(size == 0) {
            log.log(Level.WARNING, "createCronJobs() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(CronJob cronJob : cronJobs) {
            String guid = createCronJob(cronJob);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                log.log(Level.WARNING, "createCronJobs() failed for at least one cronJob. Index = " + count);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateCronJobs(List<CronJob> cronJobs) throws BaseException
    //{
    //}

}
