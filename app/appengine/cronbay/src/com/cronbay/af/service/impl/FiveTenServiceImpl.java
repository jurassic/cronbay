package com.cronbay.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.FiveTen;
import com.cronbay.af.bean.FiveTenBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.FiveTenService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FiveTenServiceImpl implements FiveTenService
{
    private static final Logger log = Logger.getLogger(FiveTenServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;
    
    public FiveTenServiceImpl()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            //props.put(GCacheFactory.EXPIRATION_DELTA, ServiceConstants.CACHE_EXPIRATION_DURATION);
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }


    //////////////////////////////////////////////////////////////////////////
    // FiveTen related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        log.fine("getFiveTen(): guid = " + guid);

        FiveTenBean bean = null;
        if(mCache != null) {
            bean = (FiveTenBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (FiveTenBean) getProxyFactory().getFiveTenServiceProxy().getFiveTen(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "FiveTenBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        FiveTenBean bean = null;
        if(mCache != null) {
            bean = (FiveTenBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (FiveTenBean) getProxyFactory().getFiveTenServiceProxy().getFiveTen(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "FiveTenBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("counter")) {
            return bean.getCounter();
        } else if(field.equals("requesterIpAddress")) {
            return bean.getRequesterIpAddress();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        log.fine("getFiveTens()");

        // TBD: Is there a better way????
        List<FiveTen> fiveTens = getProxyFactory().getFiveTenServiceProxy().getFiveTens(guids);
        if(fiveTens == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenBean list.");
        }

        log.finer("END");
        return fiveTens;
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return getAllFiveTens(null, null, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllFiveTens(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<FiveTen> fiveTens = getProxyFactory().getFiveTenServiceProxy().getAllFiveTens(ordering, offset, count);
        if(fiveTens == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenBean list.");
        }

        log.finer("END");
        return fiveTens;
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllFiveTenKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getFiveTenServiceProxy().getAllFiveTenKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("FiveTenServiceImpl.findFiveTens(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<FiveTen> fiveTens = getProxyFactory().getFiveTenServiceProxy().findFiveTens(filter, ordering, params, values, grouping, unique, offset, count);
        if(fiveTens == null) {
            log.log(Level.WARNING, "Failed to find fiveTens for the given criterion.");
        }

        log.finer("END");
        return fiveTens;
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("FiveTenServiceImpl.findFiveTenKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getFiveTenServiceProxy().findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find FiveTen keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("FiveTenServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getFiveTenServiceProxy().getCount(filter, params, values, aggregate);

        log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        FiveTenBean bean = new FiveTenBean(null, counter, requesterIpAddress);
        return createFiveTen(bean);
    }

    @Override
    public String createFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //FiveTen bean = constructFiveTen(fiveTen);
        //return bean.getGuid();

        // Param fiveTen cannot be null.....
        if(fiveTen == null) {
            log.log(Level.INFO, "Param fiveTen is null!");
            throw new BadRequestException("Param fiveTen object is null!");
        }
        FiveTenBean bean = null;
        if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else if(fiveTen instanceof FiveTen) {
            // bean = new FiveTenBean(null, fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new FiveTenBean(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        } else {
            log.log(Level.WARNING, "createFiveTen(): Arg fiveTen is of an unknown type.");
            //bean = new FiveTenBean();
            bean = new FiveTenBean(fiveTen.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getFiveTenServiceProxy().createFiveTen(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public FiveTen constructFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // Param fiveTen cannot be null.....
        if(fiveTen == null) {
            log.log(Level.INFO, "Param fiveTen is null!");
            throw new BadRequestException("Param fiveTen object is null!");
        }
        FiveTenBean bean = null;
        if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else if(fiveTen instanceof FiveTen) {
            // bean = new FiveTenBean(null, fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new FiveTenBean(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        } else {
            log.log(Level.WARNING, "createFiveTen(): Arg fiveTen is of an unknown type.");
            //bean = new FiveTenBean();
            bean = new FiveTenBean(fiveTen.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getFiveTenServiceProxy().createFiveTen(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        FiveTenBean bean = new FiveTenBean(guid, counter, requesterIpAddress);
        return updateFiveTen(bean);
    }
        
    @Override
    public Boolean updateFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //FiveTen bean = refreshFiveTen(fiveTen);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param fiveTen cannot be null.....
        if(fiveTen == null || fiveTen.getGuid() == null) {
            log.log(Level.INFO, "Param fiveTen or its guid is null!");
            throw new BadRequestException("Param fiveTen object or its guid is null!");
        }
        FiveTenBean bean = null;
        if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else {  // if(fiveTen instanceof FiveTen)
            bean = new FiveTenBean(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        }
        Boolean suc = getProxyFactory().getFiveTenServiceProxy().updateFiveTen(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }
        
    @Override
    public FiveTen refreshFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // Param fiveTen cannot be null.....
        if(fiveTen == null || fiveTen.getGuid() == null) {
            log.log(Level.INFO, "Param fiveTen or its guid is null!");
            throw new BadRequestException("Param fiveTen object or its guid is null!");
        }
        FiveTenBean bean = null;
        if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else {  // if(fiveTen instanceof FiveTen)
            bean = new FiveTenBean(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        }
        Boolean suc = getProxyFactory().getFiveTenServiceProxy().updateFiveTen(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }
        //if(suc == true)  // else ???

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getProxyFactory().getFiveTenServiceProxy().deleteFiveTen(guid);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(guid);
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // ???
    @Override
    public Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // Param fiveTen cannot be null.....
        if(fiveTen == null || fiveTen.getGuid() == null) {
            log.log(Level.INFO, "Param fiveTen or its guid is null!");
            throw new BadRequestException("Param fiveTen object or its guid is null!");
        }
        FiveTenBean bean = null;
        if(fiveTen instanceof FiveTenBean) {
            bean = (FiveTenBean) fiveTen;
        } else {  // if(fiveTen instanceof FiveTen)
            bean = new FiveTenBean(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        }
        Boolean suc = getProxyFactory().getFiveTenServiceProxy().deleteFiveTen(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getFiveTenServiceProxy().deleteFiveTens(filter, params, values);
        if(mCache != null) {
            //if(count > 0) {
            //    mCache.remove(...);
            //}
        }
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createFiveTens(List<FiveTen> fiveTens) throws BaseException
    {
        log.finer("BEGIN");

        if(fiveTens == null) {
            log.log(Level.WARNING, "createFiveTens() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = fiveTens.size();
        if(size == 0) {
            log.log(Level.WARNING, "createFiveTens() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(FiveTen fiveTen : fiveTens) {
            String guid = createFiveTen(fiveTen);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                log.log(Level.WARNING, "createFiveTens() failed for at least one fiveTen. Index = " + count);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateFiveTens(List<FiveTen> fiveTens) throws BaseException
    //{
    //}

}
