package com.cronbay.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.ws.User;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.af.bean.GaeUserStructBean;
import com.cronbay.af.bean.UserBean;
import com.cronbay.af.proxy.AbstractProxyFactory;
import com.cronbay.af.proxy.manager.ProxyFactoryManager;
import com.cronbay.af.service.ServiceConstants;
import com.cronbay.af.service.UserService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserServiceImpl implements UserService
{
    private static final Logger log = Logger.getLogger(UserServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;
    
    public UserServiceImpl()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            //props.put(GCacheFactory.EXPIRATION_DELTA, ServiceConstants.CACHE_EXPIRATION_DURATION);
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }


    //////////////////////////////////////////////////////////////////////////
    // User related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public User getUser(String guid) throws BaseException
    {
        log.fine("getUser(): guid = " + guid);

        UserBean bean = null;
        if(mCache != null) {
            bean = (UserBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (UserBean) getProxyFactory().getUserServiceProxy().getUser(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "UserBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve UserBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        UserBean bean = null;
        if(mCache != null) {
            bean = (UserBean) mCache.get(guid);
        }
        if(bean == null) {
            bean = (UserBean) getProxyFactory().getUserServiceProxy().getUser(guid);
            if(mCache != null && bean != null) {
                mCache.put(guid, bean);
            }
        } else {
            log.log(Level.INFO, "UserBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            log.log(Level.WARNING, "Failed to retrieve UserBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return bean.getManagerApp();
        } else if(field.equals("appAcl")) {
            return bean.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return bean.getGaeApp();
        } else if(field.equals("aeryId")) {
            return bean.getAeryId();
        } else if(field.equals("sessionId")) {
            return bean.getSessionId();
        } else if(field.equals("username")) {
            return bean.getUsername();
        } else if(field.equals("nickname")) {
            return bean.getNickname();
        } else if(field.equals("avatar")) {
            return bean.getAvatar();
        } else if(field.equals("email")) {
            return bean.getEmail();
        } else if(field.equals("openId")) {
            return bean.getOpenId();
        } else if(field.equals("gaeUser")) {
            return bean.getGaeUser();
        } else if(field.equals("timeZone")) {
            return bean.getTimeZone();
        } else if(field.equals("location")) {
            return bean.getLocation();
        } else if(field.equals("ipAddress")) {
            return bean.getIpAddress();
        } else if(field.equals("referer")) {
            return bean.getReferer();
        } else if(field.equals("obsolete")) {
            return bean.isObsolete();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("verifiedTime")) {
            return bean.getVerifiedTime();
        } else if(field.equals("authenticatedTime")) {
            return bean.getAuthenticatedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        log.fine("getUsers()");

        // TBD: Is there a better way????
        List<User> users = getProxyFactory().getUserServiceProxy().getUsers(guids);
        if(users == null) {
            log.log(Level.WARNING, "Failed to retrieve UserBean list.");
        }

        log.finer("END");
        return users;
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllUsers(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<User> users = getProxyFactory().getUserServiceProxy().getAllUsers(ordering, offset, count);
        if(users == null) {
            log.log(Level.WARNING, "Failed to retrieve UserBean list.");
        }

        log.finer("END");
        return users;
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.fine("getAllUserKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserServiceProxy().getAllUserKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve UserBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("UserServiceImpl.findUsers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<User> users = getProxyFactory().getUserServiceProxy().findUsers(filter, ordering, params, values, grouping, unique, offset, count);
        if(users == null) {
            log.log(Level.WARNING, "Failed to find users for the given criterion.");
        }

        log.finer("END");
        return users;
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("UserServiceImpl.findUserKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getUserServiceProxy().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find User keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("UserServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getUserServiceProxy().getCount(filter, params, values, aggregate);

        log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        GaeUserStructBean gaeUserBean = null;
        if(gaeUser instanceof GaeUserStructBean) {
            gaeUserBean = (GaeUserStructBean) gaeUser;
        } else if(gaeUser instanceof GaeUserStruct) {
            gaeUserBean = new GaeUserStructBean(gaeUser.getAuthDomain(), gaeUser.getFederatedIdentity(), gaeUser.getNickname(), gaeUser.getUserId(), gaeUser.getEmail(), gaeUser.getNote());
        } else {
            gaeUserBean = null;   // ????
        }
        UserBean bean = new UserBean(null, managerApp, appAcl, gaeAppBean, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUserBean, timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime);
        return createUser(bean);
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //User bean = constructUser(user);
        //return bean.getGuid();

        // Param user cannot be null.....
        if(user == null) {
            log.log(Level.INFO, "Param user is null!");
            throw new BadRequestException("Param user object is null!");
        }
        UserBean bean = null;
        if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else if(user instanceof User) {
            // bean = new UserBean(null, user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserBean(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
        } else {
            log.log(Level.WARNING, "createUser(): Arg user is of an unknown type.");
            //bean = new UserBean();
            bean = new UserBean(user.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserServiceProxy().createUser(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public User constructUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null) {
            log.log(Level.INFO, "Param user is null!");
            throw new BadRequestException("Param user object is null!");
        }
        UserBean bean = null;
        if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else if(user instanceof User) {
            // bean = new UserBean(null, user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new UserBean(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
        } else {
            log.log(Level.WARNING, "createUser(): Arg user is of an unknown type.");
            //bean = new UserBean();
            bean = new UserBean(user.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getUserServiceProxy().createUser(bean);
        if(mCache != null) {
            //if(guid != null && guid.length() > 0) {
            //    mCache.put(guid, bean);  // Note: bean.getCreatedTime() has changed.
            //}
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        GaeAppStructBean gaeAppBean = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppBean = (GaeAppStructBean) gaeApp;            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppBean = new GaeAppStructBean(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppBean = null;   // ????
        }
        GaeUserStructBean gaeUserBean = null;
        if(gaeUser instanceof GaeUserStructBean) {
            gaeUserBean = (GaeUserStructBean) gaeUser;            
        } else if(gaeUser instanceof GaeUserStruct) {
            gaeUserBean = new GaeUserStructBean(gaeUser.getAuthDomain(), gaeUser.getFederatedIdentity(), gaeUser.getNickname(), gaeUser.getUserId(), gaeUser.getEmail(), gaeUser.getNote());
        } else {
            gaeUserBean = null;   // ????
        }
        UserBean bean = new UserBean(guid, managerApp, appAcl, gaeAppBean, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUserBean, timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime);
        return updateUser(bean);
    }
        
    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //User bean = refreshUser(user);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param user cannot be null.....
        if(user == null || user.getGuid() == null) {
            log.log(Level.INFO, "Param user or its guid is null!");
            throw new BadRequestException("Param user object or its guid is null!");
        }
        UserBean bean = null;
        if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else {  // if(user instanceof User)
            bean = new UserBean(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
        }
        Boolean suc = getProxyFactory().getUserServiceProxy().updateUser(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }
        
    @Override
    public User refreshUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null || user.getGuid() == null) {
            log.log(Level.INFO, "Param user or its guid is null!");
            throw new BadRequestException("Param user object or its guid is null!");
        }
        UserBean bean = null;
        if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else {  // if(user instanceof User)
            bean = new UserBean(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
        }
        Boolean suc = getProxyFactory().getUserServiceProxy().updateUser(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }
        //if(suc == true)  // else ???

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getProxyFactory().getUserServiceProxy().deleteUser(guid);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(guid);
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // ???
    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null || user.getGuid() == null) {
            log.log(Level.INFO, "Param user or its guid is null!");
            throw new BadRequestException("Param user object or its guid is null!");
        }
        UserBean bean = null;
        if(user instanceof UserBean) {
            bean = (UserBean) user;
        } else {  // if(user instanceof User)
            bean = new UserBean(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructBean) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructBean) user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
        }
        Boolean suc = getProxyFactory().getUserServiceProxy().deleteUser(bean);
        if(mCache != null) {
            //if(suc == true) {
                mCache.remove(bean.getGuid());
            //}
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getUserServiceProxy().deleteUsers(filter, params, values);
        if(mCache != null) {
            //if(count > 0) {
            //    mCache.remove(...);
            //}
        }
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUsers(List<User> users) throws BaseException
    {
        log.finer("BEGIN");

        if(users == null) {
            log.log(Level.WARNING, "createUsers() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = users.size();
        if(size == 0) {
            log.log(Level.WARNING, "createUsers() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(User user : users) {
            String guid = createUser(user);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                log.log(Level.WARNING, "createUsers() failed for at least one user. Index = " + count);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateUsers(List<User> users) throws BaseException
    //{
    //}

}
