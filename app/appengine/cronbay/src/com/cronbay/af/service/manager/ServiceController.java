package com.cronbay.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.af.service.ApiConsumerService;
import com.cronbay.af.service.UserService;
import com.cronbay.af.service.CronJobService;
import com.cronbay.af.service.JobOutputService;
import com.cronbay.af.service.ServiceInfoService;
import com.cronbay.af.service.FiveTenService;

// TBD: DI
// (For now, this class is used to make ServiceManager "DI-capable".)
public class ServiceController
{
    private static final Logger log = Logger.getLogger(ServiceController.class.getName());

    // "Injectable" services.
    private ApiConsumerService apiConsumerService = null;
    private UserService userService = null;
    private CronJobService cronJobService = null;
    private JobOutputService jobOutputService = null;
    private ServiceInfoService serviceInfoService = null;
    private FiveTenService fiveTenService = null;

    // Ctor injection.
    public ServiceController(ApiConsumerService apiConsumerService, UserService userService, CronJobService cronJobService, JobOutputService jobOutputService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.userService = userService;
        this.cronJobService = cronJobService;
        this.jobOutputService = jobOutputService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }

    // Returns a ApiConsumerService instance.
	public ApiConsumerService getApiConsumerService() 
    {
        return this.apiConsumerService;
    }
    // Setter injection for ApiConsumerService.
	public void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        this.apiConsumerService = apiConsumerService;
    }

    // Returns a UserService instance.
	public UserService getUserService() 
    {
        return this.userService;
    }
    // Setter injection for UserService.
	public void setUserService(UserService userService) 
    {
        this.userService = userService;
    }

    // Returns a CronJobService instance.
	public CronJobService getCronJobService() 
    {
        return this.cronJobService;
    }
    // Setter injection for CronJobService.
	public void setCronJobService(CronJobService cronJobService) 
    {
        this.cronJobService = cronJobService;
    }

    // Returns a JobOutputService instance.
	public JobOutputService getJobOutputService() 
    {
        return this.jobOutputService;
    }
    // Setter injection for JobOutputService.
	public void setJobOutputService(JobOutputService jobOutputService) 
    {
        this.jobOutputService = jobOutputService;
    }

    // Returns a ServiceInfoService instance.
	public ServiceInfoService getServiceInfoService() 
    {
        return this.serviceInfoService;
    }
    // Setter injection for ServiceInfoService.
	public void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        this.serviceInfoService = serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public FiveTenService getFiveTenService() 
    {
        return this.fiveTenService;
    }
    // Setter injection for FiveTenService.
	public void setFiveTenService(FiveTenService fiveTenService) 
    {
        this.fiveTenService = fiveTenService;
    }

}
