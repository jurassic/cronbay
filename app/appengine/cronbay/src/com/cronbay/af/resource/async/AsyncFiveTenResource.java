package com.cronbay.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.exception.InternalServerErrorException;
import com.cronbay.ws.exception.NotImplementedException;
import com.cronbay.ws.exception.RequestConflictException;
import com.cronbay.ws.exception.RequestForbiddenException;
import com.cronbay.ws.exception.ResourceGoneException;
import com.cronbay.ws.exception.ResourceNotFoundException;
import com.cronbay.ws.exception.ServiceUnavailableException;
import com.cronbay.ws.exception.resource.BaseResourceException;
import com.cronbay.ws.resource.exception.BadRequestRsException;
import com.cronbay.ws.resource.exception.InternalServerErrorRsException;
import com.cronbay.ws.resource.exception.NotImplementedRsException;
import com.cronbay.ws.resource.exception.RequestConflictRsException;
import com.cronbay.ws.resource.exception.RequestForbiddenRsException;
import com.cronbay.ws.resource.exception.ResourceGoneRsException;
import com.cronbay.ws.resource.exception.ResourceNotFoundRsException;
import com.cronbay.ws.resource.exception.ServiceUnavailableRsException;

import com.cronbay.ws.FiveTen;
import com.cronbay.ws.stub.FiveTenStub;
import com.cronbay.ws.stub.FiveTenListStub;
import com.cronbay.af.bean.FiveTenBean;
import com.cronbay.af.proxy.FiveTenServiceProxy;
import com.cronbay.af.proxy.remote.RemoteProxyFactory;
import com.cronbay.af.proxy.remote.RemoteFiveTenServiceProxy;
import com.cronbay.af.resource.FiveTenResource;


@Path("/_task/r/fiveTens/")
public class AsyncFiveTenResource extends BaseAsyncResource implements FiveTenResource
{
    private static final Logger log = Logger.getLogger(AsyncFiveTenResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncFiveTenResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getFiveTenList(List<FiveTen> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllFiveTens(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findFiveTensAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getFiveTenAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getFiveTen(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getFiveTenAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getFiveTen(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructFiveTen(FiveTenStub fiveTen) throws BaseResourceException
    {
        log.log(Level.INFO, "constructFiveTen(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            FiveTenBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FiveTenStub realStub = (FiveTenStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertFiveTenStubToBean(realStub);
            } else {
                bean = convertFiveTenStubToBean(fiveTen);
            }
            //bean = (FiveTenBean) RemoteProxyFactory.getInstance().getFiveTenServiceProxy().constructFiveTen(bean);
            //fiveTen = FiveTenStub.convertBeanToStub(bean);
            //String guid = fiveTen.getGuid();
            // TBD: createFiveTen() or constructFiveTen()???  (constructFiveTen() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getFiveTenServiceProxy().createFiveTen(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            log.log(Level.INFO, "constructFiveTen(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(fiveTen).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createFiveTen(FiveTenStub fiveTen) throws BaseResourceException
    {
        log.log(Level.INFO, "createFiveTen(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            FiveTenBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FiveTenStub realStub = (FiveTenStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertFiveTenStubToBean(realStub);
            } else {
                bean = convertFiveTenStubToBean(fiveTen);
            }
            String guid = RemoteProxyFactory.getInstance().getFiveTenServiceProxy().createFiveTen(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            log.log(Level.INFO, "createFiveTen(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createFiveTen(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshFiveTen(String guid, FiveTenStub fiveTen) throws BaseResourceException
    {
        log.log(Level.INFO, "refreshFiveTen(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(fiveTen == null || !guid.equals(fiveTen.getGuid())) {
                log.log(Level.WARNING, "Path param guid = " + guid + " is different from fiveTen guid = " + fiveTen.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the fiveTen with guid = " + guid);
            }
            FiveTenBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FiveTenStub realStub = (FiveTenStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertFiveTenStubToBean(realStub);
            } else {
                bean = convertFiveTenStubToBean(fiveTen);
            }
            //bean = (FiveTenBean) RemoteProxyFactory.getInstance().getFiveTenServiceProxy().refreshFiveTen(bean);
            //if(bean == null) {
            //    log.log(Level.WARNING, "Failed to refresh the fiveTen with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the fiveTen with guid = " + guid);
            //}
            //fiveTen = FiveTenStub.convertBeanToStub(bean);
            // TBD: updateFiveTen() or refreshFiveTen()???  (refreshFiveTen() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getFiveTenServiceProxy().updateFiveTen(bean);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to refrefsh the fiveTen with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the fiveTen with guid = " + guid);
            }
            log.log(Level.INFO, "refreshFiveTen(): Successfully processed the request: guid = " + guid);
            return Response.ok(fiveTen).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateFiveTen(String guid, FiveTenStub fiveTen) throws BaseResourceException
    {
        log.log(Level.INFO, "updateFiveTen(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(fiveTen == null || !guid.equals(fiveTen.getGuid())) {
                log.log(Level.WARNING, "Path param guid = " + guid + " is different from fiveTen guid = " + fiveTen.getGuid());
                throw new RequestForbiddenRsException("Failed to update the fiveTen with guid = " + guid);
            }
            FiveTenBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FiveTenStub realStub = (FiveTenStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertFiveTenStubToBean(realStub);
            } else {
                bean = convertFiveTenStubToBean(fiveTen);
            }
            boolean suc = RemoteProxyFactory.getInstance().getFiveTenServiceProxy().updateFiveTen(bean);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to update the fiveTen with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the fiveTen with guid = " + guid);
            }
            log.log(Level.INFO, "updateFiveTen(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateFiveTen(String guid, Integer counter, String requesterIpAddress)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response updateFiveTen(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteFiveTen(String guid) throws BaseResourceException
    {
        log.log(Level.INFO, "deleteFiveTen(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getFiveTenServiceProxy().deleteFiveTen(guid);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to delete the fiveTen with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the fiveTen with guid = " + guid);
            }
            log.log(Level.INFO, "deleteFiveTen(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteFiveTens(String filter, String params, List<String> values) throws BaseResourceException
    {
        log.log(Level.INFO, "deleteFiveTens(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getFiveTenServiceProxy().deleteFiveTens(filter, params, values);
            log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createFiveTens(FiveTenListStub fiveTens) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        log.log(Level.INFO, "createFiveTens(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            FiveTenListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FiveTenListStub realStubs = (FiveTenListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = fiveTens;
            }

            List<FiveTenStub> stubList = fiveTens.getList();
            List<VisitorSetting> beans = new ArrayList<FiveTen>();
            for(FiveTenStub stub : stubList) {
                FiveTenBean bean = convertFiveTenStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getFiveTenServiceProxy().createFiveTens(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static FiveTenBean convertFiveTenStubToBean(FiveTen stub)
    {
        FiveTenBean bean = new FiveTenBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setGuid(stub.getGuid());
            bean.setCounter(stub.getCounter());
            bean.setRequesterIpAddress(stub.getRequesterIpAddress());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
