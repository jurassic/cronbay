package com.cronbay.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.exception.InternalServerErrorException;
import com.cronbay.ws.exception.NotImplementedException;
import com.cronbay.ws.exception.RequestConflictException;
import com.cronbay.ws.exception.RequestForbiddenException;
import com.cronbay.ws.exception.ResourceGoneException;
import com.cronbay.ws.exception.ResourceNotFoundException;
import com.cronbay.ws.exception.ServiceUnavailableException;
import com.cronbay.ws.exception.resource.BaseResourceException;
import com.cronbay.ws.resource.exception.BadRequestRsException;
import com.cronbay.ws.resource.exception.InternalServerErrorRsException;
import com.cronbay.ws.resource.exception.NotImplementedRsException;
import com.cronbay.ws.resource.exception.RequestConflictRsException;
import com.cronbay.ws.resource.exception.RequestForbiddenRsException;
import com.cronbay.ws.resource.exception.ResourceGoneRsException;
import com.cronbay.ws.resource.exception.ResourceNotFoundRsException;
import com.cronbay.ws.resource.exception.ServiceUnavailableRsException;

import com.cronbay.ws.ServiceInfo;
import com.cronbay.ws.stub.ServiceInfoStub;
import com.cronbay.ws.stub.ServiceInfoListStub;
import com.cronbay.af.bean.ServiceInfoBean;
import com.cronbay.af.proxy.ServiceInfoServiceProxy;
import com.cronbay.af.proxy.remote.RemoteProxyFactory;
import com.cronbay.af.proxy.remote.RemoteServiceInfoServiceProxy;
import com.cronbay.af.resource.ServiceInfoResource;


@Path("/_task/r/serviceInfos/")
public class AsyncServiceInfoResource extends BaseAsyncResource implements ServiceInfoResource
{
    private static final Logger log = Logger.getLogger(AsyncServiceInfoResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncServiceInfoResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getServiceInfoList(List<ServiceInfo> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllServiceInfos(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findServiceInfosAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getServiceInfoAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getServiceInfo(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getServiceInfoAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getServiceInfo(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructServiceInfo(ServiceInfoStub serviceInfo) throws BaseResourceException
    {
        log.log(Level.INFO, "constructServiceInfo(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            ServiceInfoBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ServiceInfoStub realStub = (ServiceInfoStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertServiceInfoStubToBean(realStub);
            } else {
                bean = convertServiceInfoStubToBean(serviceInfo);
            }
            //bean = (ServiceInfoBean) RemoteProxyFactory.getInstance().getServiceInfoServiceProxy().constructServiceInfo(bean);
            //serviceInfo = ServiceInfoStub.convertBeanToStub(bean);
            //String guid = serviceInfo.getGuid();
            // TBD: createServiceInfo() or constructServiceInfo()???  (constructServiceInfo() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getServiceInfoServiceProxy().createServiceInfo(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            log.log(Level.INFO, "constructServiceInfo(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(serviceInfo).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createServiceInfo(ServiceInfoStub serviceInfo) throws BaseResourceException
    {
        log.log(Level.INFO, "createServiceInfo(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            ServiceInfoBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ServiceInfoStub realStub = (ServiceInfoStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertServiceInfoStubToBean(realStub);
            } else {
                bean = convertServiceInfoStubToBean(serviceInfo);
            }
            String guid = RemoteProxyFactory.getInstance().getServiceInfoServiceProxy().createServiceInfo(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            log.log(Level.INFO, "createServiceInfo(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createServiceInfo(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshServiceInfo(String guid, ServiceInfoStub serviceInfo) throws BaseResourceException
    {
        log.log(Level.INFO, "refreshServiceInfo(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(serviceInfo == null || !guid.equals(serviceInfo.getGuid())) {
                log.log(Level.WARNING, "Path param guid = " + guid + " is different from serviceInfo guid = " + serviceInfo.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the serviceInfo with guid = " + guid);
            }
            ServiceInfoBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ServiceInfoStub realStub = (ServiceInfoStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertServiceInfoStubToBean(realStub);
            } else {
                bean = convertServiceInfoStubToBean(serviceInfo);
            }
            //bean = (ServiceInfoBean) RemoteProxyFactory.getInstance().getServiceInfoServiceProxy().refreshServiceInfo(bean);
            //if(bean == null) {
            //    log.log(Level.WARNING, "Failed to refresh the serviceInfo with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the serviceInfo with guid = " + guid);
            //}
            //serviceInfo = ServiceInfoStub.convertBeanToStub(bean);
            // TBD: updateServiceInfo() or refreshServiceInfo()???  (refreshServiceInfo() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getServiceInfoServiceProxy().updateServiceInfo(bean);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to refrefsh the serviceInfo with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the serviceInfo with guid = " + guid);
            }
            log.log(Level.INFO, "refreshServiceInfo(): Successfully processed the request: guid = " + guid);
            return Response.ok(serviceInfo).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateServiceInfo(String guid, ServiceInfoStub serviceInfo) throws BaseResourceException
    {
        log.log(Level.INFO, "updateServiceInfo(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(serviceInfo == null || !guid.equals(serviceInfo.getGuid())) {
                log.log(Level.WARNING, "Path param guid = " + guid + " is different from serviceInfo guid = " + serviceInfo.getGuid());
                throw new RequestForbiddenRsException("Failed to update the serviceInfo with guid = " + guid);
            }
            ServiceInfoBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ServiceInfoStub realStub = (ServiceInfoStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertServiceInfoStubToBean(realStub);
            } else {
                bean = convertServiceInfoStubToBean(serviceInfo);
            }
            boolean suc = RemoteProxyFactory.getInstance().getServiceInfoServiceProxy().updateServiceInfo(bean);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to update the serviceInfo with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the serviceInfo with guid = " + guid);
            }
            log.log(Level.INFO, "updateServiceInfo(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response updateServiceInfo(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteServiceInfo(String guid) throws BaseResourceException
    {
        log.log(Level.INFO, "deleteServiceInfo(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getServiceInfoServiceProxy().deleteServiceInfo(guid);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to delete the serviceInfo with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the serviceInfo with guid = " + guid);
            }
            log.log(Level.INFO, "deleteServiceInfo(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteServiceInfos(String filter, String params, List<String> values) throws BaseResourceException
    {
        log.log(Level.INFO, "deleteServiceInfos(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getServiceInfoServiceProxy().deleteServiceInfos(filter, params, values);
            log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createServiceInfos(ServiceInfoListStub serviceInfos) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        log.log(Level.INFO, "createServiceInfos(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            ServiceInfoListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ServiceInfoListStub realStubs = (ServiceInfoListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = serviceInfos;
            }

            List<ServiceInfoStub> stubList = serviceInfos.getList();
            List<VisitorSetting> beans = new ArrayList<ServiceInfo>();
            for(ServiceInfoStub stub : stubList) {
                ServiceInfoBean bean = convertServiceInfoStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getServiceInfoServiceProxy().createServiceInfos(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static ServiceInfoBean convertServiceInfoStubToBean(ServiceInfo stub)
    {
        ServiceInfoBean bean = new ServiceInfoBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setGuid(stub.getGuid());
            bean.setTitle(stub.getTitle());
            bean.setContent(stub.getContent());
            bean.setType(stub.getType());
            bean.setStatus(stub.getStatus());
            bean.setScheduledTime(stub.getScheduledTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
