package com.cronbay.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.PagerStateStruct;
import com.cronbay.ws.stub.PagerStateStructStub;
import com.cronbay.af.bean.PagerStateStructBean;


public class PagerStateStructResourceUtil
{
    private static final Logger log = Logger.getLogger(PagerStateStructResourceUtil.class.getName());

    // Static methods only.
    private PagerStateStructResourceUtil() {}

    public static PagerStateStructBean convertPagerStateStructStubToBean(PagerStateStruct stub)
    {
        PagerStateStructBean bean = new PagerStateStructBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setPagerMode(stub.getPagerMode());
            bean.setPrimaryOrdering(stub.getPrimaryOrdering());
            bean.setSecondaryOrdering(stub.getSecondaryOrdering());
            bean.setCurrentOffset(stub.getCurrentOffset());
            bean.setCurrentPage(stub.getCurrentPage());
            bean.setPageSize(stub.getPageSize());
            bean.setTotalCount(stub.getTotalCount());
            bean.setLowerBoundTotalCount(stub.getLowerBoundTotalCount());
            bean.setPreviousPageOffset(stub.getPreviousPageOffset());
            bean.setNextPageOffset(stub.getNextPageOffset());
            bean.setLastPageOffset(stub.getLastPageOffset());
            bean.setLastPageIndex(stub.getLastPageIndex());
            bean.setFirstActionEnabled(stub.isFirstActionEnabled());
            bean.setPreviousActionEnabled(stub.isPreviousActionEnabled());
            bean.setNextActionEnabled(stub.isNextActionEnabled());
            bean.setLastActionEnabled(stub.isLastActionEnabled());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
