package com.cronbay.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.stub.NotificationStructStub;
import com.cronbay.af.bean.NotificationStructBean;


public class NotificationStructResourceUtil
{
    private static final Logger log = Logger.getLogger(NotificationStructResourceUtil.class.getName());

    // Static methods only.
    private NotificationStructResourceUtil() {}

    public static NotificationStructBean convertNotificationStructStubToBean(NotificationStruct stub)
    {
        NotificationStructBean bean = new NotificationStructBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setPreferredMode(stub.getPreferredMode());
            bean.setMobileNumber(stub.getMobileNumber());
            bean.setEmailAddress(stub.getEmailAddress());
            bean.setCallbackUrl(stub.getCallbackUrl());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
