package com.cronbay.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.exception.InternalServerErrorException;
import com.cronbay.ws.exception.NotImplementedException;
import com.cronbay.ws.exception.RequestConflictException;
import com.cronbay.ws.exception.RequestForbiddenException;
import com.cronbay.ws.exception.ResourceGoneException;
import com.cronbay.ws.exception.ResourceNotFoundException;
import com.cronbay.ws.exception.ServiceUnavailableException;
import com.cronbay.ws.exception.resource.BaseResourceException;
import com.cronbay.ws.resource.exception.BadRequestRsException;
import com.cronbay.ws.resource.exception.InternalServerErrorRsException;
import com.cronbay.ws.resource.exception.NotImplementedRsException;
import com.cronbay.ws.resource.exception.RequestConflictRsException;
import com.cronbay.ws.resource.exception.RequestForbiddenRsException;
import com.cronbay.ws.resource.exception.ResourceGoneRsException;
import com.cronbay.ws.resource.exception.ResourceNotFoundRsException;
import com.cronbay.ws.resource.exception.ServiceUnavailableRsException;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.ws.User;
import com.cronbay.ws.stub.UserStub;
import com.cronbay.ws.stub.UserListStub;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.af.bean.GaeUserStructBean;
import com.cronbay.af.bean.UserBean;
import com.cronbay.af.proxy.UserServiceProxy;
import com.cronbay.af.proxy.remote.RemoteProxyFactory;
import com.cronbay.af.proxy.remote.RemoteUserServiceProxy;
import com.cronbay.af.resource.UserResource;
import com.cronbay.af.resource.util.GaeAppStructResourceUtil;
import com.cronbay.af.resource.util.GaeUserStructResourceUtil;


@Path("/_task/r/users/")
public class AsyncUserResource extends BaseAsyncResource implements UserResource
{
    private static final Logger log = Logger.getLogger(AsyncUserResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncUserResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getUserList(List<User> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllUsers(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findUsersAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getUserAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getUser(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getUserAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getUser(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructUser(UserStub user) throws BaseResourceException
    {
        log.log(Level.INFO, "constructUser(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            UserBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                UserStub realStub = (UserStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertUserStubToBean(realStub);
            } else {
                bean = convertUserStubToBean(user);
            }
            //bean = (UserBean) RemoteProxyFactory.getInstance().getUserServiceProxy().constructUser(bean);
            //user = UserStub.convertBeanToStub(bean);
            //String guid = user.getGuid();
            // TBD: createUser() or constructUser()???  (constructUser() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getUserServiceProxy().createUser(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            log.log(Level.INFO, "constructUser(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(user).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createUser(UserStub user) throws BaseResourceException
    {
        log.log(Level.INFO, "createUser(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            UserBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                UserStub realStub = (UserStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertUserStubToBean(realStub);
            } else {
                bean = convertUserStubToBean(user);
            }
            String guid = RemoteProxyFactory.getInstance().getUserServiceProxy().createUser(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            log.log(Level.INFO, "createUser(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createUser(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshUser(String guid, UserStub user) throws BaseResourceException
    {
        log.log(Level.INFO, "refreshUser(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(user == null || !guid.equals(user.getGuid())) {
                log.log(Level.WARNING, "Path param guid = " + guid + " is different from user guid = " + user.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the user with guid = " + guid);
            }
            UserBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                UserStub realStub = (UserStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertUserStubToBean(realStub);
            } else {
                bean = convertUserStubToBean(user);
            }
            //bean = (UserBean) RemoteProxyFactory.getInstance().getUserServiceProxy().refreshUser(bean);
            //if(bean == null) {
            //    log.log(Level.WARNING, "Failed to refresh the user with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the user with guid = " + guid);
            //}
            //user = UserStub.convertBeanToStub(bean);
            // TBD: updateUser() or refreshUser()???  (refreshUser() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getUserServiceProxy().updateUser(bean);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to refrefsh the user with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the user with guid = " + guid);
            }
            log.log(Level.INFO, "refreshUser(): Successfully processed the request: guid = " + guid);
            return Response.ok(user).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateUser(String guid, UserStub user) throws BaseResourceException
    {
        log.log(Level.INFO, "updateUser(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(user == null || !guid.equals(user.getGuid())) {
                log.log(Level.WARNING, "Path param guid = " + guid + " is different from user guid = " + user.getGuid());
                throw new RequestForbiddenRsException("Failed to update the user with guid = " + guid);
            }
            UserBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                UserStub realStub = (UserStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertUserStubToBean(realStub);
            } else {
                bean = convertUserStubToBean(user);
            }
            boolean suc = RemoteProxyFactory.getInstance().getUserServiceProxy().updateUser(bean);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to update the user with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the user with guid = " + guid);
            }
            log.log(Level.INFO, "updateUser(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateUser(String guid, String managerApp, Long appAcl, String gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, String gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response updateUser(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteUser(String guid) throws BaseResourceException
    {
        log.log(Level.INFO, "deleteUser(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getUserServiceProxy().deleteUser(guid);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to delete the user with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the user with guid = " + guid);
            }
            log.log(Level.INFO, "deleteUser(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteUsers(String filter, String params, List<String> values) throws BaseResourceException
    {
        log.log(Level.INFO, "deleteUsers(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getUserServiceProxy().deleteUsers(filter, params, values);
            log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createUsers(UserListStub users) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        log.log(Level.INFO, "createUsers(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            UserListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                UserListStub realStubs = (UserListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = users;
            }

            List<UserStub> stubList = users.getList();
            List<VisitorSetting> beans = new ArrayList<User>();
            for(UserStub stub : stubList) {
                UserBean bean = convertUserStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getUserServiceProxy().createUsers(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static UserBean convertUserStubToBean(User stub)
    {
        UserBean bean = new UserBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setGuid(stub.getGuid());
            bean.setManagerApp(stub.getManagerApp());
            bean.setAppAcl(stub.getAppAcl());
            bean.setGaeApp(GaeAppStructResourceUtil.convertGaeAppStructStubToBean(stub.getGaeApp()));
            bean.setAeryId(stub.getAeryId());
            bean.setSessionId(stub.getSessionId());
            bean.setUsername(stub.getUsername());
            bean.setNickname(stub.getNickname());
            bean.setAvatar(stub.getAvatar());
            bean.setEmail(stub.getEmail());
            bean.setOpenId(stub.getOpenId());
            bean.setGaeUser(GaeUserStructResourceUtil.convertGaeUserStructStubToBean(stub.getGaeUser()));
            bean.setTimeZone(stub.getTimeZone());
            bean.setLocation(stub.getLocation());
            bean.setIpAddress(stub.getIpAddress());
            bean.setReferer(stub.getReferer());
            bean.setObsolete(stub.isObsolete());
            bean.setStatus(stub.getStatus());
            bean.setVerifiedTime(stub.getVerifiedTime());
            bean.setAuthenticatedTime(stub.getAuthenticatedTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
