package com.cronbay.af.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.resource.BaseResourceException;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.stub.JobOutputStub;
import com.cronbay.ws.stub.JobOutputListStub;

public interface JobOutputResource
{

    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllJobOutputs(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findJobOutputs(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Produces({ "application/x-javascript" })
    Response findJobOutputsAsJsonp(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @DefaultValue("callback") @QueryParam("callback") String callback) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

//    @GET
//    @Path("{guid : [0-9a-fA-F\\-]+}")
//    @Produces({MediaType.TEXT_HTML})
//    Response getJobOutputAsHtml(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getJobOutput(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Produces({ "application/x-javascript" })
    Response getJobOutputAsJsonp(@PathParam("guid") String guid, @DefaultValue("callback") @QueryParam("callback") String callback) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Path("{guid : [0-9a-f\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getJobOutput(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response constructJobOutput(JobOutputStub jobOutput) throws BaseResourceException;

    @POST
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createJobOutput(JobOutputStub jobOutput) throws BaseResourceException;

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    Response createJobOutput(MultivaluedMap<String, String> formParams) throws BaseResourceException;

    @PUT
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response refreshJobOutput(@PathParam("guid") String guid, JobOutputStub jobOutput) throws BaseResourceException;

    @PUT
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    //@Produces({MediaType.TEXT_PLAIN})    // ??? updateJobOutput() returns 204 (No Content)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateJobOutput(@PathParam("guid") String guid, JobOutputStub jobOutput) throws BaseResourceException;

    //@PUT
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    //@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    //Response updateJobOutput(@PathParam("guid") String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateJobOutput(@PathParam("guid") String guid, @QueryParam("managerApp") String managerApp, @QueryParam("appAcl") Long appAcl, @QueryParam("gaeApp") String gaeApp, @QueryParam("ownerUser") String ownerUser, @QueryParam("userAcl") Long userAcl, @QueryParam("user") String user, @QueryParam("cronJob") String cronJob, @QueryParam("previousRun") String previousRun, @QueryParam("previousTry") String previousTry, @QueryParam("responseCode") String responseCode, @QueryParam("outputFormat") String outputFormat, @QueryParam("outputText") String outputText, @QueryParam("outputData") List<String> outputData, @QueryParam("outputHash") String outputHash, @QueryParam("permalink") String permalink, @QueryParam("shortlink") String shortlink, @QueryParam("result") String result, @QueryParam("status") String status, @QueryParam("extra") String extra, @QueryParam("note") String note, @QueryParam("retryCounter") Integer retryCounter, @QueryParam("scheduledTime") Long scheduledTime, @QueryParam("processedTime") Long processedTime) throws BaseResourceException;

    @POST
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    Response updateJobOutput(@PathParam("guid") String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException;

    @DELETE
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    Response deleteJobOutput(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deleteJobOutputs(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

    @POST
    @Path("bulk")
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createJobOutputs(JobOutputListStub jobOutputs) throws BaseResourceException;

//    @PUT
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response updateeJobOutputs(JobOutputListStub jobOutputs) throws BaseResourceException;

}
