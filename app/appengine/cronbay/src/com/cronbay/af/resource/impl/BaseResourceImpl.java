package com.cronbay.af.resource.impl;

import com.cronbay.af.config.Config;

// Place holder, for now.
public abstract class BaseResourceImpl
{
    // temporary
    private static final String CONFIG_KEY_IGNORE_AUTH = "cronbayapp.webservice.ignoreauth";
    private static final Boolean CONFIG_DEFAULT_IGNORE_AUTH = false;
    private static final String CONFIG_KEY_ANON_READ = "cronbayapp.webservice.anonread";
    private static final Boolean CONFIG_DEFAULT_ANON_READ = false;

    public BaseResourceImpl()
    {
    }

    // Note that this only works in the devel. environment.
    // You cannot bypass auth in the production environment.
    protected boolean isIgnoreAuth()
    {
        Boolean ignoreAuth = Config.getInstance().getBoolean(CONFIG_KEY_IGNORE_AUTH, CONFIG_DEFAULT_IGNORE_AUTH);
        return ignoreAuth;
    }

    // If true, clients can GET resources without being authenticated (even in prod env).
    protected boolean isAnonReadAllowed()
    {
        Boolean anonReadAllowed = Config.getInstance().getBoolean(CONFIG_KEY_ANON_READ, CONFIG_DEFAULT_ANON_READ);
        return anonReadAllowed;
    }

}
