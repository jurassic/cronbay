package com.cronbay.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.stub.GaeAppStructStub;
import com.cronbay.af.bean.GaeAppStructBean;


public class GaeAppStructResourceUtil
{
    private static final Logger log = Logger.getLogger(GaeAppStructResourceUtil.class.getName());

    // Static methods only.
    private GaeAppStructResourceUtil() {}

    public static GaeAppStructBean convertGaeAppStructStubToBean(GaeAppStruct stub)
    {
        GaeAppStructBean bean = new GaeAppStructBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setGroupId(stub.getGroupId());
            bean.setAppId(stub.getAppId());
            bean.setAppDomain(stub.getAppDomain());
            bean.setNamespace(stub.getNamespace());
            bean.setAcl(stub.getAcl());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
