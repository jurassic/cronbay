package com.cronbay.af.resource.registry;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;

import com.cronbay.cert.af.BaseOAuthConsumerRegistry;


// OAuth consumer key registry.
// To be used for AF service consumers.
public class OAuthConsumerRegistry extends BaseOAuthConsumerRegistry
{
    private static final Logger log = Logger.getLogger(OAuthConsumerRegistry.class.getName());

//    // Consumer key-secret map.
//    // TBD: This needs to be implemented in BaseOAuthConsumerRegistry
//    private Map<String, String> consumerSecretMap;
//
//    protected Map<String, String> getBaseConsumerSecretMap()
//    {
//        consumerSecretMap = new HashMap<String, String>();
//        return consumerSecretMap;
//    }
//    protected Map<String, String> getConsumerSecretMap()
//    {
//        return consumerSecretMap;
//    }

    private OAuthConsumerRegistry()
    {
        // TBD:  
        Map<String, String> consumerSecretMap = getBaseConsumerSecretMap();

        // Temporary. These are intended to be used for devel. purposes only.
        // Note: These key-secret pairs should be removed before the release!!!
        //consumerSecretMap.put("5b205188-c995-4c2e-af90-714bffc363e8", "2b2d0dbb-2f7e-430a-b403-9ad9e2707bb0"); 
        //consumerSecretMap.put("553b4c93-1536-4d67-a598-0cf3917488e6", "15691ede-d42e-4b5b-9488-d47545bbd73f"); 
        //consumerSecretMap.put("00b39b6e-c603-44af-b456-fe73d332dd1d", "4394c285-384f-4420-8069-511cea29da28"); 
        //consumerSecretMap.put("de6cfaed-9a45-4746-b44b-887bd621ec65", "9e812db0-4150-4a56-af38-237fd6fc2b9e"); 
        //consumerSecretMap.put("4bfe6fdc-a6ab-489b-ae9b-d260968a1b80", "9901e7d2-256c-4f35-be29-f892a9d7903d"); 

        // ...
        populateConsumerSecretMap();
    }

    // Initialization-on-demand holder.
    private static class OAuthConsumerRegistryHolder
    {
        private static final OAuthConsumerRegistry INSTANCE = new OAuthConsumerRegistry();
    }

    // Singleton method
    public static OAuthConsumerRegistry getInstance()
    {
        return OAuthConsumerRegistryHolder.INSTANCE;
    }

    // Initialize the consumerSecretMap.
    private void populateConsumerSecretMap()
    {
        // TBD:     }
        // Initialize consumerSecretMap from the data store
        // ...
    }

    // Refresh the consumerSecretMap, among other things.
    private void refreshRegistry()
    {
        // TBD:
        // Update consumerSecretMap from the data store
        // ...
    }


    public String getConsumerKey(String appId)
    {
        // TBD.
        return null;
    }

    public String getConsumerSecret(String consumerKey)
    {
        // TBD.
        String consumerSecret = getConsumerSecretMap().get(consumerKey);
        return consumerSecret;
    }

}
