package com.cronbay.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.ws.stub.GaeUserStructStub;
import com.cronbay.af.bean.GaeUserStructBean;


public class GaeUserStructResourceUtil
{
    private static final Logger log = Logger.getLogger(GaeUserStructResourceUtil.class.getName());

    // Static methods only.
    private GaeUserStructResourceUtil() {}

    public static GaeUserStructBean convertGaeUserStructStubToBean(GaeUserStruct stub)
    {
        GaeUserStructBean bean = new GaeUserStructBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setAuthDomain(stub.getAuthDomain());
            bean.setFederatedIdentity(stub.getFederatedIdentity());
            bean.setNickname(stub.getNickname());
            bean.setUserId(stub.getUserId());
            bean.setEmail(stub.getEmail());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
