package com.cronbay.af.fixture;

import java.util.logging.Logger;
import java.util.logging.Level;


public class FixtureFile
{
    private static final Logger log = Logger.getLogger(FixtureFile.class.getName());

    
    //private String name;  // ???
    private String mFilePath;
    private String mObjectType;  // Type class..
    private String mMediaType;   // XML or JSON..

    
    public FixtureFile(String filePath)
    {
        this(filePath, FixtureUtil.getObjectType(filePath));  // ???
    }
    
    public FixtureFile(String filePath, String objectType)
    {
        this(filePath, objectType, FixtureUtil.getMediaType(filePath));
    }

    public FixtureFile(String filePath, String objectType, String mediaType)
    {
        if(filePath == null || filePath.isEmpty()) {
            // ???
            filePath = FixtureUtil.getDefaultFixtureFile();
            log.info("filePath is null or empty. Using the default value: " + filePath);
        }
        this.mFilePath = filePath;
        this.mObjectType = objectType;
        this.mMediaType = mediaType;
    }

    public String getFilePath()
    {
        return mFilePath;
    }

    public void setFilePath(String filePath)
    {
        this.mFilePath = filePath;
    }

    public String getObjectType()
    {
        return mObjectType;
    }

    public void setObjectType(String objectType)
    {
        this.mObjectType = objectType;
    }

    public String getMediaType()
    {
        return mMediaType;
    }

    public void setMediaType(String mediaType)
    {
        this.mMediaType = mediaType;
    }

    
    @Override
    public String toString()
    {
        return "FixtureFile [filePath=" + mFilePath + ", objectType="
                + mObjectType + ", mediaType=" + mMediaType + "]";
    }
    
    
}
