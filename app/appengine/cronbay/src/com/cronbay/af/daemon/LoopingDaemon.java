package com.cronbay.af.daemon;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

public class LoopingDaemon
{
    private static final Logger log = Logger.getLogger(LoopingDaemon.class.getName());
    
    // TBD
    public static final String TASK_QUEUE_NAME = "looper";
    public static final String TASK_URIPATH_PREFIX = "/_daemon/";
    
    // TBD
    private static final String TRACK_DEFAULT = "default_track";


    // Max iterations per task...
    private static final int DEFAULT_MAX_LOOPING = 10000;   // ????? 

    // The task will be terminated after DEFAULT_TASK_DURATION milliseconds.
    private static final long DEFAULT_TASK_DURATION = 500 * 1000L;  // ???  8 mins 20 secs. 

    
    // Cache service
    private Cache mCache = null;
    
    // TBD:
    // "Track" is a set of looping tasks that perform the same Looplet....
    // A single LoopingDaemon should be able to support more than one track
    // because it is implemented as a singleton...
    // ...

    private int mMaxTracks = 1;

    // temporary
    private int mNumTasksPerTrack = 9;  // ???

    // max iternations per task...
    private int mTaskMaxLooping = DEFAULT_MAX_LOOPING;

    // Max duration of a looping task in MilliSeconds.
    private long mTaskMaxDuration = DEFAULT_TASK_DURATION;
    
    // TrackName -> LooperClass.
    private Map<String, String> mLoopletClasses = null;
    
//    // LoopingTask.
//    // TBD: Support multiple looping task types????
//    private LoopingTask mLoopingTask = null;
    
    // TrackName -> List<Task>.
    private Map<String, List<LoopingTask>> mLoopingTasks = null;
   
    
    
    // Singleton.
    private LoopingDaemon()
    {
        // TBD: Init.
        mLoopletClasses = new HashMap<String, String>();
        mLoopingTasks = new HashMap<String, List<LoopingTask>>();
        // ...
    }

    
    // Initialization-on-demand holder.
    private static class LoopingDaemonHolder
    {
        private static final LoopingDaemon INSTANCE = new LoopingDaemon();
    }

    // Singleton method
    public static LoopingDaemon getInstance()
    {
        return LoopingDaemonHolder.INSTANCE;
    }

    private void initCache()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            //props.put(GCacheFactory.EXPIRATION_DELTA, ServiceConstants.CACHE_EXPIRATION_DURATION);
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    
    private static String getDaemonTaskQueueName()
    {
        // TBD: Pick a random queue from a set of task queues (eg, ~10) 
        // (Queues should be pre-configured in queue.xml.)
        return TASK_QUEUE_NAME;
    }

    // Returns a relative URL....
    private static String getDaemonTaskUri()
    {
        return getDaemonTaskUri(null);
    }
    private static String getDaemonTaskUri(String track)
    {
        // TBD: Use task dependent task endpoint? 
        String url = TASK_URIPATH_PREFIX;
        if(track != null && !track.isEmpty()) {
            url += track;
        }
        return url;
    }

    public static String getTrackNameFromUrl(String url)
    {
        String track = null;

        int idx = url.indexOf(TASK_URIPATH_PREFIX);
        if(idx > 0) {
            track = url.substring(idx + TASK_URIPATH_PREFIX.length());
        } 
        
        return track;
    }


    public int getMaxTracks()
    {
        return mMaxTracks;
    }

    public void setMaxTracks(int maxTracks)
    {
        this.mMaxTracks = maxTracks;
    }

    public int getNumTasksPerTrack()
    {
        return mNumTasksPerTrack;
    }

    public void setNumTasksPerTrack(int numTasksPerTrack)
    {
        mNumTasksPerTrack = numTasksPerTrack;
    }

    
    public int getTaskMaxLooping()
    {
        return mTaskMaxLooping;
    }

    public void setTaskMaxLooping(int taskMaxLooping)
    {
        // TBD: Validation???
        this.mTaskMaxLooping = taskMaxLooping;
    }

    public long getTaskMaxDuration()
    {
        return mTaskMaxDuration;
    }

    public void setTaskMaxDuration(long maxDuration)
    {
        // TBD: maxDuration cannot be longer than 10 mins....
        this.mTaskMaxDuration = maxDuration;
    }

    
    // TBD
    public void setLoopletClass(String track, String className)
    {
        // temporary
        if(track != null) {
            mLoopletClasses.put(track, className);
        } else {
            mLoopletClasses.put(TRACK_DEFAULT, className);
        }
        // ...        
    }
    public String getLoopletClass(String track)
    {
        // temporary
        if(track != null) {
            return mLoopletClasses.get(track);
        } else {
            return mLoopletClasses.get(TRACK_DEFAULT);
        }
    }
    public void setDefaultTrackLoopletClass(String className)
    {
        setLoopletClass(TRACK_DEFAULT, className);
    }
    public String getDefaultTrackLoopletClass()
    {
        return getLoopletClass(TRACK_DEFAULT);
    }
    
    
//    public LoopingTask getLoopingTask()
//    {
//        return mLoopingTask;
//    }
//    public void setLoopingTask(LoopingTask loopingTask)
//    {
//        this.mLoopingTask = loopingTask;
//    }


    private Loopable getLoopletObject(String track)
    {
        String loopletClassName = getLoopletClass(track);

        Loopable looplet =  null;
        if(loopletClassName != null) {

            ClassLoader loader = ClassLoader.getSystemClassLoader();
            try {
                Class loopletClass = loader.loadClass(loopletClassName);
                looplet = (Loopable) loopletClass.newInstance();
                // ....
                
            } catch (ClassNotFoundException e) {
                // What to do???
                log.log(Level.SEVERE, "Failed to load a default looplet class.", e);
                // ....
            } catch (IllegalAccessException e) {
                // What to do???
                log.log(Level.SEVERE, "Failed to access a looplet class.", e);
                // ....
            } catch (InstantiationException e) {
                // What to do???
                log.log(Level.SEVERE, "Failed to instantiate a looplet object.", e);
                // ....
            }

        } else {
            // ????
            log.log(Level.SEVERE, "Looplet class not found for track " + track);            
        }
        
        return looplet;
    }
    
    
    // To be called from DaemonTaskServlet when (and only when) it is invoked by TaskQueue....
    public void performTask(String track)
    {
        log.fine("performTask() called with track = " + track);

        // TBD....
        Loopable looplet = getLoopletObject(track);
        if(looplet != null) {
        
            try {
                // TBD: ...
                LoopingTask task = new LoopingTask(looplet, track);
                task.setMaxDuration(getTaskMaxDuration());
                // ...
    
                // 
                int cnt = task.doLoop();
                log.info("Task completed: count = " + cnt);
                // ...
            } catch(Exception e) {
                // ignore all
                log.log(Level.WARNING, "", e);
            } finally {
                // ????
                tickle(track);
            }     

        } else {
            // TBD
            // what to do?????
            log.log(Level.SEVERE, "performTask() failed becase it could not instantiate a looplet for track " + track);
            // ....
        }
    }

    
    public void startTask()
    {
        startTask(null);   // null == "default" track..
    }
    public void startTask(String track)
    {
        if(track == null || track.isEmpty()) {
            track = TRACK_DEFAULT;
        }
        
        // TBD: taskDurtion x numTasks should be bigger than 10 mins !!!!!!
        int numTasks = getNumTasksPerTrack();
        long taskDuration = getTaskMaxDuration();  // ????
        
        long delta = (long) (600 * 1000L / numTasks);
        
        for(int i = 0; i<numTasks; i++) {
            long delay = delta * i;
            addTask(track, delay);
        }
    }
    
    public void tickle()
    {
        tickle(null);   // null == "default" track..
    }
    public void tickle(String track)
    {
        if(track == null || track.isEmpty()) {
            track = TRACK_DEFAULT;
        }
    
        // TBD
        // check the task queue first
        // and resubmit a task, if necessary, at an appropriate time...
        // ....
        
        
        // temporary
        addTask(track);
        // ...
    }
    
    
    private String getTaskName(String track)
    {
//        String loopletClassName = getLoopletClass(track);
//        if(loopletClassName == null) {
//            // something's wrong...
//            // What to do???
//            log.severe("Failed to add task for track = " + track);
//        }
        
//        String taskPrefix = loopletClassName;
        String taskPrefix = "Daemon";

        String taskName = taskPrefix + "-" + track + "-" + (int) ((new Date()).getTime() / 1000);
        return taskName;
    }
    
    
    private void addTask(String track)
    {
        addTask(track, 0L);
    }
    // delay in seconds...
    private void addTask(String track, long delay)
    {
        Queue q = QueueFactory.getQueue(getDaemonTaskQueueName());
        String taskName = getTaskName(track);
        TaskOptions options = TaskOptions.Builder
            .withUrl(getDaemonTaskUri(track))
            .method(Method.POST)
            .taskName(taskName)
            .countdownMillis(delay);
        q.add(options);        
    }

    
}
