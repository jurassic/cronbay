package com.cronbay.af.daemon;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cronbay.ws.core.StatusCode;

// TBD
@SuppressWarnings("serial")
public class DaemonTaskServlet extends HttpServlet
{
    private static final Logger log = Logger.getLogger(DaemonTaskServlet.class.getName());

    // {DYNAMIC}
    private static final String INI_PARAM_MAX_TRACKS = "com.cronbay.af.daemon.max_tracks";
    private static final String INI_PARAM_NUM_TASKS_PER_TRACKS = "com.cronbay.af.daemon.num_tasks";
    private static final String INI_PARAM_MAX_LOOPING = "com.cronbay.af.daemon.max_looping";
    private static final String INI_PARAM_TASK_DURATION = "com.cronbay.af.daemon.task_duration";
    private static final String INI_PARAM_LOOPLET_CLASS = "com.cronbay.af.daemon.looplet_class";
    private static final String INI_PARAM_LOOPLET_CLASS_01 = "com.cronbay.af.daemon.looplet_class_01";
    private static final String INI_PARAM_LOOPLET_CLASS_02 = "com.cronbay.af.daemon.looplet_class_02";
    private static final String INI_PARAM_LOOPLET_CLASS_03 = "com.cronbay.af.daemon.looplet_class_03";
    // ...
    

    @Override
    public void init() throws ServletException
    {
        log.fine("DaemonTaskServlet.init() called");
        super.init();
        initialize();
    }


    @Override
    public void init(ServletConfig config) throws ServletException
    {
        log.fine("DaemonTaskServlet.init(config) called");
        super.init(config);
        initialize();
    }
    
    
    private void initialize()
    {
        // Just to initialize LoopingDaemon...
        LoopingDaemon.getInstance();
        
        try {
            int maxTracks = Integer.parseInt(getInitParameter(INI_PARAM_MAX_TRACKS));
            log.info("maxTracks = " + maxTracks);
            LoopingDaemon.getInstance().setMaxTracks(maxTracks);
            // ....
        } catch(NumberFormatException e) {
            // ignore....
            log.log(Level.WARNING, "Failed to read maxTracks ini param.", e);
        }
        
        try {
            int numTasks = Integer.parseInt(getInitParameter(INI_PARAM_NUM_TASKS_PER_TRACKS));
            log.info("numTasks = " + numTasks);
            LoopingDaemon.getInstance().setNumTasksPerTrack(numTasks);
            // ....
        } catch(NumberFormatException e) {
            // ignore....
            log.log(Level.WARNING, "Failed to read numTasks ini param.", e);
        }
        
        try {
            int maxLooping = Integer.parseInt(getInitParameter(INI_PARAM_MAX_LOOPING));
            log.info("maxLooping = " + maxLooping);
            LoopingDaemon.getInstance().setTaskMaxLooping(maxLooping);
            // ....
        } catch(NumberFormatException e) {
            // ignore....
            log.log(Level.WARNING, "Failed to read maxLooping ini param.", e);
        }
        
        try {
            long taskDuration = Long.parseLong(getInitParameter(INI_PARAM_TASK_DURATION));
            log.info("taskDuration = " + taskDuration);
            LoopingDaemon.getInstance().setTaskMaxDuration(taskDuration);
            // ....
        } catch(NumberFormatException e) {
            // ignore....
            log.log(Level.WARNING, "Failed to read taskDuration ini param.", e);
        }
        
        // TBD: Support multiple tracks????
        String loopletClass = getInitParameter(INI_PARAM_LOOPLET_CLASS);
        log.info("loopletClass = " + loopletClass);
        // ....
        LoopingDaemon.getInstance().setDefaultTrackLoopletClass(loopletClass);
        // ...
        
        // Enqueue the first loop task.... 
        LoopingDaemon.getInstance().startTask();
    }
    
    @Override
    public void destroy()
    {
        log.fine("DaemonTaskServlet.destroy() called");
        super.destroy();
    }


    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException
    {
        log.fine("DaemonTaskServlet.doGet() called");
        resp.setContentType("text/plain");
        resp.getWriter().println("DaemonTaskServlet is to be called by TaskQueue through POST only.");
    }

    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.fine("DaemonTaskServlet.doPost() called");
        super.doPost(req, resp);
        // ...
        
        String uri = req.getRequestURI();
        String track = LoopingDaemon.getTrackNameFromUrl(uri);
        
        // Run the looping task....
        LoopingDaemon.getInstance().performTask(track);

        // TBD:
        // Always return OK??? Or, use ACCEPTED???? 
        resp.setStatus(StatusCode.OK);
        
    }

    
}

