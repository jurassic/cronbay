package com.cronbay.af.daemon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.core.StatusCode;


// Place holder for now.
public class TaskQueueUtil
{
    private static final Logger log = Logger.getLogger(TaskQueueUtil.class.getName());

    
    // TBD: the appname (gae project name) should be moved to config.
    // {DYNAMIC}
    private static final String TASKQUEUE_WEBSERVICE_ENDPOINT = "https://www.googleapis.com/taskqueue/v1beta1/projects/cronbay4/taskqueues/looper/tasks";

    // TBD
    private TaskQueueUtil() {}

    // temporary
    private String getWebServiceEndpoint()
    {
        return TASKQUEUE_WEBSERVICE_ENDPOINT;
    }

    
    public int getTaskCount()
    {
        // TBD:
        return 0;
    }
    
    public int getTaskCount(String track)
    {
        // TBD:
        return 0;
    }
    

    public long getLastEnqueTime()
    {
        // TBD:
        return 0L;
    }

    public long getLastEnqueTime(String track)
    {
        // TBD:
        return 0L;
    }
    
    
    public String getTasks()
    {
        try {
            URL url = new URL(getWebServiceEndpoint());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            int statusCode = connection.getResponseCode();
            log.info("statusCode = " + statusCode);
            
            // TBD:
            if(statusCode == StatusCode.OK) {

                
                BufferedReader in = null;
                try {
                    InputStream is = connection.getInputStream();
                    in = new BufferedReader(new InputStreamReader(is));
                    StringBuffer sb = new StringBuffer();
                    String line = null;
                    String NL = System.getProperty("line.separator");
                    while ((line = in.readLine()) != null) {
                        sb.append(line + NL);
                    }
                    in.close();
                    String page = sb.toString();
                    
                    log.info("page = " + page);
                    
                    // TBD ....
                    
                    
                    // temporary
                    return page;
                } finally {
                    if(in != null) {
                        try {
                            in.close();
                        } catch(Exception ex) {
                            // ???
                            log.log(Level.INFO, "", ex);
                        }
                    }
                }

            } else if(statusCode == StatusCode.ACCEPTED) {
                
                // TBD:
                // What to do???
                
            } else {
                
                // Error????                
                
            }
            
        } catch (MalformedURLException e) {
            // What to do??
            log.log(Level.WARNING, "MalformedURLException", e);
            // ....
        } catch (IOException e) {
            // What to do??
            log.log(Level.WARNING, "IOException", e);
            // ....
        } catch (IllegalStateException e) {
            // What to do??
            log.log(Level.WARNING, "IllegalStateException", e);
            // ....
        }
        
        // TBD:
        return "";
    }

    public String getTasks(String track)
    {
        // TBD:
        return "";
    }
    

}

