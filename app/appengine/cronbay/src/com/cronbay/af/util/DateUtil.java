package com.cronbay.af.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Logger;
import java.util.logging.Level;


// Temporary
public final class DateUtil
{
    private static final Logger log = Logger.getLogger(DateUtil.class.getName());

    private DateUtil() {}
    
    
    // Format the timestamp to a datetime format.

    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        //SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss z");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z");
        //TimeZone timeZone = TimeZone.getDefault();
        ////TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
        //dateFormat.setTimeZone(timeZone);
        String date = dateFormat.format(new Date(time));
        return date;
    }
    public static String formatDate(Long time, String timeZone)
    {
        if(time == null) {
            return null;  // ???
        }
        //SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a z");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        dateFormat.setTimeZone(tz);
        String date = dateFormat.format(new Date(time));
        return date;
    }
    public static String formatDateNoSeconds(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm z");
        String date = dateFormat.format(new Date(time));
        return date;
    }
    public static String formatDateNoSeconds(Long time, String timeZone)
    {
        if(time == null) {
            return null;  // ???
        }
        //SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm z");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a z");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        dateFormat.setTimeZone(tz);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static Long parseDate(String strDate)
    {
        if(strDate == null) {
            return null;  // ???
        }
        Long t = null;
        try {
            //SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss z");
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z");
            //TimeZone timeZone = TimeZone.getDefault();
            ////TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
            //dateFormat.setTimeZone(timeZone);
            Date date = dateFormat.parse(strDate);
            t = date.getTime();
        } catch (ParseException e1) {
            log.log(Level.WARNING, "Failed to parse strDate: " + strDate, e1);
        } catch (Exception e2) {
            log.log(Level.WARNING, "Failed to parse Date: strDate = " + strDate, e2);
        }
        return t;
    }
    public static Long parseDate(String strDate, String timeZone)
    {
        if(strDate == null) {
            return null;  // ???
        }
        Long t = null;
        try {
            //SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm z");
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a z");
            TimeZone tz = TimeZone.getTimeZone(timeZone);
            dateFormat.setTimeZone(tz);
            Date date = dateFormat.parse(strDate);
            t = date.getTime();
        } catch (ParseException e1) {
            log.log(Level.WARNING, "Failed to parse strDate: " + strDate, e1);
        } catch (Exception e2) {
            log.log(Level.WARNING, "Failed to parse Date: strDate = " + strDate, e2);
        }
        return t;
    }

}
