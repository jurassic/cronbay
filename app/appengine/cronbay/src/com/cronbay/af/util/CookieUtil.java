package com.cronbay.af.util;

import java.util.logging.Logger;
import java.util.logging.Level;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;


public class CookieUtil
{
    private static final Logger log = Logger.getLogger(CookieUtil.class.getName());

    private CookieUtil() {}

    
    public static Cookie findCookie(HttpServletRequest request, String name)
    {
        if(name == null || name.isEmpty()) {
            return null;
        }
        Cookie myCookie = null;
        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            for(Cookie c : cookies) {
                if(c.getName().equals(name)) {
                    myCookie = c;
                    break;
                }
            }
        }
        return myCookie;
    }

}
