package com.cronbay.af.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;


public class StringUtil
{
    private static final Logger log = Logger.getLogger(StringUtil.class.getName());

    // Static methods only.
    private StringUtil() {}

    // Returns the concatenated string of the given list.
    public static String join(List<String> list, String separator)
    {
        if(list == null) {
            return null;
        }
        if(list.isEmpty()) {
            return "";
        }
        if(separator == null) {  // ???
            separator = "";
        }

        StringBuilder sb = new StringBuilder();
        Iterator<String> it = list.iterator();
        while(it.hasNext()) {
            sb.append(it.next());
            if(it.hasNext()) {
                sb.append(separator);
            }
        }

        return sb.toString();
    }

    public static String join(String s1, String s2, String separator)
    {
        List<String> list = new ArrayList<String>();
        list.add(s1);
        list.add(s2);
        return join(list, separator);
    }

    public static String join(String s1, String s2, String s3, String separator)
    {
        List<String> list = new ArrayList<String>();
        list.add(s1);
        list.add(s2);
        list.add(s3);
        return join(list, separator);
    }


    public static boolean areEqual(String s1, String s2)
    {
        if(s1 == null && s2 == null) {
            return true;  // ????
        } else if(s1 == null && s2 != null) {
            return false;
        // } else if(s1 != null && s2 == null) {
        //     return false;
        } else {
            return s1.equals(s2);
        }
    }

//    public static boolean areDifferent(String s1, String s2)
//    {
//        if(s1 == null && s2 == null) {
//            return false;  // ????
//        } else if(s1 == null && s2 != null) {
//            return true;
//        // } else if(s1 != null && s2 == null) {
//        //     return true;
//        } else {
//            return !s1.equals(s2);
//        }
//    }


    
    public static String readInputStream(InputStream is) throws IOException
    {
        return readInputStream(is, "UTF-8");
    }

    public static String readInputStream(InputStream is, String encoding) throws IOException
    {
        final char[] buffer = new char[0x1000];
        StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(is, encoding);
        int read;
        do {
            read = in.read(buffer, 0, buffer.length);
   	        if (read>0) {
                out.append(buffer, 0, read);
            }
        } while (read>=0);
        String result = out.toString();
        return result;
    }

}
