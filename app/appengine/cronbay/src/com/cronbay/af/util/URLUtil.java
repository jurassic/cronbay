package com.cronbay.af.util;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


// Note: ServletRequest.getParameterMap returns Map<String, String[]>.
//       On the other hand, we use params arg, which is Map<String, Object>
//       where the value Object can be either String or List<String>...
// TBD: Allow other value types, such as Integer, etc. ???
public final class URLUtil
{
    private static final Logger log = Logger.getLogger(URLUtil.class.getName());

    private URLUtil() {}
    

    // TBD: Is there a better way????
    public static boolean isValidUrl(String str)
    {
        try {
            URL url = new URL(str);
            return true;
        } catch (MalformedURLException e) {
            log.log(Level.INFO, "Invalid url: " + str, e);
        }
        return false;
    }

    // TBD: Is this really necessary?
    public static Map<String, Object> parseParamMap(Map<String,String[]> paramMap)
    {
    	Map<String, Object> params = new HashMap<String,Object>();
    	if(paramMap != null) {
    		for(String k : paramMap.keySet()) {
    			String[] v = paramMap.get(k);
    			if(v == null) {
    				params.put(k, null);
    			} else if(v.length == 0) {
    				params.put(k,  "");
    			} else if(v.length == 1) {
    				params.put(k, v[0]);
    			} else {  // if(v.length > 1) {
    				List<String> list = Arrays.asList(v);
    				params.put(k, list);
    			}    		
    		}
    	}
    	return params;
    }

    // Temporary implementation
    // baseUrl may already include query string...
    public static String buildUrl(String baseUrl, Map<String, Object> params)
    {
        if(baseUrl == null) {
            log.info("baseUrl is null. Using an empty base.");
            baseUrl = "";  // ???
        }
        if(params != null && !params.isEmpty()) {            
            StringBuilder sb = new StringBuilder();
            if(baseUrl.contains("?")) {
                sb.append("&");
            } else {
                sb.append("?");
            }            
            for(String key : params.keySet()) {
                String encodedKey = null;
                try {
                    encodedKey = URLEncoder.encode(key, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    log.log(Level.WARNING, "Failed to URL encode the param key = " + key, e);
                }
                if(encodedKey != null) {
                    sb.append(encodedKey);
                    Object val = params.get(key);
                    if(val != null) {
                        String encodedVal = null;
                        if(val instanceof String) {
                            String strVal = (String) val;
                            try {
                                encodedVal = URLEncoder.encode(strVal, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                log.log(Level.WARNING, "Failed to URL encode the param value = " + strVal, e);
                            } 
                        } else if(val instanceof List) {
                            @SuppressWarnings("unchecked")
                            List<String> listVal = (List<String>) val;
                            if(listVal != null && !listVal.isEmpty()) {
                                //encodedVal = StringUtil.join(listVal, ",");
                                List<String> encodedList = new ArrayList<String>();
                                for(String v : listVal) {                                    
                                    try {
                                        encodedList.add(URLEncoder.encode(v, "UTF-8"));
                                    } catch (UnsupportedEncodingException e) {
                                        log.log(Level.WARNING, "Failed to URL encode the param value = " + v, e);
                                    }
                                }
                                encodedVal = StringUtil.join(encodedList, ",");
                            }
                        } else {
                            // ????
                            log.warning("Invalid params: val = " + val);
                        }
                        if(encodedVal != null) {
                            sb.append("=");
                            sb.append(encodedVal);
                        }
                    }
                    sb.append("&");
                }
            }
            String queryStr = sb.toString();
            if(queryStr.endsWith("&")) {
                queryStr = queryStr.substring(0, queryStr.length()-1);
            }
            baseUrl += queryStr;
        }
        return baseUrl;
    }


    // Note: Read the implementation.
    public static Map<String,Object> addQueryParam(Map<String,Object> params, String key, Object value)
    {
    	return addQueryParam(params, key, value, true);
    }
    @SuppressWarnings("unchecked")
    public static Map<String,Object> addQueryParam(Map<String,Object> params, String key, Object value, boolean clone)
    {
    	if(params == null) {
    		params = new HashMap<String,Object>();
        	params.put(key, value);
    	} else {
    		if(clone) {
    			params = new HashMap<String,Object>(params);
    		}
	    	if(params.containsKey(key)) {
	    		Object v = params.get(key);
	    		List<String> list = null;
	    		if(v instanceof String) {
	    			list = new ArrayList<String>();
	    		} else if(v instanceof List) {
	    			list = (List<String>) v;
	    		} else {
	                log.log(Level.WARNING, "Param value is of an unsupported type: " + v);
	                // TBD: Just ignore and continue... ???
	    			list = new ArrayList<String>();    			
	    		}
	    		if(value == null) {
	    			// ???
					list.add(null);
	    		} else if(value instanceof String) {
					list.add((String) value);
	    		} else if(value instanceof Short || value instanceof Integer || value instanceof Long) {
	    			list.add(value.toString());
	    		} else if(value instanceof List) {  // List<String>
					list.addAll((List<String>) value);
	    		} else {
	    			list.add(value.toString());  // ???
//	    			try {
//	    				list.add((String) value);
//	    			} catch(ClassCastException e) {
//	    				// ignore
//	    				log.log(Level.FINE, "The arg value cannot be converted to String.", e);
//	    			}
	    		}
				params.put(key, list);
	    	} else {
	    		// value could be String or List<String>
	    		// TBD: Data type validation?
	        	params.put(key, value);
	    	}
    	}
    	return params;
    }

    public static Map<String,Object> replaceQueryParam(Map<String,Object> params, String key, Object value)
    {
    	return replaceQueryParam(params, key, value, true);
    }
    public static Map<String,Object> replaceQueryParam(Map<String,Object> params, String key, Object value, boolean clone)
    {
    	if(params == null) {
    		params = new HashMap<String,Object>();
    	} else {
    		if(clone) {
    			params = new HashMap<String,Object>(params);
    		}    		
    	}
		if(value == null) {
			// ???
	    	params.put(key, null);
		} else if(value instanceof String) {
	    	params.put(key, value);
		} else if(value instanceof Short || value instanceof Integer || value instanceof Long) {
	    	params.put(key, value.toString());
		} else if(value instanceof List) {  // List<String>
	    	params.put(key, value);
		} else {
	    	params.put(key, value.toString());  // ???
		}
    	return params;
    }

    public static Map<String,Object> removeQueryParam(Map<String,Object> params, String key)
    {
    	return removeQueryParam(params, key, true);
    }
    public static Map<String,Object> removeQueryParam(Map<String,Object> params, String key, boolean clone)
    {
    	if(params == null) {
    		params = new HashMap<String,Object>();
    	} else {
    		if(clone) {
    			params = new HashMap<String,Object>(params);
    		}
    		params.remove(key);
    	}
    	return params;
    }

}
