package com.cronbay.af.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;


public class HttpContentUtil
{
    private static final Logger log = Logger.getLogger(HttpContentUtil.class.getName());

    private HttpContentUtil() {}


    public static String getStringContent(String strUrl)
    {
        String content = null;
        try {
            URL url = new URL(strUrl);
            content = getStringContent(url);
        } catch (MalformedURLException e) {
            log.log(Level.WARNING, "Invalid URL.", e);
        }
        return content;
    }

    public static String getStringContent(URL url)
    {
        String content = null;
        if(url != null) {
            try {
                content = (String) url.getContent();
            } catch (IOException e) {
                log.log(Level.WARNING, "Failed to read the content.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Unknown error.", e);
            }
        }
        return content;
    }

}
