package com.cronbay.af.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.PagerStateStruct;
import com.cronbay.ws.ApiConsumer;
import com.cronbay.ws.User;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.ServiceInfo;
import com.cronbay.ws.FiveTen;
import com.cronbay.ws.stub.ReferrerInfoStructStub;
import com.cronbay.ws.stub.ReferrerInfoStructListStub;
import com.cronbay.ws.stub.GaeAppStructStub;
import com.cronbay.ws.stub.GaeAppStructListStub;
import com.cronbay.ws.stub.GaeUserStructStub;
import com.cronbay.ws.stub.GaeUserStructListStub;
import com.cronbay.ws.stub.NotificationStructStub;
import com.cronbay.ws.stub.NotificationStructListStub;
import com.cronbay.ws.stub.PagerStateStructStub;
import com.cronbay.ws.stub.PagerStateStructListStub;
import com.cronbay.ws.stub.ApiConsumerStub;
import com.cronbay.ws.stub.ApiConsumerListStub;
import com.cronbay.ws.stub.UserStub;
import com.cronbay.ws.stub.UserListStub;
import com.cronbay.ws.stub.CronScheduleStructStub;
import com.cronbay.ws.stub.CronScheduleStructListStub;
import com.cronbay.ws.stub.RestRequestStructStub;
import com.cronbay.ws.stub.RestRequestStructListStub;
import com.cronbay.ws.stub.CronJobStub;
import com.cronbay.ws.stub.CronJobListStub;
import com.cronbay.ws.stub.JobOutputStub;
import com.cronbay.ws.stub.JobOutputListStub;
import com.cronbay.ws.stub.ServiceInfoStub;
import com.cronbay.ws.stub.ServiceInfoListStub;
import com.cronbay.ws.stub.FiveTenStub;
import com.cronbay.ws.stub.FiveTenListStub;
import com.cronbay.af.bean.ReferrerInfoStructBean;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.af.bean.GaeUserStructBean;
import com.cronbay.af.bean.NotificationStructBean;
import com.cronbay.af.bean.PagerStateStructBean;
import com.cronbay.af.bean.ApiConsumerBean;
import com.cronbay.af.bean.UserBean;
import com.cronbay.af.bean.CronScheduleStructBean;
import com.cronbay.af.bean.RestRequestStructBean;
import com.cronbay.af.bean.CronJobBean;
import com.cronbay.af.bean.JobOutputBean;
import com.cronbay.af.bean.ServiceInfoBean;
import com.cronbay.af.bean.FiveTenBean;


public final class MarshalHelper
{
    private static final Logger log = Logger.getLogger(MarshalHelper.class.getName());

    // Static methods only.
    private MarshalHelper() {}

    // temporary
    public static ReferrerInfoStructBean convertReferrerInfoStructToBean(ReferrerInfoStruct stub)
    {
        ReferrerInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ReferrerInfoStructBean();
        } else if(stub instanceof ReferrerInfoStructBean) {
        	bean = (ReferrerInfoStructBean) stub;
        } else {
        	bean = new ReferrerInfoStructBean(stub);
        }
        return bean;
    }
    public static List<ReferrerInfoStructBean> convertReferrerInfoStructListToBeanList(List<ReferrerInfoStruct> stubList)
    {
        List<ReferrerInfoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStructBean>();
            for(ReferrerInfoStruct stub : stubList) {
                beanList.add(convertReferrerInfoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ReferrerInfoStruct> convertReferrerInfoStructListStubToBeanList(ReferrerInfoStructListStub listStub)
    {
        List<ReferrerInfoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStruct>();
            for(ReferrerInfoStructStub stub : listStub.getList()) {
            	beanList.add(convertReferrerInfoStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ReferrerInfoStructStub convertReferrerInfoStructToStub(ReferrerInfoStruct bean)
    {
        ReferrerInfoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ReferrerInfoStructStub();
        } else if(bean instanceof ReferrerInfoStructStub) {
            stub = (ReferrerInfoStructStub) bean;
        } else if(bean instanceof ReferrerInfoStructBean && ((ReferrerInfoStructBean) bean).isWrapper()) {
            stub = ((ReferrerInfoStructBean) bean).getStub();
        } else {
            stub = ReferrerInfoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeAppStructBean convertGaeAppStructToBean(GaeAppStruct stub)
    {
        GaeAppStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeAppStructBean();
        } else if(stub instanceof GaeAppStructBean) {
        	bean = (GaeAppStructBean) stub;
        } else {
        	bean = new GaeAppStructBean(stub);
        }
        return bean;
    }
    public static List<GaeAppStructBean> convertGaeAppStructListToBeanList(List<GaeAppStruct> stubList)
    {
        List<GaeAppStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStructBean>();
            for(GaeAppStruct stub : stubList) {
                beanList.add(convertGaeAppStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeAppStruct> convertGaeAppStructListStubToBeanList(GaeAppStructListStub listStub)
    {
        List<GaeAppStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStruct>();
            for(GaeAppStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeAppStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static GaeAppStructStub convertGaeAppStructToStub(GaeAppStruct bean)
    {
        GaeAppStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeAppStructStub();
        } else if(bean instanceof GaeAppStructStub) {
            stub = (GaeAppStructStub) bean;
        } else if(bean instanceof GaeAppStructBean && ((GaeAppStructBean) bean).isWrapper()) {
            stub = ((GaeAppStructBean) bean).getStub();
        } else {
            stub = GaeAppStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeUserStructBean convertGaeUserStructToBean(GaeUserStruct stub)
    {
        GaeUserStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeUserStructBean();
        } else if(stub instanceof GaeUserStructBean) {
        	bean = (GaeUserStructBean) stub;
        } else {
        	bean = new GaeUserStructBean(stub);
        }
        return bean;
    }
    public static List<GaeUserStructBean> convertGaeUserStructListToBeanList(List<GaeUserStruct> stubList)
    {
        List<GaeUserStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStructBean>();
            for(GaeUserStruct stub : stubList) {
                beanList.add(convertGaeUserStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeUserStruct> convertGaeUserStructListStubToBeanList(GaeUserStructListStub listStub)
    {
        List<GaeUserStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStruct>();
            for(GaeUserStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeUserStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static GaeUserStructStub convertGaeUserStructToStub(GaeUserStruct bean)
    {
        GaeUserStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeUserStructStub();
        } else if(bean instanceof GaeUserStructStub) {
            stub = (GaeUserStructStub) bean;
        } else if(bean instanceof GaeUserStructBean && ((GaeUserStructBean) bean).isWrapper()) {
            stub = ((GaeUserStructBean) bean).getStub();
        } else {
            stub = GaeUserStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static NotificationStructBean convertNotificationStructToBean(NotificationStruct stub)
    {
        NotificationStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new NotificationStructBean();
        } else if(stub instanceof NotificationStructBean) {
        	bean = (NotificationStructBean) stub;
        } else {
        	bean = new NotificationStructBean(stub);
        }
        return bean;
    }
    public static List<NotificationStructBean> convertNotificationStructListToBeanList(List<NotificationStruct> stubList)
    {
        List<NotificationStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<NotificationStruct>();
        } else {
            beanList = new ArrayList<NotificationStructBean>();
            for(NotificationStruct stub : stubList) {
                beanList.add(convertNotificationStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<NotificationStruct> convertNotificationStructListStubToBeanList(NotificationStructListStub listStub)
    {
        List<NotificationStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<NotificationStruct>();
        } else {
            beanList = new ArrayList<NotificationStruct>();
            for(NotificationStructStub stub : listStub.getList()) {
            	beanList.add(convertNotificationStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static NotificationStructStub convertNotificationStructToStub(NotificationStruct bean)
    {
        NotificationStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new NotificationStructStub();
        } else if(bean instanceof NotificationStructStub) {
            stub = (NotificationStructStub) bean;
        } else if(bean instanceof NotificationStructBean && ((NotificationStructBean) bean).isWrapper()) {
            stub = ((NotificationStructBean) bean).getStub();
        } else {
            stub = NotificationStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static PagerStateStructBean convertPagerStateStructToBean(PagerStateStruct stub)
    {
        PagerStateStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new PagerStateStructBean();
        } else if(stub instanceof PagerStateStructBean) {
        	bean = (PagerStateStructBean) stub;
        } else {
        	bean = new PagerStateStructBean(stub);
        }
        return bean;
    }
    public static List<PagerStateStructBean> convertPagerStateStructListToBeanList(List<PagerStateStruct> stubList)
    {
        List<PagerStateStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<PagerStateStruct>();
        } else {
            beanList = new ArrayList<PagerStateStructBean>();
            for(PagerStateStruct stub : stubList) {
                beanList.add(convertPagerStateStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<PagerStateStruct> convertPagerStateStructListStubToBeanList(PagerStateStructListStub listStub)
    {
        List<PagerStateStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<PagerStateStruct>();
        } else {
            beanList = new ArrayList<PagerStateStruct>();
            for(PagerStateStructStub stub : listStub.getList()) {
            	beanList.add(convertPagerStateStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static PagerStateStructStub convertPagerStateStructToStub(PagerStateStruct bean)
    {
        PagerStateStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new PagerStateStructStub();
        } else if(bean instanceof PagerStateStructStub) {
            stub = (PagerStateStructStub) bean;
        } else if(bean instanceof PagerStateStructBean && ((PagerStateStructBean) bean).isWrapper()) {
            stub = ((PagerStateStructBean) bean).getStub();
        } else {
            stub = PagerStateStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ApiConsumerBean convertApiConsumerToBean(ApiConsumer stub)
    {
        ApiConsumerBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ApiConsumerBean();
        } else if(stub instanceof ApiConsumerBean) {
        	bean = (ApiConsumerBean) stub;
        } else {
        	bean = new ApiConsumerBean(stub);
        }
        return bean;
    }
    public static List<ApiConsumerBean> convertApiConsumerListToBeanList(List<ApiConsumer> stubList)
    {
        List<ApiConsumerBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumerBean>();
            for(ApiConsumer stub : stubList) {
                beanList.add(convertApiConsumerToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ApiConsumer> convertApiConsumerListStubToBeanList(ApiConsumerListStub listStub)
    {
        List<ApiConsumer> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumer>();
            for(ApiConsumerStub stub : listStub.getList()) {
            	beanList.add(convertApiConsumerToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ApiConsumerStub convertApiConsumerToStub(ApiConsumer bean)
    {
        ApiConsumerStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ApiConsumerStub();
        } else if(bean instanceof ApiConsumerStub) {
            stub = (ApiConsumerStub) bean;
        } else if(bean instanceof ApiConsumerBean && ((ApiConsumerBean) bean).isWrapper()) {
            stub = ((ApiConsumerBean) bean).getStub();
        } else {
            stub = ApiConsumerStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserBean convertUserToBean(User stub)
    {
        UserBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserBean();
        } else if(stub instanceof UserBean) {
        	bean = (UserBean) stub;
        } else {
        	bean = new UserBean(stub);
        }
        return bean;
    }
    public static List<UserBean> convertUserListToBeanList(List<User> stubList)
    {
        List<UserBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<UserBean>();
            for(User stub : stubList) {
                beanList.add(convertUserToBean(stub));
            }
        }
        return beanList;
    }
    public static List<User> convertUserListStubToBeanList(UserListStub listStub)
    {
        List<User> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<User>();
            for(UserStub stub : listStub.getList()) {
            	beanList.add(convertUserToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static UserStub convertUserToStub(User bean)
    {
        UserStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserStub();
        } else if(bean instanceof UserStub) {
            stub = (UserStub) bean;
        } else if(bean instanceof UserBean && ((UserBean) bean).isWrapper()) {
            stub = ((UserBean) bean).getStub();
        } else {
            stub = UserStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static CronScheduleStructBean convertCronScheduleStructToBean(CronScheduleStruct stub)
    {
        CronScheduleStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new CronScheduleStructBean();
        } else if(stub instanceof CronScheduleStructBean) {
        	bean = (CronScheduleStructBean) stub;
        } else {
        	bean = new CronScheduleStructBean(stub);
        }
        return bean;
    }
    public static List<CronScheduleStructBean> convertCronScheduleStructListToBeanList(List<CronScheduleStruct> stubList)
    {
        List<CronScheduleStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<CronScheduleStruct>();
        } else {
            beanList = new ArrayList<CronScheduleStructBean>();
            for(CronScheduleStruct stub : stubList) {
                beanList.add(convertCronScheduleStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<CronScheduleStruct> convertCronScheduleStructListStubToBeanList(CronScheduleStructListStub listStub)
    {
        List<CronScheduleStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<CronScheduleStruct>();
        } else {
            beanList = new ArrayList<CronScheduleStruct>();
            for(CronScheduleStructStub stub : listStub.getList()) {
            	beanList.add(convertCronScheduleStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static CronScheduleStructStub convertCronScheduleStructToStub(CronScheduleStruct bean)
    {
        CronScheduleStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new CronScheduleStructStub();
        } else if(bean instanceof CronScheduleStructStub) {
            stub = (CronScheduleStructStub) bean;
        } else if(bean instanceof CronScheduleStructBean && ((CronScheduleStructBean) bean).isWrapper()) {
            stub = ((CronScheduleStructBean) bean).getStub();
        } else {
            stub = CronScheduleStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static RestRequestStructBean convertRestRequestStructToBean(RestRequestStruct stub)
    {
        RestRequestStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new RestRequestStructBean();
        } else if(stub instanceof RestRequestStructBean) {
        	bean = (RestRequestStructBean) stub;
        } else {
        	bean = new RestRequestStructBean(stub);
        }
        return bean;
    }
    public static List<RestRequestStructBean> convertRestRequestStructListToBeanList(List<RestRequestStruct> stubList)
    {
        List<RestRequestStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<RestRequestStruct>();
        } else {
            beanList = new ArrayList<RestRequestStructBean>();
            for(RestRequestStruct stub : stubList) {
                beanList.add(convertRestRequestStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<RestRequestStruct> convertRestRequestStructListStubToBeanList(RestRequestStructListStub listStub)
    {
        List<RestRequestStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<RestRequestStruct>();
        } else {
            beanList = new ArrayList<RestRequestStruct>();
            for(RestRequestStructStub stub : listStub.getList()) {
            	beanList.add(convertRestRequestStructToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static RestRequestStructStub convertRestRequestStructToStub(RestRequestStruct bean)
    {
        RestRequestStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new RestRequestStructStub();
        } else if(bean instanceof RestRequestStructStub) {
            stub = (RestRequestStructStub) bean;
        } else if(bean instanceof RestRequestStructBean && ((RestRequestStructBean) bean).isWrapper()) {
            stub = ((RestRequestStructBean) bean).getStub();
        } else {
            stub = RestRequestStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static CronJobBean convertCronJobToBean(CronJob stub)
    {
        CronJobBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new CronJobBean();
        } else if(stub instanceof CronJobBean) {
        	bean = (CronJobBean) stub;
        } else {
        	bean = new CronJobBean(stub);
        }
        return bean;
    }
    public static List<CronJobBean> convertCronJobListToBeanList(List<CronJob> stubList)
    {
        List<CronJobBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<CronJob>();
        } else {
            beanList = new ArrayList<CronJobBean>();
            for(CronJob stub : stubList) {
                beanList.add(convertCronJobToBean(stub));
            }
        }
        return beanList;
    }
    public static List<CronJob> convertCronJobListStubToBeanList(CronJobListStub listStub)
    {
        List<CronJob> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<CronJob>();
        } else {
            beanList = new ArrayList<CronJob>();
            for(CronJobStub stub : listStub.getList()) {
            	beanList.add(convertCronJobToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static CronJobStub convertCronJobToStub(CronJob bean)
    {
        CronJobStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new CronJobStub();
        } else if(bean instanceof CronJobStub) {
            stub = (CronJobStub) bean;
        } else if(bean instanceof CronJobBean && ((CronJobBean) bean).isWrapper()) {
            stub = ((CronJobBean) bean).getStub();
        } else {
            stub = CronJobStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static JobOutputBean convertJobOutputToBean(JobOutput stub)
    {
        JobOutputBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new JobOutputBean();
        } else if(stub instanceof JobOutputBean) {
        	bean = (JobOutputBean) stub;
        } else {
        	bean = new JobOutputBean(stub);
        }
        return bean;
    }
    public static List<JobOutputBean> convertJobOutputListToBeanList(List<JobOutput> stubList)
    {
        List<JobOutputBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<JobOutput>();
        } else {
            beanList = new ArrayList<JobOutputBean>();
            for(JobOutput stub : stubList) {
                beanList.add(convertJobOutputToBean(stub));
            }
        }
        return beanList;
    }
    public static List<JobOutput> convertJobOutputListStubToBeanList(JobOutputListStub listStub)
    {
        List<JobOutput> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<JobOutput>();
        } else {
            beanList = new ArrayList<JobOutput>();
            for(JobOutputStub stub : listStub.getList()) {
            	beanList.add(convertJobOutputToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static JobOutputStub convertJobOutputToStub(JobOutput bean)
    {
        JobOutputStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new JobOutputStub();
        } else if(bean instanceof JobOutputStub) {
            stub = (JobOutputStub) bean;
        } else if(bean instanceof JobOutputBean && ((JobOutputBean) bean).isWrapper()) {
            stub = ((JobOutputBean) bean).getStub();
        } else {
            stub = JobOutputStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ServiceInfoBean convertServiceInfoToBean(ServiceInfo stub)
    {
        ServiceInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ServiceInfoBean();
        } else if(stub instanceof ServiceInfoBean) {
        	bean = (ServiceInfoBean) stub;
        } else {
        	bean = new ServiceInfoBean(stub);
        }
        return bean;
    }
    public static List<ServiceInfoBean> convertServiceInfoListToBeanList(List<ServiceInfo> stubList)
    {
        List<ServiceInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfoBean>();
            for(ServiceInfo stub : stubList) {
                beanList.add(convertServiceInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ServiceInfo> convertServiceInfoListStubToBeanList(ServiceInfoListStub listStub)
    {
        List<ServiceInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfo>();
            for(ServiceInfoStub stub : listStub.getList()) {
            	beanList.add(convertServiceInfoToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static ServiceInfoStub convertServiceInfoToStub(ServiceInfo bean)
    {
        ServiceInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ServiceInfoStub();
        } else if(bean instanceof ServiceInfoStub) {
            stub = (ServiceInfoStub) bean;
        } else if(bean instanceof ServiceInfoBean && ((ServiceInfoBean) bean).isWrapper()) {
            stub = ((ServiceInfoBean) bean).getStub();
        } else {
            stub = ServiceInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FiveTenBean convertFiveTenToBean(FiveTen stub)
    {
        FiveTenBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FiveTenBean();
        } else if(stub instanceof FiveTenBean) {
        	bean = (FiveTenBean) stub;
        } else {
        	bean = new FiveTenBean(stub);
        }
        return bean;
    }
    public static List<FiveTenBean> convertFiveTenListToBeanList(List<FiveTen> stubList)
    {
        List<FiveTenBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTenBean>();
            for(FiveTen stub : stubList) {
                beanList.add(convertFiveTenToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FiveTen> convertFiveTenListStubToBeanList(FiveTenListStub listStub)
    {
        List<FiveTen> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTen>();
            for(FiveTenStub stub : listStub.getList()) {
            	beanList.add(convertFiveTenToBean(stub));
            }
        }
        return beanList;
    }

    // temporary
    public static FiveTenStub convertFiveTenToStub(FiveTen bean)
    {
        FiveTenStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FiveTenStub();
        } else if(bean instanceof FiveTenStub) {
            stub = (FiveTenStub) bean;
        } else if(bean instanceof FiveTenBean && ((FiveTenBean) bean).isWrapper()) {
            stub = ((FiveTenBean) bean).getStub();
        } else {
            stub = FiveTenStub.convertBeanToStub(bean);
        }
        return stub;
    }

    
}
