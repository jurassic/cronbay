package com.cronbay.af.util;

import java.util.logging.Logger;
import java.util.logging.Level;
import javax.xml.bind.DatatypeConverter;


// This is more for "obfuscation" than encryption.
// To be used to make a string somewhat opaque to casual eyes.
// Note: DO NOT USE THIS FOR REAL ENCRYPTION PURPOSES.
public class SimpleEncryptUtil
{
    private static final Logger log = Logger.getLogger(SimpleEncryptUtil.class.getName());

    // temporary
    private static final String DEFAULT_ENCRYPT_KEY = "abcdefghijklmnopqrstuvwxyz";
    
    public SimpleEncryptUtil() {}


    public static String encrypt(String str)
    {
        return encrypt(str, null);
    }

    public static String encrypt(String str, String key)
    {
        if(str == null || str.isEmpty()) {
            return str;
        }
        if(key == null || key.isEmpty()) {
            key = DEFAULT_ENCRYPT_KEY;
        }
        
        byte[] encryptedBytes = transform(str.getBytes(), key);
        String encrypted = DatatypeConverter.printBase64Binary(encryptedBytes);
        log.fine("encrypted = " + encrypted);

        return encrypted;
    }

    public static String decrypt(String str)
    {
        return decrypt(str, null);
    }

    public static String decrypt(String str, String key)
    {
        if(str == null || str.isEmpty()) {
            return str;
        }
        if(key == null || key.isEmpty()) {
            key = DEFAULT_ENCRYPT_KEY;
        }
        
        byte[] decodedBytes = DatatypeConverter.parseBase64Binary(str);
        byte[] decryptedBytes = transform(decodedBytes, key);
        String decrypted = new String(decryptedBytes);
        log.fine("decrypted = " + decrypted);

        return decrypted;
    }

    
    // Note: tranform(tansform()) returns the orginal argument.
    private static byte[] transform(byte[] inputBytes, String key)
    {
        if(inputBytes == null || inputBytes.length == 0) {
            return inputBytes;
        }
        if(key == null || key.isEmpty()) {
            return null;
        }

        byte[] keyBytes = key.getBytes();
        int lenKey = keyBytes.length;
        int lenStr = inputBytes.length;        
        byte[] transformedBytes = new byte[lenStr];
        for(int i = 0, j = 0; i < lenStr; i++, j++) {
            if(j >= lenKey) j = 0;   // Wrap 
            transformedBytes[i] = (byte) (inputBytes[i] ^ keyBytes[j]); 
        }

        return transformedBytes;
    }
    
}
