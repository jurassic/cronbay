package com.cronbay.af.core;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;


// Constructing Jersey Client takes about 2~3 seconds on GAE !!!!!
// Need to reuse this whenever possible...
// TBD: Different code might require different client configurations ??
public class JerseyClient
{
    private static final Logger log = Logger.getLogger(JerseyClient.class.getName());

    // Jersey client
    private Client mClient = null;


    private JerseyClient()
    {
        //ClientConfig config = new DefaultClientConfig();
        //config.getClasses().add(JSONRootElementProvider.class);
        //wsClient = Client.create(config);
        //wsClient.addFilter(new LoggingFilter());

        mClient = Client.create();  // ???
        initJerseyClienturation();
    }

    // Initialization-on-demand holder.
    private static class JerseyClientHolder
    {
        private static final JerseyClient INSTANCE = new JerseyClient();
    }

    // Singleton method
    public static JerseyClient getInstance()
    {
        return JerseyClientHolder.INSTANCE;
    }

    private void initJerseyClienturation()
    {
        // TBD ...
        if(mClient != null) {
            mClient.setFollowRedirects(false);
            // Etc.
        }
    }
    
    // Dummy. Do nothing.
    // May be called for "preloading purposes"...
    public void initClient()
    {
        log.info("initClient() called.");
    }

    // Returns the jesery client.
    public Client getClient()
    {
        return getClient(false);
    }
    public Client getClient(boolean reset)
    {
        log.fine("getClient() called with reset = " + reset);

        if(reset == true) {
            resetClient();
        }
        return mClient;
    }

    // TBD:
    public void resetClient()
    {
        log.fine("resetClient() called.");

        if(mClient != null) {
            // ???
            mClient.removeAllFilters();
            // Etc. ....
        }
    }


}
