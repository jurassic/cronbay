package com.cronbay.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cronbay.af.core.JerseyClient;
import com.cronbay.app.job.JobProcessor;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.JobOutputJsBean;
import com.cronbay.helper.JobOutputHelper;
import com.cronbay.helper.URLHelper;
import com.cronbay.util.JsonBeanUtil;
import com.cronbay.wa.service.JobOutputWebService;
import com.cronbay.wa.service.UserWebService;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.core.StatusCode;


// Mainly, for ajax calls....
// Payload: { "jobOutput": jobOutputBean, "trialLaunch": trialLaunchBean }
public class JobOutputServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(JobOutputServlet.class.getName());
    
    // Arbitrary cutoff. Note: 500 is the max length for String type...
    //private static final int SUMMARY_LENGTH_CUTOFF = 450;

    // temporary
    private static final String QUERY_PARAM_TARGET_URL = "targetUrl";
    private static final String QUERY_PARAM_FETCH = "job";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private JobOutputWebService jobOutputWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private JobOutputWebService getJobOutputService()
    {
        if(jobOutputWebService == null) {
            jobOutputWebService = new JobOutputWebService();
        }
        return jobOutputWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singleton instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        //throw new ServletException("Not implemented.");
        log.info("doGet(): TOP");
        
        
        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        String targetUrl = null;
        String[] targetUrls = req.getParameterValues(QUERY_PARAM_TARGET_URL);
        if(targetUrls != null && targetUrls.length > 0) {
            // TBD: Support multiple target urls???
            
            // TBD: the query param is already url decoded!!!
            // ...
            // TBD: comment this out on the next deployment
            // ....
            targetUrl = URLDecoder.decode(targetUrls[0], "UTF-8");  // ?????
            // TBD:
            // "canonicalize" the targetUrl???
            // ....
        } else {
            // error...
            //throw new ResourceNotFoundRsException("");
            //throw new BadRequestRsException("Query parameter is missing, " + QUERY_PARAM_TARGET_URL);
            //throw new ServletException("Query parameter is missing, " + QUERY_PARAM_TARGET_URL);
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        log.fine("targetUrl = " + targetUrl);
        
        boolean job = true;   // default value.
        String[] jobStrs = req.getParameterValues(QUERY_PARAM_FETCH);
        if(jobStrs != null && jobStrs.length > 0) {
            if(jobStrs[0].equals("0") || jobStrs[0].equalsIgnoreCase("false")) {  // temporary
                job = false;
            }
            // else ignore.
        }
        log.fine("job = " + job);
        
        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        
        
        JobOutputJsBean jobOutput = JobOutputHelper.getInstance().getJobOutputByTargetUrl(targetUrl);
        if(jobOutput == null) {
            if(job == true) {
                jobOutput = new JobOutputJsBean();
                String guid = GUID.generate();
                jobOutput.setGuid(guid);
//                jobOutput.setTargetUrl(targetUrl);
                try {
                    URL url = new URL(targetUrl);  // ???
                    String query = url.getQuery();
                    String pageUrl = targetUrl;
                    if(query != null) {
                        int qidx = targetUrl.indexOf("?");
                        if(qidx > 0) {
                            pageUrl = targetUrl.substring(0, qidx);
                        }
                    }
//                    jobOutput.setPageUrl(pageUrl);
//                    if(query != null && !query.isEmpty()) {
//                        List<String> params = QueryStringUtil.parseQueryString(query);
//                        jobOutput.setQueryString(query); 
//                        jobOutput.setQueryParams(params);                    
//                    }
                } catch(MalformedURLException e) {
                    log.log(Level.WARNING, "targetUrl is malformed. targetUrl = " + targetUrl, e);
                    // ignore and continue???? or, bail out???
                    resp.setStatus(StatusCode.BAD_REQUEST);
                    return;
                }
//                jobOutput.setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  /// ???
                
                JobOutput bean = JobOutputWebService.convertJobOutputJsBeanToBean(jobOutput);
                try {
                    bean = JobProcessor.getInstance().processPageFetch(bean);
                    jobOutput = JobOutputWebService.convertJobOutputToJsBean(bean);
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Failed to job the page: targetUrl = " + targetUrl, e);
                    jobOutput = null;
                }
                
                if(jobOutput != null) {
                    try {
                        jobOutput = getJobOutputService().constructJobOutput(jobOutput);
                    } catch (WebException e) {
                        log.log(Level.WARNING, "Failed to save the jobOutput with targetUrl = " + targetUrl, e);
                        jobOutput = null;
                    }
                } 
            } else {
                // ???
            }
        }
        
        if(jobOutput != null) {
            String jsonStr = jobOutput.toJsonString();
            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                jsonStr = jsonpCallback + "(" + jsonStr + ")";
                //resp.setContentType("application/javascript");  // ???
                resp.setContentType("application/javascript;charset=UTF-8");
            } else {
                //resp.setContentType("application/json");  // ????
                resp.setContentType("application/json;charset=UTF-8");  // ???                
            }
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

    
    
    // Note:
    // Currently, only GET is "officially" supported...
    // ...
    
    
    // TBD:
    // depending on jobOutput.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPost(): TOP");

        // TBD: Check Accept header. Etc...
        // For now, both input and output are application/json.
        JobOutputJsBean outJobOutputBean = null;
        try {
            BufferedReader reader = req.getReader();
            String jsonStr = reader.readLine();

            JobOutputJsBean inJobOutputBean = null;
            Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
            for(String key : jsonObjectMap.keySet()) {
                String json = jsonObjectMap.get(key);
                log.info("key = " + key + "; json = " + json);
                
                if(key.equals("jobOutput")) {
                    inJobOutputBean = JobOutputJsBean.fromJsonString(json);
                } else {
                    // TBD:
                    // This should not happen, for now.
                    log.warning("Unregconized key in the json input string. key = " + key);
                }
            }

            // TBD: Make sure neither is null???
            // Or, just process what is inputted....
            
            // temporary!!!
            if(inJobOutputBean != null) {
                // TBD: Validate and populate some missing fields, etc...
                // ????
                // temporary....
                // etc. ...
                // ...

                outJobOutputBean = getJobOutputService().constructJobOutput(inJobOutputBean);
            }
            
            // ...

        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to save jobOutput.", e);
        }
        
        // temporary
        boolean suc = false;
        if(outJobOutputBean != null) {
            suc = true;
        }

        // Response.
        if(suc == true) {
            resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            resp.setContentType("application/json;charset=UTF-8");
            //resp.setHeader("Cache-Control", "no-cache");    // ?????

            // Location header???
            //String guid = outJobOutputBean.getGuid();
            
            // Construct output
            Map<String, String> jsonMap = new HashMap<String, String>();
            if(outJobOutputBean != null) {
                jsonMap.put("jobOutput", outJobOutputBean.toJsonString());
            }
            String output = JsonBeanUtil.generateJsonObjectString(jsonMap);

            PrintWriter out = resp.getWriter();
            out.write(output);
        } else {
            resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
        }

        log.info("doPost(): BOTTOM");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPut(): TOP");

        // TBD: Check Accept header. Etc...

        String pathInfo = req.getPathInfo();
        String guid = URLHelper.getInstance().getGuidFromPathInfo(pathInfo);        

        // guid should not be null!!!
        if(guid != null && !guid.isEmpty()) {
            Boolean suc = null;
            try {
                BufferedReader reader = req.getReader();
                String jsonStr = reader.readLine();
                
                JobOutputJsBean inJobOutputBean = null;
                Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
                for(String key : jsonObjectMap.keySet()) {
                    String json = jsonObjectMap.get(key);
                    log.info("key = " + key + "; json = " + json);
                    
                    if(key.equals("jobOutput")) {
                        inJobOutputBean = JobOutputJsBean.fromJsonString(json);
                    } else {
                        // TBD:
                        // This should not happen, for now.
                        log.warning("Unregconized key in the json input string. key = " + key);
                    }
                }

                // TBD: Make sure neither is null???
                // Or, just process what is inputted....

                Boolean sucJobOutput = null;
                if(inJobOutputBean != null) {
                    String beanGuid = inJobOutputBean.getGuid();
                    if(guid.equals(beanGuid)) {
                        inJobOutputBean.setModifiedTime(System.currentTimeMillis());
                        sucJobOutput = getJobOutputService().updateJobOutput(inJobOutputBean);
                        log.finer("sucJobOutput = " + sucJobOutput);
                    } else {
                        log.log(Level.WARNING, "Inconsistent input. pathGuid = " + guid + "; beanGuid = " + beanGuid);
                        // ???
                    }
                }

                // TBD: ...
                if(Boolean.TRUE.equals(sucJobOutput)) {  // ????
                    suc = Boolean.TRUE;
                }
                // ...

            } catch (WebException e) {
                log.log(Level.WARNING, "Failed.", e);
            }
            if(Boolean.TRUE.equals(suc)) {
                resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            } else {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
            }
        } else {
            // ???
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }

        log.info("doPut(): BOTTOM");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    // TBD:
    // validators???
    // e.g., max length of title, etc.
    // ...
    
    
    
}
