package com.cronbay.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cronbay.fe.bean.FiveTenJsBean;
import com.cronbay.wa.service.FiveTenWebService;
import com.cronbay.ws.core.StatusCode;


// Mainly, for ajax calls....
public class FiveTenServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FiveTenServlet.class.getName());

    // TBD: Is this safe for concurrent calls???
    private FiveTenWebService fiveTenWebService = null;
    // etc...

    private FiveTenWebService getFiveTenService()
    {
        if(fiveTenWebService == null) {
            fiveTenWebService = new FiveTenWebService();
        }
        return fiveTenWebService;
    }
    // etc. ...

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        List<FiveTenJsBean> beans = null;
        try {
            beans = getFiveTenService().getAllFiveTens();
            log.log(Level.INFO, "fiveTen peeng called at " + System.currentTimeMillis() + " and it returned beans: " + beans);
        } catch (Exception e) {
            // ignore
            log.log(Level.INFO, "fiveTen peeng failed.", e);
        }

        if(beans != null) {
            resp.setStatus(StatusCode.OK);
        } else {
            // Arbitrary. Just to indicate an error status to the client, in case the client is interested in the response...
            resp.setStatus(StatusCode.NOT_FOUND);
        }
    }

}
