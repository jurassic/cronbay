//////////////////////////////////////////////////////////
// <script src="/js/bean/cronschedulestructjsbean-1.0.js"></script>
// Last modified time: 1337996953387.
//////////////////////////////////////////////////////////

var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.bean = cronbay.wa.bean || {};
cronbay.wa.bean.CronScheduleStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var type;
    var schedule;
    var timezone;
    var maxIterations;
    var repeatInterval;
    var firtRunTime;
    var startTime;
    var endTime;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getType = function() { return type; };
    this.setType = function(value) { type = value; };
    this.getSchedule = function() { return schedule; };
    this.setSchedule = function(value) { schedule = value; };
    this.getTimezone = function() { return timezone; };
    this.setTimezone = function(value) { timezone = value; };
    this.getMaxIterations = function() { return maxIterations; };
    this.setMaxIterations = function(value) { maxIterations = value; };
    this.getRepeatInterval = function() { return repeatInterval; };
    this.setRepeatInterval = function(value) { repeatInterval = value; };
    this.getFirtRunTime = function() { return firtRunTime; };
    this.setFirtRunTime = function(value) { firtRunTime = value; };
    this.getStartTime = function() { return startTime; };
    this.setStartTime = function(value) { startTime = value; };
    this.getEndTime = function() { return endTime; };
    this.setEndTime = function(value) { endTime = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cronbay.wa.bean.CronScheduleStructJsBean();

      if(type) {
        o.setType(type);
      }
      if(schedule) {
        o.setSchedule(schedule);
      }
      if(timezone) {
        o.setTimezone(timezone);
      }
      if(maxIterations) {
        o.setMaxIterations(maxIterations);
      }
      if(repeatInterval) {
        o.setRepeatInterval(repeatInterval);
      }
      if(firtRunTime) {
        o.setFirtRunTime(firtRunTime);
      }
      if(startTime) {
        o.setStartTime(startTime);
      }
      if(endTime) {
        o.setEndTime(endTime);
      }
      if(note) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(type) {
        jsonObj.type = type;
      } // Otherwise ignore...
      if(schedule) {
        jsonObj.schedule = schedule;
      } // Otherwise ignore...
      if(timezone) {
        jsonObj.timezone = timezone;
      } // Otherwise ignore...
      if(maxIterations) {
        jsonObj.maxIterations = maxIterations;
      } // Otherwise ignore...
      if(repeatInterval) {
        jsonObj.repeatInterval = repeatInterval;
      } // Otherwise ignore...
      if(firtRunTime) {
        jsonObj.firtRunTime = firtRunTime;
      } // Otherwise ignore...
      if(startTime) {
        jsonObj.startTime = startTime;
      } // Otherwise ignore...
      if(endTime) {
        jsonObj.endTime = endTime;
      } // Otherwise ignore...
      if(note) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(type) {
        str += "\"type\":\"" + type + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"type\":null, ";
      }
      if(schedule) {
        str += "\"schedule\":\"" + schedule + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"schedule\":null, ";
      }
      if(timezone) {
        str += "\"timezone\":\"" + timezone + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"timezone\":null, ";
      }
      if(maxIterations) {
        str += "\"maxIterations\":" + maxIterations + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"maxIterations\":null, ";
      }
      if(repeatInterval) {
        str += "\"repeatInterval\":" + repeatInterval + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"repeatInterval\":null, ";
      }
      if(firtRunTime) {
        str += "\"firtRunTime\":" + firtRunTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"firtRunTime\":null, ";
      }
      if(startTime) {
        str += "\"startTime\":" + startTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"startTime\":null, ";
      }
      if(endTime) {
        str += "\"endTime\":" + endTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"endTime\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "type:" + type + ", ";
      str += "schedule:" + schedule + ", ";
      str += "timezone:" + timezone + ", ";
      str += "maxIterations:" + maxIterations + ", ";
      str += "repeatInterval:" + repeatInterval + ", ";
      str += "firtRunTime:" + firtRunTime + ", ";
      str += "startTime:" + startTime + ", ";
      str += "endTime:" + endTime + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.bean.CronScheduleStructJsBean.create = function(obj) {
  var o = new cronbay.wa.bean.CronScheduleStructJsBean();

  if(obj.type) {
    o.setType(obj.type);
  }
  if(obj.schedule) {
    o.setSchedule(obj.schedule);
  }
  if(obj.timezone) {
    o.setTimezone(obj.timezone);
  }
  if(obj.maxIterations) {
    o.setMaxIterations(obj.maxIterations);
  }
  if(obj.repeatInterval) {
    o.setRepeatInterval(obj.repeatInterval);
  }
  if(obj.firtRunTime) {
    o.setFirtRunTime(obj.firtRunTime);
  }
  if(obj.startTime) {
    o.setStartTime(obj.startTime);
  }
  if(obj.endTime) {
    o.setEndTime(obj.endTime);
  }
  if(obj.note) {
    o.setNote(obj.note);
  }
    
  return o;
};

cronbay.wa.bean.CronScheduleStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.bean.CronScheduleStructJsBean.create(jsonObj);
  return obj;
};
