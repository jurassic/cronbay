//////////////////////////////////////////////////////////
// <script src="/js/bean/gaeappstructjsbean-1.0.js"></script>
// Last modified time: 1337996953309.
//////////////////////////////////////////////////////////

var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.bean = cronbay.wa.bean || {};
cronbay.wa.bean.GaeAppStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var groupId;
    var appId;
    var appDomain;
    var namespace;
    var acl;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGroupId = function() { return groupId; };
    this.setGroupId = function(value) { groupId = value; };
    this.getAppId = function() { return appId; };
    this.setAppId = function(value) { appId = value; };
    this.getAppDomain = function() { return appDomain; };
    this.setAppDomain = function(value) { appDomain = value; };
    this.getNamespace = function() { return namespace; };
    this.setNamespace = function(value) { namespace = value; };
    this.getAcl = function() { return acl; };
    this.setAcl = function(value) { acl = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cronbay.wa.bean.GaeAppStructJsBean();

      if(groupId) {
        o.setGroupId(groupId);
      }
      if(appId) {
        o.setAppId(appId);
      }
      if(appDomain) {
        o.setAppDomain(appDomain);
      }
      if(namespace) {
        o.setNamespace(namespace);
      }
      if(acl) {
        o.setAcl(acl);
      }
      if(note) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(groupId) {
        jsonObj.groupId = groupId;
      } // Otherwise ignore...
      if(appId) {
        jsonObj.appId = appId;
      } // Otherwise ignore...
      if(appDomain) {
        jsonObj.appDomain = appDomain;
      } // Otherwise ignore...
      if(namespace) {
        jsonObj.namespace = namespace;
      } // Otherwise ignore...
      if(acl) {
        jsonObj.acl = acl;
      } // Otherwise ignore...
      if(note) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(groupId) {
        str += "\"groupId\":\"" + groupId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"groupId\":null, ";
      }
      if(appId) {
        str += "\"appId\":\"" + appId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appId\":null, ";
      }
      if(appDomain) {
        str += "\"appDomain\":\"" + appDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appDomain\":null, ";
      }
      if(namespace) {
        str += "\"namespace\":\"" + namespace + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"namespace\":null, ";
      }
      if(acl) {
        str += "\"acl\":" + acl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"acl\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "groupId:" + groupId + ", ";
      str += "appId:" + appId + ", ";
      str += "appDomain:" + appDomain + ", ";
      str += "namespace:" + namespace + ", ";
      str += "acl:" + acl + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.bean.GaeAppStructJsBean.create = function(obj) {
  var o = new cronbay.wa.bean.GaeAppStructJsBean();

  if(obj.groupId) {
    o.setGroupId(obj.groupId);
  }
  if(obj.appId) {
    o.setAppId(obj.appId);
  }
  if(obj.appDomain) {
    o.setAppDomain(obj.appDomain);
  }
  if(obj.namespace) {
    o.setNamespace(obj.namespace);
  }
  if(obj.acl) {
    o.setAcl(obj.acl);
  }
  if(obj.note) {
    o.setNote(obj.note);
  }
    
  return o;
};

cronbay.wa.bean.GaeAppStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.bean.GaeAppStructJsBean.create(jsonObj);
  return obj;
};
