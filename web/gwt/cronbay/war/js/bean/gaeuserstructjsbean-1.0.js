//////////////////////////////////////////////////////////
// <script src="/js/bean/gaeuserstructjsbean-1.0.js"></script>
// Last modified time: 1337996953321.
//////////////////////////////////////////////////////////

var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.bean = cronbay.wa.bean || {};
cronbay.wa.bean.GaeUserStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var authDomain;
    var federatedIdentity;
    var nickname;
    var userId;
    var email;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getAuthDomain = function() { return authDomain; };
    this.setAuthDomain = function(value) { authDomain = value; };
    this.getFederatedIdentity = function() { return federatedIdentity; };
    this.setFederatedIdentity = function(value) { federatedIdentity = value; };
    this.getNickname = function() { return nickname; };
    this.setNickname = function(value) { nickname = value; };
    this.getUserId = function() { return userId; };
    this.setUserId = function(value) { userId = value; };
    this.getEmail = function() { return email; };
    this.setEmail = function(value) { email = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cronbay.wa.bean.GaeUserStructJsBean();

      if(authDomain) {
        o.setAuthDomain(authDomain);
      }
      if(federatedIdentity) {
        o.setFederatedIdentity(federatedIdentity);
      }
      if(nickname) {
        o.setNickname(nickname);
      }
      if(userId) {
        o.setUserId(userId);
      }
      if(email) {
        o.setEmail(email);
      }
      if(note) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(authDomain) {
        jsonObj.authDomain = authDomain;
      } // Otherwise ignore...
      if(federatedIdentity) {
        jsonObj.federatedIdentity = federatedIdentity;
      } // Otherwise ignore...
      if(nickname) {
        jsonObj.nickname = nickname;
      } // Otherwise ignore...
      if(userId) {
        jsonObj.userId = userId;
      } // Otherwise ignore...
      if(email) {
        jsonObj.email = email;
      } // Otherwise ignore...
      if(note) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(authDomain) {
        str += "\"authDomain\":\"" + authDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authDomain\":null, ";
      }
      if(federatedIdentity) {
        str += "\"federatedIdentity\":\"" + federatedIdentity + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"federatedIdentity\":null, ";
      }
      if(nickname) {
        str += "\"nickname\":\"" + nickname + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nickname\":null, ";
      }
      if(userId) {
        str += "\"userId\":\"" + userId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userId\":null, ";
      }
      if(email) {
        str += "\"email\":\"" + email + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"email\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "authDomain:" + authDomain + ", ";
      str += "federatedIdentity:" + federatedIdentity + ", ";
      str += "nickname:" + nickname + ", ";
      str += "userId:" + userId + ", ";
      str += "email:" + email + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.bean.GaeUserStructJsBean.create = function(obj) {
  var o = new cronbay.wa.bean.GaeUserStructJsBean();

  if(obj.authDomain) {
    o.setAuthDomain(obj.authDomain);
  }
  if(obj.federatedIdentity) {
    o.setFederatedIdentity(obj.federatedIdentity);
  }
  if(obj.nickname) {
    o.setNickname(obj.nickname);
  }
  if(obj.userId) {
    o.setUserId(obj.userId);
  }
  if(obj.email) {
    o.setEmail(obj.email);
  }
  if(obj.note) {
    o.setNote(obj.note);
  }
    
  return o;
};

cronbay.wa.bean.GaeUserStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.bean.GaeUserStructJsBean.create(jsonObj);
  return obj;
};
