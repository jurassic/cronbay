//////////////////////////////////////////////////////////
// <script src="/js/bean/pagerstatestructjsbean-1.0.js"></script>
// Last modified time: 1337996953339.
//////////////////////////////////////////////////////////

var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.bean = cronbay.wa.bean || {};
cronbay.wa.bean.PagerStateStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var pagerMode;
    var primaryOrdering;
    var secondaryOrdering;
    var currentOffset;
    var currentPage;
    var pageSize;
    var totalCount;
    var lowerBoundTotalCount;
    var previousPageOffset;
    var nextPageOffset;
    var lastPageOffset;
    var lastPageIndex;
    var firstActionEnabled;
    var previousActionEnabled;
    var nextActionEnabled;
    var lastActionEnabled;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getPagerMode = function() { return pagerMode; };
    this.setPagerMode = function(value) { pagerMode = value; };
    this.getPrimaryOrdering = function() { return primaryOrdering; };
    this.setPrimaryOrdering = function(value) { primaryOrdering = value; };
    this.getSecondaryOrdering = function() { return secondaryOrdering; };
    this.setSecondaryOrdering = function(value) { secondaryOrdering = value; };
    this.getCurrentOffset = function() { return currentOffset; };
    this.setCurrentOffset = function(value) { currentOffset = value; };
    this.getCurrentPage = function() { return currentPage; };
    this.setCurrentPage = function(value) { currentPage = value; };
    this.getPageSize = function() { return pageSize; };
    this.setPageSize = function(value) { pageSize = value; };
    this.getTotalCount = function() { return totalCount; };
    this.setTotalCount = function(value) { totalCount = value; };
    this.getLowerBoundTotalCount = function() { return lowerBoundTotalCount; };
    this.setLowerBoundTotalCount = function(value) { lowerBoundTotalCount = value; };
    this.getPreviousPageOffset = function() { return previousPageOffset; };
    this.setPreviousPageOffset = function(value) { previousPageOffset = value; };
    this.getNextPageOffset = function() { return nextPageOffset; };
    this.setNextPageOffset = function(value) { nextPageOffset = value; };
    this.getLastPageOffset = function() { return lastPageOffset; };
    this.setLastPageOffset = function(value) { lastPageOffset = value; };
    this.getLastPageIndex = function() { return lastPageIndex; };
    this.setLastPageIndex = function(value) { lastPageIndex = value; };
    this.getFirstActionEnabled = function() { return firstActionEnabled; };
    this.setFirstActionEnabled = function(value) { firstActionEnabled = value; };
    this.getPreviousActionEnabled = function() { return previousActionEnabled; };
    this.setPreviousActionEnabled = function(value) { previousActionEnabled = value; };
    this.getNextActionEnabled = function() { return nextActionEnabled; };
    this.setNextActionEnabled = function(value) { nextActionEnabled = value; };
    this.getLastActionEnabled = function() { return lastActionEnabled; };
    this.setLastActionEnabled = function(value) { lastActionEnabled = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cronbay.wa.bean.PagerStateStructJsBean();

      if(pagerMode) {
        o.setPagerMode(pagerMode);
      }
      if(primaryOrdering) {
        o.setPrimaryOrdering(primaryOrdering);
      }
      if(secondaryOrdering) {
        o.setSecondaryOrdering(secondaryOrdering);
      }
      if(currentOffset) {
        o.setCurrentOffset(currentOffset);
      }
      if(currentPage) {
        o.setCurrentPage(currentPage);
      }
      if(pageSize) {
        o.setPageSize(pageSize);
      }
      if(totalCount) {
        o.setTotalCount(totalCount);
      }
      if(lowerBoundTotalCount) {
        o.setLowerBoundTotalCount(lowerBoundTotalCount);
      }
      if(previousPageOffset) {
        o.setPreviousPageOffset(previousPageOffset);
      }
      if(nextPageOffset) {
        o.setNextPageOffset(nextPageOffset);
      }
      if(lastPageOffset) {
        o.setLastPageOffset(lastPageOffset);
      }
      if(lastPageIndex) {
        o.setLastPageIndex(lastPageIndex);
      }
      if(firstActionEnabled) {
        o.setFirstActionEnabled(firstActionEnabled);
      }
      if(previousActionEnabled) {
        o.setPreviousActionEnabled(previousActionEnabled);
      }
      if(nextActionEnabled) {
        o.setNextActionEnabled(nextActionEnabled);
      }
      if(lastActionEnabled) {
        o.setLastActionEnabled(lastActionEnabled);
      }
      if(note) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(pagerMode) {
        jsonObj.pagerMode = pagerMode;
      } // Otherwise ignore...
      if(primaryOrdering) {
        jsonObj.primaryOrdering = primaryOrdering;
      } // Otherwise ignore...
      if(secondaryOrdering) {
        jsonObj.secondaryOrdering = secondaryOrdering;
      } // Otherwise ignore...
      if(currentOffset) {
        jsonObj.currentOffset = currentOffset;
      } // Otherwise ignore...
      if(currentPage) {
        jsonObj.currentPage = currentPage;
      } // Otherwise ignore...
      if(pageSize) {
        jsonObj.pageSize = pageSize;
      } // Otherwise ignore...
      if(totalCount) {
        jsonObj.totalCount = totalCount;
      } // Otherwise ignore...
      if(lowerBoundTotalCount) {
        jsonObj.lowerBoundTotalCount = lowerBoundTotalCount;
      } // Otherwise ignore...
      if(previousPageOffset) {
        jsonObj.previousPageOffset = previousPageOffset;
      } // Otherwise ignore...
      if(nextPageOffset) {
        jsonObj.nextPageOffset = nextPageOffset;
      } // Otherwise ignore...
      if(lastPageOffset) {
        jsonObj.lastPageOffset = lastPageOffset;
      } // Otherwise ignore...
      if(lastPageIndex) {
        jsonObj.lastPageIndex = lastPageIndex;
      } // Otherwise ignore...
      if(firstActionEnabled) {
        jsonObj.firstActionEnabled = firstActionEnabled;
      } // Otherwise ignore...
      if(previousActionEnabled) {
        jsonObj.previousActionEnabled = previousActionEnabled;
      } // Otherwise ignore...
      if(nextActionEnabled) {
        jsonObj.nextActionEnabled = nextActionEnabled;
      } // Otherwise ignore...
      if(lastActionEnabled) {
        jsonObj.lastActionEnabled = lastActionEnabled;
      } // Otherwise ignore...
      if(note) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(pagerMode) {
        str += "\"pagerMode\":\"" + pagerMode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pagerMode\":null, ";
      }
      if(primaryOrdering) {
        str += "\"primaryOrdering\":\"" + primaryOrdering + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"primaryOrdering\":null, ";
      }
      if(secondaryOrdering) {
        str += "\"secondaryOrdering\":\"" + secondaryOrdering + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"secondaryOrdering\":null, ";
      }
      if(currentOffset) {
        str += "\"currentOffset\":" + currentOffset + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"currentOffset\":null, ";
      }
      if(currentPage) {
        str += "\"currentPage\":" + currentPage + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"currentPage\":null, ";
      }
      if(pageSize) {
        str += "\"pageSize\":" + pageSize + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pageSize\":null, ";
      }
      if(totalCount) {
        str += "\"totalCount\":" + totalCount + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"totalCount\":null, ";
      }
      if(lowerBoundTotalCount) {
        str += "\"lowerBoundTotalCount\":" + lowerBoundTotalCount + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lowerBoundTotalCount\":null, ";
      }
      if(previousPageOffset) {
        str += "\"previousPageOffset\":" + previousPageOffset + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"previousPageOffset\":null, ";
      }
      if(nextPageOffset) {
        str += "\"nextPageOffset\":" + nextPageOffset + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nextPageOffset\":null, ";
      }
      if(lastPageOffset) {
        str += "\"lastPageOffset\":" + lastPageOffset + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastPageOffset\":null, ";
      }
      if(lastPageIndex) {
        str += "\"lastPageIndex\":" + lastPageIndex + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastPageIndex\":null, ";
      }
      if(firstActionEnabled) {
        str += "\"firstActionEnabled\":" + firstActionEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"firstActionEnabled\":null, ";
      }
      if(previousActionEnabled) {
        str += "\"previousActionEnabled\":" + previousActionEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"previousActionEnabled\":null, ";
      }
      if(nextActionEnabled) {
        str += "\"nextActionEnabled\":" + nextActionEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nextActionEnabled\":null, ";
      }
      if(lastActionEnabled) {
        str += "\"lastActionEnabled\":" + lastActionEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastActionEnabled\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "pagerMode:" + pagerMode + ", ";
      str += "primaryOrdering:" + primaryOrdering + ", ";
      str += "secondaryOrdering:" + secondaryOrdering + ", ";
      str += "currentOffset:" + currentOffset + ", ";
      str += "currentPage:" + currentPage + ", ";
      str += "pageSize:" + pageSize + ", ";
      str += "totalCount:" + totalCount + ", ";
      str += "lowerBoundTotalCount:" + lowerBoundTotalCount + ", ";
      str += "previousPageOffset:" + previousPageOffset + ", ";
      str += "nextPageOffset:" + nextPageOffset + ", ";
      str += "lastPageOffset:" + lastPageOffset + ", ";
      str += "lastPageIndex:" + lastPageIndex + ", ";
      str += "firstActionEnabled:" + firstActionEnabled + ", ";
      str += "previousActionEnabled:" + previousActionEnabled + ", ";
      str += "nextActionEnabled:" + nextActionEnabled + ", ";
      str += "lastActionEnabled:" + lastActionEnabled + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.bean.PagerStateStructJsBean.create = function(obj) {
  var o = new cronbay.wa.bean.PagerStateStructJsBean();

  if(obj.pagerMode) {
    o.setPagerMode(obj.pagerMode);
  }
  if(obj.primaryOrdering) {
    o.setPrimaryOrdering(obj.primaryOrdering);
  }
  if(obj.secondaryOrdering) {
    o.setSecondaryOrdering(obj.secondaryOrdering);
  }
  if(obj.currentOffset) {
    o.setCurrentOffset(obj.currentOffset);
  }
  if(obj.currentPage) {
    o.setCurrentPage(obj.currentPage);
  }
  if(obj.pageSize) {
    o.setPageSize(obj.pageSize);
  }
  if(obj.totalCount) {
    o.setTotalCount(obj.totalCount);
  }
  if(obj.lowerBoundTotalCount) {
    o.setLowerBoundTotalCount(obj.lowerBoundTotalCount);
  }
  if(obj.previousPageOffset) {
    o.setPreviousPageOffset(obj.previousPageOffset);
  }
  if(obj.nextPageOffset) {
    o.setNextPageOffset(obj.nextPageOffset);
  }
  if(obj.lastPageOffset) {
    o.setLastPageOffset(obj.lastPageOffset);
  }
  if(obj.lastPageIndex) {
    o.setLastPageIndex(obj.lastPageIndex);
  }
  if(obj.firstActionEnabled) {
    o.setFirstActionEnabled(obj.firstActionEnabled);
  }
  if(obj.previousActionEnabled) {
    o.setPreviousActionEnabled(obj.previousActionEnabled);
  }
  if(obj.nextActionEnabled) {
    o.setNextActionEnabled(obj.nextActionEnabled);
  }
  if(obj.lastActionEnabled) {
    o.setLastActionEnabled(obj.lastActionEnabled);
  }
  if(obj.note) {
    o.setNote(obj.note);
  }
    
  return o;
};

cronbay.wa.bean.PagerStateStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.bean.PagerStateStructJsBean.create(jsonObj);
  return obj;
};
