//////////////////////////////////////////////////////////
// <script src="/js/bean/fivetenjsbean-1.0.js"></script>
// Last modified time: 1337996953475.
//////////////////////////////////////////////////////////

var cronbay = cronbay || {};
cronbay.wa = cronbay.wa || {};
cronbay.wa.bean = cronbay.wa.bean || {};
cronbay.wa.bean.FiveTenJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var counter;
    var requesterIpAddress;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getCounter = function() { return counter; };
    this.setCounter = function(value) { counter = value; };
    this.getRequesterIpAddress = function() { return requesterIpAddress; };
    this.setRequesterIpAddress = function(value) { requesterIpAddress = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new cronbay.wa.bean.FiveTenJsBean();

      o.setGuid(generateUuid());
      if(counter) {
        o.setCounter(counter);
      }
      if(requesterIpAddress) {
        o.setRequesterIpAddress(requesterIpAddress);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(counter) {
        jsonObj.counter = counter;
      } // Otherwise ignore...
      if(requesterIpAddress) {
        jsonObj.requesterIpAddress = requesterIpAddress;
      } // Otherwise ignore...
      if(createdTime) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(counter) {
        str += "\"counter\":" + counter + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"counter\":null, ";
      }
      if(requesterIpAddress) {
        str += "\"requesterIpAddress\":\"" + requesterIpAddress + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"requesterIpAddress\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "counter:" + counter + ", ";
      str += "requesterIpAddress:" + requesterIpAddress + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

cronbay.wa.bean.FiveTenJsBean.create = function(obj) {
  var o = new cronbay.wa.bean.FiveTenJsBean();

  if(obj.guid) {
    o.setGuid(obj.guid);
  }
  if(obj.counter) {
    o.setCounter(obj.counter);
  }
  if(obj.requesterIpAddress) {
    o.setRequesterIpAddress(obj.requesterIpAddress);
  }
  if(obj.createdTime) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

cronbay.wa.bean.FiveTenJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = cronbay.wa.bean.FiveTenJsBean.create(jsonObj);
  return obj;
};
