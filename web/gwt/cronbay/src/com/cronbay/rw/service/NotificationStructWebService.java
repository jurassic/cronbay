package com.cronbay.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.NotificationStruct;
import com.cronbay.af.bean.NotificationStructBean;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.NotificationStructJsBean;
import com.cronbay.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class NotificationStructWebService // implements NotificationStructService
{
    private static final Logger log = Logger.getLogger(NotificationStructWebService.class.getName());
     
    public static NotificationStructJsBean convertNotificationStructToJsBean(NotificationStruct notificationStruct)
    {
        NotificationStructJsBean jsBean = new NotificationStructJsBean();
        if(notificationStruct != null) {
            jsBean.setPreferredMode(notificationStruct.getPreferredMode());
            jsBean.setMobileNumber(notificationStruct.getMobileNumber());
            jsBean.setEmailAddress(notificationStruct.getEmailAddress());
            jsBean.setCallbackUrl(notificationStruct.getCallbackUrl());
            jsBean.setNote(notificationStruct.getNote());
        }
        return jsBean;
    }

    public static NotificationStruct convertNotificationStructJsBeanToBean(NotificationStructJsBean jsBean)
    {
        NotificationStructBean notificationStruct = new NotificationStructBean();
        if(jsBean != null) {
            notificationStruct.setPreferredMode(jsBean.getPreferredMode());
            notificationStruct.setMobileNumber(jsBean.getMobileNumber());
            notificationStruct.setEmailAddress(jsBean.getEmailAddress());
            notificationStruct.setCallbackUrl(jsBean.getCallbackUrl());
            notificationStruct.setNote(jsBean.getNote());
        }
        return notificationStruct;
    }

}
