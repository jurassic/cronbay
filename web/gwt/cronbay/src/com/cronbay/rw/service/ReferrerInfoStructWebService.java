package com.cronbay.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.af.bean.ReferrerInfoStructBean;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.ReferrerInfoStructJsBean;
import com.cronbay.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ReferrerInfoStructWebService // implements ReferrerInfoStructService
{
    private static final Logger log = Logger.getLogger(ReferrerInfoStructWebService.class.getName());
     
    public static ReferrerInfoStructJsBean convertReferrerInfoStructToJsBean(ReferrerInfoStruct referrerInfoStruct)
    {
        ReferrerInfoStructJsBean jsBean = new ReferrerInfoStructJsBean();
        if(referrerInfoStruct != null) {
            jsBean.setReferer(referrerInfoStruct.getReferer());
            jsBean.setUserAgent(referrerInfoStruct.getUserAgent());
            jsBean.setLanguage(referrerInfoStruct.getLanguage());
            jsBean.setHostname(referrerInfoStruct.getHostname());
            jsBean.setIpAddress(referrerInfoStruct.getIpAddress());
            jsBean.setNote(referrerInfoStruct.getNote());
        }
        return jsBean;
    }

    public static ReferrerInfoStruct convertReferrerInfoStructJsBeanToBean(ReferrerInfoStructJsBean jsBean)
    {
        ReferrerInfoStructBean referrerInfoStruct = new ReferrerInfoStructBean();
        if(jsBean != null) {
            referrerInfoStruct.setReferer(jsBean.getReferer());
            referrerInfoStruct.setUserAgent(jsBean.getUserAgent());
            referrerInfoStruct.setLanguage(jsBean.getLanguage());
            referrerInfoStruct.setHostname(jsBean.getHostname());
            referrerInfoStruct.setIpAddress(jsBean.getIpAddress());
            referrerInfoStruct.setNote(jsBean.getNote());
        }
        return referrerInfoStruct;
    }

}
