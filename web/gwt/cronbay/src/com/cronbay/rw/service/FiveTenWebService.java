package com.cronbay.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.FiveTen;
import com.cronbay.af.bean.FiveTenBean;
import com.cronbay.af.service.FiveTenService;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.FiveTenJsBean;
import com.cronbay.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FiveTenWebService // implements FiveTenService
{
    private static final Logger log = Logger.getLogger(FiveTenWebService.class.getName());
     
    // Af service interface.
    private FiveTenService mService = null;

    public FiveTenWebService()
    {
        this(ServiceProxyFactory.getInstance().getFiveTenServiceProxy());
    }
    public FiveTenWebService(FiveTenService service)
    {
        mService = service;
    }
    
    private FiveTenService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getFiveTenServiceProxy();
        }
        return mService;
    }
    
    
    public FiveTenJsBean getFiveTen(String guid) throws WebException
    {
        try {
            FiveTen fiveTen = getService().getFiveTen(guid);
            FiveTenJsBean bean = convertFiveTenToJsBean(fiveTen);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getFiveTen(String guid, String field) throws WebException
    {
        try {
            return getService().getFiveTen(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FiveTenJsBean> getFiveTens(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<FiveTenJsBean> jsBeans = new ArrayList<FiveTenJsBean>();
            List<FiveTen> fiveTens = getService().getFiveTens(guids);
            if(fiveTens != null) {
                for(FiveTen fiveTen : fiveTens) {
                    jsBeans.add(convertFiveTenToJsBean(fiveTen));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FiveTenJsBean> getAllFiveTens() throws WebException
    {
        return getAllFiveTens(null, null, null);
    }

    public List<FiveTenJsBean> getAllFiveTens(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<FiveTenJsBean> jsBeans = new ArrayList<FiveTenJsBean>();
            List<FiveTen> fiveTens = getService().getAllFiveTens(ordering, offset, count);
            if(fiveTens != null) {
                for(FiveTen fiveTen : fiveTens) {
                    jsBeans.add(convertFiveTenToJsBean(fiveTen));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllFiveTenKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FiveTenJsBean> findFiveTens(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }

    public List<FiveTenJsBean> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<FiveTenJsBean> jsBeans = new ArrayList<FiveTenJsBean>();
            List<FiveTen> fiveTens = getService().findFiveTens(filter, ordering, params, values, grouping, unique, offset, count);
            if(fiveTens != null) {
                for(FiveTen fiveTen : fiveTens) {
                    jsBeans.add(convertFiveTenToJsBean(fiveTen));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFiveTen(Integer counter, String requesterIpAddress) throws WebException
    {
        try {
            return getService().createFiveTen(counter, requesterIpAddress);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFiveTen(FiveTenJsBean jsBean) throws WebException
    {
        try {
            FiveTen fiveTen = convertFiveTenJsBeanToBean(jsBean);
            return getService().createFiveTen(fiveTen);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FiveTenJsBean constructFiveTen(FiveTenJsBean jsBean) throws WebException
    {
        try {
            FiveTen fiveTen = convertFiveTenJsBeanToBean(jsBean);
            fiveTen = getService().constructFiveTen(fiveTen);
            jsBean = convertFiveTenToJsBean(fiveTen);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws WebException
    {
        try {
            return getService().updateFiveTen(guid, counter, requesterIpAddress);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateFiveTen(FiveTenJsBean jsBean) throws WebException
    {
        try {
            FiveTen fiveTen = convertFiveTenJsBeanToBean(jsBean);
            return getService().updateFiveTen(fiveTen);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FiveTenJsBean refreshFiveTen(FiveTenJsBean jsBean) throws WebException
    {
        try {
            FiveTen fiveTen = convertFiveTenJsBeanToBean(jsBean);
            fiveTen = getService().refreshFiveTen(fiveTen);
            jsBean = convertFiveTenToJsBean(fiveTen);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFiveTen(String guid) throws WebException
    {
        try {
            return getService().deleteFiveTen(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFiveTen(FiveTenJsBean jsBean) throws WebException
    {
        try {
            FiveTen fiveTen = convertFiveTenJsBeanToBean(jsBean);
            return getService().deleteFiveTen(fiveTen);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteFiveTens(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteFiveTens(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static FiveTenJsBean convertFiveTenToJsBean(FiveTen fiveTen)
    {
        FiveTenJsBean jsBean = new FiveTenJsBean();
        if(fiveTen != null) {
            jsBean.setGuid(fiveTen.getGuid());
            jsBean.setCounter(fiveTen.getCounter());
            jsBean.setRequesterIpAddress(fiveTen.getRequesterIpAddress());
            jsBean.setCreatedTime(fiveTen.getCreatedTime());
            jsBean.setModifiedTime(fiveTen.getModifiedTime());
        }
        return jsBean;
    }

    public static FiveTen convertFiveTenJsBeanToBean(FiveTenJsBean jsBean)
    {
        FiveTenBean fiveTen = new FiveTenBean();
        if(jsBean != null) {
            fiveTen.setGuid(jsBean.getGuid());
            fiveTen.setCounter(jsBean.getCounter());
            fiveTen.setRequesterIpAddress(jsBean.getRequesterIpAddress());
            fiveTen.setCreatedTime(jsBean.getCreatedTime());
            fiveTen.setModifiedTime(jsBean.getModifiedTime());
        }
        return fiveTen;
    }

}
