package com.cronbay.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.af.bean.CronScheduleStructBean;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.CronScheduleStructJsBean;
import com.cronbay.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class CronScheduleStructWebService // implements CronScheduleStructService
{
    private static final Logger log = Logger.getLogger(CronScheduleStructWebService.class.getName());
     
    public static CronScheduleStructJsBean convertCronScheduleStructToJsBean(CronScheduleStruct cronScheduleStruct)
    {
        CronScheduleStructJsBean jsBean = new CronScheduleStructJsBean();
        if(cronScheduleStruct != null) {
            jsBean.setType(cronScheduleStruct.getType());
            jsBean.setSchedule(cronScheduleStruct.getSchedule());
            jsBean.setTimezone(cronScheduleStruct.getTimezone());
            jsBean.setMaxIterations(cronScheduleStruct.getMaxIterations());
            jsBean.setRepeatInterval(cronScheduleStruct.getRepeatInterval());
            jsBean.setFirtRunTime(cronScheduleStruct.getFirtRunTime());
            jsBean.setStartTime(cronScheduleStruct.getStartTime());
            jsBean.setEndTime(cronScheduleStruct.getEndTime());
            jsBean.setNote(cronScheduleStruct.getNote());
        }
        return jsBean;
    }

    public static CronScheduleStruct convertCronScheduleStructJsBeanToBean(CronScheduleStructJsBean jsBean)
    {
        CronScheduleStructBean cronScheduleStruct = new CronScheduleStructBean();
        if(jsBean != null) {
            cronScheduleStruct.setType(jsBean.getType());
            cronScheduleStruct.setSchedule(jsBean.getSchedule());
            cronScheduleStruct.setTimezone(jsBean.getTimezone());
            cronScheduleStruct.setMaxIterations(jsBean.getMaxIterations());
            cronScheduleStruct.setRepeatInterval(jsBean.getRepeatInterval());
            cronScheduleStruct.setFirtRunTime(jsBean.getFirtRunTime());
            cronScheduleStruct.setStartTime(jsBean.getStartTime());
            cronScheduleStruct.setEndTime(jsBean.getEndTime());
            cronScheduleStruct.setNote(jsBean.getNote());
        }
        return cronScheduleStruct;
    }

}
