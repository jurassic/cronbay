package com.cronbay.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.af.bean.JobOutputBean;
import com.cronbay.af.service.JobOutputService;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.GaeAppStructJsBean;
import com.cronbay.fe.bean.JobOutputJsBean;
import com.cronbay.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class JobOutputWebService // implements JobOutputService
{
    private static final Logger log = Logger.getLogger(JobOutputWebService.class.getName());
     
    // Af service interface.
    private JobOutputService mService = null;

    public JobOutputWebService()
    {
        this(ServiceProxyFactory.getInstance().getJobOutputServiceProxy());
    }
    public JobOutputWebService(JobOutputService service)
    {
        mService = service;
    }
    
    private JobOutputService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getJobOutputServiceProxy();
        }
        return mService;
    }
    
    
    public JobOutputJsBean getJobOutput(String guid) throws WebException
    {
        try {
            JobOutput jobOutput = getService().getJobOutput(guid);
            JobOutputJsBean bean = convertJobOutputToJsBean(jobOutput);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getJobOutput(String guid, String field) throws WebException
    {
        try {
            return getService().getJobOutput(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<JobOutputJsBean> getJobOutputs(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<JobOutputJsBean> jsBeans = new ArrayList<JobOutputJsBean>();
            List<JobOutput> jobOutputs = getService().getJobOutputs(guids);
            if(jobOutputs != null) {
                for(JobOutput jobOutput : jobOutputs) {
                    jsBeans.add(convertJobOutputToJsBean(jobOutput));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<JobOutputJsBean> getAllJobOutputs() throws WebException
    {
        return getAllJobOutputs(null, null, null);
    }

    public List<JobOutputJsBean> getAllJobOutputs(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<JobOutputJsBean> jsBeans = new ArrayList<JobOutputJsBean>();
            List<JobOutput> jobOutputs = getService().getAllJobOutputs(ordering, offset, count);
            if(jobOutputs != null) {
                for(JobOutput jobOutput : jobOutputs) {
                    jsBeans.add(convertJobOutputToJsBean(jobOutput));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllJobOutputKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllJobOutputKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<JobOutputJsBean> findJobOutputs(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findJobOutputs(filter, ordering, params, values, null, null, null, null);
    }

    public List<JobOutputJsBean> findJobOutputs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<JobOutputJsBean> jsBeans = new ArrayList<JobOutputJsBean>();
            List<JobOutput> jobOutputs = getService().findJobOutputs(filter, ordering, params, values, grouping, unique, offset, count);
            if(jobOutputs != null) {
                for(JobOutput jobOutput : jobOutputs) {
                    jsBeans.add(convertJobOutputToJsBean(jobOutput));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findJobOutputKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findJobOutputKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createJobOutput(String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws WebException
    {
        try {
            return getService().createJobOutput(managerApp, appAcl, GaeAppStructWebService.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createJobOutput(JobOutputJsBean jsBean) throws WebException
    {
        try {
            JobOutput jobOutput = convertJobOutputJsBeanToBean(jsBean);
            return getService().createJobOutput(jobOutput);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public JobOutputJsBean constructJobOutput(JobOutputJsBean jsBean) throws WebException
    {
        try {
            JobOutput jobOutput = convertJobOutputJsBeanToBean(jsBean);
            jobOutput = getService().constructJobOutput(jobOutput);
            jsBean = convertJobOutputToJsBean(jobOutput);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateJobOutput(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws WebException
    {
        try {
            return getService().updateJobOutput(guid, managerApp, appAcl, GaeAppStructWebService.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateJobOutput(JobOutputJsBean jsBean) throws WebException
    {
        try {
            JobOutput jobOutput = convertJobOutputJsBeanToBean(jsBean);
            return getService().updateJobOutput(jobOutput);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public JobOutputJsBean refreshJobOutput(JobOutputJsBean jsBean) throws WebException
    {
        try {
            JobOutput jobOutput = convertJobOutputJsBeanToBean(jsBean);
            jobOutput = getService().refreshJobOutput(jobOutput);
            jsBean = convertJobOutputToJsBean(jobOutput);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteJobOutput(String guid) throws WebException
    {
        try {
            return getService().deleteJobOutput(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteJobOutput(JobOutputJsBean jsBean) throws WebException
    {
        try {
            JobOutput jobOutput = convertJobOutputJsBeanToBean(jsBean);
            return getService().deleteJobOutput(jobOutput);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteJobOutputs(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteJobOutputs(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static JobOutputJsBean convertJobOutputToJsBean(JobOutput jobOutput)
    {
        JobOutputJsBean jsBean = new JobOutputJsBean();
        if(jobOutput != null) {
            jsBean.setGuid(jobOutput.getGuid());
            jsBean.setManagerApp(jobOutput.getManagerApp());
            jsBean.setAppAcl(jobOutput.getAppAcl());
            jsBean.setGaeApp(GaeAppStructWebService.convertGaeAppStructToJsBean(jobOutput.getGaeApp()));
            jsBean.setOwnerUser(jobOutput.getOwnerUser());
            jsBean.setUserAcl(jobOutput.getUserAcl());
            jsBean.setUser(jobOutput.getUser());
            jsBean.setCronJob(jobOutput.getCronJob());
            jsBean.setPreviousRun(jobOutput.getPreviousRun());
            jsBean.setPreviousTry(jobOutput.getPreviousTry());
            jsBean.setResponseCode(jobOutput.getResponseCode());
            jsBean.setOutputFormat(jobOutput.getOutputFormat());
            jsBean.setOutputText(jobOutput.getOutputText());
            jsBean.setOutputData(jobOutput.getOutputData());
            jsBean.setOutputHash(jobOutput.getOutputHash());
            jsBean.setPermalink(jobOutput.getPermalink());
            jsBean.setShortlink(jobOutput.getShortlink());
            jsBean.setResult(jobOutput.getResult());
            jsBean.setStatus(jobOutput.getStatus());
            jsBean.setExtra(jobOutput.getExtra());
            jsBean.setNote(jobOutput.getNote());
            jsBean.setRetryCounter(jobOutput.getRetryCounter());
            jsBean.setScheduledTime(jobOutput.getScheduledTime());
            jsBean.setProcessedTime(jobOutput.getProcessedTime());
            jsBean.setCreatedTime(jobOutput.getCreatedTime());
            jsBean.setModifiedTime(jobOutput.getModifiedTime());
        }
        return jsBean;
    }

    public static JobOutput convertJobOutputJsBeanToBean(JobOutputJsBean jsBean)
    {
        JobOutputBean jobOutput = new JobOutputBean();
        if(jsBean != null) {
            jobOutput.setGuid(jsBean.getGuid());
            jobOutput.setManagerApp(jsBean.getManagerApp());
            jobOutput.setAppAcl(jsBean.getAppAcl());
            jobOutput.setGaeApp(GaeAppStructWebService.convertGaeAppStructJsBeanToBean(jsBean.getGaeApp()));
            jobOutput.setOwnerUser(jsBean.getOwnerUser());
            jobOutput.setUserAcl(jsBean.getUserAcl());
            jobOutput.setUser(jsBean.getUser());
            jobOutput.setCronJob(jsBean.getCronJob());
            jobOutput.setPreviousRun(jsBean.getPreviousRun());
            jobOutput.setPreviousTry(jsBean.getPreviousTry());
            jobOutput.setResponseCode(jsBean.getResponseCode());
            jobOutput.setOutputFormat(jsBean.getOutputFormat());
            jobOutput.setOutputText(jsBean.getOutputText());
            jobOutput.setOutputData(jsBean.getOutputData());
            jobOutput.setOutputHash(jsBean.getOutputHash());
            jobOutput.setPermalink(jsBean.getPermalink());
            jobOutput.setShortlink(jsBean.getShortlink());
            jobOutput.setResult(jsBean.getResult());
            jobOutput.setStatus(jsBean.getStatus());
            jobOutput.setExtra(jsBean.getExtra());
            jobOutput.setNote(jsBean.getNote());
            jobOutput.setRetryCounter(jsBean.getRetryCounter());
            jobOutput.setScheduledTime(jsBean.getScheduledTime());
            jobOutput.setProcessedTime(jsBean.getProcessedTime());
            jobOutput.setCreatedTime(jsBean.getCreatedTime());
            jobOutput.setModifiedTime(jsBean.getModifiedTime());
        }
        return jobOutput;
    }

}
