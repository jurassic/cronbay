package com.cronbay.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.af.bean.GaeAppStructBean;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.GaeAppStructJsBean;
import com.cronbay.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class GaeAppStructWebService // implements GaeAppStructService
{
    private static final Logger log = Logger.getLogger(GaeAppStructWebService.class.getName());
     
    public static GaeAppStructJsBean convertGaeAppStructToJsBean(GaeAppStruct gaeAppStruct)
    {
        GaeAppStructJsBean jsBean = new GaeAppStructJsBean();
        if(gaeAppStruct != null) {
            jsBean.setGroupId(gaeAppStruct.getGroupId());
            jsBean.setAppId(gaeAppStruct.getAppId());
            jsBean.setAppDomain(gaeAppStruct.getAppDomain());
            jsBean.setNamespace(gaeAppStruct.getNamespace());
            jsBean.setAcl(gaeAppStruct.getAcl());
            jsBean.setNote(gaeAppStruct.getNote());
        }
        return jsBean;
    }

    public static GaeAppStruct convertGaeAppStructJsBeanToBean(GaeAppStructJsBean jsBean)
    {
        GaeAppStructBean gaeAppStruct = new GaeAppStructBean();
        if(jsBean != null) {
            gaeAppStruct.setGroupId(jsBean.getGroupId());
            gaeAppStruct.setAppId(jsBean.getAppId());
            gaeAppStruct.setAppDomain(jsBean.getAppDomain());
            gaeAppStruct.setNamespace(jsBean.getNamespace());
            gaeAppStruct.setAcl(jsBean.getAcl());
            gaeAppStruct.setNote(jsBean.getNote());
        }
        return gaeAppStruct;
    }

}
