package com.cronbay.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.af.bean.GaeUserStructBean;
import com.cronbay.fe.WebException;
import com.cronbay.fe.bean.GaeUserStructJsBean;
import com.cronbay.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class GaeUserStructWebService // implements GaeUserStructService
{
    private static final Logger log = Logger.getLogger(GaeUserStructWebService.class.getName());
     
    public static GaeUserStructJsBean convertGaeUserStructToJsBean(GaeUserStruct gaeUserStruct)
    {
        GaeUserStructJsBean jsBean = new GaeUserStructJsBean();
        if(gaeUserStruct != null) {
            jsBean.setAuthDomain(gaeUserStruct.getAuthDomain());
            jsBean.setFederatedIdentity(gaeUserStruct.getFederatedIdentity());
            jsBean.setNickname(gaeUserStruct.getNickname());
            jsBean.setUserId(gaeUserStruct.getUserId());
            jsBean.setEmail(gaeUserStruct.getEmail());
            jsBean.setNote(gaeUserStruct.getNote());
        }
        return jsBean;
    }

    public static GaeUserStruct convertGaeUserStructJsBeanToBean(GaeUserStructJsBean jsBean)
    {
        GaeUserStructBean gaeUserStruct = new GaeUserStructBean();
        if(jsBean != null) {
            gaeUserStruct.setAuthDomain(jsBean.getAuthDomain());
            gaeUserStruct.setFederatedIdentity(jsBean.getFederatedIdentity());
            gaeUserStruct.setNickname(jsBean.getNickname());
            gaeUserStruct.setUserId(jsBean.getUserId());
            gaeUserStruct.setEmail(jsBean.getEmail());
            gaeUserStruct.setNote(jsBean.getNote());
        }
        return gaeUserStruct;
    }

}
