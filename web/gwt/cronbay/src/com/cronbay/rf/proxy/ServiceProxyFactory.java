package com.cronbay.rf.proxy;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ServiceProxyFactory
{
    private static final Logger log = Logger.getLogger(ServiceProxyFactory.class.getName());

    private ApiConsumerServiceProxy apiConsumerService = null;
    private UserServiceProxy userService = null;
    private CronJobServiceProxy cronJobService = null;
    private JobOutputServiceProxy jobOutputService = null;
    private ServiceInfoServiceProxy serviceInfoService = null;
    private FiveTenServiceProxy fiveTenService = null;

    private ServiceProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ServiceProxyFactoryHolder
    {
        private static final ServiceProxyFactory INSTANCE = new ServiceProxyFactory();
    }

    // Singleton method
    public static ServiceProxyFactory getInstance()
    {
        return ServiceProxyFactoryHolder.INSTANCE;
    }

    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        if(apiConsumerService == null) {
            apiConsumerService = new ApiConsumerServiceProxy();
        }
        return apiConsumerService;
    }

    public UserServiceProxy getUserServiceProxy()
    {
        if(userService == null) {
            userService = new UserServiceProxy();
        }
        return userService;
    }

    public CronJobServiceProxy getCronJobServiceProxy()
    {
        if(cronJobService == null) {
            cronJobService = new CronJobServiceProxy();
        }
        return cronJobService;
    }

    public JobOutputServiceProxy getJobOutputServiceProxy()
    {
        if(jobOutputService == null) {
            jobOutputService = new JobOutputServiceProxy();
        }
        return jobOutputService;
    }

    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new ServiceInfoServiceProxy();
        }
        return serviceInfoService;
    }

    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenService == null) {
            fiveTenService = new FiveTenServiceProxy();
        }
        return fiveTenService;
    }

}
