package com.cronbay.cert.ws;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseConsumerRegistry.class.getName());

    // Consumer key-secret map.
    // TBD: Use a better data structure which reflects OAuthConsumerInfo.
    private Map<String, String> consumerSecretMap;
    
    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD...
        consumerSecretMap.put("105d590c-1335-4afe-83cb-88f02756f7f7", "850cb260-27b6-4701-bf05-7cb4e6509026");  // CronBay + CronBayApp
        consumerSecretMap.put("3f5296cb-51b6-4464-90e8-53f4a27dfea6", "1a373da6-2ae3-4053-9fdf-f5076625658f");  // CronBay + QueryClient
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
