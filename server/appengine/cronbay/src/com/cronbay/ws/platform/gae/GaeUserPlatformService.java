package com.cronbay.ws.platform.gae;

import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;

import com.cronbay.ws.platform.UserPlatformService;
import com.cronbay.ws.platform.Patron;


// GAE Platform service.
public class GaeUserPlatformService extends UserPlatformService
{
    private static final Logger log = Logger.getLogger(GaeUserPlatformService.class.getName());

    private GaeUserPlatformService()
    {
    }

    // Initialization-on-demand holder.
    private static class GaeUserPlatformServiceHolder
    {
        private static final GaeUserPlatformService INSTANCE = new GaeUserPlatformService();
    }

    // Singleton method
    public static GaeUserPlatformService getInstance()
    {
        return GaeUserPlatformServiceHolder.INSTANCE;
    }


    @Override
    public Patron getCurrentPatron()
    {
        User user = UserServiceFactory.getUserService().getCurrentUser();
        Patron patron = new GaePatron(user);
        return patron;
    }

    @Override
    public Patron authenticate()
    {
        // TBD:
        return null;
    }

    @Override
    public Patron authenticateThroughApp()
    {
        // TBD:
        return null;
    }

    @Override
    public Patron authenticateThroughGoogle()
    {
        // TBD:
        return null;
    }

    @Override
    public Patron authenticateThroughOpenID()
    {
        // TBD:
        return null;
    }

    @Override
    public Patron authenticateThroughOpenID(String federatedIdentity, Set<String> attributesRequest)
    {
        // TBD:
        return null;
    }


    @Override
    public String createLoginURL(String authMode, String destinationURL)
    {
        // TBD: Check authMode == "google" ...
        String loginURL = UserServiceFactory.getUserService().createLoginURL(destinationURL);
        return loginURL;
    }

    @Override
    public String createLoginURL(String authMode, String destinationURL, String authDomain)
    {
        // TBD: Check authMode == "google" ...
        String loginURL = UserServiceFactory.getUserService().createLoginURL(destinationURL, authDomain);
        return loginURL;
    }

    @Override
    public String createLoginURL(String authMode, String destinationURL, String authDomain, String federatedIdentity, Set<String> attributesRequest)
    {
        // TBD: Check authMode == "google" ...
        String loginURL = UserServiceFactory.getUserService().createLoginURL(destinationURL, authDomain, federatedIdentity, attributesRequest);
        return loginURL;
    }

    @Override
    public String createLogoutURL(String destinationURL)
    {
        String logoutURL = UserServiceFactory.getUserService().createLoginURL(destinationURL);
        return logoutURL;
    }

    @Override
    public String createLogoutURL(String destinationURL, String authDomain)
    {
        String logoutURL = UserServiceFactory.getUserService().createLoginURL(destinationURL, authDomain);
        return logoutURL;
    }


    @Override
    public boolean isPatronAuthenticated()
    {
        // TBD: Check all authModes ????
        boolean patronAuthenticated = UserServiceFactory.getUserService().isUserLoggedIn();
        return patronAuthenticated;
    }

    @Override
    public List<String> getPatronRoles()
    {
        // TBD:
        return null;
    }

    @Override
    public boolean isPatronAdmin()
    {
        // TBD: Check all authModes ????
        boolean patronAdmin = UserServiceFactory.getUserService().isUserAdmin();
        return patronAdmin;
    }


}
