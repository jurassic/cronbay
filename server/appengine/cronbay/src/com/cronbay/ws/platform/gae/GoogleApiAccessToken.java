package com.cronbay.ws.platform.gae;

import com.google.appengine.api.appidentity.AppIdentityService;

public class GoogleApiAccessToken
{
    private String accessToken;
    private Long expirationTime;

    public GoogleApiAccessToken(String accessToken, Long expirationTime)
    {
        this.accessToken = accessToken;
        this.expirationTime = expirationTime;
    }

    public GoogleApiAccessToken(AppIdentityService.GetAccessTokenResult accessTokenResult)
    {
        this.accessToken = accessTokenResult.getAccessToken();
        this.expirationTime = accessTokenResult.getExpirationTime().getTime();
    }

    
    public String getAccessToken()
    {
        return accessToken;
    }
    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    public Long getExpirationTime()
    {
        return expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }
    
    
}
