package com.cronbay.ws.platform;

public class X509PublicCertificate
{
    private String certificateName;
    private String certificateInPemFormat;

    public X509PublicCertificate(String certificateName,
            String certificateInPemFormat)
    {
        super();
        this.certificateName = certificateName;
        this.certificateInPemFormat = certificateInPemFormat;
    }


    public String getCertificateName()
    {
        return certificateName;
    }
    public void setCertificateName(String certificateName)
    {
        this.certificateName = certificateName;
    }

    public String getCertificateInPemFormat()
    {
        return certificateInPemFormat;
    }

    public void setCertificateInPemFormat(String certificateInPemFormat)
    {
        this.certificateInPemFormat = certificateInPemFormat;
    }
    
    
}
