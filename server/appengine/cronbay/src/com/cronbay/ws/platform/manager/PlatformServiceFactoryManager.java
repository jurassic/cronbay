package com.cronbay.ws.platform.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.platform.PlatformServiceFactory;
import com.cronbay.ws.platform.gae.GaePlatformServiceFactory;


// We use Abstract Factory pattern.
// This "manager" class provides a way to choose a concrete factory.
public final class PlatformServiceFactoryManager
{
    private static final Logger log = Logger.getLogger(PlatformServiceFactoryManager.class.getName());

    // Prevents instantiation.
    private PlatformServiceFactoryManager() {}

    // Returns a platform service factory.
    public static PlatformServiceFactory getPlatformServiceFactory() 
    {
        // For now, hard-coded.
        // TBD: Read it from a config.
        return GaePlatformServiceFactory.getInstance();
    }

}
