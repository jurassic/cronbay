package com.cronbay.ws.platform;

import java.util.List;


// Three-legged OAuth service.
public abstract class OAuthPlatformService
{
    // Returns the OAuth consumerKey from the current request.
    public abstract String getConsumerKey();

    // Returns the user object.
    // Note: unlike GAE api, the returned api may not have been authenticated.
    public abstract Patron getCurrentPatron();
    public abstract Patron getCurrentPatron(String scope);

    // If forceAuth is set to true, it forces login screen to the user.
    public abstract Patron getCurrentPatron(boolean forceAuth);
    public abstract Patron getCurrentPatron(String scope, boolean forceAuth);

    // Authenticate the user.
    public abstract Patron authenticate(Patron patron);

    // Returns the list of roles associated with a current (authenticated) user.
    public abstract List<String> getPatronRoles();

    // Returns true if the current patron is "admin".
    // Amdin is a (predefined) special role (for GAE compatibility).
    public abstract boolean isPatronAdmin();

    // ...

}
