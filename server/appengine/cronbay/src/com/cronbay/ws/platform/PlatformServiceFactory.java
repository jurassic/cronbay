package com.cronbay.ws.platform;


// TBD:
// DataStore ???
// TaskQueue ???
// BlobStore ???
// Image service ???
// ...
public abstract class PlatformServiceFactory
{
    public abstract AppIdentityPlatformService getAppIdentityPlatformService();
    public abstract ConfigPlatformService getConfigPlatformService();
    public abstract MemcachePlatformService getMemcachePlatformService();
    public abstract UserPlatformService getUserPlatformService();
    public abstract OAuthPlatformService getOAuthPlatformService();
    public abstract MailPlatformService getMailPlatformService();
    public abstract MessagingPlatformService getMessagingPlatformService();
    // ...

}
