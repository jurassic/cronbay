package com.cronbay.ws.platform;

import java.util.List;


// Platform service.
public abstract class AppIdentityPlatformService
{
    // ...

    public abstract String getAppId();
    public abstract List<X509PublicCertificate> getPublicCertificatesForApp();
    public abstract SignedKey signForApp(byte[] signBlob);
    // ...

}
