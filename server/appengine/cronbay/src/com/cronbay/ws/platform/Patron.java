package com.cronbay.ws.platform;

import java.util.UUID;


// TBD:
// "Patron" is a base class for User or Principal...
public class Patron
{
    private String guid;

    public Patron(String guid)
    {
        if(guid != null) {
            this.guid = guid;
        } else {
            this.guid = UUID.randomUUID().toString();
        }
    }

    public String getGuid()
    {
        return guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }
    
    // temporary
    public boolean isAuthenticated()
    {
        // TBD:
        return false;
    }

}
