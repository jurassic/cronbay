package com.cronbay.ws.platform.gae;

import com.google.appengine.api.users.User;

import com.cronbay.ws.platform.Patron;


public class GaePatron extends Patron
{
    private User user = null;

    public GaePatron(String guid)
    {
        super(guid);
    }

    public GaePatron(User user)
    {
        super(null);
        this.user = user;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }


}
