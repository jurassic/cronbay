package com.cronbay.ws.platform.gae;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.google.appengine.api.oauth.OAuthService;
import com.google.appengine.api.oauth.OAuthRequestException;
import com.google.appengine.api.oauth.OAuthServiceFactory;
import com.google.appengine.api.users.User;

import com.cronbay.ws.platform.OAuthPlatformService;
import com.cronbay.ws.platform.Patron;


// GAE Platform service.
public class GaeOAuthPlatformService extends OAuthPlatformService
{
    private static final Logger log = Logger.getLogger(GaeOAuthPlatformService.class.getName());

    private GaeOAuthPlatformService()
    {
    }

    // Initialization-on-demand holder.
    private static class GaeOAuthPlatformServiceHolder
    {
        private static final GaeOAuthPlatformService INSTANCE = new GaeOAuthPlatformService();
    }

    // Singleton method
    public static GaeOAuthPlatformService getInstance()
    {
        return GaeOAuthPlatformServiceHolder.INSTANCE;
    }


    @Override
    public String getConsumerKey()
    {
        String consumerKey = null;
        try {
            consumerKey = OAuthServiceFactory.getOAuthService().getOAuthConsumerKey();
        } catch (OAuthRequestException e) {
            log.log(Level.INFO, "Failed to get consumerKey.", e);
        }
        return consumerKey;
    }

    @Override
    public Patron getCurrentPatron()
    {
        Patron patron = null;
        try {
            User user = OAuthServiceFactory.getOAuthService().getCurrentUser();
            if(user != null) {
                patron = new GaePatron(user);
            }
        } catch (OAuthRequestException e) {
            log.log(Level.INFO, "Failed to get user.", e);
        }
        return patron;
    }

    @Override
    public Patron getCurrentPatron(String scope)
    {
        Patron patron = null;
        try {
            User user = OAuthServiceFactory.getOAuthService().getCurrentUser(scope);
            if(user != null) {
                patron = new GaePatron(user);
            }
        } catch (OAuthRequestException e) {
            log.log(Level.INFO, "Failed to get user.", e);
        }
        return patron;
    }

    @Override
    public Patron getCurrentPatron(boolean forceAuth)
    {
        // TBD:
        return null;
    }

    @Override
    public Patron getCurrentPatron(String scope, boolean forceAuth)
    {
        // TBD:
        return null;
    }

    @Override
    public Patron authenticate(Patron patron)
    {
        // TBD:
        return null;
    }

    @Override
    public List<String> getPatronRoles()
    {
        // TBD:
        return null;
    }

    @Override
    public boolean isPatronAdmin()
    {
        boolean patronAdmin = false;
        try {
            patronAdmin = OAuthServiceFactory.getOAuthService().isUserAdmin();
        } catch (OAuthRequestException e) {
            log.log(Level.INFO, "Failed to test user role..", e);
        }
        return patronAdmin;
    }


}
