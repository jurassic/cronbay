package com.cronbay.ws.resource.exception;

import com.cronbay.ws.exception.resource.BaseResourceException;


public class RequestConflictRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public RequestConflictRsException() 
    {
        super();
    }
    public RequestConflictRsException(String message) 
    {
        super(message);
    }
    public RequestConflictRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public RequestConflictRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public RequestConflictRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public RequestConflictRsException(Throwable cause) 
    {
        super(cause);
    }
    public RequestConflictRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
