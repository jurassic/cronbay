package com.cronbay.ws.resource.exception;

import com.cronbay.ws.exception.resource.BaseResourceException;


public class ResourceNotFoundRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public ResourceNotFoundRsException() 
    {
        super();
    }
    public ResourceNotFoundRsException(String message) 
    {
        super(message);
    }
    public ResourceNotFoundRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public ResourceNotFoundRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public ResourceNotFoundRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public ResourceNotFoundRsException(Throwable cause) 
    {
        super(cause);
    }
    public ResourceNotFoundRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
