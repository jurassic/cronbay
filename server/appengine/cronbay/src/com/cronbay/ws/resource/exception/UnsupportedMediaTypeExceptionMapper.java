package com.cronbay.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.cronbay.ws.stub.ErrorStub;

@Provider
public class UnsupportedMediaTypeExceptionMapper implements ExceptionMapper<UnsupportedMediaTypeRsException>
{
    public Response toResponse(UnsupportedMediaTypeRsException ex) {
        return Response.status(Status.UNSUPPORTED_MEDIA_TYPE)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
