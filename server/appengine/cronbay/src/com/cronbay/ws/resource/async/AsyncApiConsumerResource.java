package com.cronbay.ws.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.exception.InternalServerErrorException;
import com.cronbay.ws.exception.NotImplementedException;
import com.cronbay.ws.exception.RequestConflictException;
import com.cronbay.ws.exception.RequestForbiddenException;
import com.cronbay.ws.exception.DataStoreException;
import com.cronbay.ws.exception.ResourceGoneException;
import com.cronbay.ws.exception.ResourceNotFoundException;
import com.cronbay.ws.exception.ResourceAlreadyPresentException;
import com.cronbay.ws.exception.ServiceUnavailableException;
import com.cronbay.ws.exception.resource.BaseResourceException;
import com.cronbay.ws.resource.exception.BadRequestRsException;
import com.cronbay.ws.resource.exception.InternalServerErrorRsException;
import com.cronbay.ws.resource.exception.NotImplementedRsException;
import com.cronbay.ws.resource.exception.RequestConflictRsException;
import com.cronbay.ws.resource.exception.RequestForbiddenRsException;
import com.cronbay.ws.resource.exception.DataStoreRsException;
import com.cronbay.ws.resource.exception.ResourceGoneRsException;
import com.cronbay.ws.resource.exception.ResourceNotFoundRsException;
import com.cronbay.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.cronbay.ws.resource.exception.ServiceUnavailableRsException;

import com.cronbay.ws.ApiConsumer;
import com.cronbay.ws.bean.ApiConsumerBean;
import com.cronbay.ws.stub.ApiConsumerListStub;
import com.cronbay.ws.stub.ApiConsumerStub;
import com.cronbay.ws.resource.ServiceManager;
import com.cronbay.ws.resource.ApiConsumerResource;


@Path("/_task/w/apiConsumers/")
public class AsyncApiConsumerResource extends BaseAsyncResource implements ApiConsumerResource
{
    private static final Logger log = Logger.getLogger(AsyncApiConsumerResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncApiConsumerResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getApiConsumerList(List<ApiConsumer> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getApiConsumerKeys(List<String> guids) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getApiConsumer(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getApiConsumer(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response createApiConsumer(ApiConsumerStub apiConsumer) throws BaseResourceException
    {
        log.log(Level.INFO, "createApiConsumer(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            ApiConsumerBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ApiConsumerStub realStub = (ApiConsumerStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertApiConsumerStubToBean(realStub);
            } else {
                bean = convertApiConsumerStubToBean(apiConsumer);
            }
            String guid = ServiceManager.getApiConsumerService().createApiConsumer(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            log.log(Level.INFO, "createApiConsumer(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ResourceAlreadyPresentException ex) {
            throw new ResourceAlreadyPresentRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateApiConsumer(String guid, ApiConsumerStub apiConsumer) throws BaseResourceException
    {
        log.log(Level.INFO, "updateApiConsumer(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            if(apiConsumer == null || !guid.equals(apiConsumer.getGuid())) {
                log.log(Level.WARNING, "Path param guid = " + guid + " is different from apiConsumer guid = " + apiConsumer.getGuid());
                throw new RequestForbiddenException("Failed to update the apiConsumer with guid = " + guid);
            }
            ApiConsumerBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ApiConsumerStub realStub = (ApiConsumerStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertApiConsumerStubToBean(realStub);
            } else {
                bean = convertApiConsumerStubToBean(apiConsumer);
            }
            boolean suc = ServiceManager.getApiConsumerService().updateApiConsumer(bean);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to update the apiConsumer with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the apiConsumer with guid = " + guid);
            }
            log.log(Level.INFO, "updateApiConsumer(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateApiConsumer(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteApiConsumer(String guid) throws BaseResourceException
    {
        log.log(Level.INFO, "deleteApiConsumer(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            boolean suc = ServiceManager.getApiConsumerService().deleteApiConsumer(guid);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to delete the apiConsumer with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the apiConsumer with guid = " + guid);
            }
            log.log(Level.INFO, "deleteApiConsumer(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteApiConsumers(String filter, String params, List<String> values) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    public static ApiConsumerBean convertApiConsumerStubToBean(ApiConsumer stub)
    {
        ApiConsumerBean bean = new ApiConsumerBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setGuid(stub.getGuid());
            bean.setAeryId(stub.getAeryId());
            bean.setName(stub.getName());
            bean.setDescription(stub.getDescription());
            bean.setAppKey(stub.getAppKey());
            bean.setAppSecret(stub.getAppSecret());
            bean.setStatus(stub.getStatus());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
