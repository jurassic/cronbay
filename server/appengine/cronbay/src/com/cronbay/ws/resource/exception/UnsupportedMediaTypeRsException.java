package com.cronbay.ws.resource.exception;

import com.cronbay.ws.exception.resource.BaseResourceException;


public class UnsupportedMediaTypeRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public UnsupportedMediaTypeRsException() 
    {
        super();
    }
    public UnsupportedMediaTypeRsException(String message) 
    {
        super(message);
    }
    public UnsupportedMediaTypeRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public UnsupportedMediaTypeRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public UnsupportedMediaTypeRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public UnsupportedMediaTypeRsException(Throwable cause) 
    {
        super(cause);
    }
    public UnsupportedMediaTypeRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
