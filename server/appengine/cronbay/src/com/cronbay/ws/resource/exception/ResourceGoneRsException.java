package com.cronbay.ws.resource.exception;

import com.cronbay.ws.exception.resource.BaseResourceException;


public class ResourceGoneRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public ResourceGoneRsException() 
    {
        super();
    }
    public ResourceGoneRsException(String message) 
    {
        super(message);
    }
    public ResourceGoneRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public ResourceGoneRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public ResourceGoneRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public ResourceGoneRsException(Throwable cause) 
    {
        super(cause);
    }
    public ResourceGoneRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
