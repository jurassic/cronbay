package com.cronbay.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.cronbay.ws.stub.ErrorStub;

@Provider
public class InternalServerErrorExceptionMapper implements ExceptionMapper<InternalServerErrorRsException>
{
    public Response toResponse(InternalServerErrorRsException ex) {
        return Response.status(Status.INTERNAL_SERVER_ERROR)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
