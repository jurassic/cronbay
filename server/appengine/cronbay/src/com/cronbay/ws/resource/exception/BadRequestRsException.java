package com.cronbay.ws.resource.exception;

import com.cronbay.ws.exception.resource.BaseResourceException;


public class BadRequestRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public BadRequestRsException() 
    {
        super();
    }
    public BadRequestRsException(String message) 
    {
        super(message);
    }
    public BadRequestRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public BadRequestRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public BadRequestRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public BadRequestRsException(Throwable cause) 
    {
        super(cause);
    }
    public BadRequestRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
