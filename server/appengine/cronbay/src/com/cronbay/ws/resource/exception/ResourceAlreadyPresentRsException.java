package com.cronbay.ws.resource.exception;

import com.cronbay.ws.exception.resource.BaseResourceException;


public class ResourceAlreadyPresentRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public ResourceAlreadyPresentRsException() 
    {
        super();
    }
    public ResourceAlreadyPresentRsException(String message) 
    {
        super(message);
    }
    public ResourceAlreadyPresentRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public ResourceAlreadyPresentRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public ResourceAlreadyPresentRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public ResourceAlreadyPresentRsException(Throwable cause) 
    {
        super(cause);
    }
    public ResourceAlreadyPresentRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
