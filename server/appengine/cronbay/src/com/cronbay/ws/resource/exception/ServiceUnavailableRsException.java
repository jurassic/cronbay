package com.cronbay.ws.resource.exception;

import com.cronbay.ws.exception.resource.BaseResourceException;


public class ServiceUnavailableRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public ServiceUnavailableRsException() 
    {
        super();
    }
    public ServiceUnavailableRsException(String message) 
    {
        super(message);
    }
    public ServiceUnavailableRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public ServiceUnavailableRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public ServiceUnavailableRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public ServiceUnavailableRsException(Throwable cause) 
    {
        super(cause);
    }
    public ServiceUnavailableRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
