package com.cronbay.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.cronbay.ws.stub.ErrorStub;

@Provider
public class ResourceAlreadyPresentExceptionMapper implements ExceptionMapper<ResourceAlreadyPresentRsException>
{
    public Response toResponse(ResourceAlreadyPresentRsException ex) {
        return Response.status(Status.GONE)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
