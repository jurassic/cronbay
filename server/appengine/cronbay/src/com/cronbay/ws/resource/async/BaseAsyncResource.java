package com.cronbay.ws.resource.async;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

// Place holder, for now.
public abstract class BaseAsyncResource
{
    private static final Logger log = Logger.getLogger(BaseAsyncResource.class.getName());

    public BaseAsyncResource()
    {
    }

    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            //props.put(GCacheFactory.EXPIRATION_DELTA, ServiceConstants.CACHE_EXPIRATION_DURATION);
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

}
