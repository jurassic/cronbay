package com.cronbay.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.cronbay.ws.stub.ErrorStub;

@Provider
public class ResourceNotFoundExceptionMapper implements ExceptionMapper<ResourceNotFoundRsException>
{
    public Response toResponse(ResourceNotFoundRsException ex) {
        return Response.status(Status.NOT_FOUND)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
