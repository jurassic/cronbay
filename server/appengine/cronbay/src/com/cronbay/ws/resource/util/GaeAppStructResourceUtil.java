package com.cronbay.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.bean.GaeAppStructBean;
import com.cronbay.ws.stub.GaeAppStructStub;


public class GaeAppStructResourceUtil
{
    private static final Logger log = Logger.getLogger(GaeAppStructResourceUtil.class.getName());

    // Static methods only.
    private GaeAppStructResourceUtil() {}

    public static GaeAppStructBean convertGaeAppStructStubToBean(GaeAppStruct stub)
    {
        GaeAppStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null GaeAppStructBean is returned.");
        } else {
            bean = new GaeAppStructBean();
            bean.setGroupId(stub.getGroupId());
            bean.setAppId(stub.getAppId());
            bean.setAppDomain(stub.getAppDomain());
            bean.setNamespace(stub.getNamespace());
            bean.setAcl(stub.getAcl());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
