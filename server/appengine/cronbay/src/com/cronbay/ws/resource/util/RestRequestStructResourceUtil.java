package com.cronbay.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.bean.RestRequestStructBean;
import com.cronbay.ws.stub.RestRequestStructStub;


public class RestRequestStructResourceUtil
{
    private static final Logger log = Logger.getLogger(RestRequestStructResourceUtil.class.getName());

    // Static methods only.
    private RestRequestStructResourceUtil() {}

    public static RestRequestStructBean convertRestRequestStructStubToBean(RestRequestStruct stub)
    {
        RestRequestStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null RestRequestStructBean is returned.");
        } else {
            bean = new RestRequestStructBean();
            bean.setServiceName(stub.getServiceName());
            bean.setServiceUrl(stub.getServiceUrl());
            bean.setRequestMethod(stub.getRequestMethod());
            bean.setRequestUrl(stub.getRequestUrl());
            bean.setTargetEntity(stub.getTargetEntity());
            bean.setQueryString(stub.getQueryString());
            bean.setQueryParams(stub.getQueryParams());
            bean.setInputFormat(stub.getInputFormat());
            bean.setInputContent(stub.getInputContent());
            bean.setOutputFormat(stub.getOutputFormat());
            bean.setMaxRetries(stub.getMaxRetries());
            bean.setRetryInterval(stub.getRetryInterval());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
