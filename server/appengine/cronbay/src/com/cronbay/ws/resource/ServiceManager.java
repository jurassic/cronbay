package com.cronbay.ws.resource;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.service.ApiConsumerService;
import com.cronbay.ws.service.impl.ApiConsumerServiceImpl;
import com.cronbay.ws.service.UserService;
import com.cronbay.ws.service.impl.UserServiceImpl;
import com.cronbay.ws.service.CronJobService;
import com.cronbay.ws.service.impl.CronJobServiceImpl;
import com.cronbay.ws.service.JobOutputService;
import com.cronbay.ws.service.impl.JobOutputServiceImpl;
import com.cronbay.ws.service.ServiceInfoService;
import com.cronbay.ws.service.impl.ServiceInfoServiceImpl;
import com.cronbay.ws.service.FiveTenService;
import com.cronbay.ws.service.impl.FiveTenServiceImpl;


// TBD:
// Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

	private static ApiConsumerService apiConsumerService = null;
	private static UserService userService = null;
	private static CronJobService cronJobService = null;
	private static JobOutputService jobOutputService = null;
	private static ServiceInfoService serviceInfoService = null;
	private static FiveTenService fiveTenService = null;

    // Prevents instantiation.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(ServiceManager.apiConsumerService == null) {
            ServiceManager.apiConsumerService = new ApiConsumerServiceImpl();
        }
        return ServiceManager.apiConsumerService;
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(ServiceManager.userService == null) {
            ServiceManager.userService = new UserServiceImpl();
        }
        return ServiceManager.userService;
    }

    // Returns a CronJobService instance.
	public static CronJobService getCronJobService() 
    {
        if(ServiceManager.cronJobService == null) {
            ServiceManager.cronJobService = new CronJobServiceImpl();
        }
        return ServiceManager.cronJobService;
    }

    // Returns a JobOutputService instance.
	public static JobOutputService getJobOutputService() 
    {
        if(ServiceManager.jobOutputService == null) {
            ServiceManager.jobOutputService = new JobOutputServiceImpl();
        }
        return ServiceManager.jobOutputService;
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(ServiceManager.serviceInfoService == null) {
            ServiceManager.serviceInfoService = new ServiceInfoServiceImpl();
        }
        return ServiceManager.serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(ServiceManager.fiveTenService == null) {
            ServiceManager.fiveTenService = new FiveTenServiceImpl();
        }
        return ServiceManager.fiveTenService;
    }

}
