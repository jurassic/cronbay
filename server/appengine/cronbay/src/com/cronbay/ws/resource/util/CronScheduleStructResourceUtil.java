package com.cronbay.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.CommonConstants;
import com.cronbay.ws.core.GUID;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.bean.CronScheduleStructBean;
import com.cronbay.ws.stub.CronScheduleStructStub;


public class CronScheduleStructResourceUtil
{
    private static final Logger log = Logger.getLogger(CronScheduleStructResourceUtil.class.getName());

    // Static methods only.
    private CronScheduleStructResourceUtil() {}

    public static CronScheduleStructBean convertCronScheduleStructStubToBean(CronScheduleStruct stub)
    {
        CronScheduleStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null CronScheduleStructBean is returned.");
        } else {
            bean = new CronScheduleStructBean();
            bean.setType(stub.getType());
            bean.setSchedule(stub.getSchedule());
            bean.setTimezone(stub.getTimezone());
            bean.setMaxIterations(stub.getMaxIterations());
            bean.setRepeatInterval(stub.getRepeatInterval());
            bean.setFirtRunTime(stub.getFirtRunTime());
            bean.setStartTime(stub.getStartTime());
            bean.setEndTime(stub.getEndTime());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
