package com.cronbay.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.cronbay.ws.stub.ErrorStub;

@Provider
public class NotImplementedExceptionMapper implements ExceptionMapper<NotImplementedRsException>
{
    public Response toResponse(NotImplementedRsException ex) {
        return Response.status(Status.fromStatusCode(501))  // NOT_IMPLEMENTED
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
