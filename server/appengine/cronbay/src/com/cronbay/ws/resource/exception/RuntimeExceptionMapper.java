package com.cronbay.ws.resource.exception;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.cronbay.ws.stub.ErrorStub;

@Provider
public class RuntimeExceptionMapper implements ExceptionMapper<RuntimeException>
{
    // To be used in case the Accept mime type is invalid/unsupported.
    protected final static String DEFAULT_MEDIA_TYPE = MediaType.APPLICATION_XML;

    public Response toResponse(RuntimeException ex) 
    {
        if(ex instanceof WebApplicationException) {
            //return ((WebApplicationException) ex).getResponse();
            
            Response response = ((WebApplicationException) ex).getResponse();
            ResponseBuilder builder = Response.status(response.getStatus());

            //if(response.getStatus() == Status.NOT_ACCEPTABLE.getStatusCode() ||
            //        response.getStatus() == Status.UNSUPPORTED_MEDIA_TYPE.getStatusCode()) {
                // TBD: Force the mime type of the error stub to XML.
                builder = builder.type(DEFAULT_MEDIA_TYPE);
            //}
            
            String message = ex.getMessage();
            if(message == null || message.length() == 0) {
                Status status = Status.fromStatusCode(response.getStatus());
                if(status != null) {
                    message = status.name() + " - " + status.getReasonPhrase();
                }
            }
            builder = builder.entity(new ErrorStub(message));
            return builder.build();
            
            /*
            String message = ex.getMessage();
            System.out.println(">>>>>>>>>>>>>>>>>> MESSAGE = " + message);
            if(message != null) {
                //message = message.replaceAll("\\<.*?>","");
                message = message.replaceAll("\\<[^>]*>","");
            }
            System.out.println(">>>>>>>>>>>>>>>>>> MESSAGE = " + message);
            
            String responseMessage = "";            
            Object responseObj = response.getEntity();
            if(responseObj != null) {
                System.out.println(">>>>>>>>>>>>>>>>>> RESPONSE-MESSAGE = " + responseObj.toString());
                //responseMessage = responseObj.toString().replaceAll("\\<.*?>","");
                responseMessage = responseObj.toString().replaceAll("\\<[^>]*>","");
            }
            System.out.println(">>>>>>>>>>>>>>>>>> RESPONSE-MESSAGE = " + responseMessage);

            String causeMessage = "";            
            Throwable causeObj = ex.getCause();
            if(causeObj != null) {
                causeMessage = causeObj.getMessage();
                System.out.println(">>>>>>>>>>>>>>>>>> CAUSE-MESSAGE = " + causeMessage);
                if(causeMessage != null) {
                    //causeMessage = causeMessage.replaceAll("\\<.*?>","");
                    causeMessage = causeMessage.replaceAll("\\<[^>]*>","");
                }
            }
            System.out.println(">>>>>>>>>>>>>>>>>> CAUSE-MESSAGE = " + causeMessage);
            
            //builder = builder.entity(new ErrorStub(message));
            builder = builder.entity(new ErrorStub(responseMessage));
            //builder = builder.entity(new ErrorStub(causeMessage));
            ////builder = builder.entity(new ErrorStub(ex.getMessage()).toString());
            
            return builder.build();
            */
        } else {
            return Response.status(Status.INTERNAL_SERVER_ERROR)
                .entity(new ErrorStub(ex.getMessage()))
                .build();
        }
    }

}
