package com.cronbay.ws.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.resource.BaseResourceException;
import com.cronbay.ws.ApiConsumer;
import com.cronbay.ws.stub.ApiConsumerStub;
import com.cronbay.ws.stub.ApiConsumerListStub;


// TBD: Partial update/overwrite?
// TBD: Field-based filtering in getApiConsumer(guid). (e.g., ?field1=x&field2=y)
// Note: Jersey (possibly, new version 1.9.1) seems to have a weird bug and
//       it throws exception with the format @Path("{guid : [0-9a-fA-F\\-]+}") (and, other variations)
//       (which somehow translates into 405 error).
// --> Workaround. Use this format: @Path("{guid: [a-zA-Z0-9\\-_]+}") across all guid path params...
public interface ApiConsumerResource
{
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllApiConsumers(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Path("allkeys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllApiConsumerKeys(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Path("keys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findApiConsumerKeys(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Path("subset")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getApiConsumerKeys(@QueryParam("guids") List<String> guids) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

    @GET
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Support both guid and key ???
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getApiConsumer(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    @Path("{guid: [a-f0-9\\-]+}/{field: [a-zA-Z_][a-zA-Z0-9_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getApiConsumer(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findApiConsumers(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createApiConsumer(ApiConsumerStub apiConsumer) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to a Jersey bug, @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateApiConsumer(@PathParam("guid") String guid, ApiConsumerStub apiConsumer) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to a Jersey bug, @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateApiConsumer(@PathParam("guid") String guid, @QueryParam("aeryId") String aeryId, @QueryParam("name") String name, @QueryParam("description") String description, @QueryParam("appKey") String appKey, @QueryParam("appSecret") String appSecret, @QueryParam("status") String status) throws BaseResourceException;

    @DELETE
    @Path("{guid: [a-zA-Z0-9\\-_]+}")    // Support both guid and key ???
    Response deleteApiConsumer(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deleteApiConsumers(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

//    @POST
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response createApiConsumers(ApiConsumerListStub apiConsumers) throws BaseResourceException;

//    @PUT
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response updateeApiConsumers(ApiConsumerListStub apiConsumers) throws BaseResourceException;

}
