package com.cronbay.ws.resource.exception;

import com.cronbay.ws.exception.resource.BaseResourceException;


public class InternalServerErrorRsException extends BaseResourceException 
{
    private static final long serialVersionUID = 1L;

    public InternalServerErrorRsException() 
    {
        super();
    }
    public InternalServerErrorRsException(String message) 
    {
        super(message);
    }
    public InternalServerErrorRsException(String message, String resource) 
    {
        super(message, resource);
    }
    public InternalServerErrorRsException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public InternalServerErrorRsException(String message, Throwable cause, String resource) 
    {
        super(message, cause, resource);
    }
    public InternalServerErrorRsException(Throwable cause) 
    {
        super(cause);
    }
    public InternalServerErrorRsException(Throwable cause, String resource) 
    {
        super(cause, resource);
    }

}
