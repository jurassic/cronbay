package com.cronbay.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.cronbay.ws.core.StatusCode;
import com.cronbay.ws.stub.ErrorStub;

@Provider
public class MethodNotAllowedExceptionMapper implements ExceptionMapper<MethodNotAllowedRsException>
{
    public Response toResponse(MethodNotAllowedRsException ex) {
        return Response.status(Status.fromStatusCode(StatusCode.METHOD_NOT_ALLOWED))
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
