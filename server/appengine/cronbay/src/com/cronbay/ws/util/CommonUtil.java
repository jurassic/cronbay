package com.cronbay.ws.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Random;
import java.util.logging.Logger;
import java.util.logging.Level;

 
public class CommonUtil 
{
    private static final Logger log = Logger.getLogger(CommonUtil.class.getName()); 
    private static final Random RNG = new Random();

    private CommonUtil() {}

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text) 
    throws NoSuchAlgorithmException, UnsupportedEncodingException  {
        MessageDigest md;
        md = MessageDigest.getInstance("SHA-1");
        byte[] sha1hash = new byte[40];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        sha1hash = md.digest();
        return convertToHex(sha1hash);
    }

    // This really needs to be generated using some kind of persistent storage
    // so that the generated ids are all unique!
    public static Long id()
    {
        // Temporary
        long r = Math.abs(RNG.nextLong());
        return r;
    }

    public static String guid()
    {
        String guid = null;
        
        // Temporary
        long r = RNG.nextLong();
        String text = (new Date()).toString() + Long.toString(r);
        try {
            guid = CommonUtil.SHA1(text);
        } catch(Exception ex) {
            log.warning("Failed to create a guid through SHA-1 hash." + ex.toString());
            // What to do???
            Formatter formatter = new Formatter();
            guid = formatter.format("%40s", Long.toString(r)).toString();  // ???
        }
        log.info("A new guid created: " + guid);
        return guid;
    }
    
    // TBD: Move to a singleton class where user-specific info (e.g., locale) can be used.
    public static String generateDateString(int year, int month, int day) {
        // TBD: Get the user's date format from Locale (or, from pref.)
        // Locale is preferred.
        // ?? what happens to the stored dates if the user changes the prefs? (or, even locale) 
        String dateStr = new StringBuilder()
        .append(month + 1).append("/") // Month is 0 based so add 1
        .append(day).append("/")
        .append(year).toString();
        return dateStr;
    }

    // [year, month, day]
    public static int[] parseDateString(String dateStr) {
        String[] parts = null;
        
        if(dateStr != null) {
            try {
                parts = dateStr.split("/");
            } catch(Exception ex) {
                // ignore...
                log.log(Level.WARNING, "Date string parse error. ", ex);
            }
        }
        
        int[] dateInt = new int[3];
        
        // Default value. (today)
        final Calendar c = Calendar.getInstance();
        dateInt[0] = c.get(Calendar.YEAR);
        dateInt[1] = c.get(Calendar.MONTH);
        dateInt[2] = c.get(Calendar.DAY_OF_MONTH);

        if(parts == null || parts.length < 2) {
            // Use just today.
            log.warning("Date string format is not recognized. Using today.");
        } else {
            if(parts.length > 2) {  // What if it's > 3?
                try {
                    dateInt[0] = Integer.parseInt(parts[2]);
                    if(dateInt[0] < 100) {
                        dateInt[0] += 2000;  // In case the year is written with the last two decimals only.
                    }
                } catch(NumberFormatException ex) {
                    // ignore
                    log.log(Level.FINE, "Year not recognized", ex);
                }
            }
            try {
                dateInt[1] = Integer.parseInt(parts[0]) - 1;
            } catch(NumberFormatException ex) {
                // ignore
                log.log(Level.FINE, "Month not recognized", ex);
            }
            try {
                dateInt[2] = Integer.parseInt(parts[1]);
            } catch(NumberFormatException ex) {
                // ignore
                log.log(Level.FINE, "Day not recognized", ex);
            }            
        }
        
        return dateInt;
    }
    
}
