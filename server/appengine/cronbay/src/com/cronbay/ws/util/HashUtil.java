package com.cronbay.ws.util;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class HashUtil
{
    private static final Logger log = Logger.getLogger(HashUtil.class.getName());

    // Static methods only.
    private HashUtil() {}

    public static String generateSha1Hash(String content)
    {
        String sha1 = null;
        try {
            sha1 = CommonUtil.SHA1(content);
        } catch (NoSuchAlgorithmException e) {
            log.log(Level.WARNING, "Failed to generate sha1 hash", e);
        } catch (UnsupportedEncodingException e) {
            log.log(Level.WARNING, "Failed to generate sha1 hash", e);
        }        
        return sha1;
    }

}
