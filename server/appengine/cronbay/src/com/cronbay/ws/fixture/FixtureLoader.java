package com.cronbay.ws.fixture;

import java.util.logging.Logger;
import java.util.logging.Level;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


// Note that fixture data loading should be idempotent...
// It may be called multiple times....
public class FixtureLoader implements ServletContextListener
{
    private static final Logger log = Logger.getLogger(FixtureLoader.class.getName());

    
    public FixtureLoader()
    {
        // TBD....
    }

    @Override
    public void contextInitialized(ServletContextEvent sce)
    {
        log.info("FixtureLoader.contextInitialized() called.");

        long now = System.currentTimeMillis();
        log.info(">>>>>>>>>>>>>>>>>>>>>> now = " + now);

        if(FixtureHelper.getInstance().isFixtureLoad()) {
            int cnt = FixtureHelper.getInstance().processFixtureFiles();
            log.info("Fixture files have been processed: count = " + cnt);
        } else {
            log.info("Fixture files have not been processed.");
        }

    }
    
    @Override
    public void contextDestroyed(ServletContextEvent sce)
    {
        log.info("FixtureLoader.contextDestroyed() called.");
        // TBD....
    }

    

}
