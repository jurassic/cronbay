package com.cronbay.ws.fixture;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.config.Config;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.bean.ApiConsumerBean;
import com.cronbay.ws.bean.UserBean;
import com.cronbay.ws.bean.CronJobBean;
import com.cronbay.ws.bean.JobOutputBean;
import com.cronbay.ws.bean.ServiceInfoBean;
import com.cronbay.ws.bean.FiveTenBean;
import com.cronbay.ws.stub.ApiConsumerStub;
import com.cronbay.ws.stub.UserStub;
import com.cronbay.ws.stub.CronJobStub;
import com.cronbay.ws.stub.JobOutputStub;
import com.cronbay.ws.stub.ServiceInfoStub;
import com.cronbay.ws.stub.FiveTenStub;
import com.cronbay.ws.stub.ApiConsumerListStub;
import com.cronbay.ws.stub.UserListStub;
import com.cronbay.ws.stub.CronJobListStub;
import com.cronbay.ws.stub.JobOutputListStub;
import com.cronbay.ws.stub.ServiceInfoListStub;
import com.cronbay.ws.stub.FiveTenListStub;
import com.cronbay.ws.resource.ServiceManager;
import com.cronbay.ws.resource.impl.ApiConsumerResourceImpl;
import com.cronbay.ws.resource.impl.UserResourceImpl;
import com.cronbay.ws.resource.impl.CronJobResourceImpl;
import com.cronbay.ws.resource.impl.JobOutputResourceImpl;
import com.cronbay.ws.resource.impl.ServiceInfoResourceImpl;
import com.cronbay.ws.resource.impl.FiveTenResourceImpl;


// "Helper" functions...
// Get the list of fixture files,
// Read them, and
// Load the data into the datastore, etc....
// See the note in FixtureUtil...
public class FixtureHelper
{
    private static final Logger log = Logger.getLogger(FixtureHelper.class.getName());

    // ???
    private static final String CONFIG_KEY_FIXTURE_LOAD = "cronbay.fixture.load";
    private static final String CONFIG_KEY_FIXTURE_DIR = "cronbay.fixture.directory";
    // ...

    // Dummy var.
    protected boolean dummyFalse1 = false;
    // ...

    
    private FixtureHelper()
    {
        // ...
    }
    
    // Initialization-on-demand holder.
    private static class FixtureHelperHolder
    {
        private static final FixtureHelper INSTANCE = new FixtureHelper();
    }

    // Singleton method
    public static FixtureHelper getInstance()
    {
        return FixtureHelperHolder.INSTANCE;
    }


    public boolean isFixtureLoad()
    {
        // temporary
        return Config.getInstance().getBoolean(CONFIG_KEY_FIXTURE_LOAD, FixtureUtil.getDefaultFixtureLoad());
    }

    public String getFixtureDirName()
    {
        // temporary
        return Config.getInstance().getString(CONFIG_KEY_FIXTURE_DIR, FixtureUtil.getDefaultFixtureDir());
    }

    
    // TBD
    private List<FixtureFile> buildFixtureFileList()
    {
        List<FixtureFile> list = new ArrayList<FixtureFile>();

        String fixtureDirName = getFixtureDirName();
        File fixtureDir = new File(fixtureDirName);
        if(!fixtureDir.exists() || !fixtureDir.isDirectory()) {
            // error. what to do????
            log.warning("Fixture file directory does not exist. fixtureDirName = " + fixtureDirName);
        } else {
            File[] files = fixtureDir.listFiles();
            for(File f : files) {
                if(f.isFile()) {
                    String filePath = f.getPath();  // ???
                    FixtureFile ff = new FixtureFile(filePath);
                    list.add(ff);
                } else {
                    // This should not happen. Ignore...                    
                }
            }
        }
        
        return list;
    }
    
    public int processFixtureFiles()
    {
        List<FixtureFile> list = buildFixtureFileList();
        if(log.isLoggable(Level.FINE)) {
            for(FixtureFile f : list) {
                log.fine("FixtureFile f = " + f);
            }
        }
        
        int count = 0;
        for(FixtureFile f : list) {  // list cannot be null.
            // [1] Read the file
            // [2] Convert the file content to objects
            // [3] Then, save it. (We use update/overwrite rather than create to ensure "idempotency".)
            String filePath = f.getFilePath();
            String objectType = f.getObjectType();
            String mediaType = f.getMediaType();
            
            // JSON only, for now....
            if(! FixtureUtil.MEDIA_TYPE_JSON.equals(mediaType)) {
                log.warning("Currently supports only Json fixture files. Skipping filePath = " + filePath);
                continue;
            }

            BufferedReader reader = null;
            File inputFile = new File(filePath);
            if(inputFile.canRead()) {
                try {
                    reader = new BufferedReader(new FileReader(inputFile));
                    StringBuilder sb = new StringBuilder(0x1000);
                    final char[] buf = new char[0x1000];
                    int read = -1;
                    do {
                        read = reader.read(buf, 0, buf.length);
                        if (read>0) {
                            sb.append(buf, 0, read);
                        }
                    } while (read>=0);

                    if(dummyFalse1) {
                    } else if("ApiConsumer".equals(objectType)) {
                        ApiConsumerStub stub = ApiConsumerStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ApiConsumerBean bean = ApiConsumerResourceImpl.convertApiConsumerStubToBean(stub);
                        boolean suc = ServiceManager.getApiConsumerService().updateApiConsumer(bean);
                        if(suc) {
                            count++;
                            log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ApiConsumerList".equals(objectType)) {
                        ApiConsumerListStub listStub = ApiConsumerListStub.fromJsonString(sb.toString());
                        List<ApiConsumerBean> beanList = ApiConsumerResourceImpl.convertApiConsumerListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ApiConsumerBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getApiConsumerService().updateApiConsumer(bean);
                                if(suc) {
                                    count++;
                                    log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("User".equals(objectType)) {
                        UserStub stub = UserStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserBean bean = UserResourceImpl.convertUserStubToBean(stub);
                        boolean suc = ServiceManager.getUserService().updateUser(bean);
                        if(suc) {
                            count++;
                            log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserList".equals(objectType)) {
                        UserListStub listStub = UserListStub.fromJsonString(sb.toString());
                        List<UserBean> beanList = UserResourceImpl.convertUserListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserService().updateUser(bean);
                                if(suc) {
                                    count++;
                                    log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("CronJob".equals(objectType)) {
                        CronJobStub stub = CronJobStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        CronJobBean bean = CronJobResourceImpl.convertCronJobStubToBean(stub);
                        boolean suc = ServiceManager.getCronJobService().updateCronJob(bean);
                        if(suc) {
                            count++;
                            log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("CronJobList".equals(objectType)) {
                        CronJobListStub listStub = CronJobListStub.fromJsonString(sb.toString());
                        List<CronJobBean> beanList = CronJobResourceImpl.convertCronJobListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(CronJobBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getCronJobService().updateCronJob(bean);
                                if(suc) {
                                    count++;
                                    log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("JobOutput".equals(objectType)) {
                        JobOutputStub stub = JobOutputStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        JobOutputBean bean = JobOutputResourceImpl.convertJobOutputStubToBean(stub);
                        boolean suc = ServiceManager.getJobOutputService().updateJobOutput(bean);
                        if(suc) {
                            count++;
                            log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("JobOutputList".equals(objectType)) {
                        JobOutputListStub listStub = JobOutputListStub.fromJsonString(sb.toString());
                        List<JobOutputBean> beanList = JobOutputResourceImpl.convertJobOutputListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(JobOutputBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getJobOutputService().updateJobOutput(bean);
                                if(suc) {
                                    count++;
                                    log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceInfo".equals(objectType)) {
                        ServiceInfoStub stub = ServiceInfoStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ServiceInfoBean bean = ServiceInfoResourceImpl.convertServiceInfoStubToBean(stub);
                        boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                        if(suc) {
                            count++;
                            log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceInfoList".equals(objectType)) {
                        ServiceInfoListStub listStub = ServiceInfoListStub.fromJsonString(sb.toString());
                        List<ServiceInfoBean> beanList = ServiceInfoResourceImpl.convertServiceInfoListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ServiceInfoBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                                if(suc) {
                                    count++;
                                    log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FiveTen".equals(objectType)) {
                        FiveTenStub stub = FiveTenStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        FiveTenBean bean = FiveTenResourceImpl.convertFiveTenStubToBean(stub);
                        boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                        if(suc) {
                            count++;
                            log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FiveTenList".equals(objectType)) {
                        FiveTenListStub listStub = FiveTenListStub.fromJsonString(sb.toString());
                        List<FiveTenBean> beanList = FiveTenResourceImpl.convertFiveTenListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(FiveTenBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                                if(suc) {
                                    count++;
                                    log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else {
                        // This cannot happen
                    }
                } catch (FileNotFoundException e) {
                    log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (IOException e) {
                    log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Failed to process a fixture file: filePath = " + filePath, e);
                } catch (Exception e) {
                    log.log(Level.WARNING, "Unexpected error while processing a fixture file: filePath = " + filePath, e);
                } finally {
                    if(reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            log.log(Level.WARNING, "Error while closing the fixture file: filePath = " + filePath, e);
                        }
                    }
                }                
            } else {
                log.warning("Skipping a fixture file because it is not readable: filePath = " + filePath);
            }
        }
                
        return count;
    }
    
}
