package com.cronbay.ws.exception;

import com.cronbay.ws.BaseException;


public class ResourceAlreadyPresentException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public ResourceAlreadyPresentException() 
    {
        super();
    }
    public ResourceAlreadyPresentException(String message) 
    {
        super(message);
    }
    public ResourceAlreadyPresentException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public ResourceAlreadyPresentException(Throwable cause) 
    {
        super(cause);
    }

}
