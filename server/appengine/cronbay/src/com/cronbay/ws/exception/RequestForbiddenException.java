package com.cronbay.ws.exception;

import com.cronbay.ws.BaseException;


public class RequestForbiddenException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public RequestForbiddenException() 
    {
        super();
    }
    public RequestForbiddenException(String message) 
    {
        super(message);
    }
    public RequestForbiddenException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public RequestForbiddenException(Throwable cause) 
    {
        super(cause);
    }

}
