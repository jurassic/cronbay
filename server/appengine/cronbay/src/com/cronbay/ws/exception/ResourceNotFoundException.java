package com.cronbay.ws.exception;

import com.cronbay.ws.BaseException;


public class ResourceNotFoundException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public ResourceNotFoundException() 
    {
        super();
    }
    public ResourceNotFoundException(String message) 
    {
        super(message);
    }
    public ResourceNotFoundException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public ResourceNotFoundException(Throwable cause) 
    {
        super(cause);
    }

}
