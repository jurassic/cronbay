package com.cronbay.ws.exception;

import com.cronbay.ws.BaseException;


public class ResourceGoneException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public ResourceGoneException() 
    {
        super();
    }
    public ResourceGoneException(String message) 
    {
        super(message);
    }
    public ResourceGoneException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public ResourceGoneException(Throwable cause) 
    {
        super(cause);
    }

}
