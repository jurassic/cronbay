package com.cronbay.ws.exception;

import com.cronbay.ws.BaseException;


public class UnauthorizedException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public UnauthorizedException() 
    {
        super();
    }
    public UnauthorizedException(String message) 
    {
        super(message);
    }
    public UnauthorizedException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public UnauthorizedException(Throwable cause) 
    {
        super(cause);
    }

}
