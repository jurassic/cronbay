package com.cronbay.ws.exception;

import com.cronbay.ws.BaseException;


public class RequestTimeoutException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public RequestTimeoutException() 
    {
        super();
    }
    public RequestTimeoutException(String message) 
    {
        super(message);
    }
   public RequestTimeoutException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public RequestTimeoutException(Throwable cause) 
    {
        super(cause);
    }

}
