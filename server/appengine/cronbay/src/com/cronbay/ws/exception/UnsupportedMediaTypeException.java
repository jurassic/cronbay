package com.cronbay.ws.exception;

import com.cronbay.ws.BaseException;


public class UnsupportedMediaTypeException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public UnsupportedMediaTypeException() 
    {
        super();
    }
    public UnsupportedMediaTypeException(String message) 
    {
        super(message);
    }
   public UnsupportedMediaTypeException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public UnsupportedMediaTypeException(Throwable cause) 
    {
        super(cause);
    }

}
