package com.cronbay.ws.exception;

import com.cronbay.ws.BaseException;


public class DataStoreException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public DataStoreException() 
    {
        super();
    }
    public DataStoreException(String message) 
    {
        super(message);
    }
    public DataStoreException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public DataStoreException(Throwable cause) 
    {
        super(cause);
    }

}
