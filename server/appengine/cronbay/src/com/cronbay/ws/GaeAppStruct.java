package com.cronbay.ws;


public interface GaeAppStruct 
{
    String  getGroupId();
    String  getAppId();
    String  getAppDomain();
    String  getNamespace();
    Long  getAcl();
    String  getNote();
}
