package com.cronbay.ws;


public interface GaeUserStruct 
{
    String  getAuthDomain();
    String  getFederatedIdentity();
    String  getNickname();
    String  getUserId();
    String  getEmail();
    String  getNote();
}
