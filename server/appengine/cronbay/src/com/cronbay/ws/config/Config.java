package com.cronbay.ws.config;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.sym.Name;


// Wrapper for System Properties, for now.
// TBD: Support for "Remote configuration". 
// TBD: "Section", "prefix", etc.
public class Config
{
    private static final Logger log = Logger.getLogger(Config.class.getName());

    private static final String KEY_CONFIG_WEB_SERVICE = "cronbay.config.webservice";
    
    // Embedded properties.
    private Properties mProps = null;
    private String mConfigWebService = null;
    
    private Config()
    {
        initConfiguration();
    }

    // Initialization-on-demand holder.
    private static class ConfigHolder
    {
        private static final Config INSTANCE = new Config();
    }

    // Singleton method
    public static Config getInstance()
    {
        return ConfigHolder.INSTANCE;
    }

    private void initConfiguration()
    {
        mProps = new Properties(System.getProperties());
        mConfigWebService = System.getProperty(KEY_CONFIG_WEB_SERVICE);
        // TBD: Add "Remote" properties. 
    }

    public Object getProperty(String key)
    {
        return mProps.getProperty(key);
    }

    public void setProperty(String key, Object value)
    {
        String strVal = null;
        if(value != null) {
            strVal = value.toString();
        }
        mProps.setProperty(key, strVal);
    }

    public void clearProperty(String key)
    {
        mProps.remove(key);
    }

    public boolean containsKey(String key)
    {
        return mProps.containsKey(key);
    }
    
    public void clear()
    {
        mProps.clear();
    }

    public boolean isEmpty()
    {
        return mProps.isEmpty();
    }


    public Iterator<String> getKeys()
    {
        Set<String> keys = new HashSet<String>();
        Enumeration<Object> e = mProps.keys();
        while(e.hasMoreElements()) {
            Object o = e.nextElement();
            keys.add((String) o);
        }
        return keys.iterator();
    }

    @SuppressWarnings("unchecked")
    public Set<String> getKeySet()
    {
        return (Set<String>)(Set<?>) mProps.keySet();
    }


    public String getString(String key)
    {
        return mProps.getProperty(key);
    }

    public String getString(String key, String defaultValue)
    {
        return mProps.getProperty(key, defaultValue);
    }

    public void setString(String key, String value)
    {
        mProps.setProperty(key, value);
    }
    
    public Boolean getBoolean(String key)
    {
        return getBoolean(key, null);
    }

    public Boolean getBoolean(String key, Boolean defaultValue)
    {
        //if(Boolean.FALSE.equals(defaultValue)) {
        //    return Boolean.getBoolean(key);
        //}
        String strVal = mProps.getProperty(key);
        if(strVal != null && strVal.trim().length() > 0) {
            try {
                return Boolean.parseBoolean(strVal);
            } catch(NumberFormatException e) {
                // ignore
                log.log(Level.INFO, "Failed to cast defaultValue, " + defaultValue, e);
            }
        }

        return defaultValue;
    }

    public void setBoolean(String key, Boolean value)
    {
        String strVal = null;
        if(value != null) {
            strVal = value.toString();
        }
        mProps.setProperty(key, strVal);
    }

    public Integer getInteger(String key)
    {
        return getInteger(key, null);
    }

    public Integer getInteger(String key, Integer defaultValue)
    {
        String strVal = mProps.getProperty(key);
        if(strVal != null && strVal.trim().length() > 0) {
            try {
                return Integer.parseInt(strVal);
            } catch(NumberFormatException e) {
                // ignore
                log.log(Level.INFO, "Failed to cast defaultValue, " + defaultValue, e);
            }
        }

        return defaultValue;
    }

    public void setInteger(String key, Integer value)
    {
        String strVal = null;
        if(value != null) {
            strVal = value.toString();
        }
        mProps.setProperty(key, strVal);
    }
   
    public Long getLong(String key)
    {
        return getLong(key, null);
    }

    public Long getLong(String key, Long defaultValue)
    {
        String strVal = mProps.getProperty(key);
        if(strVal != null && strVal.trim().length() > 0) {
            try {
                return Long.parseLong(strVal);
            } catch(NumberFormatException e) {
                // ignore
                log.log(Level.INFO, "Failed to cast defaultValue, " + defaultValue, e);
            }
	    }

        return defaultValue;
    }

    public void setLong(String key, Long value)
    {
        String strVal = null;
        if(value != null) {
            strVal = value.toString();
        }
        mProps.setProperty(key, strVal);
    }

    public Short getShort(String key)
    {
        return getShort(key, null);
    }

    public Short getShort(String key, Short defaultValue)
    {
        String strVal = mProps.getProperty(key);
        if(strVal != null && strVal.trim().length() > 0) {
            try {
                return Short.parseShort(strVal);
            } catch(NumberFormatException e) {
                // ignore
                log.log(Level.INFO, "Failed to cast defaultValue, " + defaultValue, e);
            }
	    }

        return defaultValue;
    }

    public void setShort(String key, Short value)
    {
        String strVal = null;
        if(value != null) {
            strVal = value.toString();
        }
        mProps.setProperty(key, strVal);
    }

    public Double getDouble(String key)
    {
        return getDouble(key, null);
    }

    public Double getDouble(String key, Double defaultValue)
    {
        String strVal = mProps.getProperty(key);
        if(strVal != null && strVal.trim().length() > 0) {
            try {
                return Double.parseDouble(strVal);
            } catch(NumberFormatException e) {
                // ignore
                log.log(Level.INFO, "Failed to cast defaultValue, " + defaultValue, e);
            }
	    }

        return defaultValue;
    }

    public void setDouble(String key, Double value)
    {
        String strVal = null;
        if(value != null) {
            strVal = value.toString();
        }
        mProps.setProperty(key, strVal);
    }

    public Float getFloat(String key)
    {
        return getFloat(key, null);
    }

    public Float getFloat(String key, Float defaultValue)
    {
        String strVal = mProps.getProperty(key);
        if(strVal != null && strVal.trim().length() > 0) {
            try {
                return Float.parseFloat(strVal);
            } catch(NumberFormatException e) {
                // ignore
                log.log(Level.INFO, "Failed to cast defaultValue, " + defaultValue, e);
            }
	    }

        return defaultValue;
    }

    public void setFloat(String key, Float value)
    {
        String strVal = null;
        if(value != null) {
            strVal = value.toString();
        }
        mProps.setProperty(key, strVal);
    }


    // Returns a Json object as a (recursive) map.
    // TBD: Recursively parse the jason string.
    public Map<String, Object> getValueMap(String key)
    {
        Map<String, Object> value = new LinkedHashMap<String, Object>();

        String rawVal = getString(key);
        try {
            JsonFactory f = new JsonFactory();
            JsonParser jp = f.createJsonParser(rawVal);

            jp.nextToken(); // will return JsonToken.START_OBJECT (verify?)
            while (jp.nextToken() != JsonToken.END_OBJECT) {
              String fieldname = jp.getCurrentName();
              jp.nextToken(); // move to value, or START_OBJECT/START_ARRAY
              
              // TBD
              value.put(rawVal, null);
              // ...
            
            }
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "", e);
        }
        
        return value;
    }

    // TBD
    public Map<String, Object> getValueMap(String key, Map<String, Object> defaultValue)
    {
        return null;
    }

    // TBD
    public void setValueMap(String key, Map<String, Object> value) 
    {
    }

}
