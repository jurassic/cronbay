package com.cronbay.ws.cert;


public interface OAuthConsumerInfo 
{
    String  getGuid();
    String  getServiceName();
    String  getAppId();
    String  getAppUrl();
    String  getConsumerKey();
    String  getConsumerSecret();
    String  getNote();
    String  getStatus();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
