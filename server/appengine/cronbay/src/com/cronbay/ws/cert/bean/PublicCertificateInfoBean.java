package com.cronbay.ws.cert.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.cert.PublicCertificateInfo;
import com.cronbay.ws.cert.data.PublicCertificateInfoDataObject;


public class PublicCertificateInfoBean implements PublicCertificateInfo
{
    private static final Logger log = Logger.getLogger(PublicCertificateInfoBean.class.getName());

    // Embedded data object.
    private PublicCertificateInfoDataObject dobj = null;

    public PublicCertificateInfoBean()
    {
        this(new PublicCertificateInfoDataObject());
    }
    public PublicCertificateInfoBean(String guid)
    {
        this(new PublicCertificateInfoDataObject(guid));
    }
    public PublicCertificateInfoBean(PublicCertificateInfoDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public PublicCertificateInfoDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
        }
    }

    public Long getCreatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getCreatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
            return null;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getDataObject() != null) {
            getDataObject().setCreatedTime(createdTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
        }
    }

    public Long getModifiedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getModifiedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
            return null;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setModifiedTime(modifiedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
        }
    }

    public String getAppId()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppId(String appId)
    {
        if(getDataObject() != null) {
            getDataObject().setAppId(appId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
        }
    }

    public String getAppUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppUrl(String appUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setAppUrl(appUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
        }
    }

    public String getCertName()
    {
        if(getDataObject() != null) {
            return getDataObject().getCertName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setCertName(String certName)
    {
        if(getDataObject() != null) {
            getDataObject().setCertName(certName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
        }
    }

    public String getCertInPemFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getCertInPemFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setCertInPemFormat(String certInPemFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setCertInPemFormat(certInPemFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
        }
    }

    public Long getExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationTime(expirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PublicCertificateInfoDataObject is null!");
        }
    }

    // TBD
    public PublicCertificateInfoDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
