package com.cronbay.ws.cert.dao;

import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.cert.data.PublicCertificateInfoDataObject;

// TBD: Add offset/count to getAllPublicCertificateInfos() and findPublicCertificateInfos(), etc.
public interface PublicCertificateInfoDAO
{
    PublicCertificateInfoDataObject getPublicCertificateInfo(String guid) throws BaseException;
    List<PublicCertificateInfoDataObject> getAllPublicCertificateInfos() throws BaseException;
    List<PublicCertificateInfoDataObject> getAllPublicCertificateInfos(String ordering, Long offset, Integer count) throws BaseException;
    List<PublicCertificateInfoDataObject> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<PublicCertificateInfoDataObject> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<PublicCertificateInfoDataObject> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createPublicCertificateInfo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return PublicCertificateInfoDataObject?)
    String createPublicCertificateInfo(PublicCertificateInfoDataObject publicCertificateInfo) throws BaseException;          // Returns Guid.  (Return PublicCertificateInfoDataObject?)
    //Boolean updatePublicCertificateInfo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updatePublicCertificateInfo(PublicCertificateInfoDataObject publicCertificateInfo) throws BaseException;
    Boolean deletePublicCertificateInfo(String guid) throws BaseException;
    Boolean deletePublicCertificateInfo(PublicCertificateInfoDataObject publicCertificateInfo) throws BaseException;
    Long deletePublicCertificateInfos(String filter, String params, List<String> values) throws BaseException;
}
