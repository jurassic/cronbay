package com.cronbay.ws.cert.service;

import java.util.List;

import com.cronbay.ws.cert.PublicCertificateInfo;
import com.cronbay.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface PublicCertificateInfoService
{
    // TBD: Return an interface or a bean wrapper.

    PublicCertificateInfo getPublicCertificateInfo(String guid) throws BaseException;
    Object getPublicCertificateInfo(String guid, String field) throws BaseException;
    List<PublicCertificateInfo> getAllPublicCertificateInfos() throws BaseException;
    List<PublicCertificateInfo> getAllPublicCertificateInfos(String ordering, Long offset, Integer count) throws BaseException;
    List<PublicCertificateInfo> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<PublicCertificateInfo> findPublicCertificateInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createPublicCertificateInfo(String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime) throws BaseException;
    //String createPublicCertificateInfo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return PublicCertificateInfo?)
    String createPublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException;          // Returns Guid.  (Return PublicCertificateInfo?)
    Boolean updatePublicCertificateInfo(String guid, String appId, String appUrl, String certName, String certInPemFormat, String note, String status, Long expirationTime) throws BaseException;
    //Boolean updatePublicCertificateInfo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updatePublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException;
    Boolean deletePublicCertificateInfo(String guid) throws BaseException;
    Boolean deletePublicCertificateInfo(PublicCertificateInfo publicCertificateInfo) throws BaseException;
    Long deletePublicCertificateInfos(String filter, String params, List<String> values) throws BaseException;

//    Integer createPublicCertificateInfos(List<PublicCertificateInfo> publicCertificateInfos) throws BaseException;
//    Boolean updateePublicCertificateInfos(List<PublicCertificateInfo> publicCertificateInfos) throws BaseException;

}
