package com.cronbay.ws.cert.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;

import com.cronbay.ws.cert.OAuthConsumerInfo;
import com.cronbay.ws.cert.bean.OAuthConsumerInfoBean;
import com.cronbay.ws.cert.data.OAuthConsumerInfoDataObject;
import com.cronbay.ws.cert.service.OAuthConsumerInfoService;
import com.cronbay.ws.cert.dao.OAuthConsumerInfoDAO;
import com.cronbay.ws.cert.dao.DefaultOAuthConsumerInfoDAO;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OAuthConsumerInfoServiceImpl implements OAuthConsumerInfoService
{
    private static final Logger log = Logger.getLogger(OAuthConsumerInfoServiceImpl.class.getName());

    private OAuthConsumerInfoDAO dao = null;

    private OAuthConsumerInfoDAO getOAuthConsumerInfoDAO()
    {
        if(dao == null) {
            dao = new DefaultOAuthConsumerInfoDAO();
        }
        return dao;
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OAuthConsumerInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OAuthConsumerInfo getOAuthConsumerInfo(String guid) throws BaseException
    {
        OAuthConsumerInfoDataObject dataObj = getOAuthConsumerInfoDAO().getOAuthConsumerInfo(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve OAuthConsumerInfoDataObject for guid = " + guid);
            return null;  // ????
        }
        OAuthConsumerInfoBean bean = new OAuthConsumerInfoBean(dataObj);
        return bean;
    }

    @Override
    public Object getOAuthConsumerInfo(String guid, String field) throws BaseException
    {
        OAuthConsumerInfoDataObject dataObj = getOAuthConsumerInfoDAO().getOAuthConsumerInfo(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve OAuthConsumerInfoDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("serviceName")) {
            return dataObj.getServiceName();
        } else if(field.equals("appId")) {
            return dataObj.getAppId();
        } else if(field.equals("appUrl")) {
            return dataObj.getAppUrl();
        } else if(field.equals("consumerKey")) {
            return dataObj.getConsumerKey();
        } else if(field.equals("consumerSecret")) {
            return dataObj.getConsumerSecret();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }
    
    @Override
    public List<OAuthConsumerInfo> getAllOAuthConsumerInfos() throws BaseException
    {
        return getAllOAuthConsumerInfos(null, null, null);
    }

    @Override
    public List<OAuthConsumerInfo> getAllOAuthConsumerInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        // TBD: Is there a better way????
        List<OAuthConsumerInfo> list = new ArrayList<OAuthConsumerInfo>();
        List<OAuthConsumerInfoDataObject> dataObjs = getOAuthConsumerInfoDAO().getAllOAuthConsumerInfos(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OAuthConsumerInfoDataObject list.");
        } else {
            Iterator<OAuthConsumerInfoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OAuthConsumerInfoDataObject dataObj = (OAuthConsumerInfoDataObject) it.next();
                list.add(new OAuthConsumerInfoBean(dataObj));
            }
        }
        return list;
    }

    @Override
    public List<OAuthConsumerInfo> findOAuthConsumerInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOAuthConsumerInfos(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OAuthConsumerInfo> findOAuthConsumerInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("OAuthConsumerInfoServiceImpl.findOAuthConsumerInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<OAuthConsumerInfo> list = new ArrayList<OAuthConsumerInfo>();
        List<OAuthConsumerInfoDataObject> dataObjs = getOAuthConsumerInfoDAO().findOAuthConsumerInfos(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find oAuthConsumerInfos for the given criterion.");
        } else {
            Iterator<OAuthConsumerInfoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OAuthConsumerInfoDataObject dataObj = (OAuthConsumerInfoDataObject) it.next();
                list.add(new OAuthConsumerInfoBean(dataObj));
            }
        }
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("OAuthConsumerInfoServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = getOAuthConsumerInfoDAO().getCount(filter, params, values, aggregate);
        return count;
    }

    @Override
    public String createOAuthConsumerInfo(String serviceName, String appId, String appUrl, String consumerKey, String consumerSecret, String note, String status, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        OAuthConsumerInfoDataObject dataObj = new OAuthConsumerInfoDataObject(null, serviceName, appId, appUrl, consumerKey, consumerSecret, note, status, expirationTime);
        return createOAuthConsumerInfo(dataObj);
    }

    @Override
    public String createOAuthConsumerInfo(OAuthConsumerInfo oAuthConsumerInfo) throws BaseException
    {
        // Param oAuthConsumerInfo cannot be null.....
        if(oAuthConsumerInfo == null) {
            log.log(Level.INFO, "Param oAuthConsumerInfo is null!");
            throw new BadRequestException("Param oAuthConsumerInfo object is null!");
        }
        OAuthConsumerInfoDataObject dataObj = null;
        if(oAuthConsumerInfo instanceof OAuthConsumerInfoDataObject) {
            dataObj = (OAuthConsumerInfoDataObject) oAuthConsumerInfo;
        } else if(oAuthConsumerInfo instanceof OAuthConsumerInfoBean) {
            dataObj = ((OAuthConsumerInfoBean) oAuthConsumerInfo).toDataObject();
        } else {  // if(oAuthConsumerInfo instanceof OAuthConsumerInfo)
            //dataObj = new OAuthConsumerInfoDataObject(null, oAuthConsumerInfo.getServiceName(), oAuthConsumerInfo.getAppId(), oAuthConsumerInfo.getAppUrl(), oAuthConsumerInfo.getConsumerKey(), oAuthConsumerInfo.getConsumerSecret(), oAuthConsumerInfo.getNote(), oAuthConsumerInfo.getStatus(), oAuthConsumerInfo.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OAuthConsumerInfoDataObject(oAuthConsumerInfo.getGuid(), oAuthConsumerInfo.getServiceName(), oAuthConsumerInfo.getAppId(), oAuthConsumerInfo.getAppUrl(), oAuthConsumerInfo.getConsumerKey(), oAuthConsumerInfo.getConsumerSecret(), oAuthConsumerInfo.getNote(), oAuthConsumerInfo.getStatus(), oAuthConsumerInfo.getExpirationTime());
        }
        String guid = getOAuthConsumerInfoDAO().createOAuthConsumerInfo(dataObj);
        return guid;
    }

    @Override
    public Boolean updateOAuthConsumerInfo(String guid, String serviceName, String appId, String appUrl, String consumerKey, String consumerSecret, String note, String status, Long expirationTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OAuthConsumerInfoDataObject dataObj = new OAuthConsumerInfoDataObject(guid, serviceName, appId, appUrl, consumerKey, consumerSecret, note, status, expirationTime);
        return updateOAuthConsumerInfo(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOAuthConsumerInfo(OAuthConsumerInfo oAuthConsumerInfo) throws BaseException
    {
        // Param oAuthConsumerInfo cannot be null.....
        if(oAuthConsumerInfo == null || oAuthConsumerInfo.getGuid() == null) {
            log.log(Level.INFO, "Param oAuthConsumerInfo or its guid is null!");
            throw new BadRequestException("Param oAuthConsumerInfo object or its guid is null!");
        }
        OAuthConsumerInfoDataObject dataObj = null;
        if(oAuthConsumerInfo instanceof OAuthConsumerInfoDataObject) {
            dataObj = (OAuthConsumerInfoDataObject) oAuthConsumerInfo;
        } else if(oAuthConsumerInfo instanceof OAuthConsumerInfoBean) {
            dataObj = ((OAuthConsumerInfoBean) oAuthConsumerInfo).toDataObject();
        } else {  // if(oAuthConsumerInfo instanceof OAuthConsumerInfo)
            dataObj = new OAuthConsumerInfoDataObject(oAuthConsumerInfo.getGuid(), oAuthConsumerInfo.getServiceName(), oAuthConsumerInfo.getAppId(), oAuthConsumerInfo.getAppUrl(), oAuthConsumerInfo.getConsumerKey(), oAuthConsumerInfo.getConsumerSecret(), oAuthConsumerInfo.getNote(), oAuthConsumerInfo.getStatus(), oAuthConsumerInfo.getExpirationTime());
        }
        Boolean suc = getOAuthConsumerInfoDAO().updateOAuthConsumerInfo(dataObj);
        return suc;
    }
    
    @Override
    public Boolean deleteOAuthConsumerInfo(String guid) throws BaseException
    {
        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getOAuthConsumerInfoDAO().deleteOAuthConsumerInfo(guid);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOAuthConsumerInfo(OAuthConsumerInfo oAuthConsumerInfo) throws BaseException
    {
        // Param oAuthConsumerInfo cannot be null.....
        if(oAuthConsumerInfo == null || oAuthConsumerInfo.getGuid() == null) {
            log.log(Level.INFO, "Param oAuthConsumerInfo or its guid is null!");
            throw new BadRequestException("Param oAuthConsumerInfo object or its guid is null!");
        }
        OAuthConsumerInfoDataObject dataObj = null;
        if(oAuthConsumerInfo instanceof OAuthConsumerInfoDataObject) {
            dataObj = (OAuthConsumerInfoDataObject) oAuthConsumerInfo;
        } else if(oAuthConsumerInfo instanceof OAuthConsumerInfoBean) {
            dataObj = ((OAuthConsumerInfoBean) oAuthConsumerInfo).toDataObject();
        } else {  // if(oAuthConsumerInfo instanceof OAuthConsumerInfo)
            dataObj = new OAuthConsumerInfoDataObject(oAuthConsumerInfo.getGuid(), oAuthConsumerInfo.getServiceName(), oAuthConsumerInfo.getAppId(), oAuthConsumerInfo.getAppUrl(), oAuthConsumerInfo.getConsumerKey(), oAuthConsumerInfo.getConsumerSecret(), oAuthConsumerInfo.getNote(), oAuthConsumerInfo.getStatus(), oAuthConsumerInfo.getExpirationTime());
        }
        Boolean suc = getOAuthConsumerInfoDAO().deleteOAuthConsumerInfo(dataObj);
        return suc;
    }

    @Override
    public Long deleteOAuthConsumerInfos(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getOAuthConsumerInfoDAO().deleteOAuthConsumerInfos(filter, params, values);
        return count;
    }

}
