package com.cronbay.ws.cert.registry;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.cert.OAuthConsumerInfo;
import com.cronbay.ws.cert.service.OAuthConsumerInfoService;
import com.cronbay.ws.cert.service.OAuthConsumerInfoServiceImpl;

import com.cronbay.cert.ws.BaseConsumerRegistry;


// OAuth consumer key registry.
public class ConsumerRegistry extends BaseConsumerRegistry
{
    private static final Logger log = Logger.getLogger(ConsumerRegistry.class.getName());

    private OAuthConsumerInfoService mOAuthConsumerInfoService = null;
    
    // Lazy initialization.
    private OAuthConsumerInfoService getOAuthConsumerInfoService()
    {
        if(mOAuthConsumerInfoService == null) {
            mOAuthConsumerInfoService = new OAuthConsumerInfoServiceImpl();
        }
        return mOAuthConsumerInfoService;
    }
    

//    // Consumer key-secret map.
//    // TBD: Use a better data structure which reflects OAuthConsumerInfo.
//    // TBD: This needs to be implemented in BaseConsumerRegistry
//    private Map<String, String> consumerSecretMap;
//
//    protected Map<String, String> getBaseConsumerSecretMap()
//    {
//        consumerSecretMap = new HashMap<String, String>();
//        return consumerSecretMap;
//    }
//    protected Map<String, String> getConsumerSecretMap()
//    {
//        return consumerSecretMap;
//    }

    private ConsumerRegistry()
    {
        // TBD:  
        Map<String, String> consumerSecretMap = getBaseConsumerSecretMap();

        // Temporary. These are intended to be used for devel. purposes only.
        // Note: These key-secret pairs should be removed before the release!!!
        //consumerSecretMap.put("673b48f7-6bd3-48da-ac80-0caf8f60ae7f", "3d32acc8-1ee6-4cb6-a8d5-8e29553b2bf4"); 
        //consumerSecretMap.put("ae3aaecf-92cb-4c8e-99eb-f554b0202923", "723c5b6f-233d-4cd9-b9da-196f0449113d"); 
        //consumerSecretMap.put("a21a8f23-1c4c-447f-bf0d-24718392ef01", "e2edd092-8401-46e1-9dc4-5eb80322dd04"); 
        //consumerSecretMap.put("19fed0ed-2126-4289-9046-c06ff11f1bc6", "6ce6835c-fe53-490d-8ce1-8442ed7ae13a"); 
        //consumerSecretMap.put("724095b5-7339-468b-89bc-dede0c671a7c", "30f89734-2ce5-490d-bd7f-dc1d18fdb172"); 

        // ...
        populateConsumerSecretMap();
    }

    // Initialization-on-demand holder.
    private static class ConsumerRegistryHolder
    {
        private static final ConsumerRegistry INSTANCE = new ConsumerRegistry();
    }

    // Singleton method
    public static ConsumerRegistry getInstance()
    {
        return ConsumerRegistryHolder.INSTANCE;
    }

    // Initialize the consumerSecretMap.
    private void populateConsumerSecretMap()
    {
        // TBD: Temporary implementation for now...
        List<OAuthConsumerInfo> oAuthConsumerInfos = null;
        try {
            oAuthConsumerInfos = getOAuthConsumerInfoService().getAllOAuthConsumerInfos();
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve OAuthConsumerInfo list.", e);
        }
        
        if(oAuthConsumerInfos != null && !oAuthConsumerInfos.isEmpty()) {
            Map<String, String> consumerSecretMap = getConsumerSecretMap();
            for(OAuthConsumerInfo i : oAuthConsumerInfos) {
                // temporary
                String appId = i.getAppId();
                String cKey = i.getConsumerKey();
                String cSecret = i.getConsumerSecret();
                String status = i.getStatus();
                // ...
                consumerSecretMap.put(cKey, cSecret);
                // ...
            }
            log.log(Level.INFO, "ConsumerSecretMap: Imported OAuthConsumerInfo count = " + oAuthConsumerInfos.size());
        }
    }

    // Refresh the consumerSecretMap, among other things.
    private void refreshRegistry()
    {
        // TBD:
        // Update consumerSecretMap from the data store
        // ...
    }


    public String getConsumerKey(String appId)
    {
        // TBD.
        return null;
    }

    public String getConsumerSecret(String consumerKey)
    {
        // TBD.
        String consumerSecret = getConsumerSecretMap().get(consumerKey);
        return consumerSecret;
    }

}
