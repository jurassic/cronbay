package com.cronbay.ws.cert.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.resource.BaseResourceException;
import com.cronbay.ws.cert.PublicCertificateInfo;
import com.cronbay.ws.cert.stub.PublicCertificateInfoStub;
import com.cronbay.ws.cert.stub.PublicCertificateInfoListStub;


// TBD: Partial update/overwrite?
// TBD: Field-based filtering in getPublicCertificateInfo(guid). (e.g., ?field1=x&field2=y)
public interface PublicCertificateInfoResource
{
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllPublicCertificateInfos(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findPublicCertificateInfos(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

    @GET
    @Path("{guid : [0-9a-fA-F\\-]+}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getPublicCertificateInfo(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    @Path("{guid : [0-9a-fA-F\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getPublicCertificateInfo(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createPublicCertificateInfo(PublicCertificateInfoStub publicCertificateInfo) throws BaseResourceException;

    @PUT
    @Path("{guid : [0-9a-fA-F\\-]+}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updatePublicCertificateInfo(@PathParam("guid") String guid, PublicCertificateInfoStub publicCertificateInfo) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    @Path("{guid : [0-9a-fA-F\\-]+}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updatePublicCertificateInfo(@PathParam("guid") String guid, @QueryParam("appId") String appId, @QueryParam("appUrl") String appUrl, @QueryParam("certName") String certName, @QueryParam("certInPemFormat") String certInPemFormat, @QueryParam("note") String note, @QueryParam("status") String status, @QueryParam("expirationTime") Long expirationTime) throws BaseResourceException;

    @DELETE
    @Path("{guid : [0-9a-fA-F\\-]+}")
    Response deletePublicCertificateInfo(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deletePublicCertificateInfos(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

//    @POST
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response createPublicCertificateInfos(PublicCertificateInfoListStub publicCertificateInfos) throws BaseResourceException;

//    @PUT
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response updateePublicCertificateInfos(PublicCertificateInfoListStub publicCertificateInfos) throws BaseResourceException;

}
