package com.cronbay.ws.cert.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.cert.PublicCertificateInfo;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "publicCertificateInfo")
@XmlType(propOrder = {"guid", "appId", "appUrl", "certName", "certInPemFormat", "note", "status", "expirationTime", "createdTime", "modifiedTime"})
public class PublicCertificateInfoStub implements PublicCertificateInfo, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PublicCertificateInfoStub.class.getName());

    private String guid;
    private String appId;
    private String appUrl;
    private String certName;
    private String certInPemFormat;
    private String note;
    private String status;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    public PublicCertificateInfoStub()
    {
        this(null);
    }
    public PublicCertificateInfoStub(PublicCertificateInfo bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.appId = bean.getAppId();
            this.appUrl = bean.getAppUrl();
            this.certName = bean.getCertName();
            this.certInPemFormat = bean.getCertInPemFormat();
            this.note = bean.getNote();
            this.status = bean.getStatus();
            this.expirationTime = bean.getExpirationTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getAppId()
    {
        return this.appId;
    }
    public void setAppId(String appId)
    {
        this.appId = appId;
    }

    @XmlElement
    public String getAppUrl()
    {
        return this.appUrl;
    }
    public void setAppUrl(String appUrl)
    {
        this.appUrl = appUrl;
    }

    @XmlElement
    public String getCertName()
    {
        return this.certName;
    }
    public void setCertName(String certName)
    {
        this.certName = certName;
    }

    @XmlElement
    public String getCertInPemFormat()
    {
        return this.certInPemFormat;
    }
    public void setCertInPemFormat(String certInPemFormat)
    {
        this.certInPemFormat = certInPemFormat;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("appId", this.appId);
        dataMap.put("appUrl", this.appUrl);
        dataMap.put("certName", this.certName);
        dataMap.put("certInPemFormat", this.certInPemFormat);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);
        dataMap.put("expirationTime", this.expirationTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        hash = 31 * hash + delta;
        delta = appId == null ? 0 : appId.hashCode();
        hash = 31 * hash + delta;
        delta = appUrl == null ? 0 : appUrl.hashCode();
        hash = 31 * hash + delta;
        delta = certName == null ? 0 : certName.hashCode();
        hash = 31 * hash + delta;
        delta = certInPemFormat == null ? 0 : certInPemFormat.hashCode();
        hash = 31 * hash + delta;
        delta = note == null ? 0 : note.hashCode();
        hash = 31 * hash + delta;
        delta = status == null ? 0 : status.hashCode();
        hash = 31 * hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        hash = 31 * hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        hash = 31 * hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        hash = 31 * hash + delta;
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static PublicCertificateInfoStub convertBeanToStub(PublicCertificateInfo bean)
    {
        PublicCertificateInfoStub stub = null;
        if(bean instanceof PublicCertificateInfoStub) {
            stub = (PublicCertificateInfoStub) bean;
        } else {
            if(bean != null) {
                stub = new PublicCertificateInfoStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static PublicCertificateInfoStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of PublicCertificateInfoStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write PublicCertificateInfoStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write PublicCertificateInfoStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write PublicCertificateInfoStub object as a string.", e);
        }
        
        return null;
    }
    public static PublicCertificateInfoStub fromJsonString(String jsonStr)
    {
        try {
            PublicCertificateInfoStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, PublicCertificateInfoStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into PublicCertificateInfoStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into PublicCertificateInfoStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into PublicCertificateInfoStub object.", e);
        }
        
        return null;
    }

}
