package com.cronbay.ws.cert.service;

import java.util.List;

import com.cronbay.ws.cert.OAuthConsumerInfo;
import com.cronbay.ws.BaseException;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface OAuthConsumerInfoService 
{
    // TBD: Return an interface or a bean wrapper.

    OAuthConsumerInfo getOAuthConsumerInfo(String guid) throws BaseException;
    Object getOAuthConsumerInfo(String guid, String field) throws BaseException;
    List<OAuthConsumerInfo> getAllOAuthConsumerInfos() throws BaseException;
    List<OAuthConsumerInfo> getAllOAuthConsumerInfos(String ordering, Long offset, Integer count) throws BaseException;
    List<OAuthConsumerInfo> findOAuthConsumerInfos(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<OAuthConsumerInfo> findOAuthConsumerInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createOAuthConsumerInfo(String serviceName, String appId, String appUrl, String consumerKey, String consumerSecret, String note, String status, Long expirationTime) throws BaseException;
    //String createOAuthConsumerInfo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OAuthConsumerInfo?)
    String createOAuthConsumerInfo(OAuthConsumerInfo oAuthConsumerInfo) throws BaseException;          // Returns Guid.  (Return OAuthConsumerInfo?)
    Boolean updateOAuthConsumerInfo(String guid, String serviceName, String appId, String appUrl, String consumerKey, String consumerSecret, String note, String status, Long expirationTime) throws BaseException;
    //Boolean updateOAuthConsumerInfo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOAuthConsumerInfo(OAuthConsumerInfo oAuthConsumerInfo) throws BaseException;
    Boolean deleteOAuthConsumerInfo(String guid) throws BaseException;
    Boolean deleteOAuthConsumerInfo(OAuthConsumerInfo oAuthConsumerInfo) throws BaseException;
    Long deleteOAuthConsumerInfos(String filter, String params, List<String> values) throws BaseException;

//    Integer createOAuthConsumerInfos(List<OAuthConsumerInfo> oAuthConsumerInfos) throws BaseException;
//    Boolean updateeOAuthConsumerInfos(List<OAuthConsumerInfo> oAuthConsumerInfos) throws BaseException;

}
