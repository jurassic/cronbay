package com.cronbay.ws.cert.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.cert.PublicCertificateInfo;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "publicCertificateInfos")
public class PublicCertificateInfoListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PublicCertificateInfoListStub.class.getName());

    private List<PublicCertificateInfoStub> publicCertificateInfos = null;

    public PublicCertificateInfoListStub()
    {
        this(new ArrayList<PublicCertificateInfoStub>());
    }
    public PublicCertificateInfoListStub(List<PublicCertificateInfoStub> publicCertificateInfos)
    {
        this.publicCertificateInfos = publicCertificateInfos;
    }

    @XmlElement(name = "publicCertificateInfo")
    public List<PublicCertificateInfoStub> getList()
    {
        return publicCertificateInfos;
    }
    public void setList(List<PublicCertificateInfoStub> publicCertificateInfos)
    {
        this.publicCertificateInfos = publicCertificateInfos;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<PublicCertificateInfoStub> it = this.publicCertificateInfos.iterator();
        while(it.hasNext()) {
            PublicCertificateInfoStub publicCertificateInfo = it.next();
            sb.append(publicCertificateInfo.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static PublicCertificateInfoListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of PublicCertificateInfoListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write PublicCertificateInfoListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write PublicCertificateInfoListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write PublicCertificateInfoListStub object as a string.", e);
        }
        
        return null;
    }
    public static PublicCertificateInfoListStub fromJsonString(String jsonStr)
    {
        try {
            PublicCertificateInfoListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, PublicCertificateInfoListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into PublicCertificateInfoListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into PublicCertificateInfoListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into PublicCertificateInfoListStub object.", e);
        }
        
        return null;
    }

}
