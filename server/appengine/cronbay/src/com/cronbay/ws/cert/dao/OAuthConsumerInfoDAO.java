package com.cronbay.ws.cert.dao;

import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.cert.data.OAuthConsumerInfoDataObject;


// TBD: Add offset/count to getAllOAuthConsumerInfos() and findOAuthConsumerInfos(), etc.
public interface OAuthConsumerInfoDAO
{
    OAuthConsumerInfoDataObject getOAuthConsumerInfo(String guid) throws BaseException;
    List<OAuthConsumerInfoDataObject> getAllOAuthConsumerInfos() throws BaseException;
    List<OAuthConsumerInfoDataObject> getAllOAuthConsumerInfos(String ordering, Long offset, Integer count) throws BaseException;
    List<OAuthConsumerInfoDataObject> findOAuthConsumerInfos(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<OAuthConsumerInfoDataObject> findOAuthConsumerInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<OAuthConsumerInfoDataObject> findOAuthConsumerInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOAuthConsumerInfo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OAuthConsumerInfoDataObject?)
    String createOAuthConsumerInfo(OAuthConsumerInfoDataObject oAuthConsumerInfo) throws BaseException;          // Returns Guid.  (Return OAuthConsumerInfoDataObject?)
    //Boolean updateOAuthConsumerInfo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOAuthConsumerInfo(OAuthConsumerInfoDataObject oAuthConsumerInfo) throws BaseException;
    Boolean deleteOAuthConsumerInfo(String guid) throws BaseException;
    Boolean deleteOAuthConsumerInfo(OAuthConsumerInfoDataObject oAuthConsumerInfo) throws BaseException;
    Long deleteOAuthConsumerInfos(String filter, String params, List<String> values) throws BaseException;
}
