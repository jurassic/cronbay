package com.cronbay.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.data.RestRequestStructDataObject;

public class RestRequestStructBean implements RestRequestStruct
{
    private static final Logger log = Logger.getLogger(RestRequestStructBean.class.getName());

    // Embedded data object.
    private RestRequestStructDataObject dobj = null;

    public RestRequestStructBean()
    {
        this(new RestRequestStructDataObject());
    }
    public RestRequestStructBean(RestRequestStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public RestRequestStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getServiceName()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceName(String serviceName)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceName(serviceName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public String getServiceUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceUrl(serviceUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public String getRequestMethod()
    {
        if(getDataObject() != null) {
            return getDataObject().getRequestMethod();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRequestMethod(String requestMethod)
    {
        if(getDataObject() != null) {
            getDataObject().setRequestMethod(requestMethod);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public String getRequestUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getRequestUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRequestUrl(String requestUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setRequestUrl(requestUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public String getTargetEntity()
    {
        if(getDataObject() != null) {
            return getDataObject().getTargetEntity();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setTargetEntity(String targetEntity)
    {
        if(getDataObject() != null) {
            getDataObject().setTargetEntity(targetEntity);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public String getQueryString()
    {
        if(getDataObject() != null) {
            return getDataObject().getQueryString();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setQueryString(String queryString)
    {
        if(getDataObject() != null) {
            getDataObject().setQueryString(queryString);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public List<String> getQueryParams()
    {
        if(getDataObject() != null) {
            return getDataObject().getQueryParams();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setQueryParams(List<String> queryParams)
    {
        if(getDataObject() != null) {
            getDataObject().setQueryParams(queryParams);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public String getInputFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getInputFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setInputFormat(String inputFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setInputFormat(inputFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public String getInputContent()
    {
        if(getDataObject() != null) {
            return getDataObject().getInputContent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setInputContent(String inputContent)
    {
        if(getDataObject() != null) {
            getDataObject().setInputContent(inputContent);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public String getOutputFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputFormat(String outputFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputFormat(outputFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public Integer getMaxRetries()
    {
        if(getDataObject() != null) {
            return getDataObject().getMaxRetries();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setMaxRetries(Integer maxRetries)
    {
        if(getDataObject() != null) {
            getDataObject().setMaxRetries(maxRetries);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public Integer getRetryInterval()
    {
        if(getDataObject() != null) {
            return getDataObject().getRetryInterval();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRetryInterval(Integer retryInterval)
    {
        if(getDataObject() != null) {
            getDataObject().setRetryInterval(retryInterval);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RestRequestStructDataObject is null!");
        }
    }

    // TBD
    public RestRequestStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
