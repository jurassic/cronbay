package com.cronbay.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.data.CronScheduleStructDataObject;

public class CronScheduleStructBean implements CronScheduleStruct
{
    private static final Logger log = Logger.getLogger(CronScheduleStructBean.class.getName());

    // Embedded data object.
    private CronScheduleStructDataObject dobj = null;

    public CronScheduleStructBean()
    {
        this(new CronScheduleStructDataObject());
    }
    public CronScheduleStructBean(CronScheduleStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public CronScheduleStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
        }
    }

    public String getSchedule()
    {
        if(getDataObject() != null) {
            return getDataObject().getSchedule();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSchedule(String schedule)
    {
        if(getDataObject() != null) {
            getDataObject().setSchedule(schedule);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
        }
    }

    public String getTimezone()
    {
        if(getDataObject() != null) {
            return getDataObject().getTimezone();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setTimezone(String timezone)
    {
        if(getDataObject() != null) {
            getDataObject().setTimezone(timezone);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
        }
    }

    public Integer getMaxIterations()
    {
        if(getDataObject() != null) {
            return getDataObject().getMaxIterations();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setMaxIterations(Integer maxIterations)
    {
        if(getDataObject() != null) {
            getDataObject().setMaxIterations(maxIterations);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
        }
    }

    public Integer getRepeatInterval()
    {
        if(getDataObject() != null) {
            return getDataObject().getRepeatInterval();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRepeatInterval(Integer repeatInterval)
    {
        if(getDataObject() != null) {
            getDataObject().setRepeatInterval(repeatInterval);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
        }
    }

    public Long getFirtRunTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getFirtRunTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setFirtRunTime(Long firtRunTime)
    {
        if(getDataObject() != null) {
            getDataObject().setFirtRunTime(firtRunTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
        }
    }

    public Long getStartTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getStartTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setStartTime(Long startTime)
    {
        if(getDataObject() != null) {
            getDataObject().setStartTime(startTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
        }
    }

    public Long getEndTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getEndTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setEndTime(Long endTime)
    {
        if(getDataObject() != null) {
            getDataObject().setEndTime(endTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronScheduleStructDataObject is null!");
        }
    }

    // TBD
    public CronScheduleStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
