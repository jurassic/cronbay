package com.cronbay.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.data.NotificationStructDataObject;

public class NotificationStructBean implements NotificationStruct
{
    private static final Logger log = Logger.getLogger(NotificationStructBean.class.getName());

    // Embedded data object.
    private NotificationStructDataObject dobj = null;

    public NotificationStructBean()
    {
        this(new NotificationStructDataObject());
    }
    public NotificationStructBean(NotificationStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public NotificationStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getPreferredMode()
    {
        if(getDataObject() != null) {
            return getDataObject().getPreferredMode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NotificationStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPreferredMode(String preferredMode)
    {
        if(getDataObject() != null) {
            getDataObject().setPreferredMode(preferredMode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NotificationStructDataObject is null!");
        }
    }

    public String getMobileNumber()
    {
        if(getDataObject() != null) {
            return getDataObject().getMobileNumber();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NotificationStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setMobileNumber(String mobileNumber)
    {
        if(getDataObject() != null) {
            getDataObject().setMobileNumber(mobileNumber);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NotificationStructDataObject is null!");
        }
    }

    public String getEmailAddress()
    {
        if(getDataObject() != null) {
            return getDataObject().getEmailAddress();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NotificationStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setEmailAddress(String emailAddress)
    {
        if(getDataObject() != null) {
            getDataObject().setEmailAddress(emailAddress);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NotificationStructDataObject is null!");
        }
    }

    public String getCallbackUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getCallbackUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NotificationStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setCallbackUrl(String callbackUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setCallbackUrl(callbackUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NotificationStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NotificationStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded NotificationStructDataObject is null!");
        }
    }

    // TBD
    public NotificationStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
