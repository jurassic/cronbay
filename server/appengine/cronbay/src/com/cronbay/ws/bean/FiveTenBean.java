package com.cronbay.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.FiveTen;
import com.cronbay.ws.data.FiveTenDataObject;

public class FiveTenBean extends BeanBase implements FiveTen
{
    private static final Logger log = Logger.getLogger(FiveTenBean.class.getName());

    // Embedded data object.
    private FiveTenDataObject dobj = null;

    public FiveTenBean()
    {
        this(new FiveTenDataObject());
    }
    public FiveTenBean(String guid)
    {
        this(new FiveTenDataObject(guid));
    }
    public FiveTenBean(FiveTenDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public FiveTenDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FiveTenDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FiveTenDataObject is null!");
        }
    }

    public Integer getCounter()
    {
        if(getDataObject() != null) {
            return getDataObject().getCounter();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FiveTenDataObject is null!");
            return null;   // ???
        }
    }
    public void setCounter(Integer counter)
    {
        if(getDataObject() != null) {
            getDataObject().setCounter(counter);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FiveTenDataObject is null!");
        }
    }

    public String getRequesterIpAddress()
    {
        if(getDataObject() != null) {
            return getDataObject().getRequesterIpAddress();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FiveTenDataObject is null!");
            return null;   // ???
        }
    }
    public void setRequesterIpAddress(String requesterIpAddress)
    {
        if(getDataObject() != null) {
            getDataObject().setRequesterIpAddress(requesterIpAddress);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FiveTenDataObject is null!");
        }
    }

    // TBD
    public FiveTenDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
