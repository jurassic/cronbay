package com.cronbay.ws.bean;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.data.KeyedDataObject;

public abstract class BeanBase
{
    private static final Logger log = Logger.getLogger(BeanBase.class.getName());
    
    public BeanBase()
    {
    }

    public abstract KeyedDataObject getDataObject();

    public Long getCreatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getCreatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
            return null;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getDataObject() != null) {
            getDataObject().setCreatedTime(createdTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
        }
    }

    public Long getModifiedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getModifiedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
            return null;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setModifiedTime(modifiedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DataObject is null!");
        }
    }

}
