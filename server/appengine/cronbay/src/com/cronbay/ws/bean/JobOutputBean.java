package com.cronbay.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.data.GaeAppStructDataObject;
import com.cronbay.ws.data.JobOutputDataObject;

public class JobOutputBean extends BeanBase implements JobOutput
{
    private static final Logger log = Logger.getLogger(JobOutputBean.class.getName());

    // Embedded data object.
    private JobOutputDataObject dobj = null;

    public JobOutputBean()
    {
        this(new JobOutputDataObject());
    }
    public JobOutputBean(String guid)
    {
        this(new JobOutputDataObject(guid));
    }
    public JobOutputBean(JobOutputDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public JobOutputDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getManagerApp()
    {
        if(getDataObject() != null) {
            return getDataObject().getManagerApp();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getDataObject() != null) {
            getDataObject().setManagerApp(managerApp);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public Long getAppAcl()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppAcl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getDataObject() != null) {
            getDataObject().setAppAcl(appAcl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public GaeAppStruct getGaeApp()
    {
        if(getDataObject() != null) {
            GaeAppStruct _field = getDataObject().getGaeApp();
            if(_field == null) {
                log.log(Level.INFO, "gaeApp is null.");
                return null;
            } else {
                return new GaeAppStructBean((GaeAppStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getDataObject() != null) {
            getDataObject().setGaeApp(
                (gaeApp instanceof GaeAppStructBean) ?
                ((GaeAppStructBean) gaeApp).toDataObject() :
                ((gaeApp instanceof GaeAppStructDataObject) ? gaeApp : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getOwnerUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getOwnerUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setOwnerUser(String ownerUser)
    {
        if(getDataObject() != null) {
            getDataObject().setOwnerUser(ownerUser);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public Long getUserAcl()
    {
        if(getDataObject() != null) {
            return getDataObject().getUserAcl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setUserAcl(Long userAcl)
    {
        if(getDataObject() != null) {
            getDataObject().setUserAcl(userAcl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getCronJob()
    {
        if(getDataObject() != null) {
            return getDataObject().getCronJob();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setCronJob(String cronJob)
    {
        if(getDataObject() != null) {
            getDataObject().setCronJob(cronJob);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getPreviousRun()
    {
        if(getDataObject() != null) {
            return getDataObject().getPreviousRun();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setPreviousRun(String previousRun)
    {
        if(getDataObject() != null) {
            getDataObject().setPreviousRun(previousRun);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getPreviousTry()
    {
        if(getDataObject() != null) {
            return getDataObject().getPreviousTry();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setPreviousTry(String previousTry)
    {
        if(getDataObject() != null) {
            getDataObject().setPreviousTry(previousTry);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getResponseCode()
    {
        if(getDataObject() != null) {
            return getDataObject().getResponseCode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setResponseCode(String responseCode)
    {
        if(getDataObject() != null) {
            getDataObject().setResponseCode(responseCode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getOutputFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputFormat(String outputFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputFormat(outputFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getOutputText()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputText();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputText(String outputText)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputText(outputText);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public List<String> getOutputData()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputData();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputData(List<String> outputData)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputData(outputData);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getOutputHash()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputHash();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputHash(String outputHash)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputHash(outputHash);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getPermalink()
    {
        if(getDataObject() != null) {
            return getDataObject().getPermalink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setPermalink(String permalink)
    {
        if(getDataObject() != null) {
            getDataObject().setPermalink(permalink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getShortlink()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortlink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortlink(String shortlink)
    {
        if(getDataObject() != null) {
            getDataObject().setShortlink(shortlink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getResult()
    {
        if(getDataObject() != null) {
            return getDataObject().getResult();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setResult(String result)
    {
        if(getDataObject() != null) {
            getDataObject().setResult(result);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getExtra()
    {
        if(getDataObject() != null) {
            return getDataObject().getExtra();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setExtra(String extra)
    {
        if(getDataObject() != null) {
            getDataObject().setExtra(extra);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public Integer getRetryCounter()
    {
        if(getDataObject() != null) {
            return getDataObject().getRetryCounter();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setRetryCounter(Integer retryCounter)
    {
        if(getDataObject() != null) {
            getDataObject().setRetryCounter(retryCounter);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public Long getScheduledTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getScheduledTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setScheduledTime(Long scheduledTime)
    {
        if(getDataObject() != null) {
            getDataObject().setScheduledTime(scheduledTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    public Long getProcessedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getProcessedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
            return null;   // ???
        }
    }
    public void setProcessedTime(Long processedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setProcessedTime(processedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded JobOutputDataObject is null!");
        }
    }

    // TBD
    public JobOutputDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
