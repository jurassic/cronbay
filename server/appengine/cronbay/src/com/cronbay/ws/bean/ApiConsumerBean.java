package com.cronbay.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.ApiConsumer;
import com.cronbay.ws.data.ApiConsumerDataObject;

public class ApiConsumerBean extends BeanBase implements ApiConsumer
{
    private static final Logger log = Logger.getLogger(ApiConsumerBean.class.getName());

    // Embedded data object.
    private ApiConsumerDataObject dobj = null;

    public ApiConsumerBean()
    {
        this(new ApiConsumerDataObject());
    }
    public ApiConsumerBean(String guid)
    {
        this(new ApiConsumerDataObject(guid));
    }
    public ApiConsumerBean(ApiConsumerDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public ApiConsumerDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
        }
    }

    public String getAeryId()
    {
        if(getDataObject() != null) {
            return getDataObject().getAeryId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
            return null;   // ???
        }
    }
    public void setAeryId(String aeryId)
    {
        if(getDataObject() != null) {
            getDataObject().setAeryId(aeryId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
        }
    }

    public String getName()
    {
        if(getDataObject() != null) {
            return getDataObject().getName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
            return null;   // ???
        }
    }
    public void setName(String name)
    {
        if(getDataObject() != null) {
            getDataObject().setName(name);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
        }
    }

    public String getAppKey()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppKey();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppKey(String appKey)
    {
        if(getDataObject() != null) {
            getDataObject().setAppKey(appKey);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
        }
    }

    public String getAppSecret()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppSecret();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppSecret(String appSecret)
    {
        if(getDataObject() != null) {
            getDataObject().setAppSecret(appSecret);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ApiConsumerDataObject is null!");
        }
    }

    // TBD
    public ApiConsumerDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
