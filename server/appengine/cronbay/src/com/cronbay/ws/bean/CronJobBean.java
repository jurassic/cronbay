package com.cronbay.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.data.NotificationStructDataObject;
import com.cronbay.ws.data.RestRequestStructDataObject;
import com.cronbay.ws.data.GaeAppStructDataObject;
import com.cronbay.ws.data.ReferrerInfoStructDataObject;
import com.cronbay.ws.data.CronScheduleStructDataObject;
import com.cronbay.ws.data.CronJobDataObject;

public class CronJobBean extends BeanBase implements CronJob
{
    private static final Logger log = Logger.getLogger(CronJobBean.class.getName());

    // Embedded data object.
    private CronJobDataObject dobj = null;

    public CronJobBean()
    {
        this(new CronJobDataObject());
    }
    public CronJobBean(String guid)
    {
        this(new CronJobDataObject(guid));
    }
    public CronJobBean(CronJobDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public CronJobDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getManagerApp()
    {
        if(getDataObject() != null) {
            return getDataObject().getManagerApp();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getDataObject() != null) {
            getDataObject().setManagerApp(managerApp);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public Long getAppAcl()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppAcl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getDataObject() != null) {
            getDataObject().setAppAcl(appAcl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public GaeAppStruct getGaeApp()
    {
        if(getDataObject() != null) {
            GaeAppStruct _field = getDataObject().getGaeApp();
            if(_field == null) {
                log.log(Level.INFO, "gaeApp is null.");
                return null;
            } else {
                return new GaeAppStructBean((GaeAppStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getDataObject() != null) {
            getDataObject().setGaeApp(
                (gaeApp instanceof GaeAppStructBean) ?
                ((GaeAppStructBean) gaeApp).toDataObject() :
                ((gaeApp instanceof GaeAppStructDataObject) ? gaeApp : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getOwnerUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getOwnerUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setOwnerUser(String ownerUser)
    {
        if(getDataObject() != null) {
            getDataObject().setOwnerUser(ownerUser);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public Long getUserAcl()
    {
        if(getDataObject() != null) {
            return getDataObject().getUserAcl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setUserAcl(Long userAcl)
    {
        if(getDataObject() != null) {
            getDataObject().setUserAcl(userAcl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public Integer getJobId()
    {
        if(getDataObject() != null) {
            return getDataObject().getJobId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setJobId(Integer jobId)
    {
        if(getDataObject() != null) {
            getDataObject().setJobId(jobId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getOriginJob()
    {
        if(getDataObject() != null) {
            return getDataObject().getOriginJob();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setOriginJob(String originJob)
    {
        if(getDataObject() != null) {
            getDataObject().setOriginJob(originJob);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public RestRequestStruct getRestRequest()
    {
        if(getDataObject() != null) {
            RestRequestStruct _field = getDataObject().getRestRequest();
            if(_field == null) {
                log.log(Level.INFO, "restRequest is null.");
                return null;
            } else {
                return new RestRequestStructBean((RestRequestStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setRestRequest(RestRequestStruct restRequest)
    {
        if(getDataObject() != null) {
            getDataObject().setRestRequest(
                (restRequest instanceof RestRequestStructBean) ?
                ((RestRequestStructBean) restRequest).toDataObject() :
                ((restRequest instanceof RestRequestStructDataObject) ? restRequest : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getPermalink()
    {
        if(getDataObject() != null) {
            return getDataObject().getPermalink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setPermalink(String permalink)
    {
        if(getDataObject() != null) {
            getDataObject().setPermalink(permalink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getShortlink()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortlink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortlink(String shortlink)
    {
        if(getDataObject() != null) {
            getDataObject().setShortlink(shortlink);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public Integer getJobStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getJobStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setJobStatus(Integer jobStatus)
    {
        if(getDataObject() != null) {
            getDataObject().setJobStatus(jobStatus);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getExtra()
    {
        if(getDataObject() != null) {
            return getDataObject().getExtra();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setExtra(String extra)
    {
        if(getDataObject() != null) {
            getDataObject().setExtra(extra);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public Boolean isAlert()
    {
        if(getDataObject() != null) {
            return getDataObject().isAlert();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setAlert(Boolean alert)
    {
        if(getDataObject() != null) {
            getDataObject().setAlert(alert);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public NotificationStruct getNotificationPref()
    {
        if(getDataObject() != null) {
            NotificationStruct _field = getDataObject().getNotificationPref();
            if(_field == null) {
                log.log(Level.INFO, "notificationPref is null.");
                return null;
            } else {
                return new NotificationStructBean((NotificationStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if(getDataObject() != null) {
            getDataObject().setNotificationPref(
                (notificationPref instanceof NotificationStructBean) ?
                ((NotificationStructBean) notificationPref).toDataObject() :
                ((notificationPref instanceof NotificationStructDataObject) ? notificationPref : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        if(getDataObject() != null) {
            ReferrerInfoStruct _field = getDataObject().getReferrerInfo();
            if(_field == null) {
                log.log(Level.INFO, "referrerInfo is null.");
                return null;
            } else {
                return new ReferrerInfoStructBean((ReferrerInfoStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getDataObject() != null) {
            getDataObject().setReferrerInfo(
                (referrerInfo instanceof ReferrerInfoStructBean) ?
                ((ReferrerInfoStructBean) referrerInfo).toDataObject() :
                ((referrerInfo instanceof ReferrerInfoStructDataObject) ? referrerInfo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public CronScheduleStruct getCronSchedule()
    {
        if(getDataObject() != null) {
            CronScheduleStruct _field = getDataObject().getCronSchedule();
            if(_field == null) {
                log.log(Level.INFO, "cronSchedule is null.");
                return null;
            } else {
                return new CronScheduleStructBean((CronScheduleStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setCronSchedule(CronScheduleStruct cronSchedule)
    {
        if(getDataObject() != null) {
            getDataObject().setCronSchedule(
                (cronSchedule instanceof CronScheduleStructBean) ?
                ((CronScheduleStructBean) cronSchedule).toDataObject() :
                ((cronSchedule instanceof CronScheduleStructDataObject) ? cronSchedule : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    public Long getNextRunTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getNextRunTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
            return null;   // ???
        }
    }
    public void setNextRunTime(Long nextRunTime)
    {
        if(getDataObject() != null) {
            getDataObject().setNextRunTime(nextRunTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CronJobDataObject is null!");
        }
    }

    // TBD
    public CronJobDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
