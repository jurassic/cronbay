package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.FiveTen;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "fiveTen")
@XmlType(propOrder = {"guid", "counter", "requesterIpAddress", "createdTime", "modifiedTime"})
public class FiveTenStub implements FiveTen, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FiveTenStub.class.getName());

    private String guid;
    private Integer counter;
    private String requesterIpAddress;
    private Long createdTime;
    private Long modifiedTime;

    public FiveTenStub()
    {
        this(null);
    }
    public FiveTenStub(FiveTen bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.counter = bean.getCounter();
            this.requesterIpAddress = bean.getRequesterIpAddress();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public Integer getCounter()
    {
        return this.counter;
    }
    public void setCounter(Integer counter)
    {
        this.counter = counter;
    }

    @XmlElement
    public String getRequesterIpAddress()
    {
        return this.requesterIpAddress;
    }
    public void setRequesterIpAddress(String requesterIpAddress)
    {
        this.requesterIpAddress = requesterIpAddress;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("counter", this.counter);
        dataMap.put("requesterIpAddress", this.requesterIpAddress);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = counter == null ? 0 : counter.hashCode();
        _hash = 31 * _hash + delta;
        delta = requesterIpAddress == null ? 0 : requesterIpAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static FiveTenStub convertBeanToStub(FiveTen bean)
    {
        FiveTenStub stub = null;
        if(bean instanceof FiveTenStub) {
            stub = (FiveTenStub) bean;
        } else {
            if(bean != null) {
                stub = new FiveTenStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FiveTenStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of FiveTenStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FiveTenStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FiveTenStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FiveTenStub object as a string.", e);
        }
        
        return null;
    }
    public static FiveTenStub fromJsonString(String jsonStr)
    {
        try {
            FiveTenStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FiveTenStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FiveTenStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FiveTenStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FiveTenStub object.", e);
        }
        
        return null;
    }

}
