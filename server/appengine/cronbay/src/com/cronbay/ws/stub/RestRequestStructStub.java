package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;

import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "restRequestStruct")
@XmlType(propOrder = {"serviceName", "serviceUrl", "requestMethod", "requestUrl", "targetEntity", "queryString", "queryParams", "inputFormat", "inputContent", "outputFormat", "maxRetries", "retryInterval", "note"})
public class RestRequestStructStub implements RestRequestStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RestRequestStructStub.class.getName());

    private String serviceName;
    private String serviceUrl;
    private String requestMethod;
    private String requestUrl;
    private String targetEntity;
    private String queryString;
    private List<String> queryParams;
    private String inputFormat;
    private String inputContent;
    private String outputFormat;
    private Integer maxRetries;
    private Integer retryInterval;
    private String note;

    public RestRequestStructStub()
    {
        this(null);
    }
    public RestRequestStructStub(RestRequestStruct bean)
    {
        if(bean != null) {
            this.serviceName = bean.getServiceName();
            this.serviceUrl = bean.getServiceUrl();
            this.requestMethod = bean.getRequestMethod();
            this.requestUrl = bean.getRequestUrl();
            this.targetEntity = bean.getTargetEntity();
            this.queryString = bean.getQueryString();
            this.queryParams = bean.getQueryParams();
            this.inputFormat = bean.getInputFormat();
            this.inputContent = bean.getInputContent();
            this.outputFormat = bean.getOutputFormat();
            this.maxRetries = bean.getMaxRetries();
            this.retryInterval = bean.getRetryInterval();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getServiceName()
    {
        return this.serviceName;
    }
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    @XmlElement
    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    @XmlElement
    public String getRequestMethod()
    {
        return this.requestMethod;
    }
    public void setRequestMethod(String requestMethod)
    {
        this.requestMethod = requestMethod;
    }

    @XmlElement
    public String getRequestUrl()
    {
        return this.requestUrl;
    }
    public void setRequestUrl(String requestUrl)
    {
        this.requestUrl = requestUrl;
    }

    @XmlElement
    public String getTargetEntity()
    {
        return this.targetEntity;
    }
    public void setTargetEntity(String targetEntity)
    {
        this.targetEntity = targetEntity;
    }

    @XmlElement
    public String getQueryString()
    {
        return this.queryString;
    }
    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    @XmlElement
    public List<String> getQueryParams()
    {
        return this.queryParams;
    }
    public void setQueryParams(List<String> queryParams)
    {
        this.queryParams = queryParams;
    }

    @XmlElement
    public String getInputFormat()
    {
        return this.inputFormat;
    }
    public void setInputFormat(String inputFormat)
    {
        this.inputFormat = inputFormat;
    }

    @XmlElement
    public String getInputContent()
    {
        return this.inputContent;
    }
    public void setInputContent(String inputContent)
    {
        this.inputContent = inputContent;
    }

    @XmlElement
    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    @XmlElement
    public Integer getMaxRetries()
    {
        return this.maxRetries;
    }
    public void setMaxRetries(Integer maxRetries)
    {
        this.maxRetries = maxRetries;
    }

    @XmlElement
    public Integer getRetryInterval()
    {
        return this.retryInterval;
    }
    public void setRetryInterval(Integer retryInterval)
    {
        this.retryInterval = retryInterval;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("serviceName", this.serviceName);
        dataMap.put("serviceUrl", this.serviceUrl);
        dataMap.put("requestMethod", this.requestMethod);
        dataMap.put("requestUrl", this.requestUrl);
        dataMap.put("targetEntity", this.targetEntity);
        dataMap.put("queryString", this.queryString);
        dataMap.put("queryParams", this.queryParams);
        dataMap.put("inputFormat", this.inputFormat);
        dataMap.put("inputContent", this.inputContent);
        dataMap.put("outputFormat", this.outputFormat);
        dataMap.put("maxRetries", this.maxRetries);
        dataMap.put("retryInterval", this.retryInterval);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = serviceName == null ? 0 : serviceName.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = requestMethod == null ? 0 : requestMethod.hashCode();
        _hash = 31 * _hash + delta;
        delta = requestUrl == null ? 0 : requestUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetEntity == null ? 0 : targetEntity.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryString == null ? 0 : queryString.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryParams == null ? 0 : queryParams.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputFormat == null ? 0 : inputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputContent == null ? 0 : inputContent.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFormat == null ? 0 : outputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = maxRetries == null ? 0 : maxRetries.hashCode();
        _hash = 31 * _hash + delta;
        delta = retryInterval == null ? 0 : retryInterval.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static RestRequestStructStub convertBeanToStub(RestRequestStruct bean)
    {
        RestRequestStructStub stub = null;
        if(bean instanceof RestRequestStructStub) {
            stub = (RestRequestStructStub) bean;
        } else {
            if(bean != null) {
                stub = new RestRequestStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RestRequestStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of RestRequestStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RestRequestStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RestRequestStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RestRequestStructStub object as a string.", e);
        }
        
        return null;
    }
    public static RestRequestStructStub fromJsonString(String jsonStr)
    {
        try {
            RestRequestStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RestRequestStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RestRequestStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RestRequestStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RestRequestStructStub object.", e);
        }
        
        return null;
    }

}
