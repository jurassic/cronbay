package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.ApiConsumer;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "apiConsumers")
@XmlType(propOrder = {"apiConsumer"})
public class ApiConsumerListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ApiConsumerListStub.class.getName());

    private List<ApiConsumerStub> apiConsumers = null;

    public ApiConsumerListStub()
    {
        this(new ArrayList<ApiConsumerStub>());
    }
    public ApiConsumerListStub(List<ApiConsumerStub> apiConsumers)
    {
        this.apiConsumers = apiConsumers;
    }

    public boolean isEmpty()
    {
        if(apiConsumers == null) {
            return true;
        } else {
            return apiConsumers.isEmpty();
        }
    }
    public int getSize()
    {
        if(apiConsumers == null) {
            return 0;
        } else {
            return apiConsumers.size();
        }
    }

    @XmlElement(name = "apiConsumer")
    public List<ApiConsumerStub> getApiConsumer()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ApiConsumerStub> getList()
    {
        return apiConsumers;
    }
    public void setList(List<ApiConsumerStub> apiConsumers)
    {
        this.apiConsumers = apiConsumers;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<ApiConsumerStub> it = this.apiConsumers.iterator();
        while(it.hasNext()) {
            ApiConsumerStub apiConsumer = it.next();
            sb.append(apiConsumer.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ApiConsumerListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of ApiConsumerListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ApiConsumerListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ApiConsumerListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ApiConsumerListStub object as a string.", e);
        }
        
        return null;
    }
    public static ApiConsumerListStub fromJsonString(String jsonStr)
    {
        try {
            ApiConsumerListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ApiConsumerListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ApiConsumerListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ApiConsumerListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ApiConsumerListStub object.", e);
        }
        
        return null;
    }

}
