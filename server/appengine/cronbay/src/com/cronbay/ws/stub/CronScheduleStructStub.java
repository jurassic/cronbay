package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "cronScheduleStruct")
@XmlType(propOrder = {"type", "schedule", "timezone", "maxIterations", "repeatInterval", "firtRunTime", "startTime", "endTime", "note"})
public class CronScheduleStructStub implements CronScheduleStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CronScheduleStructStub.class.getName());

    private String type;
    private String schedule;
    private String timezone;
    private Integer maxIterations;
    private Integer repeatInterval;
    private Long firtRunTime;
    private Long startTime;
    private Long endTime;
    private String note;

    public CronScheduleStructStub()
    {
        this(null);
    }
    public CronScheduleStructStub(CronScheduleStruct bean)
    {
        if(bean != null) {
            this.type = bean.getType();
            this.schedule = bean.getSchedule();
            this.timezone = bean.getTimezone();
            this.maxIterations = bean.getMaxIterations();
            this.repeatInterval = bean.getRepeatInterval();
            this.firtRunTime = bean.getFirtRunTime();
            this.startTime = bean.getStartTime();
            this.endTime = bean.getEndTime();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    @XmlElement
    public String getSchedule()
    {
        return this.schedule;
    }
    public void setSchedule(String schedule)
    {
        this.schedule = schedule;
    }

    @XmlElement
    public String getTimezone()
    {
        return this.timezone;
    }
    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }

    @XmlElement
    public Integer getMaxIterations()
    {
        return this.maxIterations;
    }
    public void setMaxIterations(Integer maxIterations)
    {
        this.maxIterations = maxIterations;
    }

    @XmlElement
    public Integer getRepeatInterval()
    {
        return this.repeatInterval;
    }
    public void setRepeatInterval(Integer repeatInterval)
    {
        this.repeatInterval = repeatInterval;
    }

    @XmlElement
    public Long getFirtRunTime()
    {
        return this.firtRunTime;
    }
    public void setFirtRunTime(Long firtRunTime)
    {
        this.firtRunTime = firtRunTime;
    }

    @XmlElement
    public Long getStartTime()
    {
        return this.startTime;
    }
    public void setStartTime(Long startTime)
    {
        this.startTime = startTime;
    }

    @XmlElement
    public Long getEndTime()
    {
        return this.endTime;
    }
    public void setEndTime(Long endTime)
    {
        this.endTime = endTime;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("type", this.type);
        dataMap.put("schedule", this.schedule);
        dataMap.put("timezone", this.timezone);
        dataMap.put("maxIterations", this.maxIterations);
        dataMap.put("repeatInterval", this.repeatInterval);
        dataMap.put("firtRunTime", this.firtRunTime);
        dataMap.put("startTime", this.startTime);
        dataMap.put("endTime", this.endTime);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = schedule == null ? 0 : schedule.hashCode();
        _hash = 31 * _hash + delta;
        delta = timezone == null ? 0 : timezone.hashCode();
        _hash = 31 * _hash + delta;
        delta = maxIterations == null ? 0 : maxIterations.hashCode();
        _hash = 31 * _hash + delta;
        delta = repeatInterval == null ? 0 : repeatInterval.hashCode();
        _hash = 31 * _hash + delta;
        delta = firtRunTime == null ? 0 : firtRunTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = startTime == null ? 0 : startTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = endTime == null ? 0 : endTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static CronScheduleStructStub convertBeanToStub(CronScheduleStruct bean)
    {
        CronScheduleStructStub stub = null;
        if(bean instanceof CronScheduleStructStub) {
            stub = (CronScheduleStructStub) bean;
        } else {
            if(bean != null) {
                stub = new CronScheduleStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CronScheduleStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of CronScheduleStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CronScheduleStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CronScheduleStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CronScheduleStructStub object as a string.", e);
        }
        
        return null;
    }
    public static CronScheduleStructStub fromJsonString(String jsonStr)
    {
        try {
            CronScheduleStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CronScheduleStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CronScheduleStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CronScheduleStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CronScheduleStructStub object.", e);
        }
        
        return null;
    }

}
