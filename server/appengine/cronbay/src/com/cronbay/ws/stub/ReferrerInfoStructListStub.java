package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "referrerInfoStructs")
@XmlType(propOrder = {"referrerInfoStruct"})
public class ReferrerInfoStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ReferrerInfoStructListStub.class.getName());

    private List<ReferrerInfoStructStub> referrerInfoStructs = null;

    public ReferrerInfoStructListStub()
    {
        this(new ArrayList<ReferrerInfoStructStub>());
    }
    public ReferrerInfoStructListStub(List<ReferrerInfoStructStub> referrerInfoStructs)
    {
        this.referrerInfoStructs = referrerInfoStructs;
    }

    public boolean isEmpty()
    {
        if(referrerInfoStructs == null) {
            return true;
        } else {
            return referrerInfoStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(referrerInfoStructs == null) {
            return 0;
        } else {
            return referrerInfoStructs.size();
        }
    }

    @XmlElement(name = "referrerInfoStruct")
    public List<ReferrerInfoStructStub> getReferrerInfoStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ReferrerInfoStructStub> getList()
    {
        return referrerInfoStructs;
    }
    public void setList(List<ReferrerInfoStructStub> referrerInfoStructs)
    {
        this.referrerInfoStructs = referrerInfoStructs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<ReferrerInfoStructStub> it = this.referrerInfoStructs.iterator();
        while(it.hasNext()) {
            ReferrerInfoStructStub referrerInfoStruct = it.next();
            sb.append(referrerInfoStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ReferrerInfoStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of ReferrerInfoStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ReferrerInfoStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ReferrerInfoStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ReferrerInfoStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static ReferrerInfoStructListStub fromJsonString(String jsonStr)
    {
        try {
            ReferrerInfoStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ReferrerInfoStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ReferrerInfoStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ReferrerInfoStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ReferrerInfoStructListStub object.", e);
        }
        
        return null;
    }

}
