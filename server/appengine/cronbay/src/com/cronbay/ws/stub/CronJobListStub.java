package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.CronJob;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "cronJobs")
@XmlType(propOrder = {"cronJob"})
public class CronJobListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CronJobListStub.class.getName());

    private List<CronJobStub> cronJobs = null;

    public CronJobListStub()
    {
        this(new ArrayList<CronJobStub>());
    }
    public CronJobListStub(List<CronJobStub> cronJobs)
    {
        this.cronJobs = cronJobs;
    }

    public boolean isEmpty()
    {
        if(cronJobs == null) {
            return true;
        } else {
            return cronJobs.isEmpty();
        }
    }
    public int getSize()
    {
        if(cronJobs == null) {
            return 0;
        } else {
            return cronJobs.size();
        }
    }

    @XmlElement(name = "cronJob")
    public List<CronJobStub> getCronJob()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<CronJobStub> getList()
    {
        return cronJobs;
    }
    public void setList(List<CronJobStub> cronJobs)
    {
        this.cronJobs = cronJobs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<CronJobStub> it = this.cronJobs.iterator();
        while(it.hasNext()) {
            CronJobStub cronJob = it.next();
            sb.append(cronJob.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CronJobListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of CronJobListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CronJobListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CronJobListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CronJobListStub object as a string.", e);
        }
        
        return null;
    }
    public static CronJobListStub fromJsonString(String jsonStr)
    {
        try {
            CronJobListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CronJobListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CronJobListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CronJobListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CronJobListStub object.", e);
        }
        
        return null;
    }

}
