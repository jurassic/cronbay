package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;

import com.cronbay.ws.JobOutput;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "jobOutputs")
@XmlType(propOrder = {"jobOutput"})
public class JobOutputListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(JobOutputListStub.class.getName());

    private List<JobOutputStub> jobOutputs = null;

    public JobOutputListStub()
    {
        this(new ArrayList<JobOutputStub>());
    }
    public JobOutputListStub(List<JobOutputStub> jobOutputs)
    {
        this.jobOutputs = jobOutputs;
    }

    public boolean isEmpty()
    {
        if(jobOutputs == null) {
            return true;
        } else {
            return jobOutputs.isEmpty();
        }
    }
    public int getSize()
    {
        if(jobOutputs == null) {
            return 0;
        } else {
            return jobOutputs.size();
        }
    }

    @XmlElement(name = "jobOutput")
    public List<JobOutputStub> getJobOutput()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<JobOutputStub> getList()
    {
        return jobOutputs;
    }
    public void setList(List<JobOutputStub> jobOutputs)
    {
        this.jobOutputs = jobOutputs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<JobOutputStub> it = this.jobOutputs.iterator();
        while(it.hasNext()) {
            JobOutputStub jobOutput = it.next();
            sb.append(jobOutput.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static JobOutputListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of JobOutputListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write JobOutputListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write JobOutputListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write JobOutputListStub object as a string.", e);
        }
        
        return null;
    }
    public static JobOutputListStub fromJsonString(String jsonStr)
    {
        try {
            JobOutputListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, JobOutputListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into JobOutputListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into JobOutputListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into JobOutputListStub object.", e);
        }
        
        return null;
    }

}
