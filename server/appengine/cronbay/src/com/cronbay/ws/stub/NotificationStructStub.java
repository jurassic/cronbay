package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "notificationStruct")
@XmlType(propOrder = {"preferredMode", "mobileNumber", "emailAddress", "callbackUrl", "note"})
public class NotificationStructStub implements NotificationStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(NotificationStructStub.class.getName());

    private String preferredMode;
    private String mobileNumber;
    private String emailAddress;
    private String callbackUrl;
    private String note;

    public NotificationStructStub()
    {
        this(null);
    }
    public NotificationStructStub(NotificationStruct bean)
    {
        if(bean != null) {
            this.preferredMode = bean.getPreferredMode();
            this.mobileNumber = bean.getMobileNumber();
            this.emailAddress = bean.getEmailAddress();
            this.callbackUrl = bean.getCallbackUrl();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getPreferredMode()
    {
        return this.preferredMode;
    }
    public void setPreferredMode(String preferredMode)
    {
        this.preferredMode = preferredMode;
    }

    @XmlElement
    public String getMobileNumber()
    {
        return this.mobileNumber;
    }
    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    @XmlElement
    public String getEmailAddress()
    {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    @XmlElement
    public String getCallbackUrl()
    {
        return this.callbackUrl;
    }
    public void setCallbackUrl(String callbackUrl)
    {
        this.callbackUrl = callbackUrl;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("preferredMode", this.preferredMode);
        dataMap.put("mobileNumber", this.mobileNumber);
        dataMap.put("emailAddress", this.emailAddress);
        dataMap.put("callbackUrl", this.callbackUrl);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = preferredMode == null ? 0 : preferredMode.hashCode();
        _hash = 31 * _hash + delta;
        delta = mobileNumber == null ? 0 : mobileNumber.hashCode();
        _hash = 31 * _hash + delta;
        delta = emailAddress == null ? 0 : emailAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = callbackUrl == null ? 0 : callbackUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static NotificationStructStub convertBeanToStub(NotificationStruct bean)
    {
        NotificationStructStub stub = null;
        if(bean instanceof NotificationStructStub) {
            stub = (NotificationStructStub) bean;
        } else {
            if(bean != null) {
                stub = new NotificationStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static NotificationStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of NotificationStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write NotificationStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write NotificationStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write NotificationStructStub object as a string.", e);
        }
        
        return null;
    }
    public static NotificationStructStub fromJsonString(String jsonStr)
    {
        try {
            NotificationStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, NotificationStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into NotificationStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into NotificationStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into NotificationStructStub object.", e);
        }
        
        return null;
    }

}
