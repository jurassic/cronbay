package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.ServiceInfo;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "serviceInfos")
@XmlType(propOrder = {"serviceInfo"})
public class ServiceInfoListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ServiceInfoListStub.class.getName());

    private List<ServiceInfoStub> serviceInfos = null;

    public ServiceInfoListStub()
    {
        this(new ArrayList<ServiceInfoStub>());
    }
    public ServiceInfoListStub(List<ServiceInfoStub> serviceInfos)
    {
        this.serviceInfos = serviceInfos;
    }

    public boolean isEmpty()
    {
        if(serviceInfos == null) {
            return true;
        } else {
            return serviceInfos.isEmpty();
        }
    }
    public int getSize()
    {
        if(serviceInfos == null) {
            return 0;
        } else {
            return serviceInfos.size();
        }
    }

    @XmlElement(name = "serviceInfo")
    public List<ServiceInfoStub> getServiceInfo()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ServiceInfoStub> getList()
    {
        return serviceInfos;
    }
    public void setList(List<ServiceInfoStub> serviceInfos)
    {
        this.serviceInfos = serviceInfos;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<ServiceInfoStub> it = this.serviceInfos.iterator();
        while(it.hasNext()) {
            ServiceInfoStub serviceInfo = it.next();
            sb.append(serviceInfo.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ServiceInfoListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of ServiceInfoListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ServiceInfoListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ServiceInfoListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ServiceInfoListStub object as a string.", e);
        }
        
        return null;
    }
    public static ServiceInfoListStub fromJsonString(String jsonStr)
    {
        try {
            ServiceInfoListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ServiceInfoListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ServiceInfoListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ServiceInfoListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ServiceInfoListStub object.", e);
        }
        
        return null;
    }

}
