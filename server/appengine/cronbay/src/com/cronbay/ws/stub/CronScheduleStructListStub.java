package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "cronScheduleStructs")
@XmlType(propOrder = {"cronScheduleStruct"})
public class CronScheduleStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CronScheduleStructListStub.class.getName());

    private List<CronScheduleStructStub> cronScheduleStructs = null;

    public CronScheduleStructListStub()
    {
        this(new ArrayList<CronScheduleStructStub>());
    }
    public CronScheduleStructListStub(List<CronScheduleStructStub> cronScheduleStructs)
    {
        this.cronScheduleStructs = cronScheduleStructs;
    }

    public boolean isEmpty()
    {
        if(cronScheduleStructs == null) {
            return true;
        } else {
            return cronScheduleStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(cronScheduleStructs == null) {
            return 0;
        } else {
            return cronScheduleStructs.size();
        }
    }

    @XmlElement(name = "cronScheduleStruct")
    public List<CronScheduleStructStub> getCronScheduleStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<CronScheduleStructStub> getList()
    {
        return cronScheduleStructs;
    }
    public void setList(List<CronScheduleStructStub> cronScheduleStructs)
    {
        this.cronScheduleStructs = cronScheduleStructs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<CronScheduleStructStub> it = this.cronScheduleStructs.iterator();
        while(it.hasNext()) {
            CronScheduleStructStub cronScheduleStruct = it.next();
            sb.append(cronScheduleStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CronScheduleStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of CronScheduleStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CronScheduleStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CronScheduleStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CronScheduleStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static CronScheduleStructListStub fromJsonString(String jsonStr)
    {
        try {
            CronScheduleStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CronScheduleStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CronScheduleStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CronScheduleStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CronScheduleStructListStub object.", e);
        }
        
        return null;
    }

}
