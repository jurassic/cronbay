package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.ws.User;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "user")
@XmlType(propOrder = {"guid", "managerApp", "appAcl", "gaeAppStub", "aeryId", "sessionId", "username", "nickname", "avatar", "email", "openId", "gaeUserStub", "timeZone", "location", "ipAddress", "referer", "obsolete", "status", "verifiedTime", "authenticatedTime", "createdTime", "modifiedTime"})
public class UserStub implements User, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserStub.class.getName());

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructStub gaeApp;
    private String aeryId;
    private String sessionId;
    private String username;
    private String nickname;
    private String avatar;
    private String email;
    private String openId;
    private GaeUserStructStub gaeUser;
    private String timeZone;
    private String location;
    private String ipAddress;
    private String referer;
    private Boolean obsolete;
    private String status;
    private Long verifiedTime;
    private Long authenticatedTime;
    private Long createdTime;
    private Long modifiedTime;

    public UserStub()
    {
        this(null);
    }
    public UserStub(User bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.managerApp = bean.getManagerApp();
            this.appAcl = bean.getAppAcl();
            this.gaeApp = GaeAppStructStub.convertBeanToStub(bean.getGaeApp());
            this.aeryId = bean.getAeryId();
            this.sessionId = bean.getSessionId();
            this.username = bean.getUsername();
            this.nickname = bean.getNickname();
            this.avatar = bean.getAvatar();
            this.email = bean.getEmail();
            this.openId = bean.getOpenId();
            this.gaeUser = GaeUserStructStub.convertBeanToStub(bean.getGaeUser());
            this.timeZone = bean.getTimeZone();
            this.location = bean.getLocation();
            this.ipAddress = bean.getIpAddress();
            this.referer = bean.getReferer();
            this.obsolete = bean.isObsolete();
            this.status = bean.getStatus();
            this.verifiedTime = bean.getVerifiedTime();
            this.authenticatedTime = bean.getAuthenticatedTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    @XmlElement
    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    @XmlElement(name = "gaeApp")
    @JsonIgnore
    public GaeAppStructStub getGaeAppStub()
    {
        return this.gaeApp;
    }
    public void setGaeAppStub(GaeAppStructStub gaeApp)
    {
        this.gaeApp = gaeApp;  // Clone???
    }
    @XmlTransient
    public GaeAppStruct getGaeApp()
    {  
        return getGaeAppStub();
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(gaeApp instanceof GaeAppStructStub) {
            setGaeAppStub((GaeAppStructStub) gaeApp);
        } else {
            // TBD
            setGaeAppStub(GaeAppStructStub.convertBeanToStub(gaeApp));
        }
    }

    @XmlElement
    public String getAeryId()
    {
        return this.aeryId;
    }
    public void setAeryId(String aeryId)
    {
        this.aeryId = aeryId;
    }

    @XmlElement
    public String getSessionId()
    {
        return this.sessionId;
    }
    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    @XmlElement
    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    @XmlElement
    public String getNickname()
    {
        return this.nickname;
    }
    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    @XmlElement
    public String getAvatar()
    {
        return this.avatar;
    }
    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

    @XmlElement
    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    @XmlElement
    public String getOpenId()
    {
        return this.openId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    @XmlElement(name = "gaeUser")
    @JsonIgnore
    public GaeUserStructStub getGaeUserStub()
    {
        return this.gaeUser;
    }
    public void setGaeUserStub(GaeUserStructStub gaeUser)
    {
        this.gaeUser = gaeUser;  // Clone???
    }
    @XmlTransient
    public GaeUserStruct getGaeUser()
    {  
        return getGaeUserStub();
    }
    public void setGaeUser(GaeUserStruct gaeUser)
    {
        if(gaeUser instanceof GaeUserStructStub) {
            setGaeUserStub((GaeUserStructStub) gaeUser);
        } else {
            // TBD
            setGaeUserStub(GaeUserStructStub.convertBeanToStub(gaeUser));
        }
    }

    @XmlElement
    public String getTimeZone()
    {
        return this.timeZone;
    }
    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }

    @XmlElement
    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    @XmlElement
    public String getIpAddress()
    {
        return this.ipAddress;
    }
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    @XmlElement
    public String getReferer()
    {
        return this.referer;
    }
    public void setReferer(String referer)
    {
        this.referer = referer;
    }

    @XmlElement
    public Boolean isObsolete()
    {
        return this.obsolete;
    }
    public void setObsolete(Boolean obsolete)
    {
        this.obsolete = obsolete;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Long getVerifiedTime()
    {
        return this.verifiedTime;
    }
    public void setVerifiedTime(Long verifiedTime)
    {
        this.verifiedTime = verifiedTime;
    }

    @XmlElement
    public Long getAuthenticatedTime()
    {
        return this.authenticatedTime;
    }
    public void setAuthenticatedTime(Long authenticatedTime)
    {
        this.authenticatedTime = authenticatedTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("aeryId", this.aeryId);
        dataMap.put("sessionId", this.sessionId);
        dataMap.put("username", this.username);
        dataMap.put("nickname", this.nickname);
        dataMap.put("avatar", this.avatar);
        dataMap.put("email", this.email);
        dataMap.put("openId", this.openId);
        dataMap.put("gaeUser", this.gaeUser);
        dataMap.put("timeZone", this.timeZone);
        dataMap.put("location", this.location);
        dataMap.put("ipAddress", this.ipAddress);
        dataMap.put("referer", this.referer);
        dataMap.put("obsolete", this.obsolete);
        dataMap.put("status", this.status);
        dataMap.put("verifiedTime", this.verifiedTime);
        dataMap.put("authenticatedTime", this.authenticatedTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = aeryId == null ? 0 : aeryId.hashCode();
        _hash = 31 * _hash + delta;
        delta = sessionId == null ? 0 : sessionId.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = nickname == null ? 0 : nickname.hashCode();
        _hash = 31 * _hash + delta;
        delta = avatar == null ? 0 : avatar.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = openId == null ? 0 : openId.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeUser == null ? 0 : gaeUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = timeZone == null ? 0 : timeZone.hashCode();
        _hash = 31 * _hash + delta;
        delta = location == null ? 0 : location.hashCode();
        _hash = 31 * _hash + delta;
        delta = ipAddress == null ? 0 : ipAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = referer == null ? 0 : referer.hashCode();
        _hash = 31 * _hash + delta;
        delta = obsolete == null ? 0 : obsolete.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = verifiedTime == null ? 0 : verifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = authenticatedTime == null ? 0 : authenticatedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static UserStub convertBeanToStub(User bean)
    {
        UserStub stub = null;
        if(bean instanceof UserStub) {
            stub = (UserStub) bean;
        } else {
            if(bean != null) {
                stub = new UserStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of UserStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserStub object as a string.", e);
        }
        
        return null;
    }
    public static UserStub fromJsonString(String jsonStr)
    {
        try {
            UserStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserStub object.", e);
        }
        
        return null;
    }

}
