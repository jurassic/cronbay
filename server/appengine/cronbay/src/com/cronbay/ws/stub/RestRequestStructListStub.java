package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;

import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "restRequestStructs")
@XmlType(propOrder = {"restRequestStruct"})
public class RestRequestStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RestRequestStructListStub.class.getName());

    private List<RestRequestStructStub> restRequestStructs = null;

    public RestRequestStructListStub()
    {
        this(new ArrayList<RestRequestStructStub>());
    }
    public RestRequestStructListStub(List<RestRequestStructStub> restRequestStructs)
    {
        this.restRequestStructs = restRequestStructs;
    }

    public boolean isEmpty()
    {
        if(restRequestStructs == null) {
            return true;
        } else {
            return restRequestStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(restRequestStructs == null) {
            return 0;
        } else {
            return restRequestStructs.size();
        }
    }

    @XmlElement(name = "restRequestStruct")
    public List<RestRequestStructStub> getRestRequestStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<RestRequestStructStub> getList()
    {
        return restRequestStructs;
    }
    public void setList(List<RestRequestStructStub> restRequestStructs)
    {
        this.restRequestStructs = restRequestStructs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<RestRequestStructStub> it = this.restRequestStructs.iterator();
        while(it.hasNext()) {
            RestRequestStructStub restRequestStruct = it.next();
            sb.append(restRequestStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RestRequestStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of RestRequestStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RestRequestStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RestRequestStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RestRequestStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static RestRequestStructListStub fromJsonString(String jsonStr)
    {
        try {
            RestRequestStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RestRequestStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RestRequestStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RestRequestStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RestRequestStructListStub object.", e);
        }
        
        return null;
    }

}
