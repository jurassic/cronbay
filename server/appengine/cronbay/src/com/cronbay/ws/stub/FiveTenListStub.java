package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.FiveTen;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "fiveTens")
@XmlType(propOrder = {"fiveTen"})
public class FiveTenListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FiveTenListStub.class.getName());

    private List<FiveTenStub> fiveTens = null;

    public FiveTenListStub()
    {
        this(new ArrayList<FiveTenStub>());
    }
    public FiveTenListStub(List<FiveTenStub> fiveTens)
    {
        this.fiveTens = fiveTens;
    }

    public boolean isEmpty()
    {
        if(fiveTens == null) {
            return true;
        } else {
            return fiveTens.isEmpty();
        }
    }
    public int getSize()
    {
        if(fiveTens == null) {
            return 0;
        } else {
            return fiveTens.size();
        }
    }

    @XmlElement(name = "fiveTen")
    public List<FiveTenStub> getFiveTen()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<FiveTenStub> getList()
    {
        return fiveTens;
    }
    public void setList(List<FiveTenStub> fiveTens)
    {
        this.fiveTens = fiveTens;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<FiveTenStub> it = this.fiveTens.iterator();
        while(it.hasNext()) {
            FiveTenStub fiveTen = it.next();
            sb.append(fiveTen.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FiveTenListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of FiveTenListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FiveTenListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FiveTenListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FiveTenListStub object as a string.", e);
        }
        
        return null;
    }
    public static FiveTenListStub fromJsonString(String jsonStr)
    {
        try {
            FiveTenListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FiveTenListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FiveTenListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FiveTenListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FiveTenListStub object.", e);
        }
        
        return null;
    }

}
