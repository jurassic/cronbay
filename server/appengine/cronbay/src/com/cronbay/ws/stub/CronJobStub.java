package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "cronJob")
@XmlType(propOrder = {"guid", "managerApp", "appAcl", "gaeAppStub", "ownerUser", "userAcl", "user", "jobId", "title", "description", "originJob", "restRequestStub", "permalink", "shortlink", "status", "jobStatus", "extra", "note", "alert", "notificationPrefStub", "referrerInfoStub", "cronScheduleStub", "nextRunTime", "createdTime", "modifiedTime"})
public class CronJobStub implements CronJob, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CronJobStub.class.getName());

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructStub gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private Integer jobId;
    private String title;
    private String description;
    private String originJob;
    private RestRequestStructStub restRequest;
    private String permalink;
    private String shortlink;
    private String status;
    private Integer jobStatus;
    private String extra;
    private String note;
    private Boolean alert;
    private NotificationStructStub notificationPref;
    private ReferrerInfoStructStub referrerInfo;
    private CronScheduleStructStub cronSchedule;
    private Long nextRunTime;
    private Long createdTime;
    private Long modifiedTime;

    public CronJobStub()
    {
        this(null);
    }
    public CronJobStub(CronJob bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.managerApp = bean.getManagerApp();
            this.appAcl = bean.getAppAcl();
            this.gaeApp = GaeAppStructStub.convertBeanToStub(bean.getGaeApp());
            this.ownerUser = bean.getOwnerUser();
            this.userAcl = bean.getUserAcl();
            this.user = bean.getUser();
            this.jobId = bean.getJobId();
            this.title = bean.getTitle();
            this.description = bean.getDescription();
            this.originJob = bean.getOriginJob();
            this.restRequest = RestRequestStructStub.convertBeanToStub(bean.getRestRequest());
            this.permalink = bean.getPermalink();
            this.shortlink = bean.getShortlink();
            this.status = bean.getStatus();
            this.jobStatus = bean.getJobStatus();
            this.extra = bean.getExtra();
            this.note = bean.getNote();
            this.alert = bean.isAlert();
            this.notificationPref = NotificationStructStub.convertBeanToStub(bean.getNotificationPref());
            this.referrerInfo = ReferrerInfoStructStub.convertBeanToStub(bean.getReferrerInfo());
            this.cronSchedule = CronScheduleStructStub.convertBeanToStub(bean.getCronSchedule());
            this.nextRunTime = bean.getNextRunTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    @XmlElement
    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    @XmlElement(name = "gaeApp")
    @JsonIgnore
    public GaeAppStructStub getGaeAppStub()
    {
        return this.gaeApp;
    }
    public void setGaeAppStub(GaeAppStructStub gaeApp)
    {
        this.gaeApp = gaeApp;  // Clone???
    }
    @XmlTransient
    public GaeAppStruct getGaeApp()
    {  
        return getGaeAppStub();
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(gaeApp instanceof GaeAppStructStub) {
            setGaeAppStub((GaeAppStructStub) gaeApp);
        } else {
            // TBD
            setGaeAppStub(GaeAppStructStub.convertBeanToStub(gaeApp));
        }
    }

    @XmlElement
    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    @XmlElement
    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public Integer getJobId()
    {
        return this.jobId;
    }
    public void setJobId(Integer jobId)
    {
        this.jobId = jobId;
    }

    @XmlElement
    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    @XmlElement
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    @XmlElement
    public String getOriginJob()
    {
        return this.originJob;
    }
    public void setOriginJob(String originJob)
    {
        this.originJob = originJob;
    }

    @XmlElement(name = "restRequest")
    @JsonIgnore
    public RestRequestStructStub getRestRequestStub()
    {
        return this.restRequest;
    }
    public void setRestRequestStub(RestRequestStructStub restRequest)
    {
        this.restRequest = restRequest;  // Clone???
    }
    @XmlTransient
    public RestRequestStruct getRestRequest()
    {  
        return getRestRequestStub();
    }
    public void setRestRequest(RestRequestStruct restRequest)
    {
        if(restRequest instanceof RestRequestStructStub) {
            setRestRequestStub((RestRequestStructStub) restRequest);
        } else {
            // TBD
            setRestRequestStub(RestRequestStructStub.convertBeanToStub(restRequest));
        }
    }

    @XmlElement
    public String getPermalink()
    {
        return this.permalink;
    }
    public void setPermalink(String permalink)
    {
        this.permalink = permalink;
    }

    @XmlElement
    public String getShortlink()
    {
        return this.shortlink;
    }
    public void setShortlink(String shortlink)
    {
        this.shortlink = shortlink;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Integer getJobStatus()
    {
        return this.jobStatus;
    }
    public void setJobStatus(Integer jobStatus)
    {
        this.jobStatus = jobStatus;
    }

    @XmlElement
    public String getExtra()
    {
        return this.extra;
    }
    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public Boolean isAlert()
    {
        return this.alert;
    }
    public void setAlert(Boolean alert)
    {
        this.alert = alert;
    }

    @XmlElement(name = "notificationPref")
    @JsonIgnore
    public NotificationStructStub getNotificationPrefStub()
    {
        return this.notificationPref;
    }
    public void setNotificationPrefStub(NotificationStructStub notificationPref)
    {
        this.notificationPref = notificationPref;  // Clone???
    }
    @XmlTransient
    public NotificationStruct getNotificationPref()
    {  
        return getNotificationPrefStub();
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if(notificationPref instanceof NotificationStructStub) {
            setNotificationPrefStub((NotificationStructStub) notificationPref);
        } else {
            // TBD
            setNotificationPrefStub(NotificationStructStub.convertBeanToStub(notificationPref));
        }
    }

    @XmlElement(name = "referrerInfo")
    @JsonIgnore
    public ReferrerInfoStructStub getReferrerInfoStub()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfoStub(ReferrerInfoStructStub referrerInfo)
    {
        this.referrerInfo = referrerInfo;  // Clone???
    }
    @XmlTransient
    public ReferrerInfoStruct getReferrerInfo()
    {  
        return getReferrerInfoStub();
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(referrerInfo instanceof ReferrerInfoStructStub) {
            setReferrerInfoStub((ReferrerInfoStructStub) referrerInfo);
        } else {
            // TBD
            setReferrerInfoStub(ReferrerInfoStructStub.convertBeanToStub(referrerInfo));
        }
    }

    @XmlElement(name = "cronSchedule")
    @JsonIgnore
    public CronScheduleStructStub getCronScheduleStub()
    {
        return this.cronSchedule;
    }
    public void setCronScheduleStub(CronScheduleStructStub cronSchedule)
    {
        this.cronSchedule = cronSchedule;  // Clone???
    }
    @XmlTransient
    public CronScheduleStruct getCronSchedule()
    {  
        return getCronScheduleStub();
    }
    public void setCronSchedule(CronScheduleStruct cronSchedule)
    {
        if(cronSchedule instanceof CronScheduleStructStub) {
            setCronScheduleStub((CronScheduleStructStub) cronSchedule);
        } else {
            // TBD
            setCronScheduleStub(CronScheduleStructStub.convertBeanToStub(cronSchedule));
        }
    }

    @XmlElement
    public Long getNextRunTime()
    {
        return this.nextRunTime;
    }
    public void setNextRunTime(Long nextRunTime)
    {
        this.nextRunTime = nextRunTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("ownerUser", this.ownerUser);
        dataMap.put("userAcl", this.userAcl);
        dataMap.put("user", this.user);
        dataMap.put("jobId", this.jobId);
        dataMap.put("title", this.title);
        dataMap.put("description", this.description);
        dataMap.put("originJob", this.originJob);
        dataMap.put("restRequest", this.restRequest);
        dataMap.put("permalink", this.permalink);
        dataMap.put("shortlink", this.shortlink);
        dataMap.put("status", this.status);
        dataMap.put("jobStatus", this.jobStatus);
        dataMap.put("extra", this.extra);
        dataMap.put("note", this.note);
        dataMap.put("alert", this.alert);
        dataMap.put("notificationPref", this.notificationPref);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("cronSchedule", this.cronSchedule);
        dataMap.put("nextRunTime", this.nextRunTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = ownerUser == null ? 0 : ownerUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAcl == null ? 0 : userAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = jobId == null ? 0 : jobId.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = originJob == null ? 0 : originJob.hashCode();
        _hash = 31 * _hash + delta;
        delta = restRequest == null ? 0 : restRequest.hashCode();
        _hash = 31 * _hash + delta;
        delta = permalink == null ? 0 : permalink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortlink == null ? 0 : shortlink.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = jobStatus == null ? 0 : jobStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = extra == null ? 0 : extra.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = alert == null ? 0 : alert.hashCode();
        _hash = 31 * _hash + delta;
        delta = notificationPref == null ? 0 : notificationPref.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = cronSchedule == null ? 0 : cronSchedule.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextRunTime == null ? 0 : nextRunTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static CronJobStub convertBeanToStub(CronJob bean)
    {
        CronJobStub stub = null;
        if(bean instanceof CronJobStub) {
            stub = (CronJobStub) bean;
        } else {
            if(bean != null) {
                stub = new CronJobStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CronJobStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of CronJobStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CronJobStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CronJobStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CronJobStub object as a string.", e);
        }
        
        return null;
    }
    public static CronJobStub fromJsonString(String jsonStr)
    {
        try {
            CronJobStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CronJobStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CronJobStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CronJobStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CronJobStub object.", e);
        }
        
        return null;
    }

}
