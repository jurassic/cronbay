package com.cronbay.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.util.JsonUtil;


@XmlRootElement(name = "jobOutput")
@XmlType(propOrder = {"guid", "managerApp", "appAcl", "gaeAppStub", "ownerUser", "userAcl", "user", "cronJob", "previousRun", "previousTry", "responseCode", "outputFormat", "outputText", "outputData", "outputHash", "permalink", "shortlink", "result", "status", "extra", "note", "retryCounter", "scheduledTime", "processedTime", "createdTime", "modifiedTime"})
public class JobOutputStub implements JobOutput, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(JobOutputStub.class.getName());

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructStub gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private String cronJob;
    private String previousRun;
    private String previousTry;
    private String responseCode;
    private String outputFormat;
    private String outputText;
    private List<String> outputData;
    private String outputHash;
    private String permalink;
    private String shortlink;
    private String result;
    private String status;
    private String extra;
    private String note;
    private Integer retryCounter;
    private Long scheduledTime;
    private Long processedTime;
    private Long createdTime;
    private Long modifiedTime;

    public JobOutputStub()
    {
        this(null);
    }
    public JobOutputStub(JobOutput bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.managerApp = bean.getManagerApp();
            this.appAcl = bean.getAppAcl();
            this.gaeApp = GaeAppStructStub.convertBeanToStub(bean.getGaeApp());
            this.ownerUser = bean.getOwnerUser();
            this.userAcl = bean.getUserAcl();
            this.user = bean.getUser();
            this.cronJob = bean.getCronJob();
            this.previousRun = bean.getPreviousRun();
            this.previousTry = bean.getPreviousTry();
            this.responseCode = bean.getResponseCode();
            this.outputFormat = bean.getOutputFormat();
            this.outputText = bean.getOutputText();
            this.outputData = bean.getOutputData();
            this.outputHash = bean.getOutputHash();
            this.permalink = bean.getPermalink();
            this.shortlink = bean.getShortlink();
            this.result = bean.getResult();
            this.status = bean.getStatus();
            this.extra = bean.getExtra();
            this.note = bean.getNote();
            this.retryCounter = bean.getRetryCounter();
            this.scheduledTime = bean.getScheduledTime();
            this.processedTime = bean.getProcessedTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    @XmlElement
    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    @XmlElement(name = "gaeApp")
    @JsonIgnore
    public GaeAppStructStub getGaeAppStub()
    {
        return this.gaeApp;
    }
    public void setGaeAppStub(GaeAppStructStub gaeApp)
    {
        this.gaeApp = gaeApp;  // Clone???
    }
    @XmlTransient
    public GaeAppStruct getGaeApp()
    {  
        return getGaeAppStub();
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(gaeApp instanceof GaeAppStructStub) {
            setGaeAppStub((GaeAppStructStub) gaeApp);
        } else {
            // TBD
            setGaeAppStub(GaeAppStructStub.convertBeanToStub(gaeApp));
        }
    }

    @XmlElement
    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    @XmlElement
    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getCronJob()
    {
        return this.cronJob;
    }
    public void setCronJob(String cronJob)
    {
        this.cronJob = cronJob;
    }

    @XmlElement
    public String getPreviousRun()
    {
        return this.previousRun;
    }
    public void setPreviousRun(String previousRun)
    {
        this.previousRun = previousRun;
    }

    @XmlElement
    public String getPreviousTry()
    {
        return this.previousTry;
    }
    public void setPreviousTry(String previousTry)
    {
        this.previousTry = previousTry;
    }

    @XmlElement
    public String getResponseCode()
    {
        return this.responseCode;
    }
    public void setResponseCode(String responseCode)
    {
        this.responseCode = responseCode;
    }

    @XmlElement
    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    @XmlElement
    public String getOutputText()
    {
        return this.outputText;
    }
    public void setOutputText(String outputText)
    {
        this.outputText = outputText;
    }

    @XmlElement
    public List<String> getOutputData()
    {
        return this.outputData;
    }
    public void setOutputData(List<String> outputData)
    {
        this.outputData = outputData;
    }

    @XmlElement
    public String getOutputHash()
    {
        return this.outputHash;
    }
    public void setOutputHash(String outputHash)
    {
        this.outputHash = outputHash;
    }

    @XmlElement
    public String getPermalink()
    {
        return this.permalink;
    }
    public void setPermalink(String permalink)
    {
        this.permalink = permalink;
    }

    @XmlElement
    public String getShortlink()
    {
        return this.shortlink;
    }
    public void setShortlink(String shortlink)
    {
        this.shortlink = shortlink;
    }

    @XmlElement
    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getExtra()
    {
        return this.extra;
    }
    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public Integer getRetryCounter()
    {
        return this.retryCounter;
    }
    public void setRetryCounter(Integer retryCounter)
    {
        this.retryCounter = retryCounter;
    }

    @XmlElement
    public Long getScheduledTime()
    {
        return this.scheduledTime;
    }
    public void setScheduledTime(Long scheduledTime)
    {
        this.scheduledTime = scheduledTime;
    }

    @XmlElement
    public Long getProcessedTime()
    {
        return this.processedTime;
    }
    public void setProcessedTime(Long processedTime)
    {
        this.processedTime = processedTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("ownerUser", this.ownerUser);
        dataMap.put("userAcl", this.userAcl);
        dataMap.put("user", this.user);
        dataMap.put("cronJob", this.cronJob);
        dataMap.put("previousRun", this.previousRun);
        dataMap.put("previousTry", this.previousTry);
        dataMap.put("responseCode", this.responseCode);
        dataMap.put("outputFormat", this.outputFormat);
        dataMap.put("outputText", this.outputText);
        dataMap.put("outputData", this.outputData);
        dataMap.put("outputHash", this.outputHash);
        dataMap.put("permalink", this.permalink);
        dataMap.put("shortlink", this.shortlink);
        dataMap.put("result", this.result);
        dataMap.put("status", this.status);
        dataMap.put("extra", this.extra);
        dataMap.put("note", this.note);
        dataMap.put("retryCounter", this.retryCounter);
        dataMap.put("scheduledTime", this.scheduledTime);
        dataMap.put("processedTime", this.processedTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = ownerUser == null ? 0 : ownerUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAcl == null ? 0 : userAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = cronJob == null ? 0 : cronJob.hashCode();
        _hash = 31 * _hash + delta;
        delta = previousRun == null ? 0 : previousRun.hashCode();
        _hash = 31 * _hash + delta;
        delta = previousTry == null ? 0 : previousTry.hashCode();
        _hash = 31 * _hash + delta;
        delta = responseCode == null ? 0 : responseCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFormat == null ? 0 : outputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputText == null ? 0 : outputText.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputData == null ? 0 : outputData.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputHash == null ? 0 : outputHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = permalink == null ? 0 : permalink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortlink == null ? 0 : shortlink.hashCode();
        _hash = 31 * _hash + delta;
        delta = result == null ? 0 : result.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = extra == null ? 0 : extra.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = retryCounter == null ? 0 : retryCounter.hashCode();
        _hash = 31 * _hash + delta;
        delta = scheduledTime == null ? 0 : scheduledTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = processedTime == null ? 0 : processedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static JobOutputStub convertBeanToStub(JobOutput bean)
    {
        JobOutputStub stub = null;
        if(bean instanceof JobOutputStub) {
            stub = (JobOutputStub) bean;
        } else {
            if(bean != null) {
                stub = new JobOutputStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static JobOutputStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of JobOutputStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write JobOutputStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write JobOutputStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write JobOutputStub object as a string.", e);
        }
        
        return null;
    }
    public static JobOutputStub fromJsonString(String jsonStr)
    {
        try {
            JobOutputStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, JobOutputStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into JobOutputStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into JobOutputStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into JobOutputStub object.", e);
        }
        
        return null;
    }

}
