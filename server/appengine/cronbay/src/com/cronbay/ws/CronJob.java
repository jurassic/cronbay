package com.cronbay.ws;


public interface CronJob 
{
    String  getGuid();
    String  getManagerApp();
    Long  getAppAcl();
    GaeAppStruct  getGaeApp();
    String  getOwnerUser();
    Long  getUserAcl();
    String  getUser();
    Integer  getJobId();
    String  getTitle();
    String  getDescription();
    String  getOriginJob();
    RestRequestStruct  getRestRequest();
    String  getPermalink();
    String  getShortlink();
    String  getStatus();
    Integer  getJobStatus();
    String  getExtra();
    String  getNote();
    Boolean  isAlert();
    NotificationStruct  getNotificationPref();
    ReferrerInfoStruct  getReferrerInfo();
    CronScheduleStruct  getCronSchedule();
    Long  getNextRunTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
