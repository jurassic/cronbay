package com.cronbay.ws.service;

import java.util.List;
import java.util.List;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface JobOutputService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    JobOutput getJobOutput(String guid) throws BaseException;
    Object getJobOutput(String guid, String field) throws BaseException;
    List<JobOutput> getJobOutputs(List<String> guids) throws BaseException;
    List<JobOutput> getAllJobOutputs() throws BaseException;
    List<JobOutput> getAllJobOutputs(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllJobOutputKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<JobOutput> findJobOutputs(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<JobOutput> findJobOutputs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findJobOutputKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createJobOutput(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws BaseException;
    //String createJobOutput(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return JobOutput?)
    String createJobOutput(JobOutput jobOutput) throws BaseException;          // Returns Guid.  (Return JobOutput?)
    Boolean updateJobOutput(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws BaseException;
    //Boolean updateJobOutput(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateJobOutput(JobOutput jobOutput) throws BaseException;
    Boolean deleteJobOutput(String guid) throws BaseException;
    Boolean deleteJobOutput(JobOutput jobOutput) throws BaseException;
    Long deleteJobOutputs(String filter, String params, List<String> values) throws BaseException;

//    Integer createJobOutputs(List<JobOutput> jobOutputs) throws BaseException;
//    Boolean updateeJobOutputs(List<JobOutput> jobOutputs) throws BaseException;

}
