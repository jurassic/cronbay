package com.cronbay.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.bean.NotificationStructBean;
import com.cronbay.ws.bean.RestRequestStructBean;
import com.cronbay.ws.bean.GaeAppStructBean;
import com.cronbay.ws.bean.ReferrerInfoStructBean;
import com.cronbay.ws.bean.CronScheduleStructBean;
import com.cronbay.ws.bean.CronJobBean;
import com.cronbay.ws.dao.DAOFactory;
import com.cronbay.ws.data.NotificationStructDataObject;
import com.cronbay.ws.data.RestRequestStructDataObject;
import com.cronbay.ws.data.GaeAppStructDataObject;
import com.cronbay.ws.data.ReferrerInfoStructDataObject;
import com.cronbay.ws.data.CronScheduleStructDataObject;
import com.cronbay.ws.data.CronJobDataObject;
import com.cronbay.ws.service.DAOFactoryManager;
import com.cronbay.ws.service.CronJobService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class CronJobServiceImpl implements CronJobService
{
    private static final Logger log = Logger.getLogger(CronJobServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // CronJob related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public CronJob getCronJob(String guid) throws BaseException
    {
        log.finer("BEGIN");

        CronJobDataObject dataObj = getDAOFactory().getCronJobDAO().getCronJob(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve CronJobDataObject for guid = " + guid);
            return null;  // ????
        }
        CronJobBean bean = new CronJobBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getCronJob(String guid, String field) throws BaseException
    {
        CronJobDataObject dataObj = getDAOFactory().getCronJobDAO().getCronJob(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve CronJobDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return dataObj.getManagerApp();
        } else if(field.equals("appAcl")) {
            return dataObj.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return dataObj.getGaeApp();
        } else if(field.equals("ownerUser")) {
            return dataObj.getOwnerUser();
        } else if(field.equals("userAcl")) {
            return dataObj.getUserAcl();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("jobId")) {
            return dataObj.getJobId();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("originJob")) {
            return dataObj.getOriginJob();
        } else if(field.equals("restRequest")) {
            return dataObj.getRestRequest();
        } else if(field.equals("permalink")) {
            return dataObj.getPermalink();
        } else if(field.equals("shortlink")) {
            return dataObj.getShortlink();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("jobStatus")) {
            return dataObj.getJobStatus();
        } else if(field.equals("extra")) {
            return dataObj.getExtra();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("alert")) {
            return dataObj.isAlert();
        } else if(field.equals("notificationPref")) {
            return dataObj.getNotificationPref();
        } else if(field.equals("referrerInfo")) {
            return dataObj.getReferrerInfo();
        } else if(field.equals("cronSchedule")) {
            return dataObj.getCronSchedule();
        } else if(field.equals("nextRunTime")) {
            return dataObj.getNextRunTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<CronJob> getCronJobs(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<CronJob> list = new ArrayList<CronJob>();
        List<CronJobDataObject> dataObjs = getDAOFactory().getCronJobDAO().getCronJobs(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve CronJobDataObject list.");
        } else {
            Iterator<CronJobDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                CronJobDataObject dataObj = (CronJobDataObject) it.next();
                list.add(new CronJobBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<CronJob> getAllCronJobs() throws BaseException
    {
        return getAllCronJobs(null, null, null);
    }

    @Override
    public List<CronJob> getAllCronJobs(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<CronJob> list = new ArrayList<CronJob>();
        List<CronJobDataObject> dataObjs = getDAOFactory().getCronJobDAO().getAllCronJobs(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve CronJobDataObject list.");
        } else {
            Iterator<CronJobDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                CronJobDataObject dataObj = (CronJobDataObject) it.next();
                list.add(new CronJobBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllCronJobKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getCronJobDAO().getAllCronJobKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve CronJob key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<CronJob> findCronJobs(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findCronJobs(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<CronJob> findCronJobs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("CronJobServiceImpl.findCronJobs(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<CronJob> list = new ArrayList<CronJob>();
        List<CronJobDataObject> dataObjs = getDAOFactory().getCronJobDAO().findCronJobs(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find cronJobs for the given criterion.");
        } else {
            Iterator<CronJobDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                CronJobDataObject dataObj = (CronJobDataObject) it.next();
                list.add(new CronJobBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findCronJobKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("CronJobServiceImpl.findCronJobKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getCronJobDAO().findCronJobKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find CronJob keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("CronJobServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getCronJobDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createCronJob(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        RestRequestStructDataObject restRequestDobj = null;
        if(restRequest instanceof RestRequestStructBean) {
            restRequestDobj = ((RestRequestStructBean) restRequest).toDataObject();
        } else if(restRequest instanceof RestRequestStruct) {
            restRequestDobj = new RestRequestStructDataObject(restRequest.getServiceName(), restRequest.getServiceUrl(), restRequest.getRequestMethod(), restRequest.getRequestUrl(), restRequest.getTargetEntity(), restRequest.getQueryString(), restRequest.getQueryParams(), restRequest.getInputFormat(), restRequest.getInputContent(), restRequest.getOutputFormat(), restRequest.getMaxRetries(), restRequest.getRetryInterval(), restRequest.getNote());
        } else {
            restRequestDobj = null;   // ????
        }
        NotificationStructDataObject notificationPrefDobj = null;
        if(notificationPref instanceof NotificationStructBean) {
            notificationPrefDobj = ((NotificationStructBean) notificationPref).toDataObject();
        } else if(notificationPref instanceof NotificationStruct) {
            notificationPrefDobj = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getCallbackUrl(), notificationPref.getNote());
        } else {
            notificationPrefDobj = null;   // ????
        }
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        CronScheduleStructDataObject cronScheduleDobj = null;
        if(cronSchedule instanceof CronScheduleStructBean) {
            cronScheduleDobj = ((CronScheduleStructBean) cronSchedule).toDataObject();
        } else if(cronSchedule instanceof CronScheduleStruct) {
            cronScheduleDobj = new CronScheduleStructDataObject(cronSchedule.getType(), cronSchedule.getSchedule(), cronSchedule.getTimezone(), cronSchedule.getMaxIterations(), cronSchedule.getRepeatInterval(), cronSchedule.getFirtRunTime(), cronSchedule.getStartTime(), cronSchedule.getEndTime(), cronSchedule.getNote());
        } else {
            cronScheduleDobj = null;   // ????
        }
        
        CronJobDataObject dataObj = new CronJobDataObject(null, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, user, jobId, title, description, originJob, restRequestDobj, permalink, shortlink, status, jobStatus, extra, note, alert, notificationPrefDobj, referrerInfoDobj, cronScheduleDobj, nextRunTime);
        return createCronJob(dataObj);
    }

    @Override
    public String createCronJob(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN");

        // Param cronJob cannot be null.....
        if(cronJob == null) {
            log.log(Level.INFO, "Param cronJob is null!");
            throw new BadRequestException("Param cronJob object is null!");
        }
        CronJobDataObject dataObj = null;
        if(cronJob instanceof CronJobDataObject) {
            dataObj = (CronJobDataObject) cronJob;
        } else if(cronJob instanceof CronJobBean) {
            dataObj = ((CronJobBean) cronJob).toDataObject();
        } else {  // if(cronJob instanceof CronJob)
            //dataObj = new CronJobDataObject(null, cronJob.getManagerApp(), cronJob.getAppAcl(), (GaeAppStructDataObject) cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), (RestRequestStructDataObject) cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), (NotificationStructDataObject) cronJob.getNotificationPref(), (ReferrerInfoStructDataObject) cronJob.getReferrerInfo(), (CronScheduleStructDataObject) cronJob.getCronSchedule(), cronJob.getNextRunTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new CronJobDataObject(cronJob.getGuid(), cronJob.getManagerApp(), cronJob.getAppAcl(), (GaeAppStructDataObject) cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), (RestRequestStructDataObject) cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), (NotificationStructDataObject) cronJob.getNotificationPref(), (ReferrerInfoStructDataObject) cronJob.getReferrerInfo(), (CronScheduleStructDataObject) cronJob.getCronSchedule(), cronJob.getNextRunTime());
        }
        String guid = getDAOFactory().getCronJobDAO().createCronJob(dataObj);

        log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateCronJob(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime) throws BaseException
    {
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        RestRequestStructDataObject restRequestDobj = null;
        if(restRequest instanceof RestRequestStructBean) {
            restRequestDobj = ((RestRequestStructBean) restRequest).toDataObject();            
        } else if(restRequest instanceof RestRequestStruct) {
            restRequestDobj = new RestRequestStructDataObject(restRequest.getServiceName(), restRequest.getServiceUrl(), restRequest.getRequestMethod(), restRequest.getRequestUrl(), restRequest.getTargetEntity(), restRequest.getQueryString(), restRequest.getQueryParams(), restRequest.getInputFormat(), restRequest.getInputContent(), restRequest.getOutputFormat(), restRequest.getMaxRetries(), restRequest.getRetryInterval(), restRequest.getNote());
        } else {
            restRequestDobj = null;   // ????
        }
        NotificationStructDataObject notificationPrefDobj = null;
        if(notificationPref instanceof NotificationStructBean) {
            notificationPrefDobj = ((NotificationStructBean) notificationPref).toDataObject();            
        } else if(notificationPref instanceof NotificationStruct) {
            notificationPrefDobj = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getCallbackUrl(), notificationPref.getNote());
        } else {
            notificationPrefDobj = null;   // ????
        }
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();            
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        CronScheduleStructDataObject cronScheduleDobj = null;
        if(cronSchedule instanceof CronScheduleStructBean) {
            cronScheduleDobj = ((CronScheduleStructBean) cronSchedule).toDataObject();            
        } else if(cronSchedule instanceof CronScheduleStruct) {
            cronScheduleDobj = new CronScheduleStructDataObject(cronSchedule.getType(), cronSchedule.getSchedule(), cronSchedule.getTimezone(), cronSchedule.getMaxIterations(), cronSchedule.getRepeatInterval(), cronSchedule.getFirtRunTime(), cronSchedule.getStartTime(), cronSchedule.getEndTime(), cronSchedule.getNote());
        } else {
            cronScheduleDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        CronJobDataObject dataObj = new CronJobDataObject(guid, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, user, jobId, title, description, originJob, restRequestDobj, permalink, shortlink, status, jobStatus, extra, note, alert, notificationPrefDobj, referrerInfoDobj, cronScheduleDobj, nextRunTime);
        return updateCronJob(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateCronJob(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN");

        // Param cronJob cannot be null.....
        if(cronJob == null || cronJob.getGuid() == null) {
            log.log(Level.INFO, "Param cronJob or its guid is null!");
            throw new BadRequestException("Param cronJob object or its guid is null!");
        }
        CronJobDataObject dataObj = null;
        if(cronJob instanceof CronJobDataObject) {
            dataObj = (CronJobDataObject) cronJob;
        } else if(cronJob instanceof CronJobBean) {
            dataObj = ((CronJobBean) cronJob).toDataObject();
        } else {  // if(cronJob instanceof CronJob)
            dataObj = new CronJobDataObject(cronJob.getGuid(), cronJob.getManagerApp(), cronJob.getAppAcl(), cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), cronJob.getNotificationPref(), cronJob.getReferrerInfo(), cronJob.getCronSchedule(), cronJob.getNextRunTime());
        }
        Boolean suc = getDAOFactory().getCronJobDAO().updateCronJob(dataObj);

        log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteCronJob(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getCronJobDAO().deleteCronJob(guid);

        log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteCronJob(CronJob cronJob) throws BaseException
    {
        log.finer("BEGIN");

        // Param cronJob cannot be null.....
        if(cronJob == null || cronJob.getGuid() == null) {
            log.log(Level.INFO, "Param cronJob or its guid is null!");
            throw new BadRequestException("Param cronJob object or its guid is null!");
        }
        CronJobDataObject dataObj = null;
        if(cronJob instanceof CronJobDataObject) {
            dataObj = (CronJobDataObject) cronJob;
        } else if(cronJob instanceof CronJobBean) {
            dataObj = ((CronJobBean) cronJob).toDataObject();
        } else {  // if(cronJob instanceof CronJob)
            dataObj = new CronJobDataObject(cronJob.getGuid(), cronJob.getManagerApp(), cronJob.getAppAcl(), cronJob.getGaeApp(), cronJob.getOwnerUser(), cronJob.getUserAcl(), cronJob.getUser(), cronJob.getJobId(), cronJob.getTitle(), cronJob.getDescription(), cronJob.getOriginJob(), cronJob.getRestRequest(), cronJob.getPermalink(), cronJob.getShortlink(), cronJob.getStatus(), cronJob.getJobStatus(), cronJob.getExtra(), cronJob.getNote(), cronJob.isAlert(), cronJob.getNotificationPref(), cronJob.getReferrerInfo(), cronJob.getCronSchedule(), cronJob.getNextRunTime());
        }
        Boolean suc = getDAOFactory().getCronJobDAO().deleteCronJob(dataObj);

        log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteCronJobs(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getCronJobDAO().deleteCronJobs(filter, params, values);

        log.finer("END: count = " + count);
        return count;
    }

}
