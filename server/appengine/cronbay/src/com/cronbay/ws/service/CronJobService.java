package com.cronbay.ws.service;

import java.util.List;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface CronJobService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    CronJob getCronJob(String guid) throws BaseException;
    Object getCronJob(String guid, String field) throws BaseException;
    List<CronJob> getCronJobs(List<String> guids) throws BaseException;
    List<CronJob> getAllCronJobs() throws BaseException;
    List<CronJob> getAllCronJobs(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllCronJobKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<CronJob> findCronJobs(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<CronJob> findCronJobs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findCronJobKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createCronJob(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime) throws BaseException;
    //String createCronJob(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return CronJob?)
    String createCronJob(CronJob cronJob) throws BaseException;          // Returns Guid.  (Return CronJob?)
    Boolean updateCronJob(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime) throws BaseException;
    //Boolean updateCronJob(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateCronJob(CronJob cronJob) throws BaseException;
    Boolean deleteCronJob(String guid) throws BaseException;
    Boolean deleteCronJob(CronJob cronJob) throws BaseException;
    Long deleteCronJobs(String filter, String params, List<String> values) throws BaseException;

//    Integer createCronJobs(List<CronJob> cronJobs) throws BaseException;
//    Boolean updateeCronJobs(List<CronJob> cronJobs) throws BaseException;

}
