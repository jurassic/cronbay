package com.cronbay.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.GaeUserStruct;
import com.cronbay.ws.User;
import com.cronbay.ws.bean.GaeAppStructBean;
import com.cronbay.ws.bean.GaeUserStructBean;
import com.cronbay.ws.bean.UserBean;
import com.cronbay.ws.dao.DAOFactory;
import com.cronbay.ws.data.GaeAppStructDataObject;
import com.cronbay.ws.data.GaeUserStructDataObject;
import com.cronbay.ws.data.UserDataObject;
import com.cronbay.ws.service.DAOFactoryManager;
import com.cronbay.ws.service.UserService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserServiceImpl implements UserService
{
    private static final Logger log = Logger.getLogger(UserServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // User related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public User getUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        UserDataObject dataObj = getDAOFactory().getUserDAO().getUser(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve UserDataObject for guid = " + guid);
            return null;  // ????
        }
        UserBean bean = new UserBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        UserDataObject dataObj = getDAOFactory().getUserDAO().getUser(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve UserDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return dataObj.getManagerApp();
        } else if(field.equals("appAcl")) {
            return dataObj.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return dataObj.getGaeApp();
        } else if(field.equals("aeryId")) {
            return dataObj.getAeryId();
        } else if(field.equals("sessionId")) {
            return dataObj.getSessionId();
        } else if(field.equals("username")) {
            return dataObj.getUsername();
        } else if(field.equals("nickname")) {
            return dataObj.getNickname();
        } else if(field.equals("avatar")) {
            return dataObj.getAvatar();
        } else if(field.equals("email")) {
            return dataObj.getEmail();
        } else if(field.equals("openId")) {
            return dataObj.getOpenId();
        } else if(field.equals("gaeUser")) {
            return dataObj.getGaeUser();
        } else if(field.equals("timeZone")) {
            return dataObj.getTimeZone();
        } else if(field.equals("location")) {
            return dataObj.getLocation();
        } else if(field.equals("ipAddress")) {
            return dataObj.getIpAddress();
        } else if(field.equals("referer")) {
            return dataObj.getReferer();
        } else if(field.equals("obsolete")) {
            return dataObj.isObsolete();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("verifiedTime")) {
            return dataObj.getVerifiedTime();
        } else if(field.equals("authenticatedTime")) {
            return dataObj.getAuthenticatedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<User> list = new ArrayList<User>();
        List<UserDataObject> dataObjs = getDAOFactory().getUserDAO().getUsers(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserDataObject list.");
        } else {
            Iterator<UserDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserDataObject dataObj = (UserDataObject) it.next();
                list.add(new UserBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<User> list = new ArrayList<User>();
        List<UserDataObject> dataObjs = getDAOFactory().getUserDAO().getAllUsers(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UserDataObject list.");
        } else {
            Iterator<UserDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserDataObject dataObj = (UserDataObject) it.next();
                list.add(new UserBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getUserDAO().getAllUserKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve User key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("UserServiceImpl.findUsers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<User> list = new ArrayList<User>();
        List<UserDataObject> dataObjs = getDAOFactory().getUserDAO().findUsers(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find users for the given criterion.");
        } else {
            Iterator<UserDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UserDataObject dataObj = (UserDataObject) it.next();
                list.add(new UserBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("UserServiceImpl.findUserKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getUserDAO().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find User keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("UserServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getUserDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        GaeUserStructDataObject gaeUserDobj = null;
        if(gaeUser instanceof GaeUserStructBean) {
            gaeUserDobj = ((GaeUserStructBean) gaeUser).toDataObject();
        } else if(gaeUser instanceof GaeUserStruct) {
            gaeUserDobj = new GaeUserStructDataObject(gaeUser.getAuthDomain(), gaeUser.getFederatedIdentity(), gaeUser.getNickname(), gaeUser.getUserId(), gaeUser.getEmail(), gaeUser.getNote());
        } else {
            gaeUserDobj = null;   // ????
        }
        
        UserDataObject dataObj = new UserDataObject(null, managerApp, appAcl, gaeAppDobj, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUserDobj, timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime);
        return createUser(dataObj);
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null) {
            log.log(Level.INFO, "Param user is null!");
            throw new BadRequestException("Param user object is null!");
        }
        UserDataObject dataObj = null;
        if(user instanceof UserDataObject) {
            dataObj = (UserDataObject) user;
        } else if(user instanceof UserBean) {
            dataObj = ((UserBean) user).toDataObject();
        } else {  // if(user instanceof User)
            //dataObj = new UserDataObject(null, user.getManagerApp(), user.getAppAcl(), (GaeAppStructDataObject) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructDataObject) user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UserDataObject(user.getGuid(), user.getManagerApp(), user.getAppAcl(), (GaeAppStructDataObject) user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), (GaeUserStructDataObject) user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
        }
        String guid = getDAOFactory().getUserDAO().createUser(dataObj);

        log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String timeZone, String location, String ipAddress, String referer, Boolean obsolete, String status, Long verifiedTime, Long authenticatedTime) throws BaseException
    {
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        GaeUserStructDataObject gaeUserDobj = null;
        if(gaeUser instanceof GaeUserStructBean) {
            gaeUserDobj = ((GaeUserStructBean) gaeUser).toDataObject();            
        } else if(gaeUser instanceof GaeUserStruct) {
            gaeUserDobj = new GaeUserStructDataObject(gaeUser.getAuthDomain(), gaeUser.getFederatedIdentity(), gaeUser.getNickname(), gaeUser.getUserId(), gaeUser.getEmail(), gaeUser.getNote());
        } else {
            gaeUserDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UserDataObject dataObj = new UserDataObject(guid, managerApp, appAcl, gaeAppDobj, aeryId, sessionId, username, nickname, avatar, email, openId, gaeUserDobj, timeZone, location, ipAddress, referer, obsolete, status, verifiedTime, authenticatedTime);
        return updateUser(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null || user.getGuid() == null) {
            log.log(Level.INFO, "Param user or its guid is null!");
            throw new BadRequestException("Param user object or its guid is null!");
        }
        UserDataObject dataObj = null;
        if(user instanceof UserDataObject) {
            dataObj = (UserDataObject) user;
        } else if(user instanceof UserBean) {
            dataObj = ((UserBean) user).toDataObject();
        } else {  // if(user instanceof User)
            dataObj = new UserDataObject(user.getGuid(), user.getManagerApp(), user.getAppAcl(), user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
        }
        Boolean suc = getDAOFactory().getUserDAO().updateUser(dataObj);

        log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUserDAO().deleteUser(guid);

        log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        log.finer("BEGIN");

        // Param user cannot be null.....
        if(user == null || user.getGuid() == null) {
            log.log(Level.INFO, "Param user or its guid is null!");
            throw new BadRequestException("Param user object or its guid is null!");
        }
        UserDataObject dataObj = null;
        if(user instanceof UserDataObject) {
            dataObj = (UserDataObject) user;
        } else if(user instanceof UserBean) {
            dataObj = ((UserBean) user).toDataObject();
        } else {  // if(user instanceof User)
            dataObj = new UserDataObject(user.getGuid(), user.getManagerApp(), user.getAppAcl(), user.getGaeApp(), user.getAeryId(), user.getSessionId(), user.getUsername(), user.getNickname(), user.getAvatar(), user.getEmail(), user.getOpenId(), user.getGaeUser(), user.getTimeZone(), user.getLocation(), user.getIpAddress(), user.getReferer(), user.isObsolete(), user.getStatus(), user.getVerifiedTime(), user.getAuthenticatedTime());
        }
        Boolean suc = getDAOFactory().getUserDAO().deleteUser(dataObj);

        log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getUserDAO().deleteUsers(filter, params, values);

        log.finer("END: count = " + count);
        return count;
    }

}
