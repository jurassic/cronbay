package com.cronbay.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.bean.GaeAppStructBean;
import com.cronbay.ws.bean.JobOutputBean;
import com.cronbay.ws.dao.DAOFactory;
import com.cronbay.ws.data.GaeAppStructDataObject;
import com.cronbay.ws.data.JobOutputDataObject;
import com.cronbay.ws.service.DAOFactoryManager;
import com.cronbay.ws.service.JobOutputService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class JobOutputServiceImpl implements JobOutputService
{
    private static final Logger log = Logger.getLogger(JobOutputServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // JobOutput related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public JobOutput getJobOutput(String guid) throws BaseException
    {
        log.finer("BEGIN");

        JobOutputDataObject dataObj = getDAOFactory().getJobOutputDAO().getJobOutput(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve JobOutputDataObject for guid = " + guid);
            return null;  // ????
        }
        JobOutputBean bean = new JobOutputBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getJobOutput(String guid, String field) throws BaseException
    {
        JobOutputDataObject dataObj = getDAOFactory().getJobOutputDAO().getJobOutput(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve JobOutputDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return dataObj.getManagerApp();
        } else if(field.equals("appAcl")) {
            return dataObj.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return dataObj.getGaeApp();
        } else if(field.equals("ownerUser")) {
            return dataObj.getOwnerUser();
        } else if(field.equals("userAcl")) {
            return dataObj.getUserAcl();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("cronJob")) {
            return dataObj.getCronJob();
        } else if(field.equals("previousRun")) {
            return dataObj.getPreviousRun();
        } else if(field.equals("previousTry")) {
            return dataObj.getPreviousTry();
        } else if(field.equals("responseCode")) {
            return dataObj.getResponseCode();
        } else if(field.equals("outputFormat")) {
            return dataObj.getOutputFormat();
        } else if(field.equals("outputText")) {
            return dataObj.getOutputText();
        } else if(field.equals("outputData")) {
            return dataObj.getOutputData();
        } else if(field.equals("outputHash")) {
            return dataObj.getOutputHash();
        } else if(field.equals("permalink")) {
            return dataObj.getPermalink();
        } else if(field.equals("shortlink")) {
            return dataObj.getShortlink();
        } else if(field.equals("result")) {
            return dataObj.getResult();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("extra")) {
            return dataObj.getExtra();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("retryCounter")) {
            return dataObj.getRetryCounter();
        } else if(field.equals("scheduledTime")) {
            return dataObj.getScheduledTime();
        } else if(field.equals("processedTime")) {
            return dataObj.getProcessedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<JobOutput> getJobOutputs(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<JobOutput> list = new ArrayList<JobOutput>();
        List<JobOutputDataObject> dataObjs = getDAOFactory().getJobOutputDAO().getJobOutputs(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve JobOutputDataObject list.");
        } else {
            Iterator<JobOutputDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                JobOutputDataObject dataObj = (JobOutputDataObject) it.next();
                list.add(new JobOutputBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<JobOutput> getAllJobOutputs() throws BaseException
    {
        return getAllJobOutputs(null, null, null);
    }

    @Override
    public List<JobOutput> getAllJobOutputs(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<JobOutput> list = new ArrayList<JobOutput>();
        List<JobOutputDataObject> dataObjs = getDAOFactory().getJobOutputDAO().getAllJobOutputs(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve JobOutputDataObject list.");
        } else {
            Iterator<JobOutputDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                JobOutputDataObject dataObj = (JobOutputDataObject) it.next();
                list.add(new JobOutputBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllJobOutputKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getJobOutputDAO().getAllJobOutputKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve JobOutput key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<JobOutput> findJobOutputs(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findJobOutputs(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<JobOutput> findJobOutputs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("JobOutputServiceImpl.findJobOutputs(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<JobOutput> list = new ArrayList<JobOutput>();
        List<JobOutputDataObject> dataObjs = getDAOFactory().getJobOutputDAO().findJobOutputs(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find jobOutputs for the given criterion.");
        } else {
            Iterator<JobOutputDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                JobOutputDataObject dataObj = (JobOutputDataObject) it.next();
                list.add(new JobOutputBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findJobOutputKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("JobOutputServiceImpl.findJobOutputKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getJobOutputDAO().findJobOutputKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find JobOutput keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("JobOutputServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getJobOutputDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createJobOutput(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        
        JobOutputDataObject dataObj = new JobOutputDataObject(null, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
        return createJobOutput(dataObj);
    }

    @Override
    public String createJobOutput(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        // Param jobOutput cannot be null.....
        if(jobOutput == null) {
            log.log(Level.INFO, "Param jobOutput is null!");
            throw new BadRequestException("Param jobOutput object is null!");
        }
        JobOutputDataObject dataObj = null;
        if(jobOutput instanceof JobOutputDataObject) {
            dataObj = (JobOutputDataObject) jobOutput;
        } else if(jobOutput instanceof JobOutputBean) {
            dataObj = ((JobOutputBean) jobOutput).toDataObject();
        } else {  // if(jobOutput instanceof JobOutput)
            //dataObj = new JobOutputDataObject(null, jobOutput.getManagerApp(), jobOutput.getAppAcl(), (GaeAppStructDataObject) jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new JobOutputDataObject(jobOutput.getGuid(), jobOutput.getManagerApp(), jobOutput.getAppAcl(), (GaeAppStructDataObject) jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
        }
        String guid = getDAOFactory().getJobOutputDAO().createJobOutput(dataObj);

        log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateJobOutput(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime) throws BaseException
    {
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        JobOutputDataObject dataObj = new JobOutputDataObject(guid, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime);
        return updateJobOutput(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateJobOutput(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        // Param jobOutput cannot be null.....
        if(jobOutput == null || jobOutput.getGuid() == null) {
            log.log(Level.INFO, "Param jobOutput or its guid is null!");
            throw new BadRequestException("Param jobOutput object or its guid is null!");
        }
        JobOutputDataObject dataObj = null;
        if(jobOutput instanceof JobOutputDataObject) {
            dataObj = (JobOutputDataObject) jobOutput;
        } else if(jobOutput instanceof JobOutputBean) {
            dataObj = ((JobOutputBean) jobOutput).toDataObject();
        } else {  // if(jobOutput instanceof JobOutput)
            dataObj = new JobOutputDataObject(jobOutput.getGuid(), jobOutput.getManagerApp(), jobOutput.getAppAcl(), jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
        }
        Boolean suc = getDAOFactory().getJobOutputDAO().updateJobOutput(dataObj);

        log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteJobOutput(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getJobOutputDAO().deleteJobOutput(guid);

        log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteJobOutput(JobOutput jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        // Param jobOutput cannot be null.....
        if(jobOutput == null || jobOutput.getGuid() == null) {
            log.log(Level.INFO, "Param jobOutput or its guid is null!");
            throw new BadRequestException("Param jobOutput object or its guid is null!");
        }
        JobOutputDataObject dataObj = null;
        if(jobOutput instanceof JobOutputDataObject) {
            dataObj = (JobOutputDataObject) jobOutput;
        } else if(jobOutput instanceof JobOutputBean) {
            dataObj = ((JobOutputBean) jobOutput).toDataObject();
        } else {  // if(jobOutput instanceof JobOutput)
            dataObj = new JobOutputDataObject(jobOutput.getGuid(), jobOutput.getManagerApp(), jobOutput.getAppAcl(), jobOutput.getGaeApp(), jobOutput.getOwnerUser(), jobOutput.getUserAcl(), jobOutput.getUser(), jobOutput.getCronJob(), jobOutput.getPreviousRun(), jobOutput.getPreviousTry(), jobOutput.getResponseCode(), jobOutput.getOutputFormat(), jobOutput.getOutputText(), jobOutput.getOutputData(), jobOutput.getOutputHash(), jobOutput.getPermalink(), jobOutput.getShortlink(), jobOutput.getResult(), jobOutput.getStatus(), jobOutput.getExtra(), jobOutput.getNote(), jobOutput.getRetryCounter(), jobOutput.getScheduledTime(), jobOutput.getProcessedTime());
        }
        Boolean suc = getDAOFactory().getJobOutputDAO().deleteJobOutput(dataObj);

        log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteJobOutputs(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getJobOutputDAO().deleteJobOutputs(filter, params, values);

        log.finer("END: count = " + count);
        return count;
    }

}
