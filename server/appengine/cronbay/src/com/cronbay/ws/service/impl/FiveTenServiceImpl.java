package com.cronbay.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.FiveTen;
import com.cronbay.ws.bean.FiveTenBean;
import com.cronbay.ws.dao.DAOFactory;
import com.cronbay.ws.data.FiveTenDataObject;
import com.cronbay.ws.service.DAOFactoryManager;
import com.cronbay.ws.service.FiveTenService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FiveTenServiceImpl implements FiveTenService
{
    private static final Logger log = Logger.getLogger(FiveTenServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // FiveTen related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FiveTen getFiveTen(String guid) throws BaseException
    {
        log.finer("BEGIN");

        FiveTenDataObject dataObj = getDAOFactory().getFiveTenDAO().getFiveTen(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenDataObject for guid = " + guid);
            return null;  // ????
        }
        FiveTenBean bean = new FiveTenBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getFiveTen(String guid, String field) throws BaseException
    {
        FiveTenDataObject dataObj = getDAOFactory().getFiveTenDAO().getFiveTen(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("counter")) {
            return dataObj.getCounter();
        } else if(field.equals("requesterIpAddress")) {
            return dataObj.getRequesterIpAddress();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<FiveTen> getFiveTens(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<FiveTen> list = new ArrayList<FiveTen>();
        List<FiveTenDataObject> dataObjs = getDAOFactory().getFiveTenDAO().getFiveTens(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenDataObject list.");
        } else {
            Iterator<FiveTenDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FiveTenDataObject dataObj = (FiveTenDataObject) it.next();
                list.add(new FiveTenBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<FiveTen> getAllFiveTens() throws BaseException
    {
        return getAllFiveTens(null, null, null);
    }

    @Override
    public List<FiveTen> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<FiveTen> list = new ArrayList<FiveTen>();
        List<FiveTenDataObject> dataObjs = getDAOFactory().getFiveTenDAO().getAllFiveTens(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTenDataObject list.");
        } else {
            Iterator<FiveTenDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FiveTenDataObject dataObj = (FiveTenDataObject) it.next();
                list.add(new FiveTenBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN");

        List<String> keys = getDAOFactory().getFiveTenDAO().getAllFiveTenKeys(ordering, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve FiveTen key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFiveTens(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<FiveTen> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("FiveTenServiceImpl.findFiveTens(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<FiveTen> list = new ArrayList<FiveTen>();
        List<FiveTenDataObject> dataObjs = getDAOFactory().getFiveTenDAO().findFiveTens(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find fiveTens for the given criterion.");
        } else {
            Iterator<FiveTenDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FiveTenDataObject dataObj = (FiveTenDataObject) it.next();
                list.add(new FiveTenBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("FiveTenServiceImpl.findFiveTenKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = getDAOFactory().getFiveTenDAO().findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find FiveTen keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("FiveTenServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getFiveTenDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createFiveTen(Integer counter, String requesterIpAddress) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        FiveTenDataObject dataObj = new FiveTenDataObject(null, counter, requesterIpAddress);
        return createFiveTen(dataObj);
    }

    @Override
    public String createFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // Param fiveTen cannot be null.....
        if(fiveTen == null) {
            log.log(Level.INFO, "Param fiveTen is null!");
            throw new BadRequestException("Param fiveTen object is null!");
        }
        FiveTenDataObject dataObj = null;
        if(fiveTen instanceof FiveTenDataObject) {
            dataObj = (FiveTenDataObject) fiveTen;
        } else if(fiveTen instanceof FiveTenBean) {
            dataObj = ((FiveTenBean) fiveTen).toDataObject();
        } else {  // if(fiveTen instanceof FiveTen)
            //dataObj = new FiveTenDataObject(null, fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new FiveTenDataObject(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        }
        String guid = getDAOFactory().getFiveTenDAO().createFiveTen(dataObj);

        log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        FiveTenDataObject dataObj = new FiveTenDataObject(guid, counter, requesterIpAddress);
        return updateFiveTen(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // Param fiveTen cannot be null.....
        if(fiveTen == null || fiveTen.getGuid() == null) {
            log.log(Level.INFO, "Param fiveTen or its guid is null!");
            throw new BadRequestException("Param fiveTen object or its guid is null!");
        }
        FiveTenDataObject dataObj = null;
        if(fiveTen instanceof FiveTenDataObject) {
            dataObj = (FiveTenDataObject) fiveTen;
        } else if(fiveTen instanceof FiveTenBean) {
            dataObj = ((FiveTenBean) fiveTen).toDataObject();
        } else {  // if(fiveTen instanceof FiveTen)
            dataObj = new FiveTenDataObject(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        }
        Boolean suc = getDAOFactory().getFiveTenDAO().updateFiveTen(dataObj);

        log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getFiveTenDAO().deleteFiveTen(guid);

        log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteFiveTen(FiveTen fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        // Param fiveTen cannot be null.....
        if(fiveTen == null || fiveTen.getGuid() == null) {
            log.log(Level.INFO, "Param fiveTen or its guid is null!");
            throw new BadRequestException("Param fiveTen object or its guid is null!");
        }
        FiveTenDataObject dataObj = null;
        if(fiveTen instanceof FiveTenDataObject) {
            dataObj = (FiveTenDataObject) fiveTen;
        } else if(fiveTen instanceof FiveTenBean) {
            dataObj = ((FiveTenBean) fiveTen).toDataObject();
        } else {  // if(fiveTen instanceof FiveTen)
            dataObj = new FiveTenDataObject(fiveTen.getGuid(), fiveTen.getCounter(), fiveTen.getRequesterIpAddress());
        }
        Boolean suc = getDAOFactory().getFiveTenDAO().deleteFiveTen(dataObj);

        log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getFiveTenDAO().deleteFiveTens(filter, params, values);

        log.finer("END: count = " + count);
        return count;
    }

}
