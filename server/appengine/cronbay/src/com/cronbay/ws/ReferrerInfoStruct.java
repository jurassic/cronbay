package com.cronbay.ws;


public interface ReferrerInfoStruct 
{
    String  getReferer();
    String  getUserAgent();
    String  getLanguage();
    String  getHostname();
    String  getIpAddress();
    String  getNote();
}
