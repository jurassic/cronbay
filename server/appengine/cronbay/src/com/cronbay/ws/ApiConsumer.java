package com.cronbay.ws;


public interface ApiConsumer 
{
    String  getGuid();
    String  getAeryId();
    String  getName();
    String  getDescription();
    String  getAppKey();
    String  getAppSecret();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
