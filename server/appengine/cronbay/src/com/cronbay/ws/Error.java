package com.cronbay.ws;

import java.util.Date;

public interface Error
{
    //String getCode();
    String getMessage();
    //String getDetail();
    String getResource();
    String getCause();
    //Date geTime();
}
