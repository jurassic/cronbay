package com.cronbay.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.util.CommonUtil;
import com.cronbay.ws.core.GUID;

@PersistenceCapable(detachable="true")
@EmbeddedOnly
public class GaeAppStructDataObject implements GaeAppStruct, Serializable{
    private static final Logger log = Logger.getLogger(GaeAppStructDataObject.class.getName());

    @Persistent(defaultFetchGroup = "true")
    private String groupId;

    @Persistent(defaultFetchGroup = "true")
    private String appId;

    @Persistent(defaultFetchGroup = "true")
    private String appDomain;

    @Persistent(defaultFetchGroup = "true")
    private String namespace;

    @Persistent(defaultFetchGroup = "true")
    private Long acl;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public GaeAppStructDataObject()
    {
        this(null, null, null, null, null, null);
    }
    public GaeAppStructDataObject(String groupId, String appId, String appDomain, String namespace, Long acl, String note)
    {

        this.groupId = groupId;
        this.appId = appId;
        this.appDomain = appDomain;
        this.namespace = namespace;
        this.acl = acl;
        this.note = note;
    }


    public String getGroupId()
    {
        return this.groupId;
    }
    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getAppId()
    {
        return this.appId;
    }
    public void setAppId(String appId)
    {
        this.appId = appId;
    }

    public String getAppDomain()
    {
        return this.appDomain;
    }
    public void setAppDomain(String appDomain)
    {
        this.appDomain = appDomain;
    }

    public String getNamespace()
    {
        return this.namespace;
    }
    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    public Long getAcl()
    {
        return this.acl;
    }
    public void setAcl(Long acl)
    {
        this.acl = acl;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("groupId", this.groupId);
        dataMap.put("appId", this.appId);
        dataMap.put("appDomain", this.appDomain);
        dataMap.put("namespace", this.namespace);
        dataMap.put("acl", this.acl);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        GaeAppStruct thatObj = (GaeAppStruct) obj;
        if( (this.groupId == null && thatObj.getGroupId() != null)
            || (this.groupId != null && thatObj.getGroupId() == null)
            || !this.groupId.equals(thatObj.getGroupId()) ) {
            return false;
        }
        if( (this.appId == null && thatObj.getAppId() != null)
            || (this.appId != null && thatObj.getAppId() == null)
            || !this.appId.equals(thatObj.getAppId()) ) {
            return false;
        }
        if( (this.appDomain == null && thatObj.getAppDomain() != null)
            || (this.appDomain != null && thatObj.getAppDomain() == null)
            || !this.appDomain.equals(thatObj.getAppDomain()) ) {
            return false;
        }
        if( (this.namespace == null && thatObj.getNamespace() != null)
            || (this.namespace != null && thatObj.getNamespace() == null)
            || !this.namespace.equals(thatObj.getNamespace()) ) {
            return false;
        }
        if( (this.acl == null && thatObj.getAcl() != null)
            || (this.acl != null && thatObj.getAcl() == null)
            || !this.acl.equals(thatObj.getAcl()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = groupId == null ? 0 : groupId.hashCode();
        _hash = 31 * _hash + delta;
        delta = appId == null ? 0 : appId.hashCode();
        _hash = 31 * _hash + delta;
        delta = appDomain == null ? 0 : appDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = namespace == null ? 0 : namespace.hashCode();
        _hash = 31 * _hash + delta;
        delta = acl == null ? 0 : acl.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
