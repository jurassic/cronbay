package com.cronbay.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.util.CommonUtil;
import com.cronbay.ws.core.GUID;

@PersistenceCapable(detachable="true")
@EmbeddedOnly
public class NotificationStructDataObject implements NotificationStruct, Serializable{
    private static final Logger log = Logger.getLogger(NotificationStructDataObject.class.getName());

    @Persistent(defaultFetchGroup = "true")
    private String preferredMode;

    @Persistent(defaultFetchGroup = "true")
    private String mobileNumber;

    @Persistent(defaultFetchGroup = "true")
    private String emailAddress;

    @Persistent(defaultFetchGroup = "true")
    private String callbackUrl;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public NotificationStructDataObject()
    {
        this(null, null, null, null, null);
    }
    public NotificationStructDataObject(String preferredMode, String mobileNumber, String emailAddress, String callbackUrl, String note)
    {

        this.preferredMode = preferredMode;
        this.mobileNumber = mobileNumber;
        this.emailAddress = emailAddress;
        this.callbackUrl = callbackUrl;
        this.note = note;
    }


    public String getPreferredMode()
    {
        return this.preferredMode;
    }
    public void setPreferredMode(String preferredMode)
    {
        this.preferredMode = preferredMode;
    }

    public String getMobileNumber()
    {
        return this.mobileNumber;
    }
    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailAddress()
    {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getCallbackUrl()
    {
        return this.callbackUrl;
    }
    public void setCallbackUrl(String callbackUrl)
    {
        this.callbackUrl = callbackUrl;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("preferredMode", this.preferredMode);
        dataMap.put("mobileNumber", this.mobileNumber);
        dataMap.put("emailAddress", this.emailAddress);
        dataMap.put("callbackUrl", this.callbackUrl);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        NotificationStruct thatObj = (NotificationStruct) obj;
        if( (this.preferredMode == null && thatObj.getPreferredMode() != null)
            || (this.preferredMode != null && thatObj.getPreferredMode() == null)
            || !this.preferredMode.equals(thatObj.getPreferredMode()) ) {
            return false;
        }
        if( (this.mobileNumber == null && thatObj.getMobileNumber() != null)
            || (this.mobileNumber != null && thatObj.getMobileNumber() == null)
            || !this.mobileNumber.equals(thatObj.getMobileNumber()) ) {
            return false;
        }
        if( (this.emailAddress == null && thatObj.getEmailAddress() != null)
            || (this.emailAddress != null && thatObj.getEmailAddress() == null)
            || !this.emailAddress.equals(thatObj.getEmailAddress()) ) {
            return false;
        }
        if( (this.callbackUrl == null && thatObj.getCallbackUrl() != null)
            || (this.callbackUrl != null && thatObj.getCallbackUrl() == null)
            || !this.callbackUrl.equals(thatObj.getCallbackUrl()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = preferredMode == null ? 0 : preferredMode.hashCode();
        _hash = 31 * _hash + delta;
        delta = mobileNumber == null ? 0 : mobileNumber.hashCode();
        _hash = 31 * _hash + delta;
        delta = emailAddress == null ? 0 : emailAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = callbackUrl == null ? 0 : callbackUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
