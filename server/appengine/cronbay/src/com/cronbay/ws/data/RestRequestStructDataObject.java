package com.cronbay.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.util.CommonUtil;
import com.cronbay.ws.core.GUID;

@PersistenceCapable(detachable="true")
@EmbeddedOnly
public class RestRequestStructDataObject implements RestRequestStruct, Serializable{
    private static final Logger log = Logger.getLogger(RestRequestStructDataObject.class.getName());

    @Persistent(defaultFetchGroup = "true")
    private String serviceName;

    @Persistent(defaultFetchGroup = "true")
    private String serviceUrl;

    @Persistent(defaultFetchGroup = "true")
    private String requestMethod;

    @Persistent(defaultFetchGroup = "true")
    private String requestUrl;

    @Persistent(defaultFetchGroup = "true")
    private String targetEntity;

    @Persistent(defaultFetchGroup = "true")
    private String queryString;

    @Persistent(defaultFetchGroup = "true")
    private List<String> queryParams;

    @Persistent(defaultFetchGroup = "true")
    private String inputFormat;

    @Persistent(defaultFetchGroup = "true")
    private List<String> inputContent;

    @Persistent(defaultFetchGroup = "true")
    private String outputFormat;

    @Persistent(defaultFetchGroup = "true")
    private Integer maxRetries;

    @Persistent(defaultFetchGroup = "true")
    private Integer retryInterval;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public RestRequestStructDataObject()
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public RestRequestStructDataObject(String serviceName, String serviceUrl, String requestMethod, String requestUrl, String targetEntity, String queryString, List<String> queryParams, String inputFormat, String inputContent, String outputFormat, Integer maxRetries, Integer retryInterval, String note)
    {

        this.serviceName = serviceName;
        this.serviceUrl = serviceUrl;
        this.requestMethod = requestMethod;
        this.requestUrl = requestUrl;
        this.targetEntity = targetEntity;
        this.queryString = queryString;
        this.queryParams = queryParams;
        this.inputFormat = inputFormat;
        this.setInputContent(inputContent);
        this.outputFormat = outputFormat;
        this.maxRetries = maxRetries;
        this.retryInterval = retryInterval;
        this.note = note;
    }


    public String getServiceName()
    {
        return this.serviceName;
    }
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    public String getRequestMethod()
    {
        return this.requestMethod;
    }
    public void setRequestMethod(String requestMethod)
    {
        this.requestMethod = requestMethod;
    }

    public String getRequestUrl()
    {
        return this.requestUrl;
    }
    public void setRequestUrl(String requestUrl)
    {
        this.requestUrl = requestUrl;
    }

    public String getTargetEntity()
    {
        return this.targetEntity;
    }
    public void setTargetEntity(String targetEntity)
    {
        this.targetEntity = targetEntity;
    }

    public String getQueryString()
    {
        return this.queryString;
    }
    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    public List<String> getQueryParams()
    {
        return this.queryParams;
    }
    public void setQueryParams(List<String> queryParams)
    {
        this.queryParams = queryParams;
    }

    public String getInputFormat()
    {
        return this.inputFormat;
    }
    public void setInputFormat(String inputFormat)
    {
        this.inputFormat = inputFormat;
    }

    public String getInputContent()
    {
        if(this.inputContent == null) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for(String v : this.inputContent) {
            sb.append(v);
        }
        return sb.toString(); 
    }
    public void setInputContent(String inputContent)
    {
        if(inputContent == null) {
            this.inputContent = null;
        } else {
            List<String> _list = new ArrayList<String>();
            int _beg = 0;
            int _rem = inputContent.length();
            while(_rem > 0) {
                int _sz;
                if((_rem - 500) >= 0) {
                    _sz = 500;
                    _rem -= 500;
                } else {
                    _sz = _rem;
                    _rem = 0;
                }
                _list.add(inputContent.substring(_beg, _beg+_sz));
                _beg += _sz;
            }
            this.inputContent = _list;
        }
    }

    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    public Integer getMaxRetries()
    {
        return this.maxRetries;
    }
    public void setMaxRetries(Integer maxRetries)
    {
        this.maxRetries = maxRetries;
    }

    public Integer getRetryInterval()
    {
        return this.retryInterval;
    }
    public void setRetryInterval(Integer retryInterval)
    {
        this.retryInterval = retryInterval;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("serviceName", this.serviceName);
        dataMap.put("serviceUrl", this.serviceUrl);
        dataMap.put("requestMethod", this.requestMethod);
        dataMap.put("requestUrl", this.requestUrl);
        dataMap.put("targetEntity", this.targetEntity);
        dataMap.put("queryString", this.queryString);
        dataMap.put("queryParams", this.queryParams);
        dataMap.put("inputFormat", this.inputFormat);
        dataMap.put("inputContent", this.inputContent);
        dataMap.put("outputFormat", this.outputFormat);
        dataMap.put("maxRetries", this.maxRetries);
        dataMap.put("retryInterval", this.retryInterval);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        RestRequestStruct thatObj = (RestRequestStruct) obj;
        if( (this.serviceName == null && thatObj.getServiceName() != null)
            || (this.serviceName != null && thatObj.getServiceName() == null)
            || !this.serviceName.equals(thatObj.getServiceName()) ) {
            return false;
        }
        if( (this.serviceUrl == null && thatObj.getServiceUrl() != null)
            || (this.serviceUrl != null && thatObj.getServiceUrl() == null)
            || !this.serviceUrl.equals(thatObj.getServiceUrl()) ) {
            return false;
        }
        if( (this.requestMethod == null && thatObj.getRequestMethod() != null)
            || (this.requestMethod != null && thatObj.getRequestMethod() == null)
            || !this.requestMethod.equals(thatObj.getRequestMethod()) ) {
            return false;
        }
        if( (this.requestUrl == null && thatObj.getRequestUrl() != null)
            || (this.requestUrl != null && thatObj.getRequestUrl() == null)
            || !this.requestUrl.equals(thatObj.getRequestUrl()) ) {
            return false;
        }
        if( (this.targetEntity == null && thatObj.getTargetEntity() != null)
            || (this.targetEntity != null && thatObj.getTargetEntity() == null)
            || !this.targetEntity.equals(thatObj.getTargetEntity()) ) {
            return false;
        }
        if( (this.queryString == null && thatObj.getQueryString() != null)
            || (this.queryString != null && thatObj.getQueryString() == null)
            || !this.queryString.equals(thatObj.getQueryString()) ) {
            return false;
        }
        if( (this.queryParams == null && thatObj.getQueryParams() != null)
            || (this.queryParams != null && thatObj.getQueryParams() == null)
            || !this.queryParams.equals(thatObj.getQueryParams()) ) {
            return false;
        }
        if( (this.inputFormat == null && thatObj.getInputFormat() != null)
            || (this.inputFormat != null && thatObj.getInputFormat() == null)
            || !this.inputFormat.equals(thatObj.getInputFormat()) ) {
            return false;
        }
        if( (this.inputContent == null && thatObj.getInputContent() != null)
            || (this.inputContent != null && thatObj.getInputContent() == null)
            || !this.inputContent.equals(thatObj.getInputContent()) ) {
            return false;
        }
        if( (this.outputFormat == null && thatObj.getOutputFormat() != null)
            || (this.outputFormat != null && thatObj.getOutputFormat() == null)
            || !this.outputFormat.equals(thatObj.getOutputFormat()) ) {
            return false;
        }
        if( (this.maxRetries == null && thatObj.getMaxRetries() != null)
            || (this.maxRetries != null && thatObj.getMaxRetries() == null)
            || !this.maxRetries.equals(thatObj.getMaxRetries()) ) {
            return false;
        }
        if( (this.retryInterval == null && thatObj.getRetryInterval() != null)
            || (this.retryInterval != null && thatObj.getRetryInterval() == null)
            || !this.retryInterval.equals(thatObj.getRetryInterval()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = serviceName == null ? 0 : serviceName.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = requestMethod == null ? 0 : requestMethod.hashCode();
        _hash = 31 * _hash + delta;
        delta = requestUrl == null ? 0 : requestUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetEntity == null ? 0 : targetEntity.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryString == null ? 0 : queryString.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryParams == null ? 0 : queryParams.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputFormat == null ? 0 : inputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = inputContent == null ? 0 : inputContent.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFormat == null ? 0 : outputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = maxRetries == null ? 0 : maxRetries.hashCode();
        _hash = 31 * _hash + delta;
        delta = retryInterval == null ? 0 : retryInterval.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
