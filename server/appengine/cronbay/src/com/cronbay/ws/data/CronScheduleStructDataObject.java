package com.cronbay.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.util.CommonUtil;
import com.cronbay.ws.core.GUID;

@PersistenceCapable(detachable="true")
@EmbeddedOnly
public class CronScheduleStructDataObject implements CronScheduleStruct, Serializable{
    private static final Logger log = Logger.getLogger(CronScheduleStructDataObject.class.getName());

    @Persistent(defaultFetchGroup = "true")
    private String type;

    @Persistent(defaultFetchGroup = "true")
    private String schedule;

    @Persistent(defaultFetchGroup = "true")
    private String timezone;

    @Persistent(defaultFetchGroup = "true")
    private Integer maxIterations;

    @Persistent(defaultFetchGroup = "true")
    private Integer repeatInterval;

    @Persistent(defaultFetchGroup = "true")
    private Long firtRunTime;

    @Persistent(defaultFetchGroup = "true")
    private Long startTime;

    @Persistent(defaultFetchGroup = "true")
    private Long endTime;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public CronScheduleStructDataObject()
    {
        this(null, null, null, null, null, null, null, null, null);
    }
    public CronScheduleStructDataObject(String type, String schedule, String timezone, Integer maxIterations, Integer repeatInterval, Long firtRunTime, Long startTime, Long endTime, String note)
    {

        this.type = type;
        this.schedule = schedule;
        this.timezone = timezone;
        this.maxIterations = maxIterations;
        this.repeatInterval = repeatInterval;
        this.firtRunTime = firtRunTime;
        this.startTime = startTime;
        this.endTime = endTime;
        this.note = note;
    }


    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getSchedule()
    {
        return this.schedule;
    }
    public void setSchedule(String schedule)
    {
        this.schedule = schedule;
    }

    public String getTimezone()
    {
        return this.timezone;
    }
    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }

    public Integer getMaxIterations()
    {
        return this.maxIterations;
    }
    public void setMaxIterations(Integer maxIterations)
    {
        this.maxIterations = maxIterations;
    }

    public Integer getRepeatInterval()
    {
        return this.repeatInterval;
    }
    public void setRepeatInterval(Integer repeatInterval)
    {
        this.repeatInterval = repeatInterval;
    }

    public Long getFirtRunTime()
    {
        return this.firtRunTime;
    }
    public void setFirtRunTime(Long firtRunTime)
    {
        this.firtRunTime = firtRunTime;
    }

    public Long getStartTime()
    {
        return this.startTime;
    }
    public void setStartTime(Long startTime)
    {
        this.startTime = startTime;
    }

    public Long getEndTime()
    {
        return this.endTime;
    }
    public void setEndTime(Long endTime)
    {
        this.endTime = endTime;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("type", this.type);
        dataMap.put("schedule", this.schedule);
        dataMap.put("timezone", this.timezone);
        dataMap.put("maxIterations", this.maxIterations);
        dataMap.put("repeatInterval", this.repeatInterval);
        dataMap.put("firtRunTime", this.firtRunTime);
        dataMap.put("startTime", this.startTime);
        dataMap.put("endTime", this.endTime);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        CronScheduleStruct thatObj = (CronScheduleStruct) obj;
        if( (this.type == null && thatObj.getType() != null)
            || (this.type != null && thatObj.getType() == null)
            || !this.type.equals(thatObj.getType()) ) {
            return false;
        }
        if( (this.schedule == null && thatObj.getSchedule() != null)
            || (this.schedule != null && thatObj.getSchedule() == null)
            || !this.schedule.equals(thatObj.getSchedule()) ) {
            return false;
        }
        if( (this.timezone == null && thatObj.getTimezone() != null)
            || (this.timezone != null && thatObj.getTimezone() == null)
            || !this.timezone.equals(thatObj.getTimezone()) ) {
            return false;
        }
        if( (this.maxIterations == null && thatObj.getMaxIterations() != null)
            || (this.maxIterations != null && thatObj.getMaxIterations() == null)
            || !this.maxIterations.equals(thatObj.getMaxIterations()) ) {
            return false;
        }
        if( (this.repeatInterval == null && thatObj.getRepeatInterval() != null)
            || (this.repeatInterval != null && thatObj.getRepeatInterval() == null)
            || !this.repeatInterval.equals(thatObj.getRepeatInterval()) ) {
            return false;
        }
        if( (this.firtRunTime == null && thatObj.getFirtRunTime() != null)
            || (this.firtRunTime != null && thatObj.getFirtRunTime() == null)
            || !this.firtRunTime.equals(thatObj.getFirtRunTime()) ) {
            return false;
        }
        if( (this.startTime == null && thatObj.getStartTime() != null)
            || (this.startTime != null && thatObj.getStartTime() == null)
            || !this.startTime.equals(thatObj.getStartTime()) ) {
            return false;
        }
        if( (this.endTime == null && thatObj.getEndTime() != null)
            || (this.endTime != null && thatObj.getEndTime() == null)
            || !this.endTime.equals(thatObj.getEndTime()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = schedule == null ? 0 : schedule.hashCode();
        _hash = 31 * _hash + delta;
        delta = timezone == null ? 0 : timezone.hashCode();
        _hash = 31 * _hash + delta;
        delta = maxIterations == null ? 0 : maxIterations.hashCode();
        _hash = 31 * _hash + delta;
        delta = repeatInterval == null ? 0 : repeatInterval.hashCode();
        _hash = 31 * _hash + delta;
        delta = firtRunTime == null ? 0 : firtRunTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = startTime == null ? 0 : startTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = endTime == null ? 0 : endTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
