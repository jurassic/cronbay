package com.cronbay.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.JobOutput;
import com.cronbay.ws.util.CommonUtil;
import com.cronbay.ws.core.GUID;

@PersistenceCapable(detachable="true")
public class JobOutputDataObject extends KeyedDataObject implements JobOutput{
    private static final Logger log = Logger.getLogger(JobOutputDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(JobOutputDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(JobOutputDataObject.class.getSimpleName(), guid);
        return key; 
    }

    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String managerApp;

    @Persistent(defaultFetchGroup = "true")
    private Long appAcl;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="groupId", columns=@Column(name="gaeAppgroupId")),
        @Persistent(name="appId", columns=@Column(name="gaeAppappId")),
        @Persistent(name="appDomain", columns=@Column(name="gaeAppappDomain")),
        @Persistent(name="namespace", columns=@Column(name="gaeAppnamespace")),
        @Persistent(name="acl", columns=@Column(name="gaeAppacl")),
        @Persistent(name="note", columns=@Column(name="gaeAppnote")),
    })
    private GaeAppStructDataObject gaeApp;

    @Persistent(defaultFetchGroup = "true")
    private String ownerUser;

    @Persistent(defaultFetchGroup = "true")
    private Long userAcl;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String cronJob;

    @Persistent(defaultFetchGroup = "true")
    private String previousRun;

    @Persistent(defaultFetchGroup = "true")
    private String previousTry;

    @Persistent(defaultFetchGroup = "true")
    private String responseCode;

    @Persistent(defaultFetchGroup = "true")
    private String outputFormat;

    @Persistent(defaultFetchGroup = "false")
    private List<String> outputText;

    @Persistent(defaultFetchGroup = "true")
    private List<String> outputData;

    @Persistent(defaultFetchGroup = "true")
    private String outputHash;

    @Persistent(defaultFetchGroup = "true")
    private String permalink;

    @Persistent(defaultFetchGroup = "true")
    private String shortlink;

    @Persistent(defaultFetchGroup = "true")
    private String result;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String extra;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private Integer retryCounter;

    @Persistent(defaultFetchGroup = "true")
    private Long scheduledTime;

    @Persistent(defaultFetchGroup = "true")
    private Long processedTime;

    public JobOutputDataObject()
    {
        this(null);
    }
    public JobOutputDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public JobOutputDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, cronJob, previousRun, previousTry, responseCode, outputFormat, outputText, outputData, outputHash, permalink, shortlink, result, status, extra, note, retryCounter, scheduledTime, processedTime, null, null);
    }
    public JobOutputDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String cronJob, String previousRun, String previousTry, String responseCode, String outputFormat, String outputText, List<String> outputData, String outputHash, String permalink, String shortlink, String result, String status, String extra, String note, Integer retryCounter, Long scheduledTime, Long processedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);

        this.managerApp = managerApp;
        this.appAcl = appAcl;
        if(gaeApp != null) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = null;
        }
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.cronJob = cronJob;
        this.previousRun = previousRun;
        this.previousTry = previousTry;
        this.responseCode = responseCode;
        this.outputFormat = outputFormat;
        this.setOutputText(outputText);
        this.outputData = outputData;
        this.outputHash = outputHash;
        this.permalink = permalink;
        this.shortlink = shortlink;
        this.result = result;
        this.status = status;
        this.extra = extra;
        this.note = note;
        this.retryCounter = retryCounter;
        this.scheduledTime = scheduledTime;
        this.processedTime = processedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return JobOutputDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return JobOutputDataObject.composeKey(getGuid());
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStruct getGaeApp()
    {
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(gaeApp == null) {
            this.gaeApp = null;
            log.log(Level.INFO, "JobOutputDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is null.");            
        } else if(gaeApp instanceof GaeAppStructDataObject) {
            this.gaeApp = (GaeAppStructDataObject) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = new GaeAppStructDataObject();   // ????
            log.log(Level.WARNING, "JobOutputDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is of an invalid type.");
        }
    }

    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getCronJob()
    {
        return this.cronJob;
    }
    public void setCronJob(String cronJob)
    {
        this.cronJob = cronJob;
    }

    public String getPreviousRun()
    {
        return this.previousRun;
    }
    public void setPreviousRun(String previousRun)
    {
        this.previousRun = previousRun;
    }

    public String getPreviousTry()
    {
        return this.previousTry;
    }
    public void setPreviousTry(String previousTry)
    {
        this.previousTry = previousTry;
    }

    public String getResponseCode()
    {
        return this.responseCode;
    }
    public void setResponseCode(String responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    public String getOutputText()
    {
        if(this.outputText == null) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for(String v : this.outputText) {
            sb.append(v);
        }
        return sb.toString(); 
    }
    public void setOutputText(String outputText)
    {
        if(outputText == null) {
            this.outputText = null;
        } else {
            List<String> _list = new ArrayList<String>();
            int _beg = 0;
            int _rem = outputText.length();
            while(_rem > 0) {
                int _sz;
                if((_rem - 500) >= 0) {
                    _sz = 500;
                    _rem -= 500;
                } else {
                    _sz = _rem;
                    _rem = 0;
                }
                _list.add(outputText.substring(_beg, _beg+_sz));
                _beg += _sz;
            }
            this.outputText = _list;
        }
    }

    public List<String> getOutputData()
    {
        return this.outputData;
    }
    public void setOutputData(List<String> outputData)
    {
        this.outputData = outputData;
    }

    public String getOutputHash()
    {
        return this.outputHash;
    }
    public void setOutputHash(String outputHash)
    {
        this.outputHash = outputHash;
    }

    public String getPermalink()
    {
        return this.permalink;
    }
    public void setPermalink(String permalink)
    {
        this.permalink = permalink;
    }

    public String getShortlink()
    {
        return this.shortlink;
    }
    public void setShortlink(String shortlink)
    {
        this.shortlink = shortlink;
    }

    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getExtra()
    {
        return this.extra;
    }
    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Integer getRetryCounter()
    {
        return this.retryCounter;
    }
    public void setRetryCounter(Integer retryCounter)
    {
        this.retryCounter = retryCounter;
    }

    public Long getScheduledTime()
    {
        return this.scheduledTime;
    }
    public void setScheduledTime(Long scheduledTime)
    {
        this.scheduledTime = scheduledTime;
    }

    public Long getProcessedTime()
    {
        return this.processedTime;
    }
    public void setProcessedTime(Long processedTime)
    {
        this.processedTime = processedTime;
    }

    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("ownerUser", this.ownerUser);
        dataMap.put("userAcl", this.userAcl);
        dataMap.put("user", this.user);
        dataMap.put("cronJob", this.cronJob);
        dataMap.put("previousRun", this.previousRun);
        dataMap.put("previousTry", this.previousTry);
        dataMap.put("responseCode", this.responseCode);
        dataMap.put("outputFormat", this.outputFormat);
        dataMap.put("outputText", this.outputText);
        dataMap.put("outputData", this.outputData);
        dataMap.put("outputHash", this.outputHash);
        dataMap.put("permalink", this.permalink);
        dataMap.put("shortlink", this.shortlink);
        dataMap.put("result", this.result);
        dataMap.put("status", this.status);
        dataMap.put("extra", this.extra);
        dataMap.put("note", this.note);
        dataMap.put("retryCounter", this.retryCounter);
        dataMap.put("scheduledTime", this.scheduledTime);
        dataMap.put("processedTime", this.processedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        JobOutput thatObj = (JobOutput) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.managerApp == null && thatObj.getManagerApp() != null)
            || (this.managerApp != null && thatObj.getManagerApp() == null)
            || !this.managerApp.equals(thatObj.getManagerApp()) ) {
            return false;
        }
        if( (this.appAcl == null && thatObj.getAppAcl() != null)
            || (this.appAcl != null && thatObj.getAppAcl() == null)
            || !this.appAcl.equals(thatObj.getAppAcl()) ) {
            return false;
        }
        if( (this.gaeApp == null && thatObj.getGaeApp() != null)
            || (this.gaeApp != null && thatObj.getGaeApp() == null)
            || !this.gaeApp.equals(thatObj.getGaeApp()) ) {
            return false;
        }
        if( (this.ownerUser == null && thatObj.getOwnerUser() != null)
            || (this.ownerUser != null && thatObj.getOwnerUser() == null)
            || !this.ownerUser.equals(thatObj.getOwnerUser()) ) {
            return false;
        }
        if( (this.userAcl == null && thatObj.getUserAcl() != null)
            || (this.userAcl != null && thatObj.getUserAcl() == null)
            || !this.userAcl.equals(thatObj.getUserAcl()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.cronJob == null && thatObj.getCronJob() != null)
            || (this.cronJob != null && thatObj.getCronJob() == null)
            || !this.cronJob.equals(thatObj.getCronJob()) ) {
            return false;
        }
        if( (this.previousRun == null && thatObj.getPreviousRun() != null)
            || (this.previousRun != null && thatObj.getPreviousRun() == null)
            || !this.previousRun.equals(thatObj.getPreviousRun()) ) {
            return false;
        }
        if( (this.previousTry == null && thatObj.getPreviousTry() != null)
            || (this.previousTry != null && thatObj.getPreviousTry() == null)
            || !this.previousTry.equals(thatObj.getPreviousTry()) ) {
            return false;
        }
        if( (this.responseCode == null && thatObj.getResponseCode() != null)
            || (this.responseCode != null && thatObj.getResponseCode() == null)
            || !this.responseCode.equals(thatObj.getResponseCode()) ) {
            return false;
        }
        if( (this.outputFormat == null && thatObj.getOutputFormat() != null)
            || (this.outputFormat != null && thatObj.getOutputFormat() == null)
            || !this.outputFormat.equals(thatObj.getOutputFormat()) ) {
            return false;
        }
        if( (this.outputText == null && thatObj.getOutputText() != null)
            || (this.outputText != null && thatObj.getOutputText() == null)
            || !this.outputText.equals(thatObj.getOutputText()) ) {
            return false;
        }
        if( (this.outputData == null && thatObj.getOutputData() != null)
            || (this.outputData != null && thatObj.getOutputData() == null)
            || !this.outputData.equals(thatObj.getOutputData()) ) {
            return false;
        }
        if( (this.outputHash == null && thatObj.getOutputHash() != null)
            || (this.outputHash != null && thatObj.getOutputHash() == null)
            || !this.outputHash.equals(thatObj.getOutputHash()) ) {
            return false;
        }
        if( (this.permalink == null && thatObj.getPermalink() != null)
            || (this.permalink != null && thatObj.getPermalink() == null)
            || !this.permalink.equals(thatObj.getPermalink()) ) {
            return false;
        }
        if( (this.shortlink == null && thatObj.getShortlink() != null)
            || (this.shortlink != null && thatObj.getShortlink() == null)
            || !this.shortlink.equals(thatObj.getShortlink()) ) {
            return false;
        }
        if( (this.result == null && thatObj.getResult() != null)
            || (this.result != null && thatObj.getResult() == null)
            || !this.result.equals(thatObj.getResult()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.extra == null && thatObj.getExtra() != null)
            || (this.extra != null && thatObj.getExtra() == null)
            || !this.extra.equals(thatObj.getExtra()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.retryCounter == null && thatObj.getRetryCounter() != null)
            || (this.retryCounter != null && thatObj.getRetryCounter() == null)
            || !this.retryCounter.equals(thatObj.getRetryCounter()) ) {
            return false;
        }
        if( (this.scheduledTime == null && thatObj.getScheduledTime() != null)
            || (this.scheduledTime != null && thatObj.getScheduledTime() == null)
            || !this.scheduledTime.equals(thatObj.getScheduledTime()) ) {
            return false;
        }
        if( (this.processedTime == null && thatObj.getProcessedTime() != null)
            || (this.processedTime != null && thatObj.getProcessedTime() == null)
            || !this.processedTime.equals(thatObj.getProcessedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = ownerUser == null ? 0 : ownerUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAcl == null ? 0 : userAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = cronJob == null ? 0 : cronJob.hashCode();
        _hash = 31 * _hash + delta;
        delta = previousRun == null ? 0 : previousRun.hashCode();
        _hash = 31 * _hash + delta;
        delta = previousTry == null ? 0 : previousTry.hashCode();
        _hash = 31 * _hash + delta;
        delta = responseCode == null ? 0 : responseCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFormat == null ? 0 : outputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputText == null ? 0 : outputText.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputData == null ? 0 : outputData.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputHash == null ? 0 : outputHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = permalink == null ? 0 : permalink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortlink == null ? 0 : shortlink.hashCode();
        _hash = 31 * _hash + delta;
        delta = result == null ? 0 : result.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = extra == null ? 0 : extra.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = retryCounter == null ? 0 : retryCounter.hashCode();
        _hash = 31 * _hash + delta;
        delta = scheduledTime == null ? 0 : scheduledTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = processedTime == null ? 0 : processedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
