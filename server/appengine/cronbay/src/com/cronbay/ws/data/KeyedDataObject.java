package com.cronbay.ws.data;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;


@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class KeyedDataObject
{
    private static final Logger log = Logger.getLogger(KeyedDataObject.class.getName());

//    @PrimaryKey
//    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
//    private Key key;
    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String key;

    @Persistent
    private Long createdTime;

    @Persistent
    private Long modifiedTime;

    
    public KeyedDataObject() 
    {
    	this(0L);
    }
    public KeyedDataObject(Long createdTime) 
    {
    	this(createdTime, 0L);
    }
    public KeyedDataObject(Long createdTime, Long modifiedTime) 
    {
    	this.createdTime = createdTime;
    	this.modifiedTime = modifiedTime;
    }
    
    public String getKey() 
    {
        return this.key;
    }
    private void resetKey(String key)
    {
        this.key = key;
    }
    private void setKey(String key)
    {
        // Key can be set only one????
        if(this.key == null) {
            resetKey(key);
        } else {
            log.info("Key is already set. New key will be ignored.");
        }
    }
    // This needs to be overridden in a subclass.
    protected String createKey()
    {
        return null;
    }
    // This needs to be overridden in a subclass.
    protected String createKey(String guid)
    {
        return null;
    }
    // This needs to be overridden in a subclass.
    protected String createKey(Long id)
    {
        return null;
    }

//    public Key getKey() 
//    {
//        return this.key;
//    }
//    private void resetKey(Key key)
//    {
//        this.key = key;
//    }
//    private void setKey(Key key)
//    {
//        // Key can be set only one????
//        if(this.key == null) {
//            resetKey(key);
//        } else {
//            log.info("Key is already set. New key will be ignored.");
//        }
//    }
//    // This needs to be overridden in a subclass.
//    protected Key createKey()
//    {
//        return null;
//    }
//    // This needs to be overridden in a subclass.
//    protected Key createKey(String guid)
//    {
//        return null;
//    }
//    // This needs to be overridden in a subclass.
//    protected Key createKey(Long id)
//    {
//        return null;
//    }

    protected void rebuildKey()
    {
        resetKey(createKey());
    }
    protected void rebuildKey(String guid)
    {
        resetKey(createKey(guid));
    }
    protected void rebuildKey(Long id)
    {
        resetKey(createKey(id));
    }
    protected void buildKey()
    {
        setKey(createKey());
    }
    protected void buildKey(String guid)
    {
        setKey(createKey(guid));
    }
    protected void buildKey(Long id)
    {
        setKey(createKey(id));
    }

    public Long getCreatedTime() 
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime() 
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime) 
    {
        this.modifiedTime = modifiedTime;
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("key", this.key);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);
        return dataMap;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

}
