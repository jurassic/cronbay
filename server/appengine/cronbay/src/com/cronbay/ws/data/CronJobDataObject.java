package com.cronbay.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.cronbay.ws.NotificationStruct;
import com.cronbay.ws.RestRequestStruct;
import com.cronbay.ws.GaeAppStruct;
import com.cronbay.ws.ReferrerInfoStruct;
import com.cronbay.ws.CronScheduleStruct;
import com.cronbay.ws.CronJob;
import com.cronbay.ws.util.CommonUtil;
import com.cronbay.ws.core.GUID;

@PersistenceCapable(detachable="true")
public class CronJobDataObject extends KeyedDataObject implements CronJob{
    private static final Logger log = Logger.getLogger(CronJobDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(CronJobDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(CronJobDataObject.class.getSimpleName(), guid);
        return key; 
    }

    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String managerApp;

    @Persistent(defaultFetchGroup = "true")
    private Long appAcl;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="groupId", columns=@Column(name="gaeAppgroupId")),
        @Persistent(name="appId", columns=@Column(name="gaeAppappId")),
        @Persistent(name="appDomain", columns=@Column(name="gaeAppappDomain")),
        @Persistent(name="namespace", columns=@Column(name="gaeAppnamespace")),
        @Persistent(name="acl", columns=@Column(name="gaeAppacl")),
        @Persistent(name="note", columns=@Column(name="gaeAppnote")),
    })
    private GaeAppStructDataObject gaeApp;

    @Persistent(defaultFetchGroup = "true")
    private String ownerUser;

    @Persistent(defaultFetchGroup = "true")
    private Long userAcl;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private Integer jobId;

    @Persistent(defaultFetchGroup = "true")
    private String title;

    @Persistent(defaultFetchGroup = "true")
    private String description;

    @Persistent(defaultFetchGroup = "true")
    private String originJob;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="serviceName", columns=@Column(name="restRequestserviceName")),
        @Persistent(name="serviceUrl", columns=@Column(name="restRequestserviceUrl")),
        @Persistent(name="requestMethod", columns=@Column(name="restRequestrequestMethod")),
        @Persistent(name="requestUrl", columns=@Column(name="restRequestrequestUrl")),
        @Persistent(name="targetEntity", columns=@Column(name="restRequesttargetEntity")),
        @Persistent(name="queryString", columns=@Column(name="restRequestqueryString")),
        @Persistent(name="queryParams", columns=@Column(name="restRequestqueryParams")),
        @Persistent(name="inputFormat", columns=@Column(name="restRequestinputFormat")),
        @Persistent(name="inputContent", columns=@Column(name="restRequestinputContent")),
        @Persistent(name="outputFormat", columns=@Column(name="restRequestoutputFormat")),
        @Persistent(name="maxRetries", columns=@Column(name="restRequestmaxRetries")),
        @Persistent(name="retryInterval", columns=@Column(name="restRequestretryInterval")),
        @Persistent(name="note", columns=@Column(name="restRequestnote")),
    })
    private RestRequestStructDataObject restRequest;

    @Persistent(defaultFetchGroup = "true")
    private String permalink;

    @Persistent(defaultFetchGroup = "true")
    private String shortlink;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Integer jobStatus;

    @Persistent(defaultFetchGroup = "true")
    private String extra;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private Boolean alert;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="preferredMode", columns=@Column(name="notificationPrefpreferredMode")),
        @Persistent(name="mobileNumber", columns=@Column(name="notificationPrefmobileNumber")),
        @Persistent(name="emailAddress", columns=@Column(name="notificationPrefemailAddress")),
        @Persistent(name="callbackUrl", columns=@Column(name="notificationPrefcallbackUrl")),
        @Persistent(name="note", columns=@Column(name="notificationPrefnote")),
    })
    private NotificationStructDataObject notificationPref;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="referer", columns=@Column(name="referrerInforeferer")),
        @Persistent(name="userAgent", columns=@Column(name="referrerInfouserAgent")),
        @Persistent(name="language", columns=@Column(name="referrerInfolanguage")),
        @Persistent(name="hostname", columns=@Column(name="referrerInfohostname")),
        @Persistent(name="ipAddress", columns=@Column(name="referrerInfoipAddress")),
        @Persistent(name="note", columns=@Column(name="referrerInfonote")),
    })
    private ReferrerInfoStructDataObject referrerInfo;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="type", columns=@Column(name="cronScheduletype")),
        @Persistent(name="schedule", columns=@Column(name="cronScheduleschedule")),
        @Persistent(name="timezone", columns=@Column(name="cronScheduletimezone")),
        @Persistent(name="maxIterations", columns=@Column(name="cronSchedulemaxIterations")),
        @Persistent(name="repeatInterval", columns=@Column(name="cronSchedulerepeatInterval")),
        @Persistent(name="firtRunTime", columns=@Column(name="cronSchedulefirtRunTime")),
        @Persistent(name="startTime", columns=@Column(name="cronSchedulestartTime")),
        @Persistent(name="endTime", columns=@Column(name="cronScheduleendTime")),
        @Persistent(name="note", columns=@Column(name="cronSchedulenote")),
    })
    private CronScheduleStructDataObject cronSchedule;

    @Persistent(defaultFetchGroup = "true")
    private Long nextRunTime;

    public CronJobDataObject()
    {
        this(null);
    }
    public CronJobDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public CronJobDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, jobId, title, description, originJob, restRequest, permalink, shortlink, status, jobStatus, extra, note, alert, notificationPref, referrerInfo, cronSchedule, nextRunTime, null, null);
    }
    public CronJobDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, Integer jobId, String title, String description, String originJob, RestRequestStruct restRequest, String permalink, String shortlink, String status, Integer jobStatus, String extra, String note, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, CronScheduleStruct cronSchedule, Long nextRunTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);

        this.managerApp = managerApp;
        this.appAcl = appAcl;
        if(gaeApp != null) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = null;
        }
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.jobId = jobId;
        this.title = title;
        this.description = description;
        this.originJob = originJob;
        if(restRequest != null) {
            this.restRequest = new RestRequestStructDataObject(restRequest.getServiceName(), restRequest.getServiceUrl(), restRequest.getRequestMethod(), restRequest.getRequestUrl(), restRequest.getTargetEntity(), restRequest.getQueryString(), restRequest.getQueryParams(), restRequest.getInputFormat(), restRequest.getInputContent(), restRequest.getOutputFormat(), restRequest.getMaxRetries(), restRequest.getRetryInterval(), restRequest.getNote());
        } else {
            this.restRequest = null;
        }
        this.permalink = permalink;
        this.shortlink = shortlink;
        this.status = status;
        this.jobStatus = jobStatus;
        this.extra = extra;
        this.note = note;
        this.alert = alert;
        if(notificationPref != null) {
            this.notificationPref = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getCallbackUrl(), notificationPref.getNote());
        } else {
            this.notificationPref = null;
        }
        if(referrerInfo != null) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = null;
        }
        if(cronSchedule != null) {
            this.cronSchedule = new CronScheduleStructDataObject(cronSchedule.getType(), cronSchedule.getSchedule(), cronSchedule.getTimezone(), cronSchedule.getMaxIterations(), cronSchedule.getRepeatInterval(), cronSchedule.getFirtRunTime(), cronSchedule.getStartTime(), cronSchedule.getEndTime(), cronSchedule.getNote());
        } else {
            this.cronSchedule = null;
        }
        this.nextRunTime = nextRunTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return CronJobDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return CronJobDataObject.composeKey(getGuid());
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStruct getGaeApp()
    {
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(gaeApp == null) {
            this.gaeApp = null;
            log.log(Level.INFO, "CronJobDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is null.");            
        } else if(gaeApp instanceof GaeAppStructDataObject) {
            this.gaeApp = (GaeAppStructDataObject) gaeApp;
        } else if(gaeApp instanceof GaeAppStruct) {
            this.gaeApp = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            this.gaeApp = new GaeAppStructDataObject();   // ????
            log.log(Level.WARNING, "CronJobDataObject.setGaeApp(GaeAppStruct gaeApp): Arg gaeApp is of an invalid type.");
        }
    }

    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public Integer getJobId()
    {
        return this.jobId;
    }
    public void setJobId(Integer jobId)
    {
        this.jobId = jobId;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getOriginJob()
    {
        return this.originJob;
    }
    public void setOriginJob(String originJob)
    {
        this.originJob = originJob;
    }

    public RestRequestStruct getRestRequest()
    {
        return this.restRequest;
    }
    public void setRestRequest(RestRequestStruct restRequest)
    {
        if(restRequest == null) {
            this.restRequest = null;
            log.log(Level.INFO, "CronJobDataObject.setRestRequest(RestRequestStruct restRequest): Arg restRequest is null.");            
        } else if(restRequest instanceof RestRequestStructDataObject) {
            this.restRequest = (RestRequestStructDataObject) restRequest;
        } else if(restRequest instanceof RestRequestStruct) {
            this.restRequest = new RestRequestStructDataObject(restRequest.getServiceName(), restRequest.getServiceUrl(), restRequest.getRequestMethod(), restRequest.getRequestUrl(), restRequest.getTargetEntity(), restRequest.getQueryString(), restRequest.getQueryParams(), restRequest.getInputFormat(), restRequest.getInputContent(), restRequest.getOutputFormat(), restRequest.getMaxRetries(), restRequest.getRetryInterval(), restRequest.getNote());
        } else {
            this.restRequest = new RestRequestStructDataObject();   // ????
            log.log(Level.WARNING, "CronJobDataObject.setRestRequest(RestRequestStruct restRequest): Arg restRequest is of an invalid type.");
        }
    }

    public String getPermalink()
    {
        return this.permalink;
    }
    public void setPermalink(String permalink)
    {
        this.permalink = permalink;
    }

    public String getShortlink()
    {
        return this.shortlink;
    }
    public void setShortlink(String shortlink)
    {
        this.shortlink = shortlink;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Integer getJobStatus()
    {
        return this.jobStatus;
    }
    public void setJobStatus(Integer jobStatus)
    {
        this.jobStatus = jobStatus;
    }

    public String getExtra()
    {
        return this.extra;
    }
    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Boolean isAlert()
    {
        return this.alert;
    }
    public void setAlert(Boolean alert)
    {
        this.alert = alert;
    }

    public NotificationStruct getNotificationPref()
    {
        return this.notificationPref;
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if(notificationPref == null) {
            this.notificationPref = null;
            log.log(Level.INFO, "CronJobDataObject.setNotificationPref(NotificationStruct notificationPref): Arg notificationPref is null.");            
        } else if(notificationPref instanceof NotificationStructDataObject) {
            this.notificationPref = (NotificationStructDataObject) notificationPref;
        } else if(notificationPref instanceof NotificationStruct) {
            this.notificationPref = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getCallbackUrl(), notificationPref.getNote());
        } else {
            this.notificationPref = new NotificationStructDataObject();   // ????
            log.log(Level.WARNING, "CronJobDataObject.setNotificationPref(NotificationStruct notificationPref): Arg notificationPref is of an invalid type.");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(referrerInfo == null) {
            this.referrerInfo = null;
            log.log(Level.INFO, "CronJobDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is null.");            
        } else if(referrerInfo instanceof ReferrerInfoStructDataObject) {
            this.referrerInfo = (ReferrerInfoStructDataObject) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = new ReferrerInfoStructDataObject();   // ????
            log.log(Level.WARNING, "CronJobDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is of an invalid type.");
        }
    }

    public CronScheduleStruct getCronSchedule()
    {
        return this.cronSchedule;
    }
    public void setCronSchedule(CronScheduleStruct cronSchedule)
    {
        if(cronSchedule == null) {
            this.cronSchedule = null;
            log.log(Level.INFO, "CronJobDataObject.setCronSchedule(CronScheduleStruct cronSchedule): Arg cronSchedule is null.");            
        } else if(cronSchedule instanceof CronScheduleStructDataObject) {
            this.cronSchedule = (CronScheduleStructDataObject) cronSchedule;
        } else if(cronSchedule instanceof CronScheduleStruct) {
            this.cronSchedule = new CronScheduleStructDataObject(cronSchedule.getType(), cronSchedule.getSchedule(), cronSchedule.getTimezone(), cronSchedule.getMaxIterations(), cronSchedule.getRepeatInterval(), cronSchedule.getFirtRunTime(), cronSchedule.getStartTime(), cronSchedule.getEndTime(), cronSchedule.getNote());
        } else {
            this.cronSchedule = new CronScheduleStructDataObject();   // ????
            log.log(Level.WARNING, "CronJobDataObject.setCronSchedule(CronScheduleStruct cronSchedule): Arg cronSchedule is of an invalid type.");
        }
    }

    public Long getNextRunTime()
    {
        return this.nextRunTime;
    }
    public void setNextRunTime(Long nextRunTime)
    {
        this.nextRunTime = nextRunTime;
    }

    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("managerApp", this.managerApp);
        dataMap.put("appAcl", this.appAcl);
        dataMap.put("gaeApp", this.gaeApp);
        dataMap.put("ownerUser", this.ownerUser);
        dataMap.put("userAcl", this.userAcl);
        dataMap.put("user", this.user);
        dataMap.put("jobId", this.jobId);
        dataMap.put("title", this.title);
        dataMap.put("description", this.description);
        dataMap.put("originJob", this.originJob);
        dataMap.put("restRequest", this.restRequest);
        dataMap.put("permalink", this.permalink);
        dataMap.put("shortlink", this.shortlink);
        dataMap.put("status", this.status);
        dataMap.put("jobStatus", this.jobStatus);
        dataMap.put("extra", this.extra);
        dataMap.put("note", this.note);
        dataMap.put("alert", this.alert);
        dataMap.put("notificationPref", this.notificationPref);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("cronSchedule", this.cronSchedule);
        dataMap.put("nextRunTime", this.nextRunTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        CronJob thatObj = (CronJob) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.managerApp == null && thatObj.getManagerApp() != null)
            || (this.managerApp != null && thatObj.getManagerApp() == null)
            || !this.managerApp.equals(thatObj.getManagerApp()) ) {
            return false;
        }
        if( (this.appAcl == null && thatObj.getAppAcl() != null)
            || (this.appAcl != null && thatObj.getAppAcl() == null)
            || !this.appAcl.equals(thatObj.getAppAcl()) ) {
            return false;
        }
        if( (this.gaeApp == null && thatObj.getGaeApp() != null)
            || (this.gaeApp != null && thatObj.getGaeApp() == null)
            || !this.gaeApp.equals(thatObj.getGaeApp()) ) {
            return false;
        }
        if( (this.ownerUser == null && thatObj.getOwnerUser() != null)
            || (this.ownerUser != null && thatObj.getOwnerUser() == null)
            || !this.ownerUser.equals(thatObj.getOwnerUser()) ) {
            return false;
        }
        if( (this.userAcl == null && thatObj.getUserAcl() != null)
            || (this.userAcl != null && thatObj.getUserAcl() == null)
            || !this.userAcl.equals(thatObj.getUserAcl()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.jobId == null && thatObj.getJobId() != null)
            || (this.jobId != null && thatObj.getJobId() == null)
            || !this.jobId.equals(thatObj.getJobId()) ) {
            return false;
        }
        if( (this.title == null && thatObj.getTitle() != null)
            || (this.title != null && thatObj.getTitle() == null)
            || !this.title.equals(thatObj.getTitle()) ) {
            return false;
        }
        if( (this.description == null && thatObj.getDescription() != null)
            || (this.description != null && thatObj.getDescription() == null)
            || !this.description.equals(thatObj.getDescription()) ) {
            return false;
        }
        if( (this.originJob == null && thatObj.getOriginJob() != null)
            || (this.originJob != null && thatObj.getOriginJob() == null)
            || !this.originJob.equals(thatObj.getOriginJob()) ) {
            return false;
        }
        if( (this.restRequest == null && thatObj.getRestRequest() != null)
            || (this.restRequest != null && thatObj.getRestRequest() == null)
            || !this.restRequest.equals(thatObj.getRestRequest()) ) {
            return false;
        }
        if( (this.permalink == null && thatObj.getPermalink() != null)
            || (this.permalink != null && thatObj.getPermalink() == null)
            || !this.permalink.equals(thatObj.getPermalink()) ) {
            return false;
        }
        if( (this.shortlink == null && thatObj.getShortlink() != null)
            || (this.shortlink != null && thatObj.getShortlink() == null)
            || !this.shortlink.equals(thatObj.getShortlink()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.jobStatus == null && thatObj.getJobStatus() != null)
            || (this.jobStatus != null && thatObj.getJobStatus() == null)
            || !this.jobStatus.equals(thatObj.getJobStatus()) ) {
            return false;
        }
        if( (this.extra == null && thatObj.getExtra() != null)
            || (this.extra != null && thatObj.getExtra() == null)
            || !this.extra.equals(thatObj.getExtra()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.alert == null && thatObj.isAlert() != null)
            || (this.alert != null && thatObj.isAlert() == null)
            || !this.alert.equals(thatObj.isAlert()) ) {
            return false;
        }
        if( (this.notificationPref == null && thatObj.getNotificationPref() != null)
            || (this.notificationPref != null && thatObj.getNotificationPref() == null)
            || !this.notificationPref.equals(thatObj.getNotificationPref()) ) {
            return false;
        }
        if( (this.referrerInfo == null && thatObj.getReferrerInfo() != null)
            || (this.referrerInfo != null && thatObj.getReferrerInfo() == null)
            || !this.referrerInfo.equals(thatObj.getReferrerInfo()) ) {
            return false;
        }
        if( (this.cronSchedule == null && thatObj.getCronSchedule() != null)
            || (this.cronSchedule != null && thatObj.getCronSchedule() == null)
            || !this.cronSchedule.equals(thatObj.getCronSchedule()) ) {
            return false;
        }
        if( (this.nextRunTime == null && thatObj.getNextRunTime() != null)
            || (this.nextRunTime != null && thatObj.getNextRunTime() == null)
            || !this.nextRunTime.equals(thatObj.getNextRunTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = managerApp == null ? 0 : managerApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = appAcl == null ? 0 : appAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = gaeApp == null ? 0 : gaeApp.hashCode();
        _hash = 31 * _hash + delta;
        delta = ownerUser == null ? 0 : ownerUser.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAcl == null ? 0 : userAcl.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = jobId == null ? 0 : jobId.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = originJob == null ? 0 : originJob.hashCode();
        _hash = 31 * _hash + delta;
        delta = restRequest == null ? 0 : restRequest.hashCode();
        _hash = 31 * _hash + delta;
        delta = permalink == null ? 0 : permalink.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortlink == null ? 0 : shortlink.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = jobStatus == null ? 0 : jobStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = extra == null ? 0 : extra.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = alert == null ? 0 : alert.hashCode();
        _hash = 31 * _hash + delta;
        delta = notificationPref == null ? 0 : notificationPref.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = cronSchedule == null ? 0 : cronSchedule.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextRunTime == null ? 0 : nextRunTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
