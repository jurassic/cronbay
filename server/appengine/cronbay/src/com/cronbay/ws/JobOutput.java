package com.cronbay.ws;

import java.util.List;

public interface JobOutput 
{
    String  getGuid();
    String  getManagerApp();
    Long  getAppAcl();
    GaeAppStruct  getGaeApp();
    String  getOwnerUser();
    Long  getUserAcl();
    String  getUser();
    String  getCronJob();
    String  getPreviousRun();
    String  getPreviousTry();
    String  getResponseCode();
    String  getOutputFormat();
    String  getOutputText();
    List<String>  getOutputData();
    String  getOutputHash();
    String  getPermalink();
    String  getShortlink();
    String  getResult();
    String  getStatus();
    String  getExtra();
    String  getNote();
    Integer  getRetryCounter();
    Long  getScheduledTime();
    Long  getProcessedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
