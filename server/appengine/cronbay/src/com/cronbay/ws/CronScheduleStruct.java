package com.cronbay.ws;


public interface CronScheduleStruct 
{
    String  getType();
    String  getSchedule();
    String  getTimezone();
    Integer  getMaxIterations();
    Integer  getRepeatInterval();
    Long  getFirtRunTime();
    Long  getStartTime();
    Long  getEndTime();
    String  getNote();
}
