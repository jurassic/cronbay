package com.cronbay.ws.auth;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.sun.jersey.api.core.HttpRequestContext;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.oauth.server.OAuthServerRequest;
import com.sun.jersey.oauth.signature.OAuthParameters;
import com.sun.jersey.oauth.signature.OAuthSecrets;
import com.sun.jersey.oauth.signature.OAuthSignature;
import com.sun.jersey.oauth.signature.OAuthSignatureException;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.BadRequestException;
import com.cronbay.ws.exception.UnauthorizedException;
import com.cronbay.ws.cert.registry.ConsumerRegistry;


public class TwoLeggedOAuthProvider
{
    private static final Logger log = Logger.getLogger(TwoLeggedOAuthProvider.class.getName());
    
    private TwoLeggedOAuthProvider()
    {
        // ...
    }

    private static class TwoLeggedOAuthProviderHolder {
        private static TwoLeggedOAuthProvider INSTANCE = new TwoLeggedOAuthProvider();
    };

    public static TwoLeggedOAuthProvider getInstance()
    {
        return TwoLeggedOAuthProviderHolder.INSTANCE;
    }
    
    public boolean verify(HttpRequestContext containerRequest) throws BaseException
    {
        OAuthServerRequest request = new OAuthServerRequest(containerRequest);

        OAuthParameters params = new OAuthParameters();
        params.readRequest(request);
       
        // Check that the timestamp has not expired
        String timestampStr = params.getTimestamp();
        // TBD: timestamp checking code ...
        String consumerKey = params.getConsumerKey();
        // TBD: get consumerSecret for the given consumerKey...
        String consumerSecret = ConsumerRegistry.getInstance().getConsumerSecret(consumerKey);

        // Set the secret(s), against which we will verify the request
        OAuthSecrets secrets = new OAuthSecrets().consumerSecret(consumerSecret);

        // Verify the signature
        try {
            if(!OAuthSignature.verify(request, params, secrets)) {
                log.warning("Two-legged OAuth verification failed.");
                //throw new UnauthorizedException("Two-legged OAuth verification failed.");
                return false;
            }
        } catch (OAuthSignatureException e) {
            log.log(Level.WARNING, "OAuth signature error.", e);
            throw new BadRequestException("OAuth signature error.", e);  // ???
        } catch (Exception e) {
            log.log(Level.WARNING, "Two-legged OAuth verification failed due to unknown error.", e);
            throw new BaseException("Two-legged OAuth verification failed due to unknown error.", e);  // ???
        }

        return true;
    }
    
}
