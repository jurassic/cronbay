package com.cronbay.ws.auth;

import java.net.URI;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.cronbay.ws.resource.exception.BadRequestRsException;
import com.cronbay.ws.resource.exception.UnauthorizedRsException;
import com.sun.jersey.oauth.signature.OAuthSignatureException;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

public class TwoLeggedOAuthFilter implements ContainerRequestFilter
{
    private static final Logger log = Logger.getLogger(TwoLeggedOAuthFilter.class.getName());

    @Override
    public ContainerRequest filter(ContainerRequest containerRequest)
    {
        // Just for logging....
        String requestUri = null;
        URI uri = containerRequest.getRequestUri();
        if(uri != null) {
            requestUri = uri.toString();
        }

        // Verify the signature
        try {
            if(!TwoLeggedOAuthProvider.getInstance().verify(containerRequest)) {
                log.warning("Two-legged OAuth verification failed.");
                throw new UnauthorizedRsException("Two-legged OAuth verification failed.");
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error during OAuth verification.", e);
            throw new BadRequestRsException("Unknown error during OAuth verification.", e, requestUri);  // ???
        }

        // Return the request
        return containerRequest;
    }

}
