package com.cronbay.ws.auth;

import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.ws.rs.WebApplicationException;
import com.cronbay.ws.resource.exception.UnauthorizedRsException;
import com.sun.jersey.oauth.server.OAuthServerRequest;
import com.sun.jersey.oauth.signature.OAuthParameters;
import com.sun.jersey.oauth.signature.OAuthSecrets;
import com.sun.jersey.oauth.signature.OAuthSignature;
import com.sun.jersey.oauth.signature.OAuthSignatureException;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

// To be deleted .....
public class AuthSignatureFilter implements ContainerRequestFilter
{
    private static final Logger log = Logger.getLogger(AuthSignatureFilter.class.getName());

    @Override
    public ContainerRequest filter(ContainerRequest containerRequest)
    {
        log.log(Level.INFO, "SignatureServerFilter.fiter() method called with containerRequest = " + containerRequest);

        // Read the OAuth parameters from the request
        OAuthServerRequest request = new OAuthServerRequest(containerRequest);
        OAuthParameters params = new OAuthParameters();
        params.readRequest(request);
       
        // TBD: Set the secret(s), against which we will verify the request
        OAuthSecrets secrets = new OAuthSecrets();
        //OAuthSecrets secrets = new OAuthSecrets().consumerSecret(consumerSecret).tokenSecret(tokenSecret);
        //secrets.setConsumerSecret(consumerSecret);
        //secrets.setTokenSecret(tokenSecret);
        
        // Check that the timestamp has not expired
        String timestampStr = params.getTimestamp();
        // TBD: timestamp checking code ...

        // Verify the signature
        try {
            if(!OAuthSignature.verify(request, params, secrets)) {
                //throw new WebApplicationException(401);
                throw new UnauthorizedRsException();  // ???
            }
        } catch (OAuthSignatureException e) {
            //throw new WebApplicationException(e, 401);
            throw new UnauthorizedRsException(e);  // ???
        }
       
        // Return the request
        return containerRequest;
    }
    
}
