package com.cronbay.ws;

import java.util.List;

public interface RestRequestStruct 
{
    String  getServiceName();
    String  getServiceUrl();
    String  getRequestMethod();
    String  getRequestUrl();
    String  getTargetEntity();
    String  getQueryString();
    List<String>  getQueryParams();
    String  getInputFormat();
    String  getInputContent();
    String  getOutputFormat();
    Integer  getMaxRetries();
    Integer  getRetryInterval();
    String  getNote();
}
