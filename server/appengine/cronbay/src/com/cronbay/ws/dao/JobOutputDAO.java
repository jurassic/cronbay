package com.cronbay.ws.dao;

import java.util.List;
import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.data.JobOutputDataObject;

// TBD: Add offset/count to getAllJobOutputs() and findJobOutputs(), etc.
public interface JobOutputDAO
{
    JobOutputDataObject getJobOutput(String guid) throws BaseException;
    List<JobOutputDataObject> getJobOutputs(List<String> guids) throws BaseException;
    List<JobOutputDataObject> getAllJobOutputs() throws BaseException;
    List<JobOutputDataObject> getAllJobOutputs(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllJobOutputKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<JobOutputDataObject> findJobOutputs(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<JobOutputDataObject> findJobOutputs(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<JobOutputDataObject> findJobOutputs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findJobOutputKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createJobOutput(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return JobOutputDataObject?)
    String createJobOutput(JobOutputDataObject jobOutput) throws BaseException;          // Returns Guid.  (Return JobOutputDataObject?)
    //Boolean updateJobOutput(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateJobOutput(JobOutputDataObject jobOutput) throws BaseException;
    Boolean deleteJobOutput(String guid) throws BaseException;
    Boolean deleteJobOutput(JobOutputDataObject jobOutput) throws BaseException;
    Long deleteJobOutputs(String filter, String params, List<String> values) throws BaseException;
}
