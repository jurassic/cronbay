package com.cronbay.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;

//import com.google.appengine.api.datastore.Key;

import com.cronbay.ws.core.GUID;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.DataStoreException;
import com.cronbay.ws.exception.ServiceUnavailableException;
import com.cronbay.ws.exception.RequestConflictException;
import com.cronbay.ws.exception.ResourceNotFoundException;
import com.cronbay.ws.exception.NotImplementedException;
import com.cronbay.ws.dao.JobOutputDAO;
import com.cronbay.ws.data.JobOutputDataObject;


public class DefaultJobOutputDAO extends DefaultDAOBase implements JobOutputDAO
{
    private static final Logger log = Logger.getLogger(DefaultJobOutputDAO.class.getName()); 

    // Returns the jobOutput for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public JobOutputDataObject getJobOutput(String guid) throws BaseException
    {
        log.finer("BEGIN: guid = " + guid);

        JobOutputDataObject jobOutput = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                //Key key = JobOutputDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = JobOutputDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                jobOutput = pm.getObjectById(JobOutputDataObject.class, key);
                jobOutput.getOutputText();  // "Touch". Otherwise this field will not be fetched by default.
                //tx.commit();
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to retrieve jobOutput for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return jobOutput;
	}

    @Override
    public List<JobOutputDataObject> getJobOutputs(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<JobOutputDataObject> jobOutputs = null;
        if(guids != null && !guids.isEmpty()) {
            jobOutputs = new ArrayList<JobOutputDataObject>();
            for(String guid : guids) {
                JobOutputDataObject obj = getJobOutput(guid); 
                jobOutputs.add(obj);
            }
	    }

        log.finer("END");
        return jobOutputs;
    }

    @Override
    public List<JobOutputDataObject> getAllJobOutputs() throws BaseException
	{
	    return getAllJobOutputs(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<JobOutputDataObject> getAllJobOutputs(String ordering, Long offset, Integer count) throws BaseException
	{
        log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<JobOutputDataObject> jobOutputs = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(JobOutputDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            jobOutputs = (List<JobOutputDataObject>) q.execute();
            if(jobOutputs != null) {
                jobOutputs.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<JobOutputDataObject> rs_jobOutputs = (Collection<JobOutputDataObject>) q.execute();
            if(rs_jobOutputs == null) {
                log.log(Level.WARNING, "Failed to retrieve all jobOutputs.");
                jobOutputs = new ArrayList<JobOutputDataObject>();  // ???           
            } else {
                jobOutputs = new ArrayList<JobOutputDataObject>(pm.detachCopyAll(rs_jobOutputs));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all jobOutputs.", ex);
            //jobOutputs = new ArrayList<JobOutputDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all jobOutputs.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return jobOutputs;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllJobOutputKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery("select key from " + JobOutputDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all JobOutput keys.", ex);
            throw new DataStoreException("Failed to retrieve all JobOutput keys.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<JobOutputDataObject> findJobOutputs(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findJobOutputs(filter, ordering, params, values, null, null);
    }

    @Override
	public List<JobOutputDataObject> findJobOutputs(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findJobOutputs(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<JobOutputDataObject> findJobOutputs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        log.info("DefaultJobOutputDAO.findJobOutputs(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findJobOutputs() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<JobOutputDataObject> jobOutputs = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(JobOutputDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                jobOutputs = (List<JobOutputDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                jobOutputs = (List<JobOutputDataObject>) q.execute();
            }
            if(jobOutputs != null) {
                jobOutputs.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(JobOutputDataObject dobj : jobOutputs) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find jobOutputs because index is missing.", ex);
            //jobOutputs = new ArrayList<JobOutputDataObject>();  // ???
            throw new DataStoreException("Failed to find jobOutputs because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find jobOutputs meeting the criterion.", ex);
            //jobOutputs = new ArrayList<JobOutputDataObject>();  // ???
            throw new DataStoreException("Failed to find jobOutputs meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return jobOutputs;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findJobOutputKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.info("DefaultJobOutputDAO.findJobOutputKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery("select key from " + JobOutputDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find JobOutput keys because index is missing.", ex);
            throw new DataStoreException("Failed to find JobOutput keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find JobOutput keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find JobOutput keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.info("DefaultJobOutputDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(JobOutputDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = new Long((Integer) q.executeWithArray(values.toArray(new Object[0])));
            } else {
                count = new Long((Integer) q.execute());
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get jobOutput count because index is missing.", ex);
            throw new DataStoreException("Failed to get jobOutput count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get jobOutput count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get jobOutput count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

	// Stores the jobOutput in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeJobOutput(JobOutputDataObject jobOutput) throws BaseException
    {
        log.fine("storeJobOutput() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
	    try {
            //tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = jobOutput.getCreatedTime();
            if(createdTime == null) {
                createdTime = (new Date()).getTime();
                jobOutput.setCreatedTime(createdTime);
            }
            Long modifiedTime = jobOutput.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                jobOutput.setModifiedTime(createdTime);
            }
            pm.makePersistent(jobOutput); 
            //tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = jobOutput.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store jobOutput because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store jobOutput because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store jobOutput.", ex);
            throw new DataStoreException("Failed to store jobOutput.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.fine("storeJobOutput(): guid = " + guid);
        return guid;
    }

    @Override
    public String createJobOutput(JobOutputDataObject jobOutput) throws BaseException
    {
        // The createdTime field will be automatically set in storeJobOutput().
        //Long createdTime = jobOutput.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = (new Date()).getTime();
        //    jobOutput.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = jobOutput.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    jobOutput.setModifiedTime(createdTime);
        //}
        return storeJobOutput(jobOutput);
    }

    @Override
	public Boolean updateJobOutput(JobOutputDataObject jobOutput) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeJobOutput()
	    // (in which case modifiedTime might be updated again).
	    jobOutput.setModifiedTime((new Date()).getTime());
	    String guid = storeJobOutput(jobOutput);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteJobOutput(JobOutputDataObject jobOutput) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            pm.deletePersistent(jobOutput);
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete jobOutput because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete jobOutput because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete jobOutput.", ex);
            throw new DataStoreException("Failed to delete jobOutput.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteJobOutput(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                //Key key = JobOutputDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = JobOutputDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                JobOutputDataObject jobOutput = pm.getObjectById(JobOutputDataObject.class, key);
                pm.deletePersistent(jobOutput);
                //tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                log.log(Level.WARNING, "Failed to delete jobOutput because the datastore is currently read-only.", ex);
                throw new ServiceUnavailableException("Failed to delete jobOutput because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to delete jobOutput for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete jobOutput for guid = " + guid, ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteJobOutputs(String filter, String params, List<String> values) throws BaseException
    {
        log.info("DefaultJobOutputDAO.deleteJobOutputs(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(JobOutputDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletejobOutputs because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete jobOutputs because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete jobOutputs because index is missing", ex);
            throw new DataStoreException("Failed to delete jobOutputs because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete jobOutputs", ex);
            throw new DataStoreException("Failed to delete jobOutputs", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

}
