package com.cronbay.ws.dao;

import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.data.ApiConsumerDataObject;

// TBD: Add offset/count to getAllApiConsumers() and findApiConsumers(), etc.
public interface ApiConsumerDAO
{
    ApiConsumerDataObject getApiConsumer(String guid) throws BaseException;
    List<ApiConsumerDataObject> getApiConsumers(List<String> guids) throws BaseException;
    List<ApiConsumerDataObject> getAllApiConsumers() throws BaseException;
    List<ApiConsumerDataObject> getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ApiConsumerDataObject> findApiConsumers(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ApiConsumerDataObject> findApiConsumers(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<ApiConsumerDataObject> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createApiConsumer(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ApiConsumerDataObject?)
    String createApiConsumer(ApiConsumerDataObject apiConsumer) throws BaseException;          // Returns Guid.  (Return ApiConsumerDataObject?)
    //Boolean updateApiConsumer(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateApiConsumer(ApiConsumerDataObject apiConsumer) throws BaseException;
    Boolean deleteApiConsumer(String guid) throws BaseException;
    Boolean deleteApiConsumer(ApiConsumerDataObject apiConsumer) throws BaseException;
    Long deleteApiConsumers(String filter, String params, List<String> values) throws BaseException;
}
