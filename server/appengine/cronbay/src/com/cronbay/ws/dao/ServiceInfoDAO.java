package com.cronbay.ws.dao;

import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.data.ServiceInfoDataObject;

// TBD: Add offset/count to getAllServiceInfos() and findServiceInfos(), etc.
public interface ServiceInfoDAO
{
    ServiceInfoDataObject getServiceInfo(String guid) throws BaseException;
    List<ServiceInfoDataObject> getServiceInfos(List<String> guids) throws BaseException;
    List<ServiceInfoDataObject> getAllServiceInfos() throws BaseException;
    List<ServiceInfoDataObject> getAllServiceInfos(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<ServiceInfoDataObject> findServiceInfos(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<ServiceInfoDataObject> findServiceInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<ServiceInfoDataObject> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createServiceInfo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ServiceInfoDataObject?)
    String createServiceInfo(ServiceInfoDataObject serviceInfo) throws BaseException;          // Returns Guid.  (Return ServiceInfoDataObject?)
    //Boolean updateServiceInfo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateServiceInfo(ServiceInfoDataObject serviceInfo) throws BaseException;
    Boolean deleteServiceInfo(String guid) throws BaseException;
    Boolean deleteServiceInfo(ServiceInfoDataObject serviceInfo) throws BaseException;
    Long deleteServiceInfos(String filter, String params, List<String> values) throws BaseException;
}
