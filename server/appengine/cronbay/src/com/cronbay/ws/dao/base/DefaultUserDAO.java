package com.cronbay.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.cronbay.ws.core.GUID;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.DataStoreException;
import com.cronbay.ws.exception.ServiceUnavailableException;
import com.cronbay.ws.exception.RequestConflictException;
import com.cronbay.ws.exception.ResourceNotFoundException;
import com.cronbay.ws.exception.NotImplementedException;
import com.cronbay.ws.dao.UserDAO;
import com.cronbay.ws.data.UserDataObject;


public class DefaultUserDAO extends DefaultDAOBase implements UserDAO
{
    private static final Logger log = Logger.getLogger(DefaultUserDAO.class.getName()); 

    // Returns the user for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public UserDataObject getUser(String guid) throws BaseException
    {
        log.finer("BEGIN: guid = " + guid);

        UserDataObject user = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                //Key key = UserDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = UserDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                user = pm.getObjectById(UserDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to retrieve user for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return user;
	}

    @Override
    public List<UserDataObject> getUsers(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<UserDataObject> users = null;
        if(guids != null && !guids.isEmpty()) {
            users = new ArrayList<UserDataObject>();
            for(String guid : guids) {
                UserDataObject obj = getUser(guid); 
                users.add(obj);
            }
	    }

        log.finer("END");
        return users;
    }

    @Override
    public List<UserDataObject> getAllUsers() throws BaseException
	{
	    return getAllUsers(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<UserDataObject> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
	{
        log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<UserDataObject> users = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(UserDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            users = (List<UserDataObject>) q.execute();
            if(users != null) {
                users.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<UserDataObject> rs_users = (Collection<UserDataObject>) q.execute();
            if(rs_users == null) {
                log.log(Level.WARNING, "Failed to retrieve all users.");
                users = new ArrayList<UserDataObject>();  // ???           
            } else {
                users = new ArrayList<UserDataObject>(pm.detachCopyAll(rs_users));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all users.", ex);
            //users = new ArrayList<UserDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all users.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return users;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery("select key from " + UserDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all User keys.", ex);
            throw new DataStoreException("Failed to retrieve all User keys.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findUsers(filter, ordering, params, values, null, null);
    }

    @Override
	public List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findUsers(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<UserDataObject> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        log.info("DefaultUserDAO.findUsers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findUsers() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<UserDataObject> users = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(UserDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                users = (List<UserDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                users = (List<UserDataObject>) q.execute();
            }
            if(users != null) {
                users.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(UserDataObject dobj : users) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find users because index is missing.", ex);
            //users = new ArrayList<UserDataObject>();  // ???
            throw new DataStoreException("Failed to find users because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find users meeting the criterion.", ex);
            //users = new ArrayList<UserDataObject>();  // ???
            throw new DataStoreException("Failed to find users meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return users;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.info("DefaultUserDAO.findUserKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery("select key from " + UserDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find User keys because index is missing.", ex);
            throw new DataStoreException("Failed to find User keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find User keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find User keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.info("DefaultUserDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(UserDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = new Long((Integer) q.executeWithArray(values.toArray(new Object[0])));
            } else {
                count = new Long((Integer) q.execute());
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get user count because index is missing.", ex);
            throw new DataStoreException("Failed to get user count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get user count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get user count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

	// Stores the user in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeUser(UserDataObject user) throws BaseException
    {
        log.fine("storeUser() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
	    try {
            //tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = user.getCreatedTime();
            if(createdTime == null) {
                createdTime = (new Date()).getTime();
                user.setCreatedTime(createdTime);
            }
            Long modifiedTime = user.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                user.setModifiedTime(createdTime);
            }
            pm.makePersistent(user); 
            //tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = user.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store user because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store user because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store user.", ex);
            throw new DataStoreException("Failed to store user.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.fine("storeUser(): guid = " + guid);
        return guid;
    }

    @Override
    public String createUser(UserDataObject user) throws BaseException
    {
        // The createdTime field will be automatically set in storeUser().
        //Long createdTime = user.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = (new Date()).getTime();
        //    user.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = user.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    user.setModifiedTime(createdTime);
        //}
        return storeUser(user);
    }

    @Override
	public Boolean updateUser(UserDataObject user) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeUser()
	    // (in which case modifiedTime might be updated again).
	    user.setModifiedTime((new Date()).getTime());
	    String guid = storeUser(user);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteUser(UserDataObject user) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            pm.deletePersistent(user);
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete user because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete user because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete user.", ex);
            throw new DataStoreException("Failed to delete user.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                //Key key = UserDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = UserDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                UserDataObject user = pm.getObjectById(UserDataObject.class, key);
                pm.deletePersistent(user);
                //tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                log.log(Level.WARNING, "Failed to delete user because the datastore is currently read-only.", ex);
                throw new ServiceUnavailableException("Failed to delete user because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to delete user for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete user for guid = " + guid, ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        log.info("DefaultUserDAO.deleteUsers(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(UserDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteusers because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete users because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete users because index is missing", ex);
            throw new DataStoreException("Failed to delete users because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete users", ex);
            throw new DataStoreException("Failed to delete users", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

}
