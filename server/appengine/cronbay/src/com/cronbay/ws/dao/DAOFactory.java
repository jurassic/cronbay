package com.cronbay.ws.dao;

// Abstract factory.
public abstract class DAOFactory
{
    public abstract ApiConsumerDAO getApiConsumerDAO();
    public abstract UserDAO getUserDAO();
    public abstract CronJobDAO getCronJobDAO();
    public abstract JobOutputDAO getJobOutputDAO();
    public abstract ServiceInfoDAO getServiceInfoDAO();
    public abstract FiveTenDAO getFiveTenDAO();
}
