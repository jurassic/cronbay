package com.cronbay.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.cronbay.ws.core.GUID;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.DataStoreException;
import com.cronbay.ws.exception.ServiceUnavailableException;
import com.cronbay.ws.exception.RequestConflictException;
import com.cronbay.ws.exception.ResourceNotFoundException;
import com.cronbay.ws.exception.NotImplementedException;
import com.cronbay.ws.dao.FiveTenDAO;
import com.cronbay.ws.data.FiveTenDataObject;


public class DefaultFiveTenDAO extends DefaultDAOBase implements FiveTenDAO
{
    private static final Logger log = Logger.getLogger(DefaultFiveTenDAO.class.getName()); 

    // Returns the fiveTen for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public FiveTenDataObject getFiveTen(String guid) throws BaseException
    {
        log.finer("BEGIN: guid = " + guid);

        FiveTenDataObject fiveTen = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                //Key key = FiveTenDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = FiveTenDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                fiveTen = pm.getObjectById(FiveTenDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to retrieve fiveTen for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return fiveTen;
	}

    @Override
    public List<FiveTenDataObject> getFiveTens(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<FiveTenDataObject> fiveTens = null;
        if(guids != null && !guids.isEmpty()) {
            fiveTens = new ArrayList<FiveTenDataObject>();
            for(String guid : guids) {
                FiveTenDataObject obj = getFiveTen(guid); 
                fiveTens.add(obj);
            }
	    }

        log.finer("END");
        return fiveTens;
    }

    @Override
    public List<FiveTenDataObject> getAllFiveTens() throws BaseException
	{
	    return getAllFiveTens(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<FiveTenDataObject> getAllFiveTens(String ordering, Long offset, Integer count) throws BaseException
	{
        log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<FiveTenDataObject> fiveTens = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(FiveTenDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            fiveTens = (List<FiveTenDataObject>) q.execute();
            if(fiveTens != null) {
                fiveTens.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<FiveTenDataObject> rs_fiveTens = (Collection<FiveTenDataObject>) q.execute();
            if(rs_fiveTens == null) {
                log.log(Level.WARNING, "Failed to retrieve all fiveTens.");
                fiveTens = new ArrayList<FiveTenDataObject>();  // ???           
            } else {
                fiveTens = new ArrayList<FiveTenDataObject>(pm.detachCopyAll(rs_fiveTens));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all fiveTens.", ex);
            //fiveTens = new ArrayList<FiveTenDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all fiveTens.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return fiveTens;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllFiveTenKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery("select key from " + FiveTenDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all FiveTen keys.", ex);
            throw new DataStoreException("Failed to retrieve all FiveTen keys.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findFiveTens(filter, ordering, params, values, null, null);
    }

    @Override
	public List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findFiveTens(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<FiveTenDataObject> findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        log.info("DefaultFiveTenDAO.findFiveTens(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findFiveTens() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<FiveTenDataObject> fiveTens = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(FiveTenDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                fiveTens = (List<FiveTenDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                fiveTens = (List<FiveTenDataObject>) q.execute();
            }
            if(fiveTens != null) {
                fiveTens.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(FiveTenDataObject dobj : fiveTens) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find fiveTens because index is missing.", ex);
            //fiveTens = new ArrayList<FiveTenDataObject>();  // ???
            throw new DataStoreException("Failed to find fiveTens because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find fiveTens meeting the criterion.", ex);
            //fiveTens = new ArrayList<FiveTenDataObject>();  // ???
            throw new DataStoreException("Failed to find fiveTens meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return fiveTens;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.info("DefaultFiveTenDAO.findFiveTenKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery("select key from " + FiveTenDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find FiveTen keys because index is missing.", ex);
            throw new DataStoreException("Failed to find FiveTen keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find FiveTen keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find FiveTen keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.info("DefaultFiveTenDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(FiveTenDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = new Long((Integer) q.executeWithArray(values.toArray(new Object[0])));
            } else {
                count = new Long((Integer) q.execute());
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get fiveTen count because index is missing.", ex);
            throw new DataStoreException("Failed to get fiveTen count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get fiveTen count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get fiveTen count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

	// Stores the fiveTen in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeFiveTen(FiveTenDataObject fiveTen) throws BaseException
    {
        log.fine("storeFiveTen() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
	    try {
            //tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = fiveTen.getCreatedTime();
            if(createdTime == null) {
                createdTime = (new Date()).getTime();
                fiveTen.setCreatedTime(createdTime);
            }
            Long modifiedTime = fiveTen.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                fiveTen.setModifiedTime(createdTime);
            }
            pm.makePersistent(fiveTen); 
            //tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = fiveTen.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store fiveTen because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store fiveTen because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store fiveTen.", ex);
            throw new DataStoreException("Failed to store fiveTen.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.fine("storeFiveTen(): guid = " + guid);
        return guid;
    }

    @Override
    public String createFiveTen(FiveTenDataObject fiveTen) throws BaseException
    {
        // The createdTime field will be automatically set in storeFiveTen().
        //Long createdTime = fiveTen.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = (new Date()).getTime();
        //    fiveTen.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = fiveTen.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    fiveTen.setModifiedTime(createdTime);
        //}
        return storeFiveTen(fiveTen);
    }

    @Override
	public Boolean updateFiveTen(FiveTenDataObject fiveTen) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeFiveTen()
	    // (in which case modifiedTime might be updated again).
	    fiveTen.setModifiedTime((new Date()).getTime());
	    String guid = storeFiveTen(fiveTen);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteFiveTen(FiveTenDataObject fiveTen) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            pm.deletePersistent(fiveTen);
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete fiveTen because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete fiveTen because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete fiveTen.", ex);
            throw new DataStoreException("Failed to delete fiveTen.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteFiveTen(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                //Key key = FiveTenDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = FiveTenDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                FiveTenDataObject fiveTen = pm.getObjectById(FiveTenDataObject.class, key);
                pm.deletePersistent(fiveTen);
                //tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                log.log(Level.WARNING, "Failed to delete fiveTen because the datastore is currently read-only.", ex);
                throw new ServiceUnavailableException("Failed to delete fiveTen because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to delete fiveTen for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete fiveTen for guid = " + guid, ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteFiveTens(String filter, String params, List<String> values) throws BaseException
    {
        log.info("DefaultFiveTenDAO.deleteFiveTens(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(FiveTenDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletefiveTens because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete fiveTens because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete fiveTens because index is missing", ex);
            throw new DataStoreException("Failed to delete fiveTens because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete fiveTens", ex);
            throw new DataStoreException("Failed to delete fiveTens", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

}
