package com.cronbay.ws.dao;

import java.util.List;

import com.cronbay.ws.BaseException;
import com.cronbay.ws.data.CronJobDataObject;

// TBD: Add offset/count to getAllCronJobs() and findCronJobs(), etc.
public interface CronJobDAO
{
    CronJobDataObject getCronJob(String guid) throws BaseException;
    List<CronJobDataObject> getCronJobs(List<String> guids) throws BaseException;
    List<CronJobDataObject> getAllCronJobs() throws BaseException;
    List<CronJobDataObject> getAllCronJobs(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllCronJobKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<CronJobDataObject> findCronJobs(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<CronJobDataObject> findCronJobs(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<CronJobDataObject> findCronJobs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findCronJobKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createCronJob(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return CronJobDataObject?)
    String createCronJob(CronJobDataObject cronJob) throws BaseException;          // Returns Guid.  (Return CronJobDataObject?)
    //Boolean updateCronJob(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateCronJob(CronJobDataObject cronJob) throws BaseException;
    Boolean deleteCronJob(String guid) throws BaseException;
    Boolean deleteCronJob(CronJobDataObject cronJob) throws BaseException;
    Long deleteCronJobs(String filter, String params, List<String> values) throws BaseException;
}
