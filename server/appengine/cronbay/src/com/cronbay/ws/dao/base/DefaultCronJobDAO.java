package com.cronbay.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.cronbay.ws.core.GUID;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.DataStoreException;
import com.cronbay.ws.exception.ServiceUnavailableException;
import com.cronbay.ws.exception.RequestConflictException;
import com.cronbay.ws.exception.ResourceNotFoundException;
import com.cronbay.ws.exception.NotImplementedException;
import com.cronbay.ws.dao.CronJobDAO;
import com.cronbay.ws.data.CronJobDataObject;


public class DefaultCronJobDAO extends DefaultDAOBase implements CronJobDAO
{
    private static final Logger log = Logger.getLogger(DefaultCronJobDAO.class.getName()); 

    // Returns the cronJob for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public CronJobDataObject getCronJob(String guid) throws BaseException
    {
        log.finer("BEGIN: guid = " + guid);

        CronJobDataObject cronJob = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                //Key key = CronJobDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = CronJobDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                cronJob = pm.getObjectById(CronJobDataObject.class, key);
                cronJob.getReferrerInfo();  // "Touch". Otherwise this field will not be fetched by default.
                //tx.commit();
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to retrieve cronJob for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return cronJob;
	}

    @Override
    public List<CronJobDataObject> getCronJobs(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<CronJobDataObject> cronJobs = null;
        if(guids != null && !guids.isEmpty()) {
            cronJobs = new ArrayList<CronJobDataObject>();
            for(String guid : guids) {
                CronJobDataObject obj = getCronJob(guid); 
                cronJobs.add(obj);
            }
	    }

        log.finer("END");
        return cronJobs;
    }

    @Override
    public List<CronJobDataObject> getAllCronJobs() throws BaseException
	{
	    return getAllCronJobs(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<CronJobDataObject> getAllCronJobs(String ordering, Long offset, Integer count) throws BaseException
	{
        log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<CronJobDataObject> cronJobs = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(CronJobDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            cronJobs = (List<CronJobDataObject>) q.execute();
            if(cronJobs != null) {
                cronJobs.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<CronJobDataObject> rs_cronJobs = (Collection<CronJobDataObject>) q.execute();
            if(rs_cronJobs == null) {
                log.log(Level.WARNING, "Failed to retrieve all cronJobs.");
                cronJobs = new ArrayList<CronJobDataObject>();  // ???           
            } else {
                cronJobs = new ArrayList<CronJobDataObject>(pm.detachCopyAll(rs_cronJobs));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all cronJobs.", ex);
            //cronJobs = new ArrayList<CronJobDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all cronJobs.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return cronJobs;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllCronJobKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery("select key from " + CronJobDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all CronJob keys.", ex);
            throw new DataStoreException("Failed to retrieve all CronJob keys.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<CronJobDataObject> findCronJobs(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findCronJobs(filter, ordering, params, values, null, null);
    }

    @Override
	public List<CronJobDataObject> findCronJobs(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findCronJobs(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<CronJobDataObject> findCronJobs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        log.info("DefaultCronJobDAO.findCronJobs(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findCronJobs() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<CronJobDataObject> cronJobs = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(CronJobDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                cronJobs = (List<CronJobDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                cronJobs = (List<CronJobDataObject>) q.execute();
            }
            if(cronJobs != null) {
                cronJobs.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(CronJobDataObject dobj : cronJobs) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find cronJobs because index is missing.", ex);
            //cronJobs = new ArrayList<CronJobDataObject>();  // ???
            throw new DataStoreException("Failed to find cronJobs because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find cronJobs meeting the criterion.", ex);
            //cronJobs = new ArrayList<CronJobDataObject>();  // ???
            throw new DataStoreException("Failed to find cronJobs meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return cronJobs;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findCronJobKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.info("DefaultCronJobDAO.findCronJobKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery("select key from " + CronJobDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find CronJob keys because index is missing.", ex);
            throw new DataStoreException("Failed to find CronJob keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find CronJob keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find CronJob keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.info("DefaultCronJobDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(CronJobDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = new Long((Integer) q.executeWithArray(values.toArray(new Object[0])));
            } else {
                count = new Long((Integer) q.execute());
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get cronJob count because index is missing.", ex);
            throw new DataStoreException("Failed to get cronJob count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get cronJob count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get cronJob count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

	// Stores the cronJob in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeCronJob(CronJobDataObject cronJob) throws BaseException
    {
        log.fine("storeCronJob() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
	    try {
            //tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = cronJob.getCreatedTime();
            if(createdTime == null) {
                createdTime = (new Date()).getTime();
                cronJob.setCreatedTime(createdTime);
            }
            Long modifiedTime = cronJob.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                cronJob.setModifiedTime(createdTime);
            }
            pm.makePersistent(cronJob); 
            //tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = cronJob.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store cronJob because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store cronJob because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store cronJob.", ex);
            throw new DataStoreException("Failed to store cronJob.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.fine("storeCronJob(): guid = " + guid);
        return guid;
    }

    @Override
    public String createCronJob(CronJobDataObject cronJob) throws BaseException
    {
        // The createdTime field will be automatically set in storeCronJob().
        //Long createdTime = cronJob.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = (new Date()).getTime();
        //    cronJob.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = cronJob.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    cronJob.setModifiedTime(createdTime);
        //}
        return storeCronJob(cronJob);
    }

    @Override
	public Boolean updateCronJob(CronJobDataObject cronJob) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeCronJob()
	    // (in which case modifiedTime might be updated again).
	    cronJob.setModifiedTime((new Date()).getTime());
	    String guid = storeCronJob(cronJob);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteCronJob(CronJobDataObject cronJob) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            pm.deletePersistent(cronJob);
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete cronJob because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete cronJob because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete cronJob.", ex);
            throw new DataStoreException("Failed to delete cronJob.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteCronJob(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                //Key key = CronJobDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = CronJobDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                CronJobDataObject cronJob = pm.getObjectById(CronJobDataObject.class, key);
                pm.deletePersistent(cronJob);
                //tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                log.log(Level.WARNING, "Failed to delete cronJob because the datastore is currently read-only.", ex);
                throw new ServiceUnavailableException("Failed to delete cronJob because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to delete cronJob for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete cronJob for guid = " + guid, ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteCronJobs(String filter, String params, List<String> values) throws BaseException
    {
        log.info("DefaultCronJobDAO.deleteCronJobs(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(CronJobDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletecronJobs because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete cronJobs because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete cronJobs because index is missing", ex);
            throw new DataStoreException("Failed to delete cronJobs because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete cronJobs", ex);
            throw new DataStoreException("Failed to delete cronJobs", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

}
