package com.cronbay.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

//import com.google.appengine.api.datastore.Key;

import com.cronbay.ws.core.GUID;
import com.cronbay.ws.BaseException;
import com.cronbay.ws.exception.DataStoreException;
import com.cronbay.ws.exception.ServiceUnavailableException;
import com.cronbay.ws.exception.RequestConflictException;
import com.cronbay.ws.exception.ResourceNotFoundException;
import com.cronbay.ws.exception.NotImplementedException;
import com.cronbay.ws.dao.ApiConsumerDAO;
import com.cronbay.ws.data.ApiConsumerDataObject;


public class DefaultApiConsumerDAO extends DefaultDAOBase implements ApiConsumerDAO
{
    private static final Logger log = Logger.getLogger(DefaultApiConsumerDAO.class.getName()); 

    // Returns the apiConsumer for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public ApiConsumerDataObject getApiConsumer(String guid) throws BaseException
    {
        log.finer("BEGIN: guid = " + guid);

        ApiConsumerDataObject apiConsumer = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                //Key key = ApiConsumerDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = ApiConsumerDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                apiConsumer = pm.getObjectById(ApiConsumerDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to retrieve apiConsumer for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return apiConsumer;
	}

    @Override
    public List<ApiConsumerDataObject> getApiConsumers(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<ApiConsumerDataObject> apiConsumers = null;
        if(guids != null && !guids.isEmpty()) {
            apiConsumers = new ArrayList<ApiConsumerDataObject>();
            for(String guid : guids) {
                ApiConsumerDataObject obj = getApiConsumer(guid); 
                apiConsumers.add(obj);
            }
	    }

        log.finer("END");
        return apiConsumers;
    }

    @Override
    public List<ApiConsumerDataObject> getAllApiConsumers() throws BaseException
	{
	    return getAllApiConsumers(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<ApiConsumerDataObject> getAllApiConsumers(String ordering, Long offset, Integer count) throws BaseException
	{
        log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<ApiConsumerDataObject> apiConsumers = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(ApiConsumerDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            apiConsumers = (List<ApiConsumerDataObject>) q.execute();
            if(apiConsumers != null) {
                apiConsumers.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            /*
            // ???
            Collection<ApiConsumerDataObject> rs_apiConsumers = (Collection<ApiConsumerDataObject>) q.execute();
            if(rs_apiConsumers == null) {
                log.log(Level.WARNING, "Failed to retrieve all apiConsumers.");
                apiConsumers = new ArrayList<ApiConsumerDataObject>();  // ???           
            } else {
                apiConsumers = new ArrayList<ApiConsumerDataObject>(pm.detachCopyAll(rs_apiConsumers));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all apiConsumers.", ex);
            //apiConsumers = new ArrayList<ApiConsumerDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all apiConsumers.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return apiConsumers;
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllApiConsumerKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery("select key from " + ApiConsumerDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all ApiConsumer keys.", ex);
            throw new DataStoreException("Failed to retrieve all ApiConsumer keys.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<ApiConsumerDataObject> findApiConsumers(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findApiConsumers(filter, ordering, params, values, null, null);
    }

    @Override
	public List<ApiConsumerDataObject> findApiConsumers(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findApiConsumers(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<ApiConsumerDataObject> findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        log.info("DefaultApiConsumerDAO.findApiConsumers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findApiConsumers() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<ApiConsumerDataObject> apiConsumers = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(ApiConsumerDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                apiConsumers = (List<ApiConsumerDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                apiConsumers = (List<ApiConsumerDataObject>) q.execute();
            }
            if(apiConsumers != null) {
                apiConsumers.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(ApiConsumerDataObject dobj : apiConsumers) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find apiConsumers because index is missing.", ex);
            //apiConsumers = new ArrayList<ApiConsumerDataObject>();  // ???
            throw new DataStoreException("Failed to find apiConsumers because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find apiConsumers meeting the criterion.", ex);
            //apiConsumers = new ArrayList<ApiConsumerDataObject>();  // ???
            throw new DataStoreException("Failed to find apiConsumers meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return apiConsumers;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.info("DefaultApiConsumerDAO.findApiConsumerKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery("select key from " + ApiConsumerDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find ApiConsumer keys because index is missing.", ex);
            throw new DataStoreException("Failed to find ApiConsumer keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find ApiConsumer keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find ApiConsumer keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.info("DefaultApiConsumerDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(ApiConsumerDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = new Long((Integer) q.executeWithArray(values.toArray(new Object[0])));
            } else {
                count = new Long((Integer) q.execute());
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get apiConsumer count because index is missing.", ex);
            throw new DataStoreException("Failed to get apiConsumer count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get apiConsumer count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get apiConsumer count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

	// Stores the apiConsumer in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeApiConsumer(ApiConsumerDataObject apiConsumer) throws BaseException
    {
        log.fine("storeApiConsumer() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
	    try {
            //tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = apiConsumer.getCreatedTime();
            if(createdTime == null) {
                createdTime = (new Date()).getTime();
                apiConsumer.setCreatedTime(createdTime);
            }
            Long modifiedTime = apiConsumer.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                apiConsumer.setModifiedTime(createdTime);
            }
            pm.makePersistent(apiConsumer); 
            //tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = apiConsumer.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store apiConsumer because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store apiConsumer because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store apiConsumer.", ex);
            throw new DataStoreException("Failed to store apiConsumer.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.fine("storeApiConsumer(): guid = " + guid);
        return guid;
    }

    @Override
    public String createApiConsumer(ApiConsumerDataObject apiConsumer) throws BaseException
    {
        // The createdTime field will be automatically set in storeApiConsumer().
        //Long createdTime = apiConsumer.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = (new Date()).getTime();
        //    apiConsumer.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = apiConsumer.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    apiConsumer.setModifiedTime(createdTime);
        //}
        return storeApiConsumer(apiConsumer);
    }

    @Override
	public Boolean updateApiConsumer(ApiConsumerDataObject apiConsumer) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeApiConsumer()
	    // (in which case modifiedTime might be updated again).
	    apiConsumer.setModifiedTime((new Date()).getTime());
	    String guid = storeApiConsumer(apiConsumer);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteApiConsumer(ApiConsumerDataObject apiConsumer) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            pm.deletePersistent(apiConsumer);
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete apiConsumer because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete apiConsumer because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete apiConsumer.", ex);
            throw new DataStoreException("Failed to delete apiConsumer.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteApiConsumer(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                //Key key = ApiConsumerDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = ApiConsumerDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                ApiConsumerDataObject apiConsumer = pm.getObjectById(ApiConsumerDataObject.class, key);
                pm.deletePersistent(apiConsumer);
                //tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                log.log(Level.WARNING, "Failed to delete apiConsumer because the datastore is currently read-only.", ex);
                throw new ServiceUnavailableException("Failed to delete apiConsumer because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to delete apiConsumer for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete apiConsumer for guid = " + guid, ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteApiConsumers(String filter, String params, List<String> values) throws BaseException
    {
        log.info("DefaultApiConsumerDAO.deleteApiConsumers(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(ApiConsumerDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteapiConsumers because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete apiConsumers because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete apiConsumers because index is missing", ex);
            throw new DataStoreException("Failed to delete apiConsumers because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete apiConsumers", ex);
            throw new DataStoreException("Failed to delete apiConsumers", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END: count = " + count);
        return count;
    }

}
