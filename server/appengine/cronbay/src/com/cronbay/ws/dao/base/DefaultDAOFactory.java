package com.cronbay.ws.dao.base;

import java.util.logging.Logger;

import com.cronbay.ws.dao.DAOFactory;
import com.cronbay.ws.dao.ApiConsumerDAO;
import com.cronbay.ws.dao.UserDAO;
import com.cronbay.ws.dao.CronJobDAO;
import com.cronbay.ws.dao.JobOutputDAO;
import com.cronbay.ws.dao.ServiceInfoDAO;
import com.cronbay.ws.dao.FiveTenDAO;

// Default DAO factory uses JDO on Google AppEngine.
public class DefaultDAOFactory extends DAOFactory
{
    private static final Logger log = Logger.getLogger(DefaultDAOFactory.class.getName());

    // Ctor. Prevents instantiation.
    private DefaultDAOFactory()
    {
        // ...
    }

    // Initialization-on-demand holder.
    private static class DefaultDAOFactoryHolder
    {
        private static final DefaultDAOFactory INSTANCE = new DefaultDAOFactory();
    }

    // Singleton method
    public static DefaultDAOFactory getInstance()
    {
        return DefaultDAOFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerDAO getApiConsumerDAO()
    {
        return new DefaultApiConsumerDAO();
    }

    @Override
    public UserDAO getUserDAO()
    {
        return new DefaultUserDAO();
    }

    @Override
    public CronJobDAO getCronJobDAO()
    {
        return new DefaultCronJobDAO();
    }

    @Override
    public JobOutputDAO getJobOutputDAO()
    {
        return new DefaultJobOutputDAO();
    }

    @Override
    public ServiceInfoDAO getServiceInfoDAO()
    {
        return new DefaultServiceInfoDAO();
    }

    @Override
    public FiveTenDAO getFiveTenDAO()
    {
        return new DefaultFiveTenDAO();
    }

}
