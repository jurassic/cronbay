package com.cronbay.ws;


public interface NotificationStruct 
{
    String  getPreferredMode();
    String  getMobileNumber();
    String  getEmailAddress();
    String  getCallbackUrl();
    String  getNote();
}
