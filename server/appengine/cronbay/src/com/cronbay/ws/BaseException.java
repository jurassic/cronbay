package com.cronbay.ws;

// Base exception.
public class BaseException extends Exception  // extends RuntimeException ???
{
    private static final long serialVersionUID = 1L;

    public BaseException() 
    {
        super();
    }
    public BaseException(String message) 
    {
        super(message);
    }
    public BaseException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public BaseException(Throwable cause) 
    {
        super(cause);
    }

}